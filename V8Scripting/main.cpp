#include <iostream>
#include <v8.h>

#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "v8_base.ia32.lib")
#pragma comment(lib, "v8_snapshot.lib")
#pragma comment(lib, "v8_nosnapshot.ia32.lib")

using namespace v8;

int main()
{
	std::cout<<"Testing V8 Javascript embedded:"<<std::endl;
	std::cout<<"==================================================="<<std::endl;

	Isolate* isolate = Isolate::GetCurrent();
	HandleScope handle_scope(isolate);
	Handle<Context> context = Context::New(isolate);
	Persistent<Context> persistent_context(isolate, context);
	Context::Scope context_scope(context);

	Handle<String> source = String::New("'Hello' + ', World'");
	Handle<Script> script = Script::Compile(source);
	Handle<Value> result = script->Run();
	String::AsciiValue ascii(result);
	std::cout<<"Results:"<<std::endl;
	std::cout<<*ascii<<std::endl;

	persistent_context.Dispose();

	std::getchar();
}