#include <iostream>
#include <string>
#include <queue>
#include <boost/thread.hpp>

int sharedCount = 50;
boost::mutex countMutex;

struct func
{
	void operator()()
	{
		std::cout<<"Welcome dude :<"<<std::endl;
		
		{
			boost::mutex::scoped_lock slock(countMutex);
			while(sharedCount > 0)
			{
				--sharedCount;
				std::cout<<"<"<<sharedCount<<">"<<" ";
			}
		}
	}
};

// Producer-Consumer using conditions
std::queue<char> output;
bool bRun = true;
boost::condition_variable outAvailable;
boost::mutex pcMutex;

struct Producer
{
	void operator()()
	{
		std::string textmsg = "ZenithSal & Angi\t";
		for(unsigned int i = 0; i < textmsg.length(); ++i)
		{
			{
				boost::mutex::scoped_lock slock(pcMutex);
				output.push(textmsg[i]);
				std::cout<<"p";
			}
			outAvailable.notify_one();
		}
	}
};

struct Consumer
{
	void operator()()
	{
		boost::mutex::scoped_lock slock(pcMutex);
		for(;;)
		{			
			while(output.empty())
			{
				outAvailable.wait(slock);
			}
			std::cout<<"c";
			char c = output.front();
			if(c == '\t')
				break;
			std::cout<<output.front();
			output.pop();
		}
	}
};

int main()
{
	std::cout<<"Hello :D"<<std::endl;

	func x;
	boost::thread t1(x);
	boost::thread t2(x);
	boost::thread t3(x);
	boost::thread t4(x);
	t1.join();
	t2.join();
	t3.join();
	t4.join();
	std::cout<<std::endl<<"Count: "<<sharedCount<<std::endl;

	Producer p;
	Consumer c;
	boost::thread t5(c);
	boost::thread t6(p);
	t6.join();
	t5.join();
	std::cout<<std::endl<<"done, output size: "<<output.size()<<std::endl;

	std::getchar();
}
