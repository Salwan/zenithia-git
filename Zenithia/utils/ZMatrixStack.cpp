////////////////////////////////////////////////////////////////
// Name: ZMatrixStack
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ZMatrixStack.h"
#include "ZMemoryDebug.h"
#include "../exceptions/ZException.h"

using namespace zen;

//////////////////////////////////////////////////// Definitions
const size_t ZMatrixStack::MatrixStackSize = 128;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZMatrixStack::ZMatrixStack() : pStack(nullptr), uStackDepth(0)
{
	pStack = znew_array(XMMATRIX, MatrixStackSize);
	pStack[0] = XMMatrixIdentity();
	uStackDepth = 0;
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZMatrixStack::~ZMatrixStack()
{
	zdelete_array(pStack);
}

void ZMatrixStack::push(const XMMATRIX& matrix)
{
	uStackDepth += 1;
	if(uStackDepth >= MatrixStackSize) {
		throw ZEXCEPTION_MESSAGE(L"ZMatrixStack::push() stack overflow!");
	}
	pStack[uStackDepth] = XMMatrixMultiply(matrix, pStack[uStackDepth - 1]);
	test_stack.push(matrix);
}

void ZMatrixStack::pop()
{
	if(uStackDepth < 1) {
		throw ZEXCEPTION_MESSAGE(L"ZMatrixStack::pop() stack underflow!");
	}
	uStackDepth -= 1;
	test_stack.pop();
}

const XMMATRIX& ZMatrixStack::top() const
{	
	return pStack[uStackDepth];
}
