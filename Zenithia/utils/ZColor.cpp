////////////////////////////////////////////////////////////////
// Name: ZColor
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "stdafx.h"
#include "ZColor.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZColor::ZColor() : r(1.0f), g(1.0f), b(1.0f), a(1.0f)
{
	
}

////////////////////////////////////////////////////////////////
/// Direct Constructor
/// 
ZColor::ZColor(float _r, float _g, float _b, float _a) : r(_r), g(_g), b(_b), a(_a)
{

}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZColor::~ZColor()
{
	
}

////////////////////////////////////////////////////////////////
/// Copy constructor
/// 
ZColor::ZColor(const ZColor& color)
{
	r = color.r;
	g = color.g;
	b = color.b;
	a = color.a;
}

////////////////////////////////////////////////////////////////
/// Assignment operator
/// 
ZColor& ZColor::operator=(const ZColor& color)
{
	r = color.r;
	g = color.g;
	b = color.b;
	a = color.a;
	return *this;
}

ZColor::operator D3DCOLORVALUE() const
{
	return toD3DColorValue();
}

ZColor::operator XMFLOAT4() const
{
	return toXMFloat4();
}

ZColor::operator XMVECTOR() const
{
	return toXMVector();
}

ZColor::operator int() const
{
	return toInt();
}

ZColor::operator unsigned int() const
{
	return toUInt();
}

////////////////////////////////////////////////////////////////
/// Convert to D3DCOLORVALUE
///
D3DCOLORVALUE ZColor::toD3DColorValue() const
{
	return createD3DColorValue(r, g, b, a);
}

////////////////////////////////////////////////////////////////
/// Convert to XMFLOAT4
/// 
XMFLOAT4 ZColor::toXMFloat4() const
{
	return XMFLOAT4(r, g, b, a);
}

////////////////////////////////////////////////////////////////
/// Convert to XMVECTOR
/// 
XMVECTOR ZColor::toXMVector() const
{
	const XMFLOAT4 c = toXMFloat4();
	return XMLoadFloat4(&c);
}

////////////////////////////////////////////////////////////////
/// Convert to int
/// 
int ZColor::toInt() const
{
	return int(
		(unsigned int(unsigned char(b * 255.0f))) + 
		(unsigned int(unsigned char(g * 255.0f)) << 8U) + 
		(unsigned int(unsigned char(r * 255.0f)) << 16U) + 
		(unsigned int(unsigned char(a * 255.0f)) << 24U));
}

////////////////////////////////////////////////////////////////
/// Convert to unsigned int
/// 
unsigned int ZColor::toUInt() const
{
	return unsigned int(
		(unsigned int(unsigned char(b * 255.0f))) + 
		(unsigned int(unsigned char(g * 255.0f)) << 8U) + 
		(unsigned int(unsigned char(r * 255.0f)) << 16U) + 
		(unsigned int(unsigned char(a * 255.0f)) << 24U));
}

////////////////////////////////////////////////////////////////
/// Convert from XMFLOAT4
/// 
void ZColor::fromXMFloat4(const XMFLOAT4& color)
{
	r = color.x;
	g = color.y;
	b = color.z;
	a = color.w;
}

////////////////////////////////////////////////////////////////
/// Convert from XMVECTOR
/// 
void ZColor::fromXMVector(const XMVECTOR& color)
{
	XMFLOAT4 c;
	XMStoreFloat4(&c, color);
	fromXMFloat4(c);
}

////////////////////////////////////////////////////////////////
/// Convert from int
/// 
void ZColor::fromInt(int color)
{
	a = GET_ALPHA(color) / 255.0f;
	r = GET_RED(color) / 255.0f;
	g = GET_GREEN(color) / 255.0f;
	b = GET_BLUE(color) / 255.0f;
}

////////////////////////////////////////////////////////////////
/// Convert from unsigned int
/// 
void ZColor::fromUInt(unsigned int color)
{
	a = GET_ALPHA(color) / 255.0f;
	r = GET_RED(color) / 255.0f;
	g = GET_GREEN(color) / 255.0f;
	b = GET_BLUE(color) / 255.0f;
}

////////////////////////////////////////////////////////////////
/// D3DCOLORVALUE constructor
/// 
D3DCOLORVALUE ZColor::createD3DColorValue(float r, float g, float b, float a)
{
	D3DCOLORVALUE c;
	c.r = r;
	c.g = g;
	c.b = b;
	c.a = a;
	return c;
}
