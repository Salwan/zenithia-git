////////////////////////////////////////////////////////////////
// Name: ZAssert
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ZAssert.h"
#include <Windows.h>
#include <iostream>
#include <sstream>

using namespace zen;

bool ZUtils::AssertMessage(const wchar_t* text, const wchar_t* file, int line)
{
	std::wstringstream ss;
	ss<<L"ZAssert: "<<text<<std::endl;
	ss<<L"File: "<<file<<std::endl;
	ss<<L"Line: "<<line<<std::endl;
	OutputDebugStringW(ss.str().c_str());
	std::wclog<<ss.str().c_str();

#ifdef ASSERT_DEBUG
	return true;
#else
	return false;
#endif
}
