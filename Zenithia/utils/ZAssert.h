////////////////////////////////////////////////////////////////
// Name: ZAssert
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZASSERT_HEADER_
#define _ZASSERT_HEADER_

/////////////////////////////////////////////////////// Includes
#include "../ZDefs.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	namespace ZUtils
	{
		bool AssertMessage(const wchar_t* text, const wchar_t* file, int line);
	};
};

#ifdef _DEBUG
#define ZASSERT(expression, text)	\
	if(!(expression) && zen::ZUtils::AssertMessage(text, __WFILE__, __LINE__)) __debugbreak();
#else
#define ZASSERT(expression, text)
#endif

#endif
