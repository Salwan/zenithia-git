////////////////////////////////////////////////////////////////
// Name: ZTimer
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "ZTimer.h"
#include <Windows.h>

//////////////////////////////////////////////////// Definitions
using namespace zen;

ZTimer::ZTimer() : dSecondsPerCount(0.0), dDeltaTime(-1.0), iBaseTime(0),
	iPausedTime(0), iPrevTime(0), iCurrTime(0), bStopped(false)
{
	__int64 counts_per_sec;
	QueryPerformanceFrequency((LARGE_INTEGER*)&counts_per_sec);
	dSecondsPerCount = 1.0 / (double)counts_per_sec;
}

void ZTimer::tick()
{
	if(bStopped)
	{
		dDeltaTime = 0.0;
		return;
	}

	__int64 curr_time;
	QueryPerformanceCounter((LARGE_INTEGER*)&curr_time);
	iCurrTime = curr_time;

	dDeltaTime = (iCurrTime - iPrevTime) * dSecondsPerCount;

	iPrevTime = iCurrTime;
	if(dDeltaTime < 0.0)
	{
		dDeltaTime = 0.0;
	}
}

float ZTimer::getDeltaTime() const
{
	return (float)dDeltaTime;
}

void ZTimer::reset()
{
	__int64 curr_time;
	QueryPerformanceCounter((LARGE_INTEGER*)&curr_time);

	iBaseTime = curr_time;
	iPrevTime = curr_time;
	iStopTime = 0;
	bStopped = false;
}

void ZTimer::stop()
{
    if(!bStopped)
    {
        __int64 curr_time;
        QueryPerformanceCounter((LARGE_INTEGER*)&curr_time);

        iStopTime = curr_time;
        bStopped  = true;
    }
}

void ZTimer::start()
{
    __int64 start_time;
    QueryPerformanceCounter((LARGE_INTEGER*)&start_time);

    // Accumulate the time elapsed between stop and start pairs.
    //
    //                |<-------d------->|
    // ---------------*-----------------*------------> time
    //             iStopTime        startTime

	if(bStopped)
    {
        iPausedTime += (start_time - iStopTime);
        iPrevTime = start_time;
        iStopTime = 0;
        bStopped  = false;
    }
}

float ZTimer::getGameTime()const
{
    // If we are stopped, do not count the time that has passed since
    // we stopped.
    //
    // ----*---------------*------------------------------*------> time
    //  iBaseTime       iStopTime                      iCurrTime

    if( bStopped )
    {
        return (float)((iStopTime - iBaseTime) * dSecondsPerCount);
    }
    // The distance iCurrTime - iBaseTime includes paused time,
    // which we do not want to count. To correct this, we can subtract
    // the paused time from iCurrTime:
    //
    //  (iCurrTime - iPausedTime) - iBaseTime
    //
    //                     |<-------d------->|
    // ----*---------------*-----------------*------------*------> time
    // iBaseTime        iStopTime        startTime     iCurrTime
    else
    {
        return (float)(((iCurrTime - iPausedTime) - iBaseTime) * dSecondsPerCount);
    }
}



