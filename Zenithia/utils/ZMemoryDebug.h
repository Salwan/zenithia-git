////////////////////////////////////////////////////////////////
// Name: ZMemoryDebug
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "../ZDefs.h"
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <sstream>

#ifndef _ZMEMORY
#define _ZMEMORY
	#ifdef _DEBUG
		#define znew(type, args) zen::ZMemoryDebug::ZNew<type>(new type args, __WFILE__, __LINE__)
		#define znew_array(type, size) zen::ZMemoryDebug::ZNewArray<type>(new type [size], size, __WFILE__, __LINE__)
		#define zdelete(ptr) {delete ptr; zen::ZMemoryDebug::ZDelete((void*)ptr, __WFILE__, __LINE__);}
		#define zdelete_array(ptr) {delete [] ptr; zen::ZMemoryDebug::ZDeleteArray((void*)ptr, __WFILE__, __LINE__);}
	#else
		#define znew(type, args) new type args
		#define znew_array(type, size) new type [size]
		#define zdelete(ptr) delete ptr
		#define zdelete_array(ptr) delete [] ptr
	#endif
#endif

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZMemoryDebug
	{
	public:
		template <typename T>
		static T* ZNew(T* pointer, const std::wstring& file, unsigned int line)
		{
			auto found = mAllocations.find((char*)pointer);
			if(found != mAllocations.end()) {
				// Leak, address already allocated to something else
				std::wostringstream ss;
				ss << L"Error: Duplicate allocation error in " << file << L"(" << line << L")";
				mErrors.push_back(ss.str());
			} else {
				SMemoryAllocation ma;
				ma.bArray = false;
				ma.strFile = file;
				ma.uLine = line;
				ma.uSize = sizeof(T);
				mAllocations.insert(std::pair<void*, SMemoryAllocation>((void*)pointer, ma));
			}
			return pointer;
		}

		template <typename T>
		static T* ZNewArray(T* pointer, unsigned int size, const std::wstring& file, unsigned int line)
		{
			auto found = mAllocations.find((char*)pointer);
			if(found != mAllocations.end()) {
				// Leak, address already allocated to something else
				std::wostringstream ss;
				ss << L"Error: Duplicate array allocation error in " << file << L"(" << line << L")";
				mErrors.push_back(ss.str());
			} else {
				SMemoryAllocation ma;
				ma.bArray = true;
				ma.strFile = file;
				ma.uLine = line;
				ma.uSize = size * sizeof(T);
				mAllocations.insert(std::pair<void*, SMemoryAllocation>((void*)pointer, ma)); 
			}
			return pointer;
		}

		static void ZDelete(void* pointer, const std::wstring& file, unsigned int line) {
			auto found = mAllocations.find(pointer);
			if(found != mAllocations.end()) {
				mAllocations.erase(found);
			} else {
				// Allocation not found, perhaps it wasn't allocated using ZMemoryDebug
				std::wostringstream ss;
				ss << L"Warning Unknown memory deallocation in " << file << L"(" << line << L")";
				mErrors.push_back(ss.str());
			}
		}

		static void ZDeleteArray(void* pointer, const std::wstring& file, unsigned int line) {
			auto found = mAllocations.find(pointer);
			if(found != mAllocations.end()) {
				mAllocations.erase(found);
			} else {
				// Allocation not found, perhaps it wasn't allocated using ZMemoryDebug
				std::wostringstream ss;
				ss << L"Warning: Unknown memory array deallocation in " << file << L"(" << line << L")";
				mErrors.push_back(ss.str());
			}
		}

		static void logMemoryReport()
		{
#ifdef _DEBUG
			std::wclog << std::endl;
			std::wclog << L"==================================================================" << std::endl;
			std::wclog << L"= Zenithia Memory Report - Start =================================" << std::endl;
			std::wclog << L"==================================================================" << std::endl;
			if(mErrors.empty()) {
			} else {
				std::wclog << L"[Allocation Errors]" << std::endl;
				for(auto& ae : mErrors) {
					std::wclog << ae << std::endl;
				}
				std::wclog << L"- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - " << std::endl;
			}
			std::wclog << L"[Memory Leaks]" << std::endl;
			if(mAllocations.empty()) {
				std::wclog << L"No leaks found! great job :)" << std::endl;
			} else {
				unsigned int count = 0;
				unsigned int total_size = 0;
				for(auto& ml : mAllocations) {
					count += 1;
					std::wclog << L"Memory Leak #" << count << L": ";
					logAllocationDesc(ml.second);
					total_size += ml.second.uSize;
				}
				std::wclog << std::endl << L"Total leaks = " << total_size << L" bytes" << std::endl;
			}

			std::wclog << L"=========================== End ==================================" << std::endl;			
#else
			std::wclog << L"Zenithia Memory Report - Release Mode - Nothing to report" << std::endl;
#endif
		}

	private:
		struct SMemoryAllocation
		{
			std::wstring strFile;
			unsigned int uLine;
			unsigned int uSize;
			bool bArray;
		};

		static void logAllocationDesc(SMemoryAllocation& ma) {
			std::wclog << L"type: " << (ma.bArray == false? L"single" : L"array") << " size: " << ma.uSize << L" bytes allocated in " << ma.strFile << L"(" << ma.uLine << L")" << std::endl;
		}

		static std::vector<std::wstring> mErrors;
		static std::map<void*, SMemoryAllocation> mAllocations;
	};
};

