////////////////////////////////////////////////////////////////
// Name: ZTimer
// Desc: A self-contained high frequency timer with pause 
//       functionality. Can be used to run the demo or game.
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes\

//////////////////////////////////////////////////// Definitions
namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @class ZTimer
	/// @brief A self-contained high frequency timer with pause 
	///	functionality. Can be used to run the demo or game.
	/// 
	class ZTimer
	{
	public:
		ZTimer();

		float getGameTime() const;
		float getDeltaTime() const;

		void reset();
		void start();
		void stop();
		void tick();

	private:
		double dSecondsPerCount;
		double dDeltaTime;

		__int64 iBaseTime;
		__int64 iPausedTime;
		__int64 iStopTime;
		__int64 iPrevTime;
		__int64 iCurrTime;

		bool bStopped;
	};
};