////////////////////////////////////////////////////////////////
// Name: ZClock
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ZClock.h"
#include <Windows.h>

using namespace zen;

double ZClock::dTimeDelta = 0.0;
float ZClock::fTimeDelta = 0.0f;
unsigned int ZClock::uTimeDelta = 0;
unsigned int ZClock::uFPS = 0;

struct ZClock::PIMPL
{
	LARGE_INTEGER frequency;
	LARGE_INTEGER prevTime;
};

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZClock::ZClock(double maxTimeDeltaSec)
	: dMaxElapsedTime(maxTimeDeltaSec * 1000.0), dFrameTimeAccum(0.0),
	uFrameCounter(0)
{
	pimpl = znew(PIMPL, ());
	QueryPerformanceFrequency(&pimpl->frequency);
	QueryPerformanceCounter(&pimpl->prevTime);
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZClock::~ZClock()
{
	zdelete(pimpl);
}

////////////////////////////////////////////////////////////////
/// Does the necessary per-frame timing computations.
///
void ZClock::tick()
{
	LARGE_INTEGER currTime;
	double elapsedTime;

	QueryPerformanceCounter(&currTime);
	elapsedTime = ((currTime.QuadPart - pimpl->prevTime.QuadPart) * 1000.0) / static_cast<double>(pimpl->frequency.QuadPart);
	pimpl->prevTime = currTime;

	// Sanity check, it should never be less than 0 or more than the max elapsed time limit
	if(elapsedTime > 0.0)
	{
		elapsedTime = min(elapsedTime, dMaxElapsedTime);
		uTimeDelta = static_cast<unsigned int>(elapsedTime);
		dTimeDelta = elapsedTime / 1000.0;
		fTimeDelta = static_cast<float>(dTimeDelta);
	}

	uFrameCounter++;
	dFrameTimeAccum += dTimeDelta;
	if(dFrameTimeAccum >= 1.0)
	{
		dFrameTimeAccum -= 1.0;
		calculateFPS();
	}
}

void ZClock::calculateFPS()
{
	uFPS = uFrameCounter;
	uFrameCounter = 0;
}
