﻿////////////////////////////////////////////////////////////////
// Name: ZLog
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ZLog.h"
#include <iostream>
#include <fstream>

using namespace zen;

static std::wstreambuf* StdBuf = NULL;
static std::wofstream LogFile;

void ZLog::logToFile(const std::wstring& logFilename)
{
	LogFile.open(logFilename);	
	if(!StdBuf)
	{
		StdBuf = std::wclog.rdbuf();
	}
	std::wclog.rdbuf(LogFile.rdbuf());
	
	std::wclog<<L"Zenithia Log"<<std::endl;
	std::wclog<<L"====================================================="<<std::endl;
}

void ZLog::logToStd()
{
	if(StdBuf)
	{
		std::wclog.rdbuf(StdBuf);
	}
	if(LogFile.is_open())
	{
		LogFile.close();
	}
}
