////////////////////////////////////////////////////////////////
// Name: ZClock
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes

//////////////////////////////////////////////////// Definitions
namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @class ZClock
	/// @brief Provides a high resolution clock and FPS counter.
	/// 
	class ZClock 
	{
	public:
		explicit ZClock(double maxTimeDeltaSec = 0.1);
		virtual ~ZClock();

		void tick();

		static double dTimeDelta;
		static float fTimeDelta;
		static unsigned int uTimeDelta;
		static unsigned int uFPS;

	private:
		ZClock(const ZClock&){}
		ZClock& operator=(const ZClock&){}

		void calculateFPS();

		// Private Implementation
		struct PIMPL;
		PIMPL* pimpl;

		double dMaxElapsedTime;
	
		unsigned int uFrameCounter;
		double dFrameTimeAccum;
	};
};

