////////////////////////////////////////////////////////////////
// Name: ZMatrixStack
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <Windows.h>
#include <xnamath.h>
#include <stack>

#if defined(_MSC_VER)
#pragma warning(push) // Save warning settings.
#pragma warning(disable : 4324) // "structure was padded due to __declspec(align())"
#endif 

//////////////////////////////////////////////////// Definitions
namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @class ZMatrixStack
	/// @brief XMMATRIX-based stack, used by scenegraph traverser.
	/// 
	__declspec(align(16)) class ZMatrixStack 
	{
	public:
		static const size_t MatrixStackSize;

	public:
		ZMatrixStack();
		virtual ~ZMatrixStack();

		void push(const XMMATRIX& matrix);
		void pop();
		const XMMATRIX& top() const;

	private:
		ZMatrixStack(const ZMatrixStack&){}
		ZMatrixStack& operator=(const ZMatrixStack&){}

		XMMATRIX* pStack;
		unsigned int uStackDepth;
		std::stack<XMMATRIX> test_stack;
	};
};

#if defined(_MSC_VER)
#pragma warning(pop) // Restore warnings to previous state.
#endif 

