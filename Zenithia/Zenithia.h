////////////////////////////////////////////////////////////////
// Name: Zenithia
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
/// @file Zenithia.h
/// The main header file for Zenithia framework.
///
#pragma once

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "DxErr.lib")
#ifndef _DEBUG
#pragma comment(lib, "d3dx9.lib")
#else
#pragma comment(lib, "d3dx9d.lib")
#endif

#include "ZDefs.h"
#include "utils/ZUtils.h"
#include "utils/ZMemoryDebug.h"

#include "utils/ZLog.h"
#include "exceptions/ZError.h"
#include "exceptions/ZException.h"
#include "utils/ZAssert.h"

#include "utils/ZClock.h"
#include "ZApplication.h"
#include "ZWindow.h"
#include "events/ZEventDispatcher.h"
#include "events/ZEventListener.h"
#include "events/ZEvent.h"

#include "d3d9/ZD3D9.h"
#include "d3d9/ZVertexDeclaration.h"
#include "d3d9/ZVertexBuffer.h"
#include "d3d9/ZIndexBuffer.h"
#include "d3d9/ZSampler.h"
#include "utils/ZColor.h"
#include "d3d9/ZTexture.h"
#include "d3d9/ZLight.h"
#include "d3d9/ZAlphaBlending.h"
#include "d3d9/ZStencil.h"
#include "input/ZInput.h"

#include "models/ZMaterial.h"
#include "models/ZMesh.h"
#include "models/ZModel.h"
#include "models/ZManualMesh.h"
#include "models/ZColladaMesh.h"

#include "effects/ZEffect.h"
#include "effects/ZConstant.h"

#include "resources/ZResourceSystem.h"
#include "resources/ZResourcePack.h"
#include "resources/ZResource.h"

#include "scenegraph/ZSceneGraph.h"

