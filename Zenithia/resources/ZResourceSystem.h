////////////////////////////////////////////////////////////////
// Name: ZResourceSystem
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZResourcePack.h"
#include <map>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZResource;
	class ZApplication;

	////////////////////////////////////////////////////////////////
	/// @class ZResourceSystem
	/// @brief
	/// 
	class ZResourceSystem 
	{
		friend class ZApplication;

	public:
		static ZResourceSystem* instance();
		static void destroy();

	private:
		static void init();
		
		static ZResourceSystem* mInstance; 
		static bool bInitialized;

	public:
		virtual ~ZResourceSystem();
		void loadResourcePack(const ZResourcePack& resourcePack);
		ZResource* loadTexture(const std::wstring& alias, const std::wstring& file);
		ZResource* loadModel(const std::wstring& alias, const std::wstring& file);
		ZResource* loadEffect(const std::wstring& alias, const std::wstring& file);
		ZResource* getResource(const std::wstring& id) const;
		
		void setAssetsFolder(const std::wstring& assetsFolder) {strAssetsPath = assetsFolder;}
		const std::wstring& getAssetsFolder() const {return strAssetsPath;}

	private:
		ZResourceSystem();
		ZResourceSystem(const ZResourceSystem&){}
		ZResourceSystem& operator=(const ZResourceSystem&){}

		void process();
		std::wstring _getFileName(const std::wstring& file);
		std::wstring _getAssetPath(const std::wstring& file);
		void _prepareResource(ZResource* resource, const std::wstring& alias, const std::wstring& file);
		ZResource* _checkResource(const std::wstring& alias, const std::wstring& file);

		// Assets folder is appended to any resource file name if defined
		std::wstring strAssetsPath;
		// Used to get resource via an alias name
		std::map<const std::wstring, ZResource*> mResourcesMap;
		// Used to compare if file given is already loaded
		std::map<const std::wstring, ZResource*> mResourceFilesMap;

		// Resource loading worker
		void workerResourceLoader();

		// Resource Loading Thread
		std::thread threadResourceLoader;
		std::mutex mutexResourceLoader;
		std::condition_variable conditionResourcePending;
		bool bTerminateLoaderThread;
		std::mutex mutexResourceQueue;
		// Communication with main thread
		std::queue<ZResource*> mResourceQueue;
		std::mutex mutexLoadedResourceQueue;
		std::queue<ZResource*> mLoadedResourceQueue;
	};
};