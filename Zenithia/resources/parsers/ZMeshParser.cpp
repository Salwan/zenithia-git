﻿////////////////////////////////////////////////////////////////
// Name: ZMeshParser
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include <iostream>
#include "ZMeshParser.h"
#include "../../utils/ZAssert.h"
#include "../../d3d9/ZVertexDeclaration.h"
#include "../../d3d9/ZVertexBuffer.h"
#include "../../d3d9/ZIndexBuffer.h"
#include "../../models/ZMaterial.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;
using namespace pugi;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZMeshParser::ZMeshParser(const std::wstring& modelFile, bool autoCreateMesh)
	: bAutoCreateMesh(autoCreateMesh), bMeshCreated(false)
{
	_loadFromFile(modelFile);
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZMeshParser::~ZMeshParser()
{

	for(auto iter = mMeshSources.begin(); iter != mMeshSources.end(); ++iter) {
		if(iter->second) {
			if(iter->second->pData) {
				zdelete_array(iter->second->pData);
			}
			zdelete(iter->second);
		}
	}

	for(auto iter = mMaterials.begin(); iter != mMaterials.end(); ++iter) {
		if(iter->second) {
			zdelete(iter->second);
		}
	}

	for(auto iter = mMeshNodes.begin(); iter != mMeshNodes.end(); ++iter) {
		if(iter->second->subsets.size() > 0) {
			for(unsigned int i = 0; i < iter->second->subsets.size(); ++i) {
				if(iter->second->subsets[i]->vertexData.pVertexData) {
					free(iter->second->subsets[i]->vertexData.pVertexData);
				}
				zdelete(iter->second->subsets[i]);
			}
			zdelete(iter->second);
		}
	}
}

////////////////////////////////////////////////////////////////
// Create the actual mesh.
//
void ZMeshParser::createMesh()
{
	ZASSERT(!bMeshCreated, L"[ZMeshParser] createMesh() mesh is already created!");
	if(!bMeshCreated) {
		ZASSERT(mMeshNodes.size() > 0, L"[ZMeshParser] ::createMesh() no mesh was parsed!");
		_createMesh((mMeshNodes.begin()->second));
	}
}

////////////////////////////////////////////////////////////////
/// Parses a model file and arranges data.
///
void ZMeshParser::_loadFromFile(const std::wstring& modelFile)
{
	// Add support for FBX here
	_parseColladaMesh(modelFile);
}

////////////////////////////////////////////////////////////////
//  ████  ███  ██    ██     ███  ████   ███
// ██    ██  █ ██    ██    ██  █ ██  █ ██  █
// ██    ██  █ ██    ██    ██  █ ██  █ ██  █
// ██    ██  █ ██    ██    █████ ██  █ █████
//  ████  ███  █████ █████ ██  █ ████  ██  █
//--------------------------------------------------------------
// Notes/Future:
// - Hierarchical objects are represents with a <node> hierarchy

void ZMeshParser::_parseColladaMesh(const std::wstring& colladaFile)
{
	// COLLADA

	// (1)
	// - asset							// - Description of scene
	// (4)
	// - library_images					// - Materials
	// - library_effects
	// - library_materials
	// (5)
	// - library geometries				// - Geometry library
	// (3)
	// - library_visual_scenes			// - Scene nodes
	// (2)
	// - scene							// - Scenes

	xml_document collada_doc;
	xml_parse_result parse_result = collada_doc.load_file(colladaFile.c_str());
	if(parse_result.status != pugi::status_ok) {
		std::wstringstream ss;
		ss << "ZMeshParser: Error parsing collada model file \"" << colladaFile << "\"" << std::endl;
		ss << parse_result.description() << std::endl;
		throw ZEXCEPTION_MESSAGE(ss.str());
	} else {
		std::wclog << "[ZMeshParser] Parsed XML file \"" << colladaFile << "\"" << std::endl;
	}
	xml_node collada = collada_doc.child(L"COLLADA");
	_parseAsset(collada.child(L"asset"));
	_parseScene(collada.child(L"scene"));
	_parseLibraryVisualScenes(collada.child(L"library_visual_scenes"), strInstanceVisualSceneURL);	
	_parseLibraryMaterials(collada);
	_parseLibraryGeometries(collada.child(L"library_geometries"));

	if(bAutoCreateMesh) {
		ZASSERT(mMeshNodes.size() > 0, L"[ZMeshParser] ::_parseColladaMesh() no mesh was parsed!");
		_createMesh((mMeshNodes.begin()->second));
	}
}

// (1) Description of scene
// <asset>							
//     <contributor> (*)
//	       <author>
//         <authoring_tool>
//         <comments>
//     <created>
//     <modified>
//     <revision>
//     <title>
//     <subject>
//     <keywords>
//     <unit>
//     <up_axis>
void ZMeshParser::_parseAsset(const xml_node& node)
{
	ZASSERT(wcscmp(node.name(), L"asset") == 0, L"[ZMeshParser] ::parseAsset() node given is not an asset xml node.");
	strTitle = node.child(L"title").child_value();
	std::wstring up_axis = node.child(L"up_axis").child_value(); // Y_UP by default
	if(up_axis == L"X_UP") {
		eUpAxis = UpAxis_X;
	} else if(up_axis == L"Y_UP") {
		eUpAxis = UpAxis_Y;
	} else if(up_axis == L"Z_UP") {
		eUpAxis = UpAxis_Z;
	}
}

// (2) Scenes
// <scene> (1)
//     <instance_visual_scene> (1)		// -- Root node of scene
void ZMeshParser::_parseScene(const xml_node& node)
{
	ZASSERT(wcscmp(node.name(), L"scene") == 0, L"[ZMeshParser] ::parseScene() node given is not  <asset> node.");
	strInstanceVisualSceneURL = _hashlessID(node.child(L"instance_visual_scene").attribute(L"url").as_string());
	ZASSERT(!strInstanceVisualSceneURL.empty(), L"[ZMeshParser] ::parseScene() no instance visual scene is defined, there must be at most.");
}

// (3) Scene nodes
// <library_visual_scenes>
//     <visual_scene>					// -- Root node
//         <node>							// --- Node name
//             <rotate>						// ---- Node rotation
//             <instance_geometry>			// ---- geometric element name
void ZMeshParser::_parseLibraryVisualScenes(const xml_node& node, const std::wstring& visualSceneId)
{
	ZASSERT(wcscmp(node.name(), L"library_visual_scenes") == 0, L"[ZMeshParser] ::parseLibraryVisualScenes() node given is not <library_visual_scenes> node.");
	xml_node visual_scene = node.find_child_by_attribute(L"id", visualSceneId.c_str());
	ZASSERT(visual_scene, L"[ZMeshParser] ::parseLibraryVisualScenes() no visual scene found with the given id.");
	// TODO each node should feed directly into the Zenithia scene nodes
	// DONE parse transformation data
	// Only first node is read for now
	for(xml_node scene_node = visual_scene.child(L"node"); scene_node; scene_node = scene_node.next_sibling(L"node")) {
		std::wstring node_name = scene_node.attribute(L"name").as_string();
		xml_node instance_geometry = scene_node.child(L"instance_geometry");
		if(instance_geometry) {
			// Geometry URL
			std::wstring geometry_url = _hashlessID(instance_geometry.attribute(L"url").as_string());
			strGeometryURLs.push_back(geometry_url);
			// Transformation
			XMMATRIX transform_matrix = XMMATRIX(1.0f, 0.0f, 0.0f, 0.0f,
												 0.0f, 1.0f, 0.0f, 0.0f,
												 0.0f, 0.0f, 1.0f, 0.0f,
												 0.0f, 0.0f, 0.0f, 1.0f);
			xml_node transform_geometry = scene_node.child(L"matrix");
			if(transform_geometry) {
				// Using matrix
				std::wstring matrix_sid = transform_geometry.attribute(L"sid").as_string();
				if(transform_geometry && matrix_sid == L"transform") {			
					float* matrix_array = _parseFloatArray(transform_geometry.child_value(), 16);
					transform_matrix = XMMatrixTranspose(matrix_array);
					zdelete_array(matrix_array);
				}
			} else {
				// Using transformations
				xml_node translate_geometry = scene_node.child(L"translate");
				if(translate_geometry) {
					XMFLOAT3 t_translate = _parseFloat3(translate_geometry.child_value());
					XMMATRIX m_translate = XMMatrixTranslation(t_translate.x, t_translate.y, t_translate.z);
					transform_matrix = XMMatrixMultiply(transform_matrix, m_translate);
				}
				for(xml_node rotate_geometry = scene_node.child(L"rotate"); 
					rotate_geometry; 
					rotate_geometry = rotate_geometry.next_sibling(L"rotate")) {
					XMFLOAT4 t_rotate = _parseFloat4(rotate_geometry.child_value());
					XMMATRIX m_rotate;
					if(t_rotate.x + t_rotate.y + t_rotate.z == 1.0f) {
						m_rotate = XMMatrixRotationNormal(XMLoadFloat4(&t_rotate), t_rotate.w);
					} else {
						m_rotate = XMMatrixRotationAxis(XMLoadFloat4(&t_rotate), t_rotate.w);
					}
					transform_matrix = XMMatrixMultiply(transform_matrix, m_rotate);
				}
				xml_node scale_geometry = scene_node.child(L"scale");
				if(scale_geometry) {
					XMFLOAT3 t_scale = _parseFloat3(scale_geometry.child_value());
					XMMATRIX m_scale = XMMatrixScaling(t_scale.x, t_scale.y, t_scale.z);
					transform_matrix = XMMatrixMultiply(transform_matrix, m_scale);
				}
			}
			XMFLOAT4X4 transform_matrix_f44;
			XMStoreFloat4x4(&transform_matrix_f44, transform_matrix);
			matGeometryTransforms.insert(std::pair<std::wstring, XMFLOAT4X4>(geometry_url, transform_matrix_f44));
		}
	}
	ZASSERT(!strGeometryURLs.empty(), L"[ZMeshParser] ::parseLibraryVisualScenes() no geometry found in visual scene.");
}

// (4) Materials library
// Geometry Material Specifier:
// library_visual_scenes->visual_scene->node->instance_geometry->
// bind_material->technique_common->instance_material(symbol/target)
//
// Material specifier:
// library_materials(id/name)->instance_effect(url)
//
// Effect specifier:
// library_effects->effect(id)->profile_COMMON->..
// - Surface: newparam(sid)->surface(sid=file_id-surface->init_from(file_id)
// - Sampler: newparam(sid)->sampler2D(sid=file_id-sampler)->source(file_id-surface)
// - Material: technique(sid)->phong->...
//			- emission: color
//			- ambient: color
//			- diffuse: texture(file_id-sampler)/color
//			- specular: color
//			- shininess: float
//			- transparency: float
//			- index_of_refraction: float
//
// Images specifier:
// library_images->image(id=file_id, name)->init_from(filename)
void ZMeshParser::_parseLibraryMaterials(const xml_node& top_node)
{
	xml_node library_materials = top_node.child(L"library_materials");
	if(!library_materials) return;
	std::unordered_map<std::wstring, xml_node> materials;
	for(xml_node m = library_materials.child(L"material"); m; m = m.next_sibling(L"material")) {
		materials.insert(std::pair<std::wstring, xml_node>(
			_hashlessID(m.attribute(L"id").as_string()), m));
	}
	if(materials.size() == 0) return;
	xml_node library_effects = top_node.child(L"library_effects");
	std::unordered_map<std::wstring, xml_node> effects;
	for(xml_node e = library_effects.child(L"effect"); e; e = e.next_sibling(L"effect")) {
		effects.insert(std::pair<std::wstring, xml_node>(
			_hashlessID(e.attribute(L"id").as_string()), e));
	}
	xml_node library_images = top_node.child(L"library_images");
	mTextures.clear();
	for(xml_node i = library_images.child(L"image"); i; i = i.next_sibling(L"image")) {
		xml_node texture_file = i.child(L"init_from");
		if(texture_file) {
			mTextures.insert(std::pair<std::wstring, std::wstring> (
				_hashlessID(i.attribute(L"id").as_string()), 
				texture_file.child_value()));
		}
	}
	// DONE: Store textures somewhere

	for(auto& m : materials) {
		std::wstring effect_url = _hashlessID(m.second.child(L"instance_effect").attribute(L"url").as_string());
		ZASSERT(effects.find(effect_url) != effects.end(), L"[ZMeshParser] ::_parseLibraryMaterials() sanity check failed.");
		xml_node effect = effects[effect_url];
		xml_node effect_details = effect.child(L"profile_COMMON");
		if(!effect_details) continue;
		std::unordered_map<std::wstring, xml_node> newparams;
		for(xml_node np = effect_details.child(L"newparam"); np; np = np.next_sibling()) {
			newparams.insert(std::pair<std::wstring, xml_node>(
				np.attribute(L"sid").as_string(),
				np));
		}
		xml_node technique = effect_details.child(L"technique");
		if(!technique) continue;
		SMaterialDesc* mtrl = znew(SMaterialDesc, ());
		xml_node ambient_node = technique.child(L"phong").child(L"ambient");
		xml_node diffuse_node = technique.child(L"phong").child(L"diffuse");
		xml_node specular_node = technique.child(L"phong").child(L"specular");
		xml_node shininess_node = technique.child(L"phong").child(L"shininess");
		xml_node transparency_node = technique.child(L"phong").child(L"transparency");
		xml_node index_of_refraction_node = technique.child(L"phong").child(L"index_of_refraction");
		if(ambient_node) {
			if(ambient_node.child(L"color")) {
				mtrl->colorAmbient = _parseColor(ambient_node.child(L"color").child_value());
			}
		}
		if(diffuse_node) {
			if(diffuse_node.child(L"color")) {
				mtrl->colorDiffuse = _parseColor(diffuse_node.child(L"color").child_value());
			}			
			// DONE: reference textures from Material.
			if(diffuse_node.child(L"texture")) {
				// TODO assuming now that there is only one set of textures always for set 0
				// Otherwise you need to get it via texcoord property which points to:
				// library_visual_scenes->visual_scene->node->instance_geometry>bind_material
				// ->technique_common->instance_material->bind_vertex_input
				// Attributes: semantic=texcoord, input_semantic="TEXCOORD", and input_set="0"
				xml_node sampler_node = newparams[
					diffuse_node.child(L"texture").attribute(L"texture").as_string()];
				xml_node surface_node = newparams[
					sampler_node.child(L"sampler2D").child(L"source").child_value()];
				// Assumed to always have 2D type textures
				std::wstring texture_id = 
					surface_node.child(L"surface").child(L"init_from").child_value();
				mtrl->mapDiffuseTextures.insert(
					std::pair<std::wstring, std::wstring>(texture_id, mTextures[texture_id]));
			}
		}
		if(specular_node) {
			if(specular_node.child(L"color")) {
				mtrl->colorSpecular = _parseColor(specular_node.child(L"color").child_value());
			}
		}
		if(shininess_node) {
			std::wistringstream ss(shininess_node.child(L"float").child_value());
			ss >> mtrl->fShininess;
		}
		if(transparency_node) {
			std::wistringstream ss(transparency_node.child(L"float").child_value());
			ss >> mtrl->fTransparency;
		}
		if(index_of_refraction_node) {
			std::wistringstream ss(index_of_refraction_node.child(L"float").child_value());
			ss >> mtrl->fIndexOfRefraction;
		}
		mMaterials.insert(std::pair<std::wstring, SMaterialDesc*>(m.first, mtrl));
	}
}

// (5) Geometry library
// <library_geometries>				// - Geometry library
//     <geometry>						// -- geometric element
//         <mesh>
//             <source>						// ---- data source
//                 <float_array>				// ----- data
//                 <technique_common>			// ----- description (count, stride, data type)
//                     <accessor>
//                         <param>
//             <vertices>					// ---- Semantic and source
//             <polygons>|<polylist>		// ---- Polygons
//                 <input>						// ----- Semantic/Vertex structure
//                 <p>							// ----- indices
void ZMeshParser::_parseLibraryGeometries(const xml_node& node)
{
	ZASSERT(wcscmp(node.name(), L"library_geometries") == 0, L"[ZMeshParser] ::parseLibraryGeometries() node given is not an <library_geometries> xml node.");
	for(unsigned int i = 0; i < strGeometryURLs.size(); ++i) {
		xml_node geometry = node.find_child_by_attribute(L"geometry", L"id", strGeometryURLs[i].c_str());
		if(geometry) {
			SMeshNode* mesh_node = znew(SMeshNode, ());
			mMeshNodes.insert(std::pair<std::wstring, SMeshNode*>(strGeometryURLs[i], mesh_node));
			mesh_node->matTransform = matGeometryTransforms[strGeometryURLs[i]];
			_parseGeometry(mesh_node, geometry);
		} else {
			ZASSERT(false, L"[ZMeshParser] ::parseLibraryGeometries() geometry is invalid! (shouldn't happen)");
		}
	}
}

void ZMeshParser::_parseGeometry(SMeshNode* mesh_node, const xml_node& geometry)
{
	ZASSERT(geometry && mesh_node, L"[ZMeshParser] ::parseGeometry() error.");
	xml_node mesh = geometry.child(L"mesh");
	ZASSERT(mesh, L"[ZMeshParser] ::parseGeometry() error.");

	// Parse Data Sources
	for(xml_node source = mesh.child(L"source"); source; source = source.next_sibling(L"source")) {
		SMeshSource* mesh_source = znew(SMeshSource, ());

		xml_node data_array = source.child(L"float_array");
		if(data_array) {
			mesh_source->eDataType = DataType_Float;
			mesh_source->uDataTypeSize = sizeof(float);
			mesh_source->uCount = data_array.attribute(L"count").as_uint();
			mesh_source->pData = static_cast<void*>(_parseFloatArray(data_array.child_value(), mesh_source->uCount));
		} else {
			throw ZEXCEPTION_MESSAGE(L"ZMeshParser::parseGeometry() Unsupported data array found in <source>.");
		}

		xml_node technique_common = source.child(L"technique_common");
		ZASSERT(technique_common, L"[ZMeshParser] ::parseGeometry() error.");

		xml_node technique_accessor = technique_common.child(L"accessor");
		ZASSERT(technique_accessor, L"[ZMeshParser] ::parseGeometry() error.");

		mesh_source->uElementCount = technique_accessor.attribute(L"count").as_uint();
		mesh_source->uElementStride = technique_accessor.attribute(L"stride").as_uint();
		mesh_source->uElementSize = mesh_source->uDataTypeSize * mesh_source->uElementStride;
		ZASSERT(mesh_source->uElementCount > 0 && mesh_source->uElementStride > 0, L"[ZMeshParser] ::parseGeometry() error.");

		for(xml_node accessor_param = technique_accessor.child(L"param"); accessor_param; accessor_param = accessor_param.next_sibling(L"param")) {
			mesh_source->vParamNames.push_back(accessor_param.attribute(L"name").as_string());
		}

		mMeshSources[source.attribute(L"id").as_string()] = mesh_source;
	}

	//-------------------------------------------------------------------
	// Parse Polygons and create actual structures
	// DONE: One polygons/polylist per subset
	std::wstring polytag = L"polylist";
	xml_object_range<xml_named_node_iterator> subsets_list = mesh.children(polytag.c_str());
	if(subsets_list.begin() == subsets_list.end()) { // Empty!
		polytag = L"polygons";
		subsets_list = mesh.children(polytag.c_str());
		if(subsets_list.begin() == subsets_list.end()) {
			throw ZEXCEPTION_MESSAGE(L"ZMeshParser::parseGeometry() geometry being parsed has no polygons!");
		}
	}

	for(xml_node polygons = mesh.child(polytag.c_str()); polygons; polygons = polygons.next_sibling(polytag.c_str())) {
		SMeshSubset* subset = znew(SMeshSubset, ());
		size_t polygons_count = polygons.attribute(L"count").as_uint();
		ZASSERT(polygons, L"[ZMeshParser] ::parseGeometry() error.");

		// Parse <vertices>, overrides input semantic "VERTEX" and provides the actual source of data (why?! who knows)
		xml_node vertices_node = mesh.child(L"vertices");

		// Parse semantics into temporary vertex declaration buffer
		unsigned int vertex_elements_count = 0;
		std::vector<std::wstring> semantics_sources;
		// DONE: There is only one vertex declaration buffer for the first geometry, this should be dynamic
		// DONE: There is a different declaration per subset
		subset->vVertexDeclarations.clear();
		for(xml_node semantic = polygons.child(L"input"); semantic; semantic = semantic.next_sibling(L"input")) {
			unsigned int semantic_offset = semantic.attribute(L"offset").as_uint();
			// Find out how many elements there are, COLLADA supports defining two different semantics with the same offset.. this is why only the max offset is taken
			if(semantic_offset + 1 > vertex_elements_count) {
				vertex_elements_count = semantic_offset + 1;
			}
			unsigned char semantic_set = static_cast<unsigned char>(semantic.attribute(L"set").as_int(0));
			std::wstring semantic_name = semantic.attribute(L"semantic").as_string();
			std::wstring semantic_source = _hashlessID(semantic.attribute(L"source").as_string());
			// Process vertices special override tag "VERTEX" to "POSITION"
			if(vertices_node && semantic_source == vertices_node.attribute(L"id").as_string()) {
				xml_node position_semantic = vertices_node.child(L"input");
				semantic_name = position_semantic.attribute(L"semantic").as_string();
				semantic_source = _hashlessID(std::wstring(position_semantic.attribute(L"source").as_string()));
			}
			// Parse semantics
			SMeshSource* ms = mMeshSources[semantic_source];
			SVertexDeclaration vd;
			vd.ucDeclarationType = _getDeclType(ms->eDataType, ms->uElementStride);
			vd.ucSet = semantic_set;
			if(semantic_name == L"VERTEX" || semantic_name == L"POSITION") {
				vd.ucDeclarationUsage = VertexDeclUsage::Position;
				semantics_sources.push_back(semantic_source);
				_updatePositionDataForUpAxis(ms->pData, ms->uCount, ms->uElementStride);
			} else if(semantic_name == L"NORMAL") {
				vd.ucDeclarationUsage = VertexDeclUsage::Normal;
				semantics_sources.push_back(semantic_source);
				_updatePositionDataForUpAxis(ms->pData, ms->uCount, ms->uElementStride);
			} else if(semantic_name == L"TEXCOORD") {
				vd.ucDeclarationUsage = VertexDeclUsage::TexCoord;
				semantics_sources.push_back(semantic_source);
			} else {
				std::wostringstream ss;
				ss << "ZMeshParser::parseGeometry() found an unsupported semantic: ";
				ss << "\"" << semantic_source << "\"";
				throw ZEXCEPTION_MESSAGE(ss.str());
			}
			// DONE: There is a different declaration per subset
			subset->vVertexDeclarations.push_back(vd);
		}

		// Sanity check
		ZASSERT(vertex_elements_count <= semantics_sources.size(), L"[ZMeshParser] ::parseGeometry() error.");

		// Calculate Vertex size
		size_t vertex_size = 0;
		for(unsigned int s = 0; s < semantics_sources.size(); ++s) {
			SMeshSource* ms = mMeshSources[semantics_sources[s]];
			vertex_size += ms->uElementSize;
		}

		// Parse polygons indices
		//std::vector<unsigned int> vcount;
		//for(xml_node vcount_node = polygons.child(L"vcount"); vcount_node; vcount_node = vcount_node.next_sibling(L"vcount")) {
		//	std::wistringstream iss(vcount_node.child_value());
		//	while(!iss.eof()) {
		//		unsigned int vcount_item;
		//		iss >> vcount_item;
		//		vcount.push_back(vcount_item);
		//	}
		//}
		//unsigned int vcount_index = 0;

		unsigned int polygon_vertex_count = 3;
		unsigned int writing_index = 0;
		char* temp_data = static_cast<char*>(malloc(polygons_count * polygon_vertex_count * vertex_size));
		memset(temp_data, 0, polygons_count * 3 * vertex_size);
		for(xml_node p = polygons.child(L"p"); p; p = p.next_sibling(L"p")) {
			std::wistringstream iss(p.child_value());
			while(!iss.eof()) {
				polygon_vertex_count = 3;

				//if(!vcount.empty()) {
				//	ZASSERT(vcount_index < vcount.size(), L"[ZMeshParser] parseLibraryGeometries() vcount index is out of range.");
				//	polygon_vertex_count = vcount[vcount_index];
				//	vcount_index++;
				//}

				for(unsigned int ti = 0; ti < polygon_vertex_count; ++ti) {
					for(unsigned int pi = 0; pi < vertex_elements_count; ++pi) {
						unsigned int element_index;
						iss >> element_index;
						SMeshSource* ms = mMeshSources[semantics_sources[pi]];
						char* element_data = static_cast<char*>(ms->pData) + (element_index * ms->uElementSize);
						memcpy(static_cast<void*>(temp_data + writing_index), element_data, ms->uElementSize);
						writing_index += ms->uElementSize;
					}
				}
			}
		}
		// DONE: Should be dynamic
		// DONE: Different vertex data/count per subset
		subset->vertexData.pVertexData = temp_data;
		subset->vertexData.uVertexCount = polygons_count * 3;
		mesh_node->subsets.push_back(subset);

		// Material ID
		subset->material = _hashlessID(polygons.attribute(L"material").as_string());
	}
	ZASSERT(mesh_node->subsets.size() > 0, L"ZMeshParser::parseGeometry() no subsets were parsed for this geometry!");
}

////////////////////////////////////////////////////////////////
// █████ ████  ██   █
// ██    ██  █  ██ █
// ████  ████    ██
// ██    ██  █  ██ █
// ██    ████  ██   █

////////////////////////////////////////////////////////////////
// ████  ████   ████
//     █ ██  █ ██
//  ███  ██  █  ███
//     █ ██  █     █
// ████  ████  ████

// █████████████████████████████████████████████████████████████████

////////////////////////////////////////////////////////////////////
/// TEMP
/// Creates actual device primitive objects and materials for rendering
///
void ZMeshParser::_createMesh(SMeshNode* mesh_node)
{
	ZASSERT(
		mesh_node->subsets.size() >= 1 && 
		!mesh_node->subsets[0]->vVertexDeclarations.empty() && 
		mesh_node->subsets[0]->vertexData.pVertexData && 
		mesh_node->subsets[0]->vertexData.uVertexCount > 0 && !bMeshCreated, 
		L"[ZMeshParser] ::_createMesh() sanity check failed.");
	unsigned int subset = _createSubset();
	ZVertexDeclaration& vd = _getVD(subset);
	ZVertexBuffer& vb = _getVB(subset);
	//ZIndexBuffer& ib = _getIB(subset);
	ZMaterial& mtrl = _getMaterial(subset);

	// Create vertex declaration
	for(auto& d : mesh_node->subsets[0]->vVertexDeclarations) {
		vd.pushElement(d.ucDeclarationType, d.ucDeclarationUsage, d.ucSet);
	}
	vd.pushEndElement();

	// Create vertex buffer
	vb.create(vd, mesh_node->subsets[0]->vertexData.uVertexCount);
	vb.writeVertices(0, mesh_node->subsets[0]->vertexData.pVertexData, mesh_node->subsets[0]->vertexData.uVertexCount);

	// Create materials
	// TODO It should create material for this subset only distinguished by geometry material id
	if(mMaterials.size() > 0) {
		mtrl.create(*(mMaterials.begin()->second));
	}

	bMeshCreated = true;
}

std::wstring ZMeshParser::_hashlessID(const std::wstring& hashedId)
{
	if(hashedId[0] == L'#') {
		std::wstring out_str = hashedId;
		out_str.erase(0, 1);
		return out_str;
	} else {
		return hashedId;
	}
}

float* ZMeshParser::_parseFloatArray(const std::wstring& textData, unsigned int count)
{
	ZASSERT(!textData.empty() && count > 0, L"[ZMeshParser] ::parseFloatArray() invalid data given.");
	float* data = znew_array(float, count);
	std::wistringstream is(textData);
	for(unsigned int i = 0; i < count; ++i) {
		is >> data[i];
	}
	return data;
}

XMFLOAT4 ZMeshParser::_parseColor(const std::wstring& textData)
{
	ZASSERT(!textData.empty(), L"[ZMeshParser] ::parseColor() invalid data given.");
	float data [4] = {0.0f, 0.0f, 0.0f, 1.0f};
	std::wistringstream is(textData);
	for(unsigned int i = 0; i < 4; ++i) {
		is >> data[i];
	}
	return XMFLOAT4(data[0], data[1], data[2], data[3]);
}

XMFLOAT3 ZMeshParser::_parseFloat3(const std::wstring& textData)
{
	ZASSERT(!textData.empty(), L"[ZMeshParser] ::parseFloat3() invalid data given.");
	float data [3] = {0.0f, 0.0f, 0.0f};
	std::wistringstream is(textData);
	for(unsigned int i = 0; i < 3; ++i) {
		is >> data[i];
	}
	return XMFLOAT3(data[0], data[1], data[2]);
}

XMFLOAT4 ZMeshParser::_parseFloat4(const std::wstring& textData) 
{
	ZASSERT(!textData.empty(), L"[ZMeshParser] ::parseFloat4() invalid data given.");
	float data [4] = {0.0f, 0.0f, 0.0f, 0.0f};
	std::wistringstream is(textData);
	for(unsigned int i = 0; i < 4; ++i) {
		is >> data[i];
	}
	return XMFLOAT4(data[0], data[1], data[2], data[3]);
}

unsigned char ZMeshParser::_getDeclType(ESourceDataType dataType, unsigned int elementStride)
{
	unsigned char decl_type = 0;
	switch(dataType) {
		case DataType_Float:
			switch(elementStride) {
				case 1:
					decl_type = VertexDeclType::Float1;
					break;
			
				case 2:
					decl_type = VertexDeclType::Float2;
					break;

				case 3:
					decl_type = VertexDeclType::Float3;
					break;

				case 4:
					decl_type = VertexDeclType::Float4;
					break;
			}
			break;

		default:
			std::wostringstream ss;
			ss << L"ZMeshParser::getDeclType() unsupported inputs." << std::endl;
			ss << L"Data Type: \"" << dataType << std::endl;
			ss << L"Element Stride: \"" << elementStride;
			throw ZEXCEPTION_MESSAGE(ss.str());
	}
	return decl_type;
}

void ZMeshParser::_updatePositionDataForUpAxis(void* positionData, unsigned int elementCount, unsigned int elementStride)
{
	// TODO: check conversion function when nodes and transformation are functioning (currently model is displayed in model space)
	if(eUpAxis == UpAxis_Y) return;
	// get rid of annoying warnings
	positionData; 
	elementCount;
	elementStride;
	// Assuming position data to always be full float (4 bytes)
	/*float* data = static_cast<float*>(positionData);
	float* element_data;
	float temp;
	for(unsigned int i = 0; i < elementCount; i+=elementStride) {
		element_data = &data[i];
		if(eUpAxis == UpAxis_Z) {
			temp = element_data[1];
			element_data[1] = -element_data[2];
			element_data[2] = temp;
		} else if(eUpAxis == UpAxis_X) {
		} else {
			throw new ZEXCEPTION_MESSAGE(L"ZMeshParser::_updatePositionDataForUpAxis() error occured.");
		}
	}*/
}