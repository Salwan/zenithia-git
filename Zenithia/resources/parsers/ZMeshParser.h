////////////////////////////////////////////////////////////////
// Name: ZMeshParser
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <string>
#include <unordered_map>
#include "../../pugixml/pugixml.hpp"
#include "../../models/ZMesh.h"

//////////////////////////////////////////////////// Definitions

namespace zen
{
	// Front definitions
	struct SMaterialDesc;

	////////////////////////////////////////////////////////////////
	/// @class ZMeshParser
	/// @brief Mesh parser that supports multiple model formats (collada only for now)
	/// 
	class ZMeshParser : public ZMesh
	{
	public:
		explicit ZMeshParser(const std::wstring& modelFile, bool autoCreateMesh = true);
		virtual ~ZMeshParser();

		void createMesh();

	public: 		
		/// Model data types
		enum ESourceDataType
		{
			DataType_Float,
			DataType_Int,
		};
		enum EModelUpAxis
		{
			UpAxis_X,
			UpAxis_Y,
			UpAxis_Z,
		};
		/// Map/Structure to hold all necessary information about each mesh source
		struct SMeshSource
		{
			void* pData;
			unsigned int uCount;
			unsigned int uElementCount;
			unsigned int uElementStride;
			unsigned int uElementSize;
			unsigned int uDataTypeSize;
			ESourceDataType eDataType;
			std::vector<std::wstring> vParamNames;
		};
		/// One vertex declaration element
		struct SVertexDeclaration
		{
			unsigned char ucDeclarationType;
			unsigned char ucDeclarationUsage;
			unsigned char ucSet;
		};
		/// Vertex data of mesh
		struct SMeshData
		{
			SMeshData() : pVertexData(nullptr), uVertexCount(0) {}
			char* pVertexData;
			unsigned int uVertexCount;
		};
		/// One subset of mesh data with declaration
		struct SMeshSubset
		{
			std::vector<SVertexDeclaration> vVertexDeclarations;
			SMeshData vertexData;
			std::wstring material; // Subset material id
		};		
		/// A mesh node is a group of mesh subsets
		struct SMeshNode
		{
			XMFLOAT4X4 matTransform;
			std::vector<SMeshSubset*> subsets;
		};

	private:
		ZMeshParser(const ZMeshParser&){}
		ZMeshParser& operator=(const ZMeshParser&){}
		
		void _loadFromFile(const std::wstring& modelFile);
		void _parseColladaMesh(const std::wstring& colladaFile);

		void _parseAsset(const pugi::xml_node& node);
		void _parseScene(const pugi::xml_node& node);
		void _parseLibraryVisualScenes(const pugi::xml_node& node, const std::wstring& visualSceneId);
		void _parseLibraryGeometries(const pugi::xml_node& node);
		void _parseGeometry(SMeshNode* mesh_node, const pugi::xml_node& node);
		void _parseLibraryMaterials(const pugi::xml_node& top_node);
		std::wstring _hashlessID(const std::wstring& hashedId);
		float* _parseFloatArray(const std::wstring& textData, unsigned int count);
		XMFLOAT4 _parseColor(const std::wstring& textData);
		XMFLOAT3 _parseFloat3(const std::wstring& textData);
		XMFLOAT4 _parseFloat4(const std::wstring& textData);
		unsigned char _getDeclType(ESourceDataType dataType, unsigned int elementStride);
		void _updatePositionDataForUpAxis(void* positionData, unsigned int elementCount, unsigned int elementStride);
		void _createMesh(SMeshNode* mesh_node);

		// Asset
		std::wstring strTitle;
		EModelUpAxis eUpAxis;

		// Scene
		std::wstring strInstanceVisualSceneURL;

		// TODO: Eleminate all these unordered maps that use the geometry url as a key
		// you need only 1 with a date structure.

		// Library Visual Scenes
		std::vector<std::wstring> strGeometryURLs;
		std::unordered_map<std::wstring, XMFLOAT4X4> matGeometryTransforms;
		std::unordered_map<std::wstring, SMeshSource*> mMeshSources;

		// Material/Effect information
		std::unordered_map<std::wstring, SMaterialDesc*> mMaterials;
		std::unordered_map<std::wstring, std::wstring> mTextures;

		// Temporary mesh data for defferred creation
		bool bAutoCreateMesh;
		bool bMeshCreated;
		std::vector<SVertexDeclaration> vVertexDeclarations;
		SMeshData mVertexData;

		//------------ Multi-geometry - Multi-subset
		std::unordered_map<std::wstring, SMeshNode*> mMeshNodes;

	public:
		// Accessors
		const std::unordered_map<std::wstring, SMeshNode*>& getMeshNodes() const {return mMeshNodes;}
		const std::unordered_map<std::wstring, SMaterialDesc*>& getMaterials() const {return mMaterials;}


		/////////////////////////////////////////// Parsed Data
		// <asset>
		//   <up_axis> eUpAxis </up_axis>
		// <scene>
		//   <instance_visual_scene> strInstanceVisualSceneURL </instance_visual_scene>
		// <library_visual_scenes>
		//   <visual_scene>
		//     <node>
		//       <instance_geometry>
		//         [strGeometryURLs]
		//         [matGeometryTransforms]
		//
		// <library_geometries>				// - Geometry library
		//     <geometry>						// -- geometric element
		//         <mesh>
		//             <source>						// ---- data source
		//                 <float_array>				// ----- data
		//                 <technique_common>			// ----- description (count, stride, data type)
		//                     <accessor>
		//                         <param>
		//             <vertices>					// ---- Semantic and source
		//             <polygons>|<polylist>		// ---- Polygons
		//                 <input>						// ----- Semantic/Vertex structure
		//                 <p>							// ----- indices
	};
};

