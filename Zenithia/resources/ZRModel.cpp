////////////////////////////////////////////////////////////////
// Name: ZRModel
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include "ZRModel.h"
#include "../utils/ZMemoryDebug.h"
#include "../exceptions/ZException.h"
#include "../utils/ZAssert.h"
#include "parsers/ZMeshParser.h"
#include "ZRMesh.h"
#include <fstream>

//////////////////////////////////////////////////// Definitions
using namespace zen;

ZMeshParser* g_modelParser = nullptr;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZRModel::ZRModel(const std::wstring& modelFile)
	: strFile(modelFile)
{
	eType = ZResource::EResourceType::Resource_ColladaModel;
	strDesc = L"model." + modelFile;
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZRModel::~ZRModel()
{
	DELETE_OBJECT(g_modelParser)
	if(mMeshResources.size() > 0) {
		for(unsigned int i = 0; i < mMeshResources.size(); ++i) {
			if(mMeshResources[i]) {
				zdelete(mMeshResources[i]);
			}
		}
	}
	mMeshResources.clear();
}

bool ZRModel::prepare()
{
	return true;
}

bool ZRModel::loadData()
{
	g_modelParser = znew(ZMeshParser, (strFile, false));
	return true;
}

bool ZRModel::createObjects()
{
	for(auto mesh_node = g_modelParser->getMeshNodes().begin(); 
		mesh_node != g_modelParser->getMeshNodes().end(); 
		++mesh_node) {
		ZRMesh* mesh_resource = znew(ZRMesh, (mesh_node->first));
		mesh_resource->_initMesh(mesh_node->second, g_modelParser->getMaterials());
		mMeshResources.push_back(mesh_resource);
	}
	return true;
}

bool ZRModel::finish()
{
	return true;
}

