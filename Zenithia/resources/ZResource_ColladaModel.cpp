////////////////////////////////////////////////////////////////
// Name:  {BaseInheritance}
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include "ZResource_ColladaModel.h"
#include "../exceptions/ZException.h"
#include "../models/ZEmptyMesh.h"
#include "../models/ZColladaMesh.h"
#include "../utils/ZAssert.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZResource_ColladaModel::ZResource_ColladaModel(const std::wstring& modelFile)
	: strFile(modelFile), mMesh(nullptr), pTemporaryMesh(nullptr)
{
	eType = ZResource::EResourceType::Resource_ColladaModel;
	strDesc = L"model." + modelFile;
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZResource_ColladaModel::~ZResource_ColladaModel()
{
	DELETE_OBJECT(mMesh);
}

bool ZResource_ColladaModel::prepare()
{
	mMesh = znew(ZEmptyMesh, ());
	return true;
}

bool ZResource_ColladaModel::loadData()
{
	pTemporaryMesh = znew(ZColladaMesh, (strFile, false));
	return true;
}

bool ZResource_ColladaModel::createObjects()
{
	pTemporaryMesh->createMesh();
	return true;
}

bool ZResource_ColladaModel::finish()
{
	DELETE_OBJECT(mMesh);
	mMesh = pTemporaryMesh;
	return true;
}

const ZMesh* ZResource_ColladaModel::getModel() const
{
	ZASSERT(mMesh != nullptr, L"[ZResource_ColladaModel] ::getModel() sanity check failed.");
	return mMesh;
}
