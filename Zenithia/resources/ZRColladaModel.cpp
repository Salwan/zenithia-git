////////////////////////////////////////////////////////////////
// Name: ZRColladaModel
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include "ZRColladaModel.h"
#include "../exceptions/ZException.h"
#include "../utils/ZAssert.h"
#include <fstream>

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZRColladaModel::ZRColladaModel(const std::wstring& modelFile)
	: strFile(modelFile)
{
	eType = ZResource::EResourceType::Resource_ColladaModel;
	strDesc = L"model." + modelFile;
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZRColladaModel::~ZRColladaModel()
{
	
}

bool ZRColladaModel::prepare()
{
	return true;
}

bool ZRColladaModel::loadData()
{
	return true;
}

bool ZRColladaModel::createObjects()
{
	return true;
}

bool ZRColladaModel::finish()
{
	return true;
}

