////////////////////////////////////////////////////////////////
// Name: ZResourceSystem
// Notes: 
//	> Do unloading of textures (currently everything leaks)
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include "ZResourceSystem.h"
#include <functional>
#include <iostream>
#include "ZResource.h"
#include "ZResource_2DTexture.h"
#include "ZResource_ColladaModel.h"
#include "ZResource_Effect.h"
#include "../d3d9/ZD3D9.h"
#include "../utils/ZAssert.h"
#include "../exceptions/ZError.h"
// Resource System Mark II
#include "ZRModel.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;
ZResourceSystem* ZResourceSystem::mInstance = nullptr;
bool ZResourceSystem::bInitialized = false;

///////////////////////////////////////////////////////// STATIC
/// Instance
///
ZResourceSystem* ZResourceSystem::instance()
{
	if(!bInitialized) {
		init();
	}
	return mInstance;
}

void ZResourceSystem::destroy()
{
	if(mInstance) {
		// Terminate loader thread
		mInstance->bTerminateLoaderThread = true;
		mInstance->conditionResourcePending.notify_all();
		mInstance->threadResourceLoader.join();
		// Destroy instance
		zdelete(mInstance);
	}
	bInitialized = false;
}

///////////////////////////////////////////////////////// STATIC
// Initialize resource system, called automatically.
//
void ZResourceSystem::init() 
{
	ZASSERT(!bInitialized, L"[ZResourceSystem] init() ZResourceSystem already initialized.");
	mInstance = znew(ZResourceSystem, ());
	bInitialized = true;
	mInstance->threadResourceLoader = std::thread(&ZResourceSystem::workerResourceLoader, mInstance);
}

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZResourceSystem::ZResourceSystem() : bTerminateLoaderThread(false)
{

}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZResourceSystem::~ZResourceSystem()
{
	while(mResourceQueue.size()) mResourceQueue.pop();
	while(mLoadedResourceQueue.size()) mLoadedResourceQueue.pop();
	for(auto& r : mResourcesMap) {
		zdelete(r.second);
	}
	mResourcesMap.clear();
	mResourceFilesMap.clear();
}

void ZResourceSystem::loadResourcePack(const ZResourcePack& resourcePack)
{
	for(auto ti = resourcePack.mTextures.begin(); ti != resourcePack.mTextures.end(); ++ti) {
		loadTexture(ti->first, ti->second);
	}

	for(auto mi = resourcePack.mModels.begin(); mi != resourcePack.mModels.end(); ++mi) {
		loadModel(mi->first, mi->second);
	}

	for(auto ei = resourcePack.mEffects.begin(); ei != resourcePack.mEffects.end(); ++ei) {
		loadEffect(ei->first, ei->second);
	}
}

ZResource* ZResourceSystem::loadTexture(const std::wstring& alias, const std::wstring& file)
{
	ZResource* resource = _checkResource(alias, file);
	if(resource == nullptr) {
		resource = znew(ZResource_2DTexture, (_getAssetPath(file)));
		_prepareResource(resource, alias, file);
	}
	return resource;
}

ZResource* ZResourceSystem::loadModel(const std::wstring& alias, const std::wstring& file)
{
	ZResource* resource = _checkResource(alias, file);
	if(resource == nullptr) {
		//resource = znew(ZResource_ColladaModel, (_getAssetPath(file)));
		//_prepareResource(resource, alias, file);
		//ZResource* test = znew(ZRModel, (_getAssetPath(file)));
		//test->prepare();
		resource = znew(ZRModel, (_getAssetPath(file)));
		_prepareResource(resource, alias, file);
	}
	return resource;
}

ZResource* ZResourceSystem::loadEffect(const std::wstring& alias, const std::wstring& file)
{
	ZResource* resource = _checkResource(alias, file);
	if(resource == nullptr) {
		resource = znew(ZResource_Effect, (_getAssetPath(file)));
		_prepareResource(resource, alias, file);
	}
	return resource;
}

ZResource* ZResourceSystem::getResource(const std::wstring& alias) const 
{
	auto resource = mResourcesMap.find(alias);
	if(resource != mResourcesMap.end()) {
		return resource->second;
	} else {
		std::wostringstream ss;
		ss << L"ZResourceSystem::getResource() could not find requrested resource:"<<std::endl;
		ss << L"Requested resource alias: " << alias;
		throw ZEXCEPTION_MESSAGE(ss.str());
	}
}

void ZResourceSystem::_prepareResource(ZResource* resource, const std::wstring& alias, const std::wstring& file)
{	
	resource->prepare();
	std::unique_lock<std::mutex> lockLoader(mutexResourceQueue);
	mResourceQueue.push(resource);
	lockLoader.unlock();
	conditionResourcePending.notify_one();
	mResourcesMap.insert(std::pair<const std::wstring, ZResource*>(alias, resource));
	mResourceFilesMap.insert(std::pair<const std::wstring, ZResource*>(_getFileName(file), resource));
}

std::wstring ZResourceSystem::_getFileName(const std::wstring& file)
{
	std::wstring out;
	std::wstring::size_type idx = file.find_last_of(L"/\\");
	if(idx == std::wstring::npos) {
		out = file;
	} else {
		ZASSERT(idx + 1 < file.length() - 1, L"[ZResourceSystem] ::_getFileName() sanity check failed.");
		out = file.substr(idx + 1);
	}
	return out;
}

std::wstring ZResourceSystem::_getAssetPath(const std::wstring& file)
{
	if(strAssetsPath.empty()) {
		return file;
	} else {
		return strAssetsPath + file;
	}
}

ZResource* ZResourceSystem::_checkResource(const std::wstring& alias, const std::wstring& file) 
{
	ZResource* resource = nullptr;
	auto alias_found = mResourcesMap.find(alias);
	if(alias_found != mResourcesMap.end()) {
		resource = alias_found->second;
	} else {
		std::wstring fname = _getFileName(file);
		auto file_found = mResourceFilesMap.find(fname);
		if(file_found != mResourceFilesMap.end()) {
			resource = file_found->second;
			// Alias not found for this resource, but file is already loaded: add as new alias
			mResourcesMap[alias] = resource;
		}
	}
	return resource;
}

////////////////////////////////////////////////////////////////
/// Process resources
///
void ZResourceSystem::process()
{
	std::unique_lock<std::mutex> mutexLoadedResourceQueue(mutexLoadedResourceQueue, std::defer_lock);
	if(mutexLoadedResourceQueue.try_lock()) {
		if(!mLoadedResourceQueue.empty()) {
			// Retrieve loaded resource
			ZResource* resource = mLoadedResourceQueue.front();
			mLoadedResourceQueue.pop();
			mutexLoadedResourceQueue.unlock();
			// Create resource objects and finalize
			resource->createObjects();
			resource->finish();
			resource->dispatchLoadingComplete();
			//
			std::wostringstream ss;
			ss << L"[ResourceSystem] loading complete: " << resource->getDesc() << std::endl;
			OutputDebugStringW(ss.str().c_str());
		}
	}
}

////////////////////////////////////////////////////////////////
/// Resource Loading Worker
///
void ZResourceSystem::workerResourceLoader()
{
	std::unique_lock<std::mutex> lockLoader(mutexResourceLoader);
	while(1) {
		conditionResourcePending.wait(lockLoader);
		if(bTerminateLoaderThread) break;
		while(!mResourceQueue.empty()) {
			// Retrieve object
			std::unique_lock<std::mutex> locked(mutexResourceQueue);
			ZResource* resource = mResourceQueue.front();
			mResourceQueue.pop();
			locked.unlock();
			try {
				resource->loadData();
				std::unique_lock<std::mutex> lockedComplete(mutexLoadedResourceQueue);
				mLoadedResourceQueue.push(resource);
				lockedComplete.unlock();
			}
			catch(ZException* e) {
				ZError::showError(e->getMessage());
			}
		}
	}
}
