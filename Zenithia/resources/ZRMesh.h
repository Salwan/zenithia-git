////////////////////////////////////////////////////////////////
// Name: ZRMesh
// Desc: A device mesh
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZResource.h"
#include "parsers/ZMeshParser.h"
#include <xnamath.h>

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZMeshParser;
	class ZManualMesh;
	class ZCMesh;

	////////////////////////////////////////////////////////////////
	/// @class ZRMesh
	/// @brief Holds a raw mesh and device objects.
	/// 
	class ZRMesh : public ZResource
	{
		friend class ZRModel;
		friend class ZCMesh;

	public:
		virtual ~ZRMesh();

	protected:		
		explicit ZRMesh(const std::wstring& mesh_descriptor);
		virtual ZMesh* getMesh() const override;

	private:
		void _initMesh(ZMeshParser::SMeshNode* mesh_node, 
			const std::unordered_map<std::wstring, SMaterialDesc*>& materials);
		
		bool bMeshInitialized;
		ZManualMesh* mMesh;
		XMFLOAT4X4 matTransform;
	};
};