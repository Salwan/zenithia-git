////////////////////////////////////////////////////////////////
// Name: ZResourcePack
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include "ZResourcePack.h"
#include "../exceptions/ZException.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZResourcePack::ZResourcePack(const std::wstring& packName)
	: strPackName(packName)
{
	
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZResourcePack::~ZResourcePack()
{
	
}

////////////////////////////////////////////////////////////////
// Adds a texture to the pack
//
void ZResourcePack::addTexture(const std::wstring& textureAlias, const std::wstring& textureFile)
{
	auto ti = mTextures.find(textureAlias);
	if(ti == mTextures.end()) {
		mTextures[textureAlias] = textureFile;
	} else {
		std::wostringstream ss;
		ss << L"ZResourcePack::addTexture() texture already exists." << std::endl;
		ss << L"Texture Alias: " << textureAlias << std::endl;
		ss << L"Texture File: " << textureFile;
		throw ZEXCEPTION_MESSAGE(ss.str());
	}
}

////////////////////////////////////////////////////////////////
// Addsd a collada model file to pack
//
void ZResourcePack::addModel(const std::wstring& modelAlias, const std::wstring& modelFile)
{
	auto ti = mModels.find(modelAlias);
	if(ti == mModels.end()) {
		mModels[modelAlias] = modelFile;
	} else {
		std::wostringstream ss;
		ss << L"ZResourcePack::addModel() model already exists." << std::endl;
		ss << L"Model Alias: " << modelAlias << std::endl;
		ss << L"Model File: " << modelFile;
		throw ZEXCEPTION_MESSAGE(ss.str());
	}
}

////////////////////////////////////////////////////////////////
// Adds an effect file to pack
void ZResourcePack::addEffect(const std::wstring& effectAlias, const std::wstring& effectFile)
{
	auto ei = mEffects.find(effectAlias);
	if(ei == mEffects.end()) {
		mEffects[effectAlias] = effectFile;
	} else {
		std::wostringstream ss;
		ss << L"ZResourcePack::addEffect() effect already exists." << std::endl;
		ss << L"Effect Alias: " << effectAlias << std::endl;
		ss << L"Effect File: " << effectFile;
		throw ZEXCEPTION_MESSAGE(ss.str());
	}
}
