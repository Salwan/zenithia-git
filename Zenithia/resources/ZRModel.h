////////////////////////////////////////////////////////////////
// Name: ZRModel
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZResource.h"
#include "../d3d9/ZD3D9.h"
#include "../models/ZMesh.h"
#include <string>
#include <vector>

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZRMesh;

	////////////////////////////////////////////////////////////////
	/// @class ZRModel
	/// @brief
	/// 
	class ZRModel : public ZResource
	{
		friend class ZResourceSystem;
		friend class ZCModel;

	public:
		virtual ~ZRModel();

	protected:		
		explicit ZRModel(const std::wstring& modelFile);

		const std::vector<ZRMesh*>& getMeshResources() const {return mMeshResources;}
				
		virtual bool prepare() override;
		virtual bool loadData() override;
		virtual bool createObjects() override;
		virtual bool finish() override;

		std::wstring strFile;
		std::vector<ZRMesh*> mMeshResources;
	};
};