////////////////////////////////////////////////////////////////
// Name: ZResourcePack
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <string>
#include <unordered_map>

//////////////////////////////////////////////////// Definitions

namespace zen
{
	class ZResourceSystem;

	////////////////////////////////////////////////////////////////
	/// @class ZResourcePack
	/// @brief
	/// 
	class ZResourcePack 
	{
		friend class ZResourceSystem;
	public:
		explicit ZResourcePack(const std::wstring& packName);
		virtual ~ZResourcePack();

		void addTexture(const std::wstring& textureAlias, const std::wstring& textureFile);
		void addModel(const std::wstring& modelAlias, const std::wstring& modelFile);
		void addEffect(const std::wstring& effectAlias, const std::wstring& effectFile);

	private:
		std::wstring strPackName;
		std::unordered_map<std::wstring, std::wstring> mTextures;
		std::unordered_map<std::wstring, std::wstring> mModels;
		std::unordered_map<std::wstring, std::wstring> mEffects;
	};
};