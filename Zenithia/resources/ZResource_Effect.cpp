////////////////////////////////////////////////////////////////
// Name:  ZResource_Effect
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include "ZResource_Effect.h"
#include "../exceptions/ZException.h"
#include "../utils/ZAssert.h"
#include "../effects/ZEffect.h"
#include "../d3d9/ZEffectInclude.h"
#include "../utils/ZMemoryDebug.h"
#include <fstream>
#include <iostream>

//////////////////////////////////////////////////// Definitions
using namespace zen;

std::string g_strDefaultShader = "float4x4 matViewProjection : WorldViewProjection;"
	"float4 mainVS(float3 pos : POSITION) : POSITION { return mul(float4(pos.xyz, 1.0), matViewProjection); }"
	"float4 mainPS() : COLOR  { return float4(1.0, 1.0, 1.0, 1.0); }"
	"technique technique0 { pass p0 { VertexShader = compile vs_2_0 mainVS(); PixelShader = compile ps_2_0 mainPS(); } };";

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZResource_Effect::ZResource_Effect(const std::wstring& effectFile)
	: strFile(effectFile), mEffect(nullptr), bEmptyEffect(true)
{
	eType = ZResource::EResourceType::Resource_Effect;
	strDesc = L"effect." + effectFile;
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZResource_Effect::~ZResource_Effect()
{
	SAFE_RELEASE(mEffect);
}

bool ZResource_Effect::prepare()
{
	// Create empty shader
	mEffect = _createEffect(g_strDefaultShader);
	bEmptyEffect = true;
	return true;
}

bool ZResource_Effect::loadData()
{
	std::ifstream effect_stream(strFile, std::ios_base::in);
	if(effect_stream.fail()) {
		std::wostringstream ss;
		ss << L"ZResource_Effect failed to open file." << std::endl;
		ss << L"File: " << strFile;
		throw ZEXCEPTION_MESSAGE(ss.str());
	} else {
		std::string shader_data((std::istreambuf_iterator<char>(effect_stream)), std::istreambuf_iterator<char>());
		effect_stream.close();
		strShader = shader_data;
		return true;
	}
}

bool ZResource_Effect::createObjects()
{
	SAFE_RELEASE(mEffect);
	mEffect = _createEffect(strShader);
	strShader.clear();
	return true;
}

bool ZResource_Effect::finish()
{
	bEmptyEffect = false;
	return true;
}

const ID3DXEffect* ZResource_Effect::getEffect() const
{
	ZASSERT(mEffect != nullptr, L"[ZResource_Effect] ::getEffect() sanity check failed.");
	return mEffect;
}

////////////////////////////////////////////////////////////////
/// Load an effect from memory.
///
ID3DXEffect* ZResource_Effect::_createEffect(const std::string& effectCode)
{
	HRESULT hr;
	ID3DXEffect* effect = nullptr;
	ID3DXBuffer* error_buffer = nullptr;
	DWORD debug_flag = 0;
#ifdef _DEBUG
	debug_flag = D3DXSHADER_DEBUG;
#endif
	ZEffectInclude* d3d_include = znew(ZEffectInclude, (strFile));
	hr = D3DXCreateEffect(ZD3D9::device, effectCode.c_str(), effectCode.size(), nullptr, d3d_include, 
		debug_flag, nullptr, &effect, &error_buffer);
	if(error_buffer) {
		const char* error_message = static_cast<const char*>(error_buffer->GetBufferPointer());
		std::wclog << L"[ZResource_Effect] ::_createEffect() error message: "
			<< error_message<<std::endl;
		std::wstringstream ss;
		ss << L"ZResource_Effect::_createEffect() failed to create effect." << std::endl;
		ss << L"Error Message: " << std::endl << error_message;
		throw ZEXCEPTION_MESSAGE(ss.str());
	}
	if(FAILED(hr)) {
		std::wclog << L"[ZResource_Effect] ::_createEffect() failed to create effect." << std::endl;
		effect = nullptr;
	}
	zdelete(d3d_include);
	return effect;
}