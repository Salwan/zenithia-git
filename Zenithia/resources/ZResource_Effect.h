////////////////////////////////////////////////////////////////
// Name: ZResource_Effect
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZResource.h"
#include <string>

struct ID3DXEffect;

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZResourceSystem;
	class ZEffect;

	////////////////////////////////////////////////////////////////
	/// @class ZResource_ColladaModel
	/// @brief
	/// 
	class ZResource_Effect : public ZResource
	{
		friend class ZResourceSystem;
		friend class ZEffect;

	public:
		virtual ~ZResource_Effect();

	protected:
		explicit ZResource_Effect(const std::wstring& effectFile);
		
		virtual bool prepare() override;
		virtual bool loadData() override;
		virtual bool createObjects() override;
		virtual bool finish() override;
		virtual const ID3DXEffect* getEffect() const override;

		ID3DXEffect* _createEffect(const std::string& effectCode);
		bool _isEmptyEffect() const { return bEmptyEffect; }

		std::wstring strFile;
		std::string strShader;
		ID3DXEffect* mEffect;
		bool bEmptyEffect;
	};
};