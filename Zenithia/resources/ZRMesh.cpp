////////////////////////////////////////////////////////////////
// Name: ZRMesh
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include "ZRMesh.h"
#include "../ZDefs.h"
#include "../utils/ZMemoryDebug.h"
#include "../exceptions/ZException.h"
#include "../utils/ZAssert.h"
#include "../models/ZManualMesh.h"
#include "../d3d9/ZVertexDeclaration.h"
#include "../d3d9/ZVertexBuffer.h"
#include "../models/ZMaterial.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZRMesh::ZRMesh(const std::wstring& mesh_descriptor) : mMesh(nullptr), 
	bMeshInitialized(false)
{
	eType = ZResource::EResourceType::Resource_Mesh;
	strDesc = L"mesh." + mesh_descriptor;
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZRMesh::~ZRMesh()
{
	DELETE_OBJECT(mMesh);
}

void ZRMesh::_initMesh(ZMeshParser::SMeshNode* mesh_node, 
					   const std::unordered_map<std::wstring, SMaterialDesc*>& materials) 
{
	ZASSERT(mesh_node != nullptr, L"Mesh node is null");
	ZASSERT(mesh_node->subsets.size() >= 1 , L"[ZRMesh] ::initMesh() sanity check failed.");
	ZASSERT(!mesh_node->subsets[0]->vVertexDeclarations.empty(), L"[ZRMesh] ::initMesh() sanity check failed.");
	ZASSERT(mesh_node->subsets[0]->vertexData.pVertexData, L"[ZRMesh] ::initMesh() sanity check failed.");
	ZASSERT(mesh_node->subsets[0]->vertexData.uVertexCount > 0, L"[ZRMesh] ::initMesh() sanity check failed.");
	ZASSERT(!bMeshInitialized, L"[ZRMesh] ::initMesh() sanity check failed.");
	mMesh = znew(ZManualMesh, ());
	for(unsigned int i = 0; i < mesh_node->subsets.size(); ++i) {
		unsigned int subset = mMesh->createSubset();
		ZVertexDeclaration& vd = mMesh->getVertexDeclaration(subset);
		ZVertexBuffer& vb = mMesh->getVertexBuffer(subset);
		ZMaterial& mtrl = mMesh->getMaterial(subset);

		// Create vertex declaration
		for(auto& d : mesh_node->subsets[i]->vVertexDeclarations) {
			vd.pushElement(d.ucDeclarationType, d.ucDeclarationUsage, d.ucSet);
		}
		vd.pushEndElement();
	
		// Create vertex buffer
		vb.create(vd, mesh_node->subsets[i]->vertexData.uVertexCount);
		vb.writeVertices(0, mesh_node->subsets[i]->vertexData.pVertexData, mesh_node->subsets[i]->vertexData.uVertexCount);
	
		// Create materials
		mtrl.create(*(materials.at(mesh_node->subsets[i]->material.c_str())));
	}

	// Get transform
	matTransform = mesh_node->matTransform;

	bMeshInitialized = true;
}

ZMesh* ZRMesh::getMesh() const
{
	ZASSERT(mMesh != nullptr, L"Mesh not initialized");
	return mMesh;
}
