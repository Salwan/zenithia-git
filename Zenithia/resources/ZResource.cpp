////////////////////////////////////////////////////////////////
// Name: ZResource
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include "ZResource.h"
#include "../d3d9/ZD3D9.h"
#include "../models/ZMesh.h"
#include "../effects/ZEffect.h"
#include "../events/ZEvent_LoadingComplete.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

ZResource::ZResource() : bLoadingComplete(false),
	eType(EResourceType::Resource_Unknown), strDesc(L"None")
{

}

ZResource::ZResource(const ZResource& copy)
{
	eType = copy.eType;
}

ZResource& ZResource::operator=(const ZResource& rhs)
{
	eType = rhs.eType;
	return *this;
}

// Creates any necessary dummy data for the texture to be valid (Main Thread)
bool ZResource::prepare()
{
	return true;
}

// Load the resource data from file (Secondary Thread)
bool ZResource::loadData()
{
	return true;
}

// Create actual device objects (Main Thread)
bool ZResource::createObjects()
{
	return true;
}

// Resource loading finalization (Main Thread)
bool ZResource::finish()
{
	return true;
}

void ZResource::dispatchLoadingComplete()
{
	bLoadingComplete = true;
	ZEvent_LoadingComplete e;
	dispatchEvent(e);
}

//////////////////////////////////////////////////////////////
// 2D Texture
//
const IDirect3DTexture9* ZResource::get2DTexture() const
{
	throw ZEXCEPTION_MESSAGE(L"ZResource::get2DTexture() called on a none 2D texture resource.");
}

//////////////////////////////////////////////////////////////
// Model
//
const ZMesh* ZResource::getModel() const
{
	throw ZEXCEPTION_MESSAGE(L"ZResource::getModel() called on a none mesh resource.");
}

//////////////////////////////////////////////////////////////
// Mesh
//
const ZMesh* ZResource::getMesh() const
{
	throw ZEXCEPTION_MESSAGE(L"ZResource::getMesh() called on a none mesh resource.");
}

//////////////////////////////////////////////////////////////
// Effect
//
const ID3DXEffect* ZResource::getEffect() const
{
	throw ZEXCEPTION_MESSAGE(L"ZResource::getEffect() called on a none effect resource.");
}

