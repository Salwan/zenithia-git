////////////////////////////////////////////////////////////////
// Name: ZRColladaModel
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZResource.h"
#include "../d3d9/ZD3D9.h"
#include <string>

//////////////////////////////////////////////////// Definitions
namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @class ZRColladaModel
	/// @brief
	/// 
	class ZRColladaModel : public ZResource
	{
		friend class ZResourceSystem;

	public:
		virtual ~ZRColladaModel();

	protected:		
		explicit ZRColladaModel(const std::wstring& textureFile);
				
		virtual bool prepare() override;
		virtual bool loadData() override;
		virtual bool createObjects() override;
		virtual bool finish() override;

		std::wstring strFile;
	};
};