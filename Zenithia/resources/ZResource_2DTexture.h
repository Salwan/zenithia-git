////////////////////////////////////////////////////////////////
// Name: ZResouece_2DTexture
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZResource.h"
#include "../d3d9/ZD3D9.h"
#include <string>

//////////////////////////////////////////////////// Definitions
namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @class ZResouce_2DTexture
	/// @brief
	/// 
	class ZResource_2DTexture : public ZResource
	{
		friend class ZResourceSystem;

	public:
		virtual ~ZResource_2DTexture();

	protected:		
		explicit ZResource_2DTexture(const std::wstring& textureFile);
				
		virtual bool prepare() override;
		virtual bool loadData() override;
		virtual bool createObjects() override;
		virtual bool finish() override;
		virtual const IDirect3DTexture9* get2DTexture() const override { return mTexture; }

		std::wstring strFile;
		IDirect3DTexture9* mTexture;
		char* pReadBuffer;
		size_t sReadBufferSize;
	};
};