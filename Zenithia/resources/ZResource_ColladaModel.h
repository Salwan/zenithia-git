////////////////////////////////////////////////////////////////
// Name: ZResource_ColladaModel
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZResource.h"
#include <string>

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZMesh;
	class ZResourceSystem;
	class ZModel;
	class ZColladaMesh;

	////////////////////////////////////////////////////////////////
	/// @class ZResource_ColladaModel
	/// @brief
	/// 
	class ZResource_ColladaModel : public ZResource
	{
		friend class ZResourceSystem;

	public:
		virtual ~ZResource_ColladaModel();

	protected:
		explicit ZResource_ColladaModel(const std::wstring& modelFile);
		
		virtual bool prepare() override;
		virtual bool loadData() override;
		virtual bool createObjects() override;
		virtual bool finish() override;

		virtual const ZMesh* getModel() const override;

		std::wstring strFile;
		ZMesh* mMesh;
		ZColladaMesh* pTemporaryMesh;
	};
};