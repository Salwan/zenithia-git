////////////////////////////////////////////////////////////////
// Name: ZResouece_Texture
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include "ZResource_2DTexture.h"
#include "../exceptions/ZException.h"
#include "../utils/ZAssert.h"
#include <fstream>

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZResource_2DTexture::ZResource_2DTexture(const std::wstring& textureFile) 
	: strFile(textureFile), mTexture(nullptr), pReadBuffer(nullptr), sReadBufferSize(0)
{
	eType = ZResource::EResourceType::Resource_2DTexture;
	strDesc = L"texture." + textureFile;
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZResource_2DTexture::~ZResource_2DTexture()
{
	
}

bool ZResource_2DTexture::prepare()
{
	HRESULT hr = D3DXCreateTexture(ZD3D9::device, 32, 32, 1, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED, &mTexture);
	if(FAILED(hr)) {
		throw ZEXCEPTION_DXERROR(hr);
	}
	return true;
}

bool ZResource_2DTexture::loadData()
{
	std::ifstream ifs(strFile.c_str(), std::ifstream::in|std::ifstream::binary|std::ifstream::ate);
	if(ifs.fail()) {
		std::wostringstream ss;
		ss << L"ZResource_2DTexture failed to open file." << std::endl;
		ss << L"File: " << strFile;
		throw ZEXCEPTION_MESSAGE(ss.str());
	} else {
		sReadBufferSize = static_cast<size_t>(ifs.tellg());
		ifs.seekg(std::ifstream::beg);
		pReadBuffer = znew_array(char, sReadBufferSize);
		ifs.read(pReadBuffer, sReadBufferSize);
		ifs.close();
		return true;
	}
}

bool ZResource_2DTexture::createObjects()
{
	HRESULT hr = D3DXCreateTextureFromFileInMemory(ZD3D9::device, pReadBuffer, sReadBufferSize, &mTexture);
	if(FAILED(hr)) {
		throw ZEXCEPTION_DXERROR(hr);
	}
	zdelete_array(pReadBuffer);
	pReadBuffer = nullptr;
	sReadBufferSize = 0;
	return true;
}

bool ZResource_2DTexture::finish()
{
	return true;
}

