////////////////////////////////////////////////////////////////
// Name: ZResource
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <string>
#include "../events/ZEventDispatcher.h"
#include "../events/ZEvent_LoadingComplete.h"

//////////////////////////////////////////////////// Definitions
struct IDirect3DTexture9;
struct ID3DXEffect;

namespace zen
{
	class ZD3D9;
	class ZResourceSystem;
	class ZTexture;
	class ZConstant;
	class ZMesh;
	class ZModel;

	////////////////////////////////////////////////////////////////
	/// @class ZResource
	/// @brief
	/// 
	class ZResource : public ZEventDispatcher
	{
		friend class ZResourceSystem;
		friend class ZTexture;
		friend class ZConstant;
		friend class ZModel;
		friend class ZEffect;

	public:	
		enum class EResourceType
		{
			Resource_Unknown,
			Resource_2DTexture,
			Resource_Mesh,
			Resource_ColladaModel,
			Resource_Effect,
		};
		static const unsigned int EVENT_LOADING_COMPLETE = ZEvent_LoadingComplete::EVENT_LOADING_COMPLETE;

	public:
		ZResource();
		ZResource(const ZResource&);
		ZResource& operator=(const ZResource&);
		virtual ~ZResource() {}

		EResourceType getType() const {return eType;}
		const wchar_t* getDesc() const {return strDesc.c_str();}
		bool isLoadingComplete() const {return bLoadingComplete;}

	protected:
		// Creates any necessary dummy resources
		virtual bool prepare();
		// Load the resource data from file (Secondary Thread)
		virtual bool loadData();
		// Create actual device objects (Main Thread)
		virtual bool createObjects();
		// Resource loading finalization (Main Thread)
		virtual bool finish();

		void dispatchLoadingComplete();

		// Access calls for resources
		// These are overriden by the resource, you cant call them on ZResource
		virtual const IDirect3DTexture9* get2DTexture() const;
		virtual const ZMesh* getModel() const;
		virtual const ZMesh* getMesh() const;
		virtual const ID3DXEffect* getEffect() const;
		// Reasoning for why get2DTexture() is implemented like this:
		// I want user to be able to keep a reference and move around a ZResource without
		// having to know what's inside it. I also want various other functions of the 
		// framework to access the texture without having to dynamic_cast ZResource to
		// ZResource_2DTexture everytime which is a relatively expensive operation that
		// will repeat a lot.

		EResourceType eType;
		std::wstring strDesc;
		bool bLoadingComplete;
	};
};