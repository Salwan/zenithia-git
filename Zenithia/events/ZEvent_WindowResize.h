////////////////////////////////////////////////////////////////
// Name: ZEvent_WindowResize
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZEvent.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @class ZEvent_WindowResize
	/// @brief
	/// 
	class ZEvent_WindowResize : public ZEvent
	{
	public:
		static const unsigned int EVENT_WINDOW_RESIZE = 1;

		int iWidth;
		int iHeight;

	public:
		explicit ZEvent_WindowResize(int newWidth, int newHeight) 
			: ZEvent(EVENT_WINDOW_RESIZE), iWidth(newWidth), 
			  iHeight(newHeight) {}
		virtual ~ZEvent_WindowResize() {}

	private:
		ZEvent_WindowResize(const ZEvent_WindowResize&){}
		ZEvent_WindowResize& operator=(const ZEvent_WindowResize&){}

	};
};

