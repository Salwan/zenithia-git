////////////////////////////////////////////////////////////////
// Name: ZEventDispatcher
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZEventListener.h"
#include "ZEvent.h"
#include "../utils/ZHeapPtr.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @class ZEventDispatcher
	/// @brief
	/// 
	class ZEventDispatcher 
	{
	public:
		explicit ZEventDispatcher();
		virtual ~ZEventDispatcher();

		void addEventListener(unsigned int type, ZEventListener* listener, ZEventHandlerPtr handler);
		void addEventListener(unsigned int type, ZHeapPtr* listener, ZEventHandlerPtr handler);
		void removeEventListener(unsigned int type, ZEventListener* listener, ZEventHandlerPtr handler);
		void removeEventListener(unsigned int type, ZHeapPtr* listener, ZEventHandlerPtr handler);
		void dispatchEvent(const ZEvent& e);

	protected:
		struct PIMPL;
		PIMPL* pimpl;

	private:
		ZEventDispatcher(const ZEventDispatcher&){}
		ZEventDispatcher& operator=(const ZEventDispatcher&){}

	};
};

