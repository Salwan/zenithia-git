////////////////////////////////////////////////////////////////
// Name: ZEventListener
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZEvent;

	////////////////////////////////////////////////////////////////
	/// @class ZEventListener
	/// @brief
	/// 
	class ZEventListener 
	{
	public:
		explicit ZEventListener();
		virtual ~ZEventListener();

	private:
		ZEventListener(const ZEventListener&){}
		ZEventListener& operator=(const ZEventListener&){}

	};
	typedef void(ZEventListener::*ZEventHandlerPtr)(const ZEvent&);
};

#define EVENT_HANDLER_PTR(ptr) static_cast<zen::ZEventHandlerPtr>(ptr)
