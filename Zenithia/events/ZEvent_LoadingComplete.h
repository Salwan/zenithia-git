////////////////////////////////////////////////////////////////
// Name: ZEvent_LoadingComplete
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZEvent.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @class ZEvent_LoadingComplete
	/// @brief
	/// 
	class ZEvent_LoadingComplete : public ZEvent
	{
	public:
		static const unsigned int EVENT_LOADING_COMPLETE = 2;

	public:
		explicit ZEvent_LoadingComplete() 
			: ZEvent(EVENT_LOADING_COMPLETE) {}
		virtual ~ZEvent_LoadingComplete() {}

	private:
		ZEvent_LoadingComplete(const ZEvent_LoadingComplete&){}
		ZEvent_LoadingComplete& operator=(const ZEvent_LoadingComplete&){}

	};
};

