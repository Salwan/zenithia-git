////////////////////////////////////////////////////////////////
// Name: ZEventDispatcher
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include "ZEventDispatcher.h"
#include <map>
#include <list>
#include <vector>
#include "../utils/ZAssert.h"

/////////////////////////////////////////////////// Definitions
using namespace zen;

struct ZEventRecord
{
	ZEventRecord() : pListener(nullptr), pHandler(nullptr) {}
	ZEventRecord(ZEventListener* listener, ZEventHandlerPtr handler, ZHeapPtr* heapPtr = nullptr) 
		: pListener(listener), pHandler(handler), pHeap(heapPtr) {}
	ZEventListener* pListener;
	ZEventHandlerPtr pHandler;
	ZHeapPtr* pHeap;
};
typedef std::list<ZEventRecord> EventList;
typedef std::list<ZEventRecord>::iterator EventListIter;
typedef std::map<unsigned int, EventList> EventMap;
typedef std::map<unsigned int, EventList>::iterator EventMapIter;
struct ZEventDispatcher::PIMPL
{
	PIMPL() : pToDelete(nullptr) {}
	EventMap events;
	std::vector<EventListIter>* pToDelete;
};

////////////////////////////////////////////////////////////////
/// Default Constructor
///	
ZEventDispatcher::ZEventDispatcher()
{
	pimpl = znew(PIMPL, ());
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZEventDispatcher::~ZEventDispatcher()
{
	zdelete(pimpl);
}

////////////////////////////////////////////////////////////////
/// Registers a new event listener directly, object will be
/// responsible for removing the event when its destructed, 
/// otherwise the world is gonna end.
///
void ZEventDispatcher::addEventListener(unsigned int type, ZEventListener* listener, ZEventHandlerPtr handler)
{
	ZASSERT(listener && handler, L"ZEventDispatcher::addEventListener() given null event listener and/or event handler.");
	EventList& elist = pimpl->events[type];
	elist.push_back(ZEventRecord(listener, handler));
}

////////////////////////////////////////////////////////////////
/// Registers a new event listener.
///
void ZEventDispatcher::addEventListener(unsigned int type, ZHeapPtr* listener, 
	ZEventHandlerPtr handler)
{
	ZASSERT(listener && handler, L"ZEventDispatcher::addEventListener() given null heap pointer and/or event handler.");
	EventList& elist = pimpl->events[type];
	elist.push_back(ZEventRecord(listener->getPtr<ZEventListener>(), handler, listener));
}

////////////////////////////////////////////////////////////////
/// Removes an event listener stored directly
///
void ZEventDispatcher::removeEventListener(unsigned int type, ZEventListener* listener, 
	ZEventHandlerPtr handler)
{
	ZASSERT(listener && handler, L"ZEventDispatcher::removeEventListener() given null event listener and/or event handler.");
	EventList& elist = pimpl->events[type];
	if(elist.size() > 0)
	{
		for(EventListIter iter = elist.begin(); iter != elist.end(); ++iter)
		{
			if(!iter->pHeap && iter->pListener == listener && iter->pHandler == handler)
			{
				if(pimpl->pToDelete == nullptr) {
					elist.erase(iter);
				} else {
					pimpl->pToDelete->push_back(iter);
				}
				break;
			}
		}
	}
}

////////////////////////////////////////////////////////////////
/// Removes an event listener stored as heap pointer
///
void ZEventDispatcher::removeEventListener(unsigned int type, ZHeapPtr* listener,
	ZEventHandlerPtr handler)
{
	ZASSERT(listener && handler, L"ZEventDispatcher::removeEventListener() given null heap pointer and/or event handler.");
	EventList& elist = pimpl->events[type];
	if(elist.size() > 0)
	{
		for(EventListIter iter = elist.begin(); iter != elist.end(); ++iter)
		{
			if(iter->pHeap && iter->pHeap == listener && iter->pHandler == handler)
			{
				if(pimpl->pToDelete == nullptr) {
					elist.erase(iter);
				} else {
					pimpl->pToDelete->push_back(iter);
				}
				break;
			}
		}
	}
}

////////////////////////////////////////////////////////////////
/// Dispatches an event
///
void ZEventDispatcher::dispatchEvent(const ZEvent& e)
{
	std::vector<EventListIter> to_delete;
	EventList& elist = pimpl->events[e.getType()];
	pimpl->pToDelete = &to_delete;
	if(elist.size() > 0)
	{
		for(EventListIter iter = elist.begin(); iter != elist.end(); ++iter)
		{
			if(iter->pHeap)
			{
				// stored using heap pointer
				if(iter->pHeap->isValid())
				{
					(iter->pListener->*(iter->pHandler))(e);
				}
				else
				{
					to_delete.push_back(iter);
				}
			}
			else
			{
				// stored directly
				(iter->pListener->*(iter->pHandler))(e);
			}
		}
		if(to_delete.size() > 0)
		{
			for(unsigned int i = 0; i < to_delete.size(); ++i)
			{
				elist.erase(to_delete[i]);
			}
		}
	}
	pimpl->pToDelete = nullptr;
}
