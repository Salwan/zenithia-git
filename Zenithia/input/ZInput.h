////////////////////////////////////////////////////////////////
// Name: ZInput
// Desc: Zenithia input system
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

#define DIRECTINPUT_VERSION 0x800

/////////////////////////////////////////////////////// Includes
#include <string>
#include <hash_map>
#include <dinput.h>

namespace zen
{

	//////////////////////////////////////////////////// Definitions

	enum ZInputState
	{
		ZINPUT_NONE = 0,
		ZINPUT_HIT,
		ZINPUT_HELD,
		ZINPUT_RELEASED,
	};

	////////////////////////////////////////////////////////////////
	/// @class ZInput
	/// @brief Zenithia input system consists of 3 layers:
	/// - Acquire Layer: getting input information from hardware
	/// - Processing Layer: processing input to turn it into useful data
	/// - Dispatch Layer: dispatching the high level input events
	/// 
	class ZInput 
	{
	public:
		virtual ~ZInput();
	
		void process();
		void mapKeyToAction(unsigned int actionId, unsigned int key, ZInputState keyState);
		bool getActionState(unsigned int actionId);
		void clear();

	private:	
		ZInput();
		ZInput(const ZInput&){}
		ZInput& operator=(const ZInput&){}

		void create();
		void initKeyboard();
		void initMouse();
		void processKeyboard();
		void processMouse();

	private:
		struct _KeyboardAction
		{
			unsigned int key;
			ZInputState key_state;
		};

		IDirectInput8* mInput;
		IDirectInputDevice8* mKeyboard;
		IDirectInputDevice8* mMouse;
		HANDLE hMouseEvent;

		// Holds pairs of: action id, list of keyboard actions
		std::hash_map<unsigned int, std::vector<_KeyboardAction> > mActionMap;
		std::hash_map<unsigned int, std::vector<_KeyboardAction> >::iterator mActionMapIter;

		// Holds pairs of: key code, current input state for key
		std::hash_map<unsigned int, ZInputState> mKeyMap;
		std::hash_map<unsigned int, ZInputState>::iterator mKeyMapIter;

		// Holds pairs of: action id, whether action is true or not
		std::hash_map<unsigned int, bool> mStateMap;
		std::hash_map<unsigned int, bool>::iterator mStateMapIter;

	public:
		static ZInput* instance();
		static void destroy();

	private:
		static ZInput* mInstance;
	};
};
