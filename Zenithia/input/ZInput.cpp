////////////////////////////////////////////////////////////////
// Name: ZInput
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////// Includes
#include "stdafx.h"
#include <Windows.h>
#include "../input/ZInput.h"
#include "../d3d9/ZD3D9.h"
#include "../utils/ZUtils.h"
#include "../exceptions/ZException.h"
#include "../utils/ZAssert.h"

using namespace zen;

//////////////////////////////////////////////////// Definitions
ZInput* ZInput::mInstance = nullptr;
const unsigned int MouseBufferSize = 16;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZInput::ZInput() : mInput(nullptr), mKeyboard(nullptr)
{
	
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZInput::~ZInput()
{	
	clear();
	if(mKeyboard) mKeyboard->Unacquire();
	if(mMouse) mMouse->Unacquire();
	SAFE_RELEASE(mMouse);
	SAFE_RELEASE(mKeyboard);
	SAFE_RELEASE(mInput);
}

ZInput* ZInput::instance()
{
	if(!mInstance)
	{
		mInstance = znew(ZInput, ());
		mInstance->create();
	}
	return mInstance;
}

void ZInput::destroy()
{
	if(mInstance) {
		zdelete(mInstance);
		mInstance = nullptr;
	}
}

void ZInput::create()
{
	HRESULT hr;

	// DirectInput8
	// - Creating direct input object
	hr = DirectInput8Create(::GetModuleHandle(NULL), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&mInput, NULL);
	if(FAILED(hr))
	{
		throw ZEXCEPTION_MESSAGE(L"ZInput::create() couldn't create DirectInput8 object");
	}

	initKeyboard();
	initMouse();
}

void ZInput::initKeyboard()
{
	HRESULT hr;
	// - Creating direct input keyboard object
	hr = mInput->CreateDevice(GUID_SysKeyboard, &mKeyboard, NULL);
	if(FAILED(hr))
	{
		throw ZEXCEPTION_MESSAGE(L"ZInput::initKeyboard() couldn't create DirectInput8 Keyboard object");
	}
	// - Setting keyboard data format
	hr = mKeyboard->SetDataFormat(&c_dfDIKeyboard);
	if(FAILED(hr))
	{
		throw ZEXCEPTION_MESSAGE(L"ZInput::initKeyboard() couldn't set keyboard data format");
	}
	// - Setting keyboard cooperative level
	hr = mKeyboard->SetCooperativeLevel(ZD3D9::getWindowHandle(), DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if(FAILED(hr))
	{
		throw ZEXCEPTION_MESSAGE(L"ZInput::initKeyboard() couldn't set keyboard cooperative level");
	}
	// - Acquiring keyboard
	mKeyboard->Acquire();
}

void ZInput::initMouse()
{
	HRESULT hr;
	// - Creating direct input mouse object
	hr = mInput->CreateDevice(GUID_SysMouse, &mMouse, NULL);
	if(FAILED(hr))
	{
		throw ZEXCEPTION_MESSAGE(L"ZInput::initMouse() couldn't create DirectInput8 mouse object");
	}
	// - Setting mouse data format
	hr = mMouse->SetDataFormat(&c_dfDIMouse);
	if(FAILED(hr))
	{
		throw ZEXCEPTION_MESSAGE(L"ZInput::initMouse() couldn't set mouse data format");
	}
	// - Setting mouse cooperative level
	hr = mMouse->SetCooperativeLevel(ZD3D9::getWindowHandle(), DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if(FAILED(hr))
	{
		throw ZEXCEPTION_MESSAGE(L"ZInput::initMouse() couldn't set mouse cooperative level");
	}
	// - Setup buffered event
	hMouseEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	if(hMouseEvent == NULL)
	{
		throw ZEXCEPTION_MESSAGE(L"ZInput::initMouse() couldn't create buffering event");
	}
	hr = mMouse->SetEventNotification(hMouseEvent);
	if(FAILED(hr))
	{
		throw ZEXCEPTION_MESSAGE(L"ZInput::initMouse() couldn't set event notification");
	}
	DIPROPDWORD dipdw;
	dipdw.diph.dwSize = sizeof(DIPROPDWORD);
	dipdw.diph.dwHeaderSize = sizeof(DIPROPHEADER);
	dipdw.diph.dwObj = 0;
	dipdw.diph.dwHow = DIPH_DEVICE;
	dipdw.dwData = MouseBufferSize;
	hr = mMouse->SetProperty(DIPROP_BUFFERSIZE, &dipdw.diph);
	if(FAILED(hr))
	{
		throw ZEXCEPTION_MESSAGE(L"ZInput::initMouse() couldn't set buffer size");
	}
	// - Acquiring mouse
	mMouse->Acquire();
}

void ZInput::process()
{
	processKeyboard();
	processMouse();
}

void ZInput::processKeyboard()
{
	ZASSERT(mKeyboard != NULL, L"ZInput::processKeyboard() called while keyboard device is NULL");
	unsigned char kbuffer [256];
	HRESULT hr;

	// Acquire keyboard status
	hr = mKeyboard->GetDeviceState(sizeof(kbuffer), (LPVOID)kbuffer);
	if(FAILED(hr))
	{
		if(hr == DIERR_INPUTLOST || hr == DIERR_NOTACQUIRED)
		{
			// Attempt to reaquire
			mKeyboard->Acquire();
			return;
		}
		else
		{
			throw ZEXCEPTION_MESSAGE(L"ZInput::processKeyboard() couldn't retrieve keyboard status");
		}
	}

	// Process keyboard input information into key states
	for(mKeyMapIter = mKeyMap.begin(); mKeyMapIter != mKeyMap.end(); ++mKeyMapIter)
	{
		unsigned int key = mKeyMapIter->first;
		ZInputState current_state = mKeyMapIter->second;
		if(kbuffer[key] & 0x80) // Key is down
		{
			switch(current_state)
			{
			case ZINPUT_NONE:
			case ZINPUT_RELEASED:
				mKeyMapIter->second = ZINPUT_HIT;
				break;

			case ZINPUT_HIT:
				mKeyMapIter->second = ZINPUT_HELD;
				break;
			}
		}
		else // Key is up
		{
			switch(current_state)
			{
			case ZINPUT_RELEASED:
				mKeyMapIter->second = ZINPUT_NONE;
				break;

			case ZINPUT_HIT:
			case ZINPUT_HELD:
				mKeyMapIter->second = ZINPUT_RELEASED;
				break;
			}
		}
	}

	// Dispatch keyboard actions, figure out which actions are true
	for(mActionMapIter = mActionMap.begin(); mActionMapIter != mActionMap.end(); ++mActionMapIter)
	{
		std::vector<_KeyboardAction>& ka = mActionMapIter->second;
		mStateMap[mActionMapIter->first] = false;
		for(unsigned int i = 0; i < ka.size(); ++i)
		{
			if(mKeyMap[ka[i].key] == ka[i].key_state)
			{
				mStateMap[mActionMapIter->first] = true;
				break;
			}
		}
	}
}

void ZInput::processMouse()
{
	ZASSERT(mMouse != NULL, L"ZInput::processMouse() called while mouse device is NULL");
	HRESULT hr;
	bool done = false;
	while(!done)
	{
		DIDEVICEOBJECTDATA od;
		DWORD item_count = 1;
		hr = mMouse->GetDeviceData(sizeof(DIDEVICEOBJECTDATA), &od, &item_count, 0);
		if(FAILED(hr))
		{
			if(hr == DIERR_INPUTLOST || hr == DIERR_NOTACQUIRED)
			{
				// Try to reacquire
				mMouse->Acquire();
				break;
			}
			else
			{
				throw ZEXCEPTION_MESSAGE(L"ZInput::processMouse() couldn't retrieve mouse data");
			}
		}
		if(item_count == 0)
		{
			break;
		}

		switch(od.dwOfs)
		{
		case DIMOFS_X:
			break;

		case DIMOFS_Y:
			break;

		case DIMOFS_Z:
			break;

		case DIMOFS_BUTTON0:
			if(od.dwData & 0x80)
			{
				// Left button down
			}
			else
			{
				// Right button up
			}
			break;

		case DIMOFS_BUTTON1:
			if(od.dwData & 0x80)
			{
				// Right button down
			}
			else
			{
				// Right button up
			}
			break;

		case DIMOFS_BUTTON2:
			if(od.dwData & 0x80)
			{
				// Middle button down
			}
			else
			{
				// Middle button up
			}
		}
	}
}

void ZInput::mapKeyToAction(unsigned int actionId, unsigned int key, ZInputState keyState)
{
	_KeyboardAction action;
	action.key = key;
	action.key_state = keyState;

	if(mActionMap.find(actionId) == mActionMap.end())
	{
		std::vector<_KeyboardAction> new_action;
		new_action.push_back(action);
		mActionMap.insert(std::pair<unsigned int, std::vector<_KeyboardAction> >(actionId, new_action));
	}
	else
	{
		mActionMap[actionId].push_back(action);		
	}

	if(mKeyMap.find(key) == mKeyMap.end())
	{
		mKeyMap.insert(std::pair<unsigned int, ZInputState>(key, ZINPUT_NONE));
	}

	if(mStateMap.find(actionId) == mStateMap.end())
	{
		mStateMap.insert(std::pair<unsigned int, bool>(actionId, false));
	}
}

bool ZInput::getActionState(unsigned int action_id)
{
#ifdef _DEBUG
	ZASSERT(mActionMap.find(action_id) != mActionMap.end(), L"ZInput::getActionState() couldn't find given action id");
#endif
	return mStateMap[action_id];
}

void ZInput::clear()
{
	mActionMap.clear();
	mKeyMap.clear();
	mStateMap.clear();
}