////////////////////////////////////////////////////////////////
// Name: ZEffect
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <string>
#include <vector>
#include <map>
#include <d3d9.h>
#include <d3dx9.h>
#include "../utils/ZAssert.h"
#include "../utils/ZHeapPtr.h"
#include "ZConstant.h"
#include "../events/ZEventListener.h"

//////////////////////////////////////////////////// Definitions
struct ID3DXEffect;

namespace zen
{
	class ZResource;

	////////////////////////////////////////////////////////////////
	/// @class ZEffect
	/// @brief
	/// 
	class ZEffect : protected ZEventListener
	{
	public:
		explicit ZEffect(ZResource* resource = nullptr);
		virtual ~ZEffect();

		ZConstant& ZEffect::operator()(const char* name);
		
		void setEffect(ZResource* effectResource);
		void beginTechnique();
		bool nextPass();
		void endTechnique();

		// Accessors
		const std::string& getTechniqueName() const {return strCompatibleTechniqueName;}
		unsigned int getPassCount() const {return uPassCount;}

	private:
		ZEffect(const ZEffect&){}
		ZEffect& operator=(const ZEffect&){}

		void _initEffect(ID3DXEffect* effect);
		void _onResourceLoadingComplete(const ZEvent&);
		
		typedef std::map<const char*, ZConstant*> TConstantMap;
		typedef std::map<const char*, ZConstant*>::iterator ConstantMapIter;
		TConstantMap mConstants;

		bool bIgnoreErrors;
		ZConstant mDummyConstant;

		ZResource* mResource;
		ID3DXEffect* mEffect;

		int iChosenTechnique;
		unsigned int uPassCount;
		D3DXHANDLE mCompatibleTechnique;
		std::string strCompatibleTechniqueName;
		bool bTechniqueActive;
		unsigned int uCurrentPass;
	};
};
