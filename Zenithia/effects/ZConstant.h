////////////////////////////////////////////////////////////////
// Name: ZConstant
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <d3d9.h>
#include <d3dx9.h>
#include "../d3d9/ZTexture.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZResource;

	////////////////////////////////////////////////////////////////
	/// @class ZConstant
	/// @brief
	/// 
	class ZConstant 
	{
		friend class ZEffect;

	public:
		virtual ~ZConstant();

		// Read constants
		operator bool() const;
		operator float() const;
		operator int() const;
		operator XMFLOAT2() const;
		operator XMFLOAT3() const;
		operator XMFLOAT4() const;
		operator XMVECTOR() const;
		operator XMFLOAT4X4() const;
		operator XMMATRIX() const;
		operator LPDIRECT3DBASETEXTURE9() const;
		operator ZTexture*() const;

		// Write constants
		ZConstant& operator=(float data);
		ZConstant& operator=(int data);
		ZConstant& operator=(const XMFLOAT2& data);
		ZConstant& operator=(const XMFLOAT3& data);
		ZConstant& operator=(const XMFLOAT4& data);
		ZConstant& operator=(const XMVECTOR& data);
		ZConstant& operator=(const XMFLOAT4X4& data);
		ZConstant& operator=(const XMMATRIX& data);
		ZConstant& operator=(bool data);
		ZConstant& operator=(LPDIRECT3DBASETEXTURE9 data);
		ZConstant& operator=(const ZTexture* data);
		ZConstant& operator=(const ZResource* data);

	private:
		ZConstant();
		ZConstant(ID3DXEffect* effect, D3DXHANDLE handle, void* data, size_t size);
		ZConstant(const ZConstant&) {}
		ZConstant& operator=(const ZConstant&) {}

		ID3DXEffect* mEffect;
		D3DXHANDLE mHandle;
		void* mData;
		size_t mSize;
		bool bEmpty;
	};
};

