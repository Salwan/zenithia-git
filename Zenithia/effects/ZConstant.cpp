////////////////////////////////////////////////////////////////
// Name: ZConstant
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include "ZConstant.h"
#include "../utils/ZAssert.h"
#include "../resources/ZResource.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
// Empty constant constructor
//
ZConstant::ZConstant() : bEmpty(true)
{

}

////////////////////////////////////////////////////////////////
// Default constructor
//
ZConstant::ZConstant(ID3DXEffect* effect, D3DXHANDLE handle, void* data, size_t size) 
	: mEffect(effect), mHandle(handle), mData(data), mSize(size), bEmpty(false)
{

}

////////////////////////////////////////////////////////////////
// Destructor
//
ZConstant::~ZConstant()
{
	if(!bEmpty)	{
		zdelete_array(mData);
		mData = nullptr;
	}
}

/////////////////////////////////////////////////////
// Read data
/////////////////////////////////////////////////////
ZConstant::operator bool() const
{
	if(bEmpty) return false;
	ZASSERT(mSize >= sizeof(bool), L"[ZConstant] bool() is called but data size is invalid.");
	return *(static_cast<bool*>(mData));
}

ZConstant::operator float() const
{
	if(bEmpty) return 0.0f;
	ZASSERT(mSize >= sizeof(float), L"[ZConstant] float() is called but data size is invalid.");
	return *(static_cast<float*>(mData));
}

ZConstant::operator int() const
{
	if(bEmpty) return 0;
	ZASSERT(mSize >= sizeof(int), L"[ZConstant] int() is called but data size is invalid.");
	return *(static_cast<int*>(mData));
}

ZConstant::operator XMFLOAT2() const
{
	if(bEmpty) return XMFLOAT2();
	ZASSERT(mSize >= sizeof(XMFLOAT2), L"[ZConstant] XMFLOAT2() is called but data size is invalid.");
	return *(static_cast<XMFLOAT2*>(mData));
}

ZConstant::operator XMFLOAT3() const
{
	if(bEmpty) return XMFLOAT3();
	ZASSERT(mSize >= sizeof(XMFLOAT3), L"[ZConstant] XMFLOAT3() is called but data size is invalid.");
	return *(static_cast<XMFLOAT3*>(mData));
}

ZConstant::operator XMFLOAT4() const
{
	if(bEmpty) return XMFLOAT4();
	ZASSERT(mSize >= sizeof(XMFLOAT4), L"[ZConstant] XMFLOAT4() is called but data size is invalid.");
	return *(static_cast<XMFLOAT4*>(mData));
}

ZConstant::operator XMVECTOR() const
{
	if(bEmpty) return XMVECTOR();
	ZASSERT(mSize >= sizeof(XMVECTOR), L"[ZConstant] XMVECTOR() is called but data size is invalid.");
	const XMFLOAT4& vector_out = *(static_cast<XMFLOAT4*>(mData));
	return XMLoadFloat4(&vector_out);
}

ZConstant::operator XMFLOAT4X4() const
{
	if(bEmpty) return XMFLOAT4X4();
	ZASSERT(mSize >= sizeof(XMFLOAT4X4), L"[ZConstant] XMFLOAT4X4() is called but data size is invalid.");
	return *(static_cast<XMFLOAT4X4*>(mData));
}

ZConstant::operator XMMATRIX() const
{
	if(bEmpty) return XMMATRIX();
	ZASSERT(mSize >= sizeof(XMFLOAT4X4), L"[ZConstant] XMMATRIX() is called but data size is invalid.");
	XMFLOAT4X4 matrix_out = *(static_cast<XMFLOAT4X4*>(mData));
	return XMLoadFloat4x4(&matrix_out);
}

ZConstant::operator LPDIRECT3DBASETEXTURE9() const
{
	if(bEmpty) return NULL;
	ZASSERT(mSize >= sizeof(LPDIRECT3DBASETEXTURE9), L"[ZConstant] LPDIRECT3DBASETEXTURE9() is called but data size is invalid.");
	// The pointer is stored in memory pointed to by mData
	return reinterpret_cast<LPDIRECT3DBASETEXTURE9>(*(static_cast<intptr_t*>(mData)));
}

ZConstant::operator ZTexture*() const
{
	if(bEmpty) return NULL;
	ZASSERT(mSize >= sizeof(ZTexture*), L"[ZConstant] ZTexture*() is called but data size is invalid.");
	// The pointer is stored in memory pointed to by mData
	return reinterpret_cast<ZTexture*>(*(static_cast<intptr_t*>(mData)));
}

//////////////////////////////////////////////////////
// Write Data
//////////////////////////////////////////////////////
ZConstant& ZConstant::operator=(bool data)
{
	if(bEmpty) return *this;
	ZASSERT(mSize >= sizeof(bool), L"[ZConstant] operator=(bool) is called but data size is invalid.");
	*(static_cast<float*>(mData)) = data;
	mEffect->SetBool(mHandle, data);
	return *this;
}

ZConstant& ZConstant::operator=(float data)
{
	if(bEmpty) return *this;
	ZASSERT(mSize >= sizeof(float), L"[ZConstant] operator=(float) is called but data size is invalid.");
	*(static_cast<float*>(mData)) = data;
	mEffect->SetFloat(mHandle, data);
	return *this;
}

ZConstant& ZConstant::operator=(int data)
{
	if(bEmpty) return *this;
	ZASSERT(mSize >= sizeof(int), L"[ZConstant] operator=(int) is called but data size is invalid.");
	*(static_cast<int*>(mData)) = data;
	mEffect->SetInt(mHandle, data);
	return *this;
}

ZConstant& ZConstant::operator=(const XMFLOAT2& data)
{
	if(bEmpty) return *this;
	ZASSERT(mSize >= sizeof(XMFLOAT2), L"[ZConstant] operator=(XMFLOAT2) is called but data size is invalid.");
	*(static_cast<XMFLOAT2*>(mData)) = data;
	mEffect->SetFloatArray(mHandle, &data.x, 2);
	return *this;
}

ZConstant& ZConstant::operator=(const XMFLOAT3& data)
{
	if(bEmpty) return *this;
	ZASSERT(mSize >= sizeof(XMFLOAT3), L"[ZConstant] operator=(XMFLOAT3) is called but data size is invalid.");
	*(static_cast<XMFLOAT3*>(mData)) = data;
	mEffect->SetFloatArray(mHandle, &data.x, 3);
	return *this;
}

ZConstant& ZConstant::operator=(const XMFLOAT4& data)
{
	if(bEmpty) return *this;
	ZASSERT(mSize >= sizeof(XMFLOAT4), L"[ZConstant] operator=(XMFLOAT4) is called but data size is invalid.");
	*(static_cast<XMFLOAT4*>(mData)) = data;
	mEffect->SetVector(mHandle, reinterpret_cast<const D3DXVECTOR4*>(&data));
	return *this;
}

ZConstant& ZConstant::operator=(const XMVECTOR& data)
{
	if(bEmpty) return *this;
	ZASSERT(mSize >= sizeof(XMVECTOR), L"[ZConstant] operator=(XMVECTOR) is called but data size is invalid.");
	XMFLOAT4 vector_in;
	XMStoreFloat4(&vector_in, data);
	*(static_cast<XMFLOAT4*>(mData)) = vector_in;
	mEffect->SetVector(mHandle, reinterpret_cast<const D3DXVECTOR4*>(&vector_in));
	return *this;
}

ZConstant& ZConstant::operator=(const XMFLOAT4X4& data)
{
	if(bEmpty) return *this;
	ZASSERT(mSize >= sizeof(XMFLOAT4X4), L"[ZConstant] operator=(XMFLOAT4X4) is called but data size is invalid.");
	*(static_cast<XMFLOAT4X4*>(mData)) = data;
	mEffect->SetMatrix(mHandle, reinterpret_cast<const D3DXMATRIX*>(&data));
	return *this;
}

ZConstant& ZConstant::operator=(const XMMATRIX& data)
{
	if(bEmpty) return *this;
	ZASSERT(mSize >= sizeof(XMFLOAT4X4), L"[ZConstant] operator=(XMMATRIX) is called but data size is invalid.");
	XMFLOAT4X4 matrix_in;
	XMStoreFloat4x4(&matrix_in, data);
	*(static_cast<XMFLOAT4X4*>(mData)) = matrix_in;
	mEffect->SetMatrix(mHandle, reinterpret_cast<const D3DXMATRIX*>(&matrix_in));
	return *this;
}

ZConstant& ZConstant::operator=(LPDIRECT3DBASETEXTURE9 data)
{
	if(bEmpty) return *this;
	ZASSERT(mSize >= sizeof(LPDIRECT3DBASETEXTURE9), L"[ZConstant] operator=(LPDIRECT3DBASETEXTURE9) is called but data size is invalid.");
	// The pointer is stored in memory pointed to by mData
	*(static_cast<intptr_t*>(mData)) = reinterpret_cast<intptr_t>(data);
	mEffect->SetTexture(mHandle, data);
	return *this;
}

ZConstant& ZConstant::operator=(const ZTexture* data)
{
	if(bEmpty) return *this;
	ZASSERT(mSize >= sizeof(ZTexture*), L"[ZConstant] operator=(ZTexture) is called but data size is invalid.");
	// The pointer is stored in memory pointed to by mData
	*(static_cast<intptr_t*>(mData)) = reinterpret_cast<intptr_t>(data);
	mEffect->SetTexture(mHandle, data->mTexture);
	return *this;	
}

ZConstant& ZConstant::operator=(const ZResource* data)
{
	if(bEmpty) return *this;
	const IDirect3DTexture9* texture = data->get2DTexture();
	ZASSERT(mSize >= sizeof(IDirect3DTexture9*), L"[ZConstant] operator=(ZTexture) is called but data size is invalid.");
	// The pointer is stored in memory pointed to by mData
	*(static_cast<intptr_t*>(mData)) = reinterpret_cast<intptr_t>(texture);
	mEffect->SetTexture(mHandle, const_cast<IDirect3DTexture9*>(texture));
	return *this;
}

