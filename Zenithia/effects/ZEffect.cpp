////////////////////////////////////////////////////////////////
// Name: ZEffect
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "stdafx.h"
#include "ZEffect.h"
#include <iostream>
#include <fstream>
#include "../utils/ZUtils.h"
#include "../d3d9/ZD3D9.h"
#include "../utils/ZAssert.h"
#include "../resources/ZResource_Effect.h"
#include "../events/ZEvent_LoadingComplete.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZEffect::ZEffect(ZResource* resource) 
	: mResource(nullptr), mEffect(nullptr), iChosenTechnique(-1),
		mCompatibleTechnique(nullptr), uPassCount(0), uCurrentPass(0),
		strCompatibleTechniqueName(""), bTechniqueActive(false),
		bIgnoreErrors(false)
{
	if(resource != nullptr) {
		setEffect(resource);
	}
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZEffect::~ZEffect()
{
	if(!mConstants.empty()) {
		for(ConstantMapIter iter = mConstants.begin(); iter != mConstants.end(); ++iter) {
			DELETE_OBJECT(iter->second);
		}
	}
	mConstants.clear();
}

///////////////////////////////////////////////////////////////
// Sets a new effect and initializes it
//
void ZEffect::setEffect(ZResource* effectResource)
{
	ZASSERT(effectResource != nullptr && effectResource->getType() == ZResource::EResourceType::Resource_Effect, 
			L"[ZEffect] ::ZEffect() given a resource that is not an effect.");
	if(mResource) {
		mResource->removeEventListener(ZEvent_LoadingComplete::EVENT_LOADING_COMPLETE, this,
			EVENT_HANDLER_PTR(&ZEffect::_onResourceLoadingComplete));
	}
	mResource = effectResource;
	mResource->addEventListener(ZEvent_LoadingComplete::EVENT_LOADING_COMPLETE, this,
		EVENT_HANDLER_PTR(&ZEffect::_onResourceLoadingComplete));
	if(mEffect) {
		if(bTechniqueActive) {
			mEffect->End();
		}
	}
	mEffect = const_cast<ID3DXEffect*>(mResource->getEffect());
	_initEffect(mEffect);
}

////////////////////////////////////////////////////////////////
/// Load an effect from memory.
///
void ZEffect::_initEffect(ID3DXEffect* effect)
{
	if(!mConstants.empty()) {
		for(ConstantMapIter iter = mConstants.begin(); iter != mConstants.end(); ++iter) {
			DELETE_OBJECT(iter->second);
		}
	}
	mConstants.clear();
	mCompatibleTechnique = nullptr;
	uPassCount = 0;
	strCompatibleTechniqueName = "";
	bTechniqueActive = false;
	uCurrentPass = 0;

	// If empty effect, disable errors
	if(reinterpret_cast<ZResource_Effect*>(mResource)->_isEmptyEffect()) {
		bIgnoreErrors = true;
	} else {
		bIgnoreErrors = false;
	}

	// Find the first compatible technique
	HRESULT hr;
	D3DXHANDLE tech;
	unsigned int next_technique = 0;
	iChosenTechnique = -1;
	while((tech = effect->GetTechnique(next_technique)) != 0) {
		hr = effect->ValidateTechnique(tech);
		if(SUCCEEDED(hr)) {
			iChosenTechnique = next_technique;
			break;
		} else {
			next_technique += 1;
			continue;
		}
	}
	if(iChosenTechnique == -1) {
		throw ZEXCEPTION_MESSAGE(L"[ZEffect] ::_initEffect() could not find a compatible technique!");
	}
	mCompatibleTechnique = effect->GetTechnique(iChosenTechnique);
	D3DXTECHNIQUE_DESC tech_desc;
	effect->GetTechniqueDesc(mCompatibleTechnique, &tech_desc);
	uPassCount = tech_desc.Passes;
	strCompatibleTechniqueName = tech_desc.Name;
}

void ZEffect::_onResourceLoadingComplete(const ZEvent&)
{
	mResource->removeEventListener(ZEvent_LoadingComplete::EVENT_LOADING_COMPLETE, this,
		EVENT_HANDLER_PTR(&ZEffect::_onResourceLoadingComplete));
	// Reinit effect
	mEffect = const_cast<ID3DXEffect*>(mResource->getEffect());
	_initEffect(mEffect);
}

///////////////////////////////////////////////////////////////
// Begin techniques
//
void ZEffect::beginTechnique()
{
	ZASSERT(mEffect, L"[ZEffect] beginTechnique() no effect is initialized.");
	ZASSERT(!bTechniqueActive, L"[ZEffect] beginTechnique() technique is already activated.");
	D3DXHANDLE tech = mEffect->GetTechnique(iChosenTechnique);
	ZASSERT(tech && uPassCount > 0, L"[ZEffect] beginTechnique() invalid technique chosen.");
	mEffect->SetTechnique(tech);
	mEffect->Begin(NULL, 0);
	bTechniqueActive = true;
	uCurrentPass = 0;
}

bool ZEffect::nextPass()
{
	ZASSERT(mEffect, L"[ZEffect] nextTechnique() no effect is initialized.");
	ZASSERT(bTechniqueActive, L"[ZEffect] nextPass() technique is deactivated.");
	HRESULT hr;
	if(uCurrentPass < uPassCount) {
		if(uCurrentPass > 0) { // Not first pass? deactivate previous pass
			mEffect->EndPass();
		}
		hr = mEffect->BeginPass(uCurrentPass);
		if(FAILED(hr)) {
			std::wstringstream ss;
			ss << L"ZEffect::nextPass() failed to activate pass, perhaps its already active? Effect(" << mResource->getDesc() << L"/" << strCompatibleTechniqueName.c_str() << L")" << std::endl;
			throw ZEXCEPTION_MESSAGE(ss.str());
		}
		uCurrentPass += 1;
		return true;
	} else {
		return false;
	}
}

void ZEffect::endTechnique()
{
	ZASSERT(mEffect, L"[ZEffect] beginTechnique() no effect is initialized.");
	ZASSERT(bTechniqueActive, L"[ZEffect] endTechnique() technique is deactivated.");
	if(bTechniqueActive) {
		bTechniqueActive = false;
		mEffect->EndPass();
		mEffect->End();
	}
}

///////////////////////////////////////////////////////////////
// Get constant value
//
ZConstant& ZEffect::operator()(const char* name)
{
	ZASSERT(mEffect, L"[ZEffect] () cannot retrieve constant if effect is not initialized first.");
	TConstantMap::const_iterator search = mConstants.find(name);
	if(search != mConstants.end()) {
		// Its buffered
		return *(search->second);
	} else {
		D3DXHANDLE handle = mEffect->GetParameterByName(NULL, name);
		if(handle) {
			D3DXPARAMETER_DESC param_desc;
			mEffect->GetParameterDesc(handle, &param_desc);
			unsigned int size = param_desc.Bytes;
			void* p_data = static_cast<void*>(znew_array(char, size));
			mEffect->GetValue(handle, p_data, size);
			if(!p_data || size == 0) {
				if(bIgnoreErrors) return mDummyConstant;
				std::wstringstream ss;
				ss << L"ZEffect::() failed to get value of constant \"" << name << "\""<<std::endl;
				throw ZEXCEPTION_MESSAGE(ss.str());
			} else {
				ZConstant* constant = znew(ZConstant, (mEffect, handle, p_data, size));
				mConstants.insert(std::pair<const char*, ZConstant*>(name, constant));
				return *constant;
			}
		} else {
			if(bIgnoreErrors) return mDummyConstant;
			std::wstringstream ss;
			ss << L"ZEffect::() parameter \"" << name << L"\" is not found in effect " << mResource->getDesc() << std::endl;
			throw ZEXCEPTION_MESSAGE(ss.str());
		}
	}
}