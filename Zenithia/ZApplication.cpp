////////////////////////////////////////////////////////////////
// Name: ZApplication
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ZApplication.h"
#include "ZWindow.h"
#include "utils/ZAssert.h"
#include "utils/ZClock.h"
#include "d3d9/ZD3D9.h"
#include "input/ZInput.h"
#include "resources/ZResourceSystem.h"
#include "utils/ZMemoryDebug.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;
ZApplication* ZApplication::application = NULL;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZApplication::ZApplication() : bFullscreen(false)
{
	mClock = znew(ZClock, ());
	application = this;
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZApplication::~ZApplication()
{
	application = NULL;
	zdelete(mClock);
}

////////////////////////////////////////////////////////////////
/// Runs the application.
///
unsigned int ZApplication::run(ZWindow* window)
{
	ZASSERT(window, L"ZApplication::run() given window is NULL!");
	
	// Enable runtime memory check for debug
#if defined(DEBUG) || defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	pWindow = window;
	// Subscribe to window events
	pWindow->addEventListener(ZEvent_WindowResize::EVENT_WINDOW_RESIZE, this, EVENT_HANDLER_PTR(&ZApplication::onWindowResize));
	pWindow->start();
	init();
	ZInput::instance()->process();
	start();
	while(pWindow->isAlive())
	{
		pWindow->process();
		mClock->tick();
		ZInput::instance()->process();
		update();
		render();
		ZResourceSystem::instance()->process();
	}
	end();
	ZInput::destroy();
	ZResourceSystem::destroy();

	return 0;
}

////////////////////////////////////////////////////////////////
/// Switches the application rendering to fullscreen mode if it's windowed.
/// Window will be resized, and device will be reset.
/// 
void ZApplication::goFullscreen(ZD3D9& zd3d9)
{
	ZASSERT(bFullscreen == false, L"ZApplication::goFullscreen() window is already in fullscreen mode");
	if(!bFullscreen)
	{
		pWindow->goFullscreen(zd3d9);
		zd3d9.goFullscreen();
		onDeviceReset();
		bFullscreen = true;
	}
}

////////////////////////////////////////////////////////////////
/// Switches the application rendering to windowed mode if it's fullscreen
/// Window will be resized, and device will be reset.
/// 
void ZApplication::goWindowed(ZD3D9& zd3d9)
{
	ZASSERT(bFullscreen == true, L"ZApplication::goWindowed() window is already in windowed mode");
	if(bFullscreen)
	{
		pWindow->goWindowed();
		zd3d9.goWindowed();
		onDeviceReset();
		bFullscreen = false;
	}
}

////////////////////////////////////////////////////////////////
/// Called by window on window resize event
/// 
void ZApplication::onWindowResize(const ZEvent&)
{

}


////////////////////////////////////////////////////////////////
/// Resizes the direct3d device to match the window size if there is a difference.
/// 
void ZApplication::resizeD3D(ZD3D9& zd3d9)
{
	ZASSERT(!bFullscreen, L"ZApplication::resizeD3D() window is in fullscreen");
	if(!bFullscreen)
	{
		zd3d9.doResize();
		onDeviceReset();
	}
}
