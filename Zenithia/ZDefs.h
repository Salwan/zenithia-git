////////////////////////////////////////////////////////////////
// Name: ZDefs
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes

//////////////////////////////////////////////////// Definitions
#define WIN32_LEAN_AND_MEAN

/// If an assert fails, it's logged and a breakpoint is triggered.
/// If this wasn't defined, the assert will be logged but the
/// program is allowed to continue excution anyway.
#define ASSERT_DEBUG


#define WIDEN2(str) L ## str
#define WIDEN(str) WIDEN2(str)
#define __WFILE__ WIDEN(__FILE__)

#define DELETE_OBJECT(obj) if(obj) {zdelete(obj); obj = NULL;}
#define SAFE_RELEASE(object) if(object){object->Release(); object = NULL;}

namespace zen
{

}

