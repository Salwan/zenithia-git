////////////////////////////////////////////////////////////////
// Name: ZTexture
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "stdafx.h"
#include "ZTexture.h"
#include "../exceptions/ZException.h"
#include "../utils/ZAssert.h"
#include "../utils/ZUtils.h"
#include "../resources/ZResource.h"
#include "../resources/ZResource_2DTexture.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

void ZTexture::setTexture(unsigned int stage, const ZResource* resource)
{
	ZASSERT(resource->getType() == ZResource::EResourceType::Resource_2DTexture, L"[ZTexture::setResource() given a non-texture resource.");
	ZD3D9::device->SetTexture(stage, const_cast<IDirect3DTexture9*>(resource->get2DTexture()));
}

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZTexture::ZTexture(const wchar_t* fileName)
{
	ZASSERT(fileName != NULL, L"ZTexture::ZTexture() given NULL file name");
	IDirect3DTexture9* texture;
	HRESULT hr = D3DXCreateTextureFromFile(ZD3D9::device, fileName, &texture);
	if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
	mTexture = texture;
}

////////////////////////////////////////////////////////////////
///
ZTexture::ZTexture(const IDirect3DTexture9* texture)
{
	ZASSERT(texture != NULL, L"ZTexture::ZTexture() given NULL texture");
	mTexture = const_cast<IDirect3DTexture9*>(texture);
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZTexture::~ZTexture()
{
	SAFE_RELEASE(mTexture);
}

////////////////////////////////////////////////////////////////
/// Set the texture to the given stage
/// 
void ZTexture::set(unsigned int stage)
{
	ZASSERT(mTexture != NULL, L"ZTexture::set() attempting to set NULL texture!");
	ZD3D9::device->SetTexture(stage, mTexture);
}
