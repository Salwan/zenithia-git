////////////////////////////////////////////////////////////////
// Name: ZTexture
// Desc: A Direct3D9 texture
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <string>
#include "ZD3D9.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZResource;

	////////////////////////////////////////////////////////////////
	/// @class ZTexture
	/// @brief A Direct3D9 texture
	/// 
	class ZTexture 
	{
		friend class ZConstant;

	public:
		static void setTexture(unsigned int stage, const ZResource* resource);

	public:
		explicit ZTexture(const wchar_t* fileName);
		explicit ZTexture(const IDirect3DTexture9* texture);
		virtual ~ZTexture();

		void set(unsigned int stage);

	private:
		ZTexture(const ZTexture&){}
		ZTexture& operator=(const ZTexture&){}

		IDirect3DTexture9* mTexture;

	};
};

