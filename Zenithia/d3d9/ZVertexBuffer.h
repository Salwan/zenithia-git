////////////////////////////////////////////////////////////////
// Name: ZVertexBuffer
// Desc: Wraps a vertex buffer to simplify the process of 
//       definition and usage. (ZMesh will depend on it)
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZD3D9.h"
#include <vector>

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZVertexDeclaration;

	////////////////////////////////////////////////////////////////
	/// @class ZVertexBuffer
	/// @brief Wraps a vertex buffer to simplify it's definition and usage
	/// 
	class ZVertexBuffer 
	{
	public:
		ZVertexBuffer();
		virtual ~ZVertexBuffer();

		// Methods
		void create(ZVertexDeclaration& vertexDeclaration, unsigned int vertexCount);
		void set();
		void lock();
		void unlock();
		template <typename ElementType>
		void writeElements(unsigned int elementIndex, const ElementType* elementData, unsigned int elementCount, unsigned int elementOffset = 0);
		void writeVertices(unsigned int offsetInBytes, void* vertices, unsigned int vertexCount);

		// Accessors
		unsigned int getVertexCount() const {return uVertexCount;}

	private:
		ZVertexBuffer(const ZVertexBuffer&){}
		ZVertexBuffer& operator=(const ZVertexBuffer&){}

		void lock(unsigned int offsetToLock, unsigned int sizeToLock);
		void construct();

		IDirect3DVertexBuffer9* pVB;
		std::vector<WORD> mElementOffset;
		unsigned int uVertexCount;
		unsigned int uElementCount;
		unsigned int uStride;
		void* pData;
		bool bLocked;
	};

	#include "ZVertexBuffer.inl"
};

