////////////////////////////////////////////////////////////////
// Name: ZLight
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "stdafx.h"
#include "ZLight.h"
#include "../utils/ZColor.h"
#include "../utils/ZAssert.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZLight::ZLight()
{
	mLight.Type = ::D3DLIGHT_POINT;
	mLight.Ambient = ZColor::createD3DColorValue(0.1f, 0.1f, 0.1f);
	mLight.Diffuse = ZColor::createD3DColorValue(1.0f, 1.0f, 1.0f);
	mLight.Specular = ZColor::createD3DColorValue(1.0f, 1.0f, 1.0f);
	mLight.Attenuation0 = 0.0f;
	mLight.Attenuation1 = 0.125f;
	mLight.Attenuation2 = 0.0f;
	mLight.Direction.x = 0.0f;
	mLight.Direction.y = -1.0f;
	mLight.Direction.z = 0.0f;
	mLight.Position.x = 0.0f;
	mLight.Position.y = 0.0f;
	mLight.Position.z = 0.0f;
	mLight.Range = 100.0f;
	mLight.Falloff = 0.0f;
	mLight.Phi = 0.0f;
	mLight.Theta = 0.0f;
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZLight::~ZLight()
{
	
}

////////////////////////////////////////////////////////////////
/// operator =
///
ZLight& ZLight::operator=(const ZLight& other) 
{
	mLight = other.mLight;
	return *this;
}

////////////////////////////////////////////////////////////////
/// Copy constructor
///
ZLight::ZLight(const ZLight& other)
{
	mLight = other.mLight;
}

////////////////////////////////////////////////////////////////
/// Sets light
/// 
void ZLight::set(unsigned int index)
{
	ZD3D9::device->SetLight(index, &mLight);
}

////////////////////////////////////////////////////////////////
/// Set this light to be point light, other parameters:
/// - Attenuation0, 1, and 2
/// 
void ZLight::setPointLight(const ZColor& diffuse, const XMFLOAT3& position, float range)
{
	mLight.Type = ::D3DLIGHT_POINT;
	mLight.Diffuse = diffuse.toD3DColorValue();
	setPosition(position);
	mLight.Range = range;
}

////////////////////////////////////////////////////////////////
/// Set this light to be directional
/// 
void ZLight::setDirectionalLight(const ZColor& diffuse, const XMFLOAT3& direction)
{
	mLight.Type = ::D3DLIGHT_DIRECTIONAL;
	mLight.Diffuse = diffuse.toD3DColorValue();
	setDirection(direction);
}

////////////////////////////////////////////////////////////////
/// Set this light to be spot, other parameters:
/// - Attenuation0, 1, and 2
/// 
void ZLight::setSpotLight(const ZColor& diffuse, const XMFLOAT3& position, const XMFLOAT3& direction, float range, float theta, float phi, float falloff)
{
	mLight.Type = ::D3DLIGHT_SPOT;
	mLight.Diffuse = diffuse.toD3DColorValue();
	setPosition(position);
	setDirection(direction);
	mLight.Range = range;
	mLight.Theta = theta;
	mLight.Phi = phi;
	mLight.Falloff = falloff;
}

void ZLight::setAttenuation(float attenuation_0, float attenuation_1, float attenuation_2)
{
	mLight.Attenuation0 = attenuation_0;
	mLight.Attenuation1 = attenuation_1;
	mLight.Attenuation2 = attenuation_2;
}

void ZLight::setPosition(const XMFLOAT3& position)
{
	mLight.Position.x = position.x;
	mLight.Position.y = position.y;
	mLight.Position.z = position.z;
}

void ZLight::setDirection(const XMFLOAT3& direction)
{
	mLight.Direction.x = direction.x;
	mLight.Direction.y = direction.y;
	mLight.Direction.z = direction.z;
}

void ZLight::setDiffuse(const ZColor& diffuse)
{
	mLight.Diffuse = diffuse.toD3DColorValue();
}

void ZLight::setAmbient(const ZColor& ambient)
{
	mLight.Ambient = ambient.toD3DColorValue();
}

////////////////////////// STATIC //////////////////////////////
//==============================================================
////////////////////////////////////////////////////////////////
/// Enables a light in Direct3D
/// 
void ZLight::Enable(unsigned int index)
{
	ZD3D9::device->LightEnable(index, true);
}

////////////////////////////////////////////////////////////////
/// Disabled a light in Direct3D
/// 
void ZLight::Disable(unsigned int index)
{
	ZD3D9::device->LightEnable(index, false);
}

////////////////////////////////////////////////////////////////
/// Figures out whether a light is enabled or disabled
/// Only works with non-pure devices
/// 
bool ZLight::IsEnabled(unsigned int index)
{
	BOOL en;
	ZD3D9::device->GetLightEnable(index, &en);
	return en? true : false;
}

void ZLight::EnableLighting()
{
	ZD3D9::device->SetRenderState(::D3DRS_LIGHTING, TRUE);
}

void ZLight::DisableLighting()
{
	ZD3D9::device->SetRenderState(::D3DRS_LIGHTING, FALSE);
}

ZLight::ELightType ZLight::getType() const
{
	switch(mLight.Type)
	{
	case D3DLIGHT_DIRECTIONAL:
		return ZLight::ELightType::LightType_Directional;

	case D3DLIGHT_POINT:
		return ZLight::ELightType::LightType_Point;

	case D3DLIGHT_SPOT:
		return ZLight::ELightType::LightType_Spot;
	}
	ZASSERT(false, L"Light type is not valid!");
	return LightType_Unknown;
}

XMFLOAT4 ZLight::getPosition() const
{
	return XMFLOAT4(mLight.Position.x, mLight.Position.y, mLight.Position.z, 1.0f);
}

XMFLOAT4 ZLight::getDirection() const
{
	return XMFLOAT4(mLight.Direction.x, mLight.Direction.y, mLight.Direction.z, 0.0f);
}

XMFLOAT4 ZLight::getDiffuse() const
{
	return XMFLOAT4(mLight.Diffuse.r, mLight.Diffuse.g, mLight.Diffuse.b, 0.0f);
}

XMFLOAT3 ZLight::getAttenuation() const
{
	return XMFLOAT3(mLight.Attenuation0, mLight.Attenuation1, mLight.Attenuation2);
}
