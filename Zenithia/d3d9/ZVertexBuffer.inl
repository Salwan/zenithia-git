////////////////////////////////////////////////////////////////
// Name: ZVertexBuffer
// Desc: Inline definition file for templates
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
/// Fills the vertex buffer element by element.
/// @param element_index the index of the element in the vertex declaration list
/// @param elements_data the data to use (an array of element)
/// @param element_count how many elements to write
/// @param element_offset (optional) which element to start filling at
///
template <typename ElementType>
void ZVertexBuffer::writeElements(unsigned int elementIndex, 
	const ElementType* elementsData, unsigned int elementCount, 
	unsigned int elementOffset)
{
	if(!pVB) throw ZEXCEPTION_MESSAGE("ZVertexBuffer::writeElements() attempting to modify a vertex buffer that hasn't been created yet");
	bool didlock = false;
	if(!bLocked)
	{
		unsigned int lock_offset = elementOffset * uStride;
		unsigned int lock_size = elementCount * uStride;
		didlock = true;
		lock(lock_offset, lock_size);
	}

	for(unsigned int i = 0; i < elementCount; ++i)
	{
		// pData is void*, to use + on it, it must be casted to bytes (VS2010 by design)
		// any higher type will cause heap corruption (writing where you shouldn't)  ie. int* + 1 != char* + 1
		memcpy((char*)pData + (i * uStride) + mElementOffset[elementIndex],
			&elementsData[i], sizeof(ElementType));
	}

	if(didlock)
	{
		unlock();
	}
}