////////////////////////////////////////////////////////////////
// Name: ZEffectInclude
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "stdafx.h"
#include "ZEffectInclude.h"
#include <fstream>
#include "../resources/ZResourceSystem.h"
#include "../utils/ZAssert.h"
#include "../exceptions/ZException.h"
#include "../utils/ZMemoryDebug.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default constructor.
///
ZEffectInclude::ZEffectInclude(const std::wstring& shader_file) : strShaderFile(shader_file)
{
	ZASSERT(!strShaderFile.empty(), L"Sanity check failed.");
}

////////////////////////////////////////////////////////////////
///
ZEffectInclude::~ZEffectInclude()
{

}

////////////////////////////////////////////////////////////////
/// HRESULT Open(
///   [in]   D3DXINCLUDE_TYPE IncludeType, // The location of the #include file.
///   [in]   LPCSTR pFileName, // Name of the #include file.
///   [in]   LPCVOID pParentData, // Pointer to the container that includes the #include file.
///   [out]  LPCVOID *ppData, // Pointer to the returned buffer that contains the include directives. This pointer remains valid until ID3DXInclude::Close is called.
///   [out]  UINT *pBytes // Number of bytes returned in ppData.
/// );
HRESULT __stdcall ZEffectInclude::Open(D3DXINCLUDE_TYPE IncludeType, LPCSTR pFileName,
			LPCVOID, LPCVOID* ppData, UINT *pBytes)
{
	ZASSERT(IncludeType == D3DXINCLUDE_TYPE::D3DXINC_LOCAL, L"Currently only local #include is supported. (#include \"file.fx\")");
	std::wostringstream ss_file;
	ss_file << _getLocalIncludePath().c_str() << pFileName;
	OutputDebugStringW(ss_file.str().c_str());
	std::ifstream file_stream(ss_file.str(), std::ios_base::in);
	if(file_stream.fail()) {
		std::wostringstream ss;
		ss << L"ZEffectInclude: Could not find included file!" << std::endl;
		ss << L"Effect: " << strShaderFile << std::endl;
		ss << L"Included file: " << ss_file.str().c_str();
		throw ZEXCEPTION_MESSAGE(ss.str());
	} else {
		strIncludedContent = std::string((std::istreambuf_iterator<char>(file_stream)), std::istreambuf_iterator<char>());
		file_stream.close();
		*ppData = strIncludedContent.c_str();
		*pBytes = strIncludedContent.length();
	}
	return S_OK;
}

////////////////////////////////////////////////////////////////
/// HRESULT Close(
///   [in]  LPCVOID pData
/// );
HRESULT __stdcall ZEffectInclude::Close(LPCVOID)
{
	strIncludedContent.clear();
	return S_OK;
}

////////////////////////////////////////////////////////////////
/// Generates the local include path for this shader.
///
const std::wstring ZEffectInclude::_getLocalIncludePath() const
{
	unsigned int last_slash_index = strShaderFile.rfind(L"/");
	std::wstring path;
	if(last_slash_index != std::wstring::npos) {
		 path = strShaderFile.substr(0, last_slash_index) + L"/";
	} else {
		path = L"";
	}
	return path;
}
