////////////////////////////////////////////////////////////////
// Name: ZD3D9
// Desc: Represents Direct3D9, does all basic functions needed 
//		 like initializing, loading, updating, resizing, etc...
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <d3d9.h>
#include <d3dx9.h>
#include <xnamath.h>
#include "../events/ZEventDispatcher.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZWindow;
	class ZException;

	////////////////////////////////////////////////////////////////
	/// @class ZD3D9
	/// @brief
	/// 
	class ZD3D9 : public ZEventDispatcher
	{
	public:
		static IDirect3DDevice9* device;
		static ZD3D9* d3d;
		static HWND getWindowHandle();

		static bool supportsTwoSidedStencil();
		static int numMultipleRenderToTexture();
		static bool supportsMRTPostPixelShaderBlending();

	public:
		explicit ZD3D9(ZWindow& window);
		virtual ~ZD3D9();

		// Methods
		void clear(D3DCOLOR fillColor = 0x00000000);
		void present();
		void goFullscreen();
		void goWindowed();
		void doResize();

		// Accessors
		IDirect3D9& getD3D9() const {return *pD3D9;}
		IDirect3DDevice9& getDevice() const {return *pDevice;}
		const D3DDISPLAYMODE& getFullscreenMode() const {return mFullscreenMode;}
		const D3DCAPS9& getCaps() const {return mD3DCaps;}

	protected:

	private:
		ZD3D9(const ZD3D9&){}
		ZD3D9& operator=(const ZD3D9&){}

		void createD3D9();
		void destroyD3D9();
		void resizeDevice();

		// Private Implementation
		struct PIMPL;
		PIMPL* pimpl;

		IDirect3D9* pD3D9;
		IDirect3DDevice9* pDevice;
		D3DPRESENT_PARAMETERS mPresentParams;
		D3DCAPS9 mD3DCaps;
		D3DDISPLAYMODE mFullscreenMode;
		bool bFullscreen;
	};
};
