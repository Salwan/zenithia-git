////////////////////////////////////////////////////////////////
// Name: ZIndexBuffer
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZD3D9.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @class ZIndexBuffer
	/// @brief
	/// 
	class ZIndexBuffer 
	{
	public:
		explicit ZIndexBuffer();
		virtual ~ZIndexBuffer();

		// Methods
		void create(unsigned int indexCount, D3DFORMAT indicesFormat = D3DFMT_INDEX16);
		void set();
		void lock();
		void unlock();
		void writeIndices(unsigned int offsetInIndices, void* indices, unsigned int indexCount);

		// Accessors
		unsigned int getFaceCount() const {return uIndexCount / 3;}
		bool empty() const {return !pIB? true : false;}

	private:
		ZIndexBuffer(const ZIndexBuffer&){}
		ZIndexBuffer& operator=(const ZIndexBuffer&){}

		void lock(unsigned int offsetToLock, unsigned int sizeToLock);
		void construct();

		IDirect3DIndexBuffer9* pIB;
		D3DFORMAT mFormat;
		unsigned int uIndexCount;
		unsigned int uIndexStride;
		void* pData;
		bool bLocked;
	};
};

