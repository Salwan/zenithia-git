////////////////////////////////////////////////////////////////
// Name: ZVertexElement
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ZVertexDeclaration.h"
#include "../utils/ZAssert.h"
#include "../exceptions/ZException.h"
#include "../utils/ZUtils.h"

using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZVertexDeclaration::ZVertexDeclaration() 
	: pVD(NULL), bOpened(true), wOffset(0), uElementCount(0)
{

}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZVertexDeclaration::~ZVertexDeclaration()
{
	SAFE_RELEASE(pVD);
}

void ZVertexDeclaration::construct()
{
	HRESULT hr = ZD3D9::device->CreateVertexDeclaration(&vElements[0], &pVD);
	if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
}

////////////////////////////////////////////////////////////////
/// Sends the vertex declaration object to the device.
/// 
void ZVertexDeclaration::set()
{
	if(!pVD) throw ZEXCEPTION_MESSAGE(L"ZVertexDeclaration::set() attempting to set vertex declaration before it's fully initialized, make sure you called pushEndElement()");
	ZD3D9::device->SetVertexDeclaration(pVD);
}

////////////////////////////////////////////////////////////////
/// Pushes a new vertex element unto the declaration.
/// Will assert to check whether the declaration has not been
/// ended by a previous call to pushEndElement().
/// @param d3ddecltype the type of the element (D3DDECLTYPE_?)
/// @param d3ddeclusage the usage of the element (D3DDECLUSAGE_?)
/// @param usage_index the usage index of the element
/// 
void ZVertexDeclaration::pushElement(BYTE d3ddecltype, BYTE d3ddeclusage, BYTE usageIndex)
{
	ZASSERT(bOpened, L"ZVertexElement::pushElement() declaration ended! you are trying to write past end.");
	D3DVERTEXELEMENT9 e;
	e.Stream = 0;
	e.Offset = wOffset;
	e.Type = d3ddecltype;
	e.Method = ::D3DDECLMETHOD_DEFAULT;
	e.Usage = d3ddeclusage;
	e.UsageIndex = usageIndex;
	vElements.push_back(e);

	switch(e.Type)
	{
	case D3DDECLTYPE_FLOAT1:
	case D3DDECLTYPE_D3DCOLOR: // from GPU Gems 2 Ch5 example, D3DCOLOR is packed, expanded to RGBA
	case D3DDECLTYPE_UBYTE4:
	case D3DDECLTYPE_SHORT2:
	case D3DDECLTYPE_UBYTE4N:
	case D3DDECLTYPE_SHORT2N:
	case D3DDECLTYPE_USHORT2N:
	case D3DDECLTYPE_FLOAT16_2:
		wOffset += 4;
		break;

	case D3DDECLTYPE_FLOAT2:
	case D3DDECLTYPE_SHORT4:
	case D3DDECLTYPE_SHORT4N:
	case D3DDECLTYPE_USHORT4N:
	case D3DDECLTYPE_FLOAT16_4:
		wOffset += 4 * 2;
		break;

	case D3DDECLTYPE_FLOAT3:
	case D3DDECLTYPE_UDEC3:
	case D3DDECLTYPE_DEC3N:
		wOffset += 4 * 3;
		break;

	case D3DDECLTYPE_FLOAT4:
		wOffset += 4 * 4;
		break;
	}

	uElementCount++;
}

////////////////////////////////////////////////////////////////
/// Pushes the special end element to the declaration.
/// This should always be the last element to be pushed.
/// 
void ZVertexDeclaration::pushEndElement()
{
	ZASSERT(bOpened, L"ZVertexElement::pushEndElement() declaration already ended! you are trying to write a second end or something?");
	D3DVERTEXELEMENT9 e[] = {D3DDECL_END()};
	vElements.push_back(e[0]);
	bOpened = false;
	construct();
}

////////////////////////////////////////////////////////////////
/// Returns the vertex element declaration.
/// 
D3DVERTEXELEMENT9* ZVertexDeclaration::getElementsDeclaration()
{
	ZASSERT(!bOpened, L"ZVertexDeclaration::getElementsDeclaration() declaration has not been ended by calling pushEndElement()");
	return &vElements[0];
}


size_t ZVertexDeclaration::getVertexSize() const
{
	ZASSERT(!bOpened, L"ZVertexDeclaration::getVertexSize() declaration has not been ended by calling pushEndElement()");
	return wOffset;
}
