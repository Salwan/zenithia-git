////////////////////////////////////////////////////////////////
// Name: ZIndexBuffer
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ZIndexBuffer.h"
#include "../utils/ZAssert.h"
#include "../exceptions/ZException.h"
#include "../utils/ZUtils.h"

using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZIndexBuffer::ZIndexBuffer() : pIB(NULL), bLocked(false), pData(NULL)
{
	
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZIndexBuffer::~ZIndexBuffer()
{
	SAFE_RELEASE(pIB);
}

void ZIndexBuffer::create(unsigned int indexCount, D3DFORMAT indicesFormat)
{
	ZASSERT(indexCount > 0, L"ZIndexBuffer::create() index count is 0? :-S");
	ZASSERT(!pIB, L"ZIndexBuffer::create() called on an already created index buffer");
	ZASSERT(indicesFormat == D3DFMT_INDEX16 || indicesFormat == D3DFMT_INDEX32, L"ZIndexBuffer::create() unknwon index format");
	if(pIB)
	{
		SAFE_RELEASE(pIB);
	}
	uIndexCount = indexCount;
	mFormat = indicesFormat;
	if(mFormat == D3DFMT_INDEX16)
	{
		uIndexStride = 2;
	}
	else
	{
		uIndexStride = 4;
	}
	construct();
}

void ZIndexBuffer::construct()
{
	HRESULT hr;
	hr = ZD3D9::device->CreateIndexBuffer(uIndexCount * uIndexStride,
		D3DUSAGE_WRITEONLY, mFormat, D3DPOOL_MANAGED, &pIB, NULL);
	if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
}

void ZIndexBuffer::set()
{
	if(!pIB) throw ZEXCEPTION_MESSAGE(L"ZIndexBuffer::set() attempting to set an index buffer that hasn't been created yet");
	ZD3D9::device->SetIndices(pIB);
}

void ZIndexBuffer::lock()
{
	if(!pIB) throw ZEXCEPTION_MESSAGE(L"ZIndexBuffer::lock() attempting to modify an index buffer that hasn't been created yet");
	ZASSERT(!bLocked, L"ZIndexBuffer::lock() attempting to lock an index buffer that's already locked");
	HRESULT hr = pIB->Lock(0, 0, &pData, 0);
	if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
	bLocked = true;
}

void ZIndexBuffer::lock(unsigned int offsetToLock, unsigned int sizeToLock)
{
	if(!pIB) throw ZEXCEPTION_MESSAGE(L"ZIndexBuffer::lock() attempting to modify an index buffer that hasn't been created yet");
	ZASSERT(!bLocked, L"ZIndexBuffer::lock() attempting to lock an index buffer that's already locked");
	HRESULT hr = pIB->Lock(offsetToLock, sizeToLock, &pData, 0);
	if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
	bLocked = true;
}

void ZIndexBuffer::unlock()
{
	if(!pIB) throw ZEXCEPTION_MESSAGE(L"ZIndexBuffer::unlock() attempting to modify an index buffer that hasn't been created yet");
	ZASSERT(bLocked, L"ZIndexBuffer::unlock() attempting to unlock an index buffer  that's not locked");
	pIB->Unlock();
	bLocked = false;
}

////////////////////////////////////////////////////////////////
/// Write indices to the index buffer.
/// @param offset_in_indices which index to start writing at
/// @param indices index data to write
/// @param index_count number of indices to write
///
void ZIndexBuffer::writeIndices(unsigned int offsetInIndices, void* indices, unsigned int indexCount)
{
	if(!pIB) throw ZEXCEPTION_MESSAGE(L"ZIndexBuffer::writeIndices() attempting to modify an index buffer that hasn't been created yet");
	bool didlock = false;
	if(!bLocked)
	{
		lock(offsetInIndices * uIndexStride, indexCount * uIndexStride);
		didlock = true;
	}

	memcpy(pData, indices, indexCount * uIndexStride);

	if(didlock)
	{
		unlock();
	}
}