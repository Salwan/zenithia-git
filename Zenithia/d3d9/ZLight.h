////////////////////////////////////////////////////////////////
// Name: ZLight
// Desc: Represents a basic light
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZD3D9.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZColor;

	////////////////////////////////////////////////////////////////
	/// @class ZLight
	/// @brief
	/// 
	class ZLight 
	{
	public:
		enum ELightType
		{
			LightType_Unknown,
			LightType_Directional,
			LightType_Point,
			LightType_Spot
		};

	public:
		ZLight();
		virtual ~ZLight();
		ZLight& operator=(const ZLight&);
		ZLight(const ZLight&);

		void set(unsigned int index);
		void setPointLight(const ZColor& diffuse, const XMFLOAT3& position, float range);
		void setDirectionalLight(const ZColor& diffuse, const XMFLOAT3& direction);
		void setSpotLight(const ZColor& diffuse, const XMFLOAT3& position, const XMFLOAT3& direction, float range, float theta, float phi, float falloff = 1.0f);
		void setAttenuation(float attenuation_0, float attenuation_1, float attenuation_2);
		void setPosition(const XMFLOAT3& position);
		void setDirection(const XMFLOAT3& direction);
		void setDiffuse(const ZColor& diffuse);
		void setAmbient(const ZColor& ambient);

		ELightType getType() const;
		XMFLOAT4 getPosition() const;
		XMFLOAT4 getDirection() const;
		XMFLOAT4 getDiffuse() const;
		XMFLOAT3 getAttenuation() const;

	private:
		D3DLIGHT9 mLight;

	public:
		static void Enable(unsigned int index);
		static void Disable(unsigned int index);
		static bool IsEnabled(unsigned int index);
		static void EnableLighting();
		static void DisableLighting();
	};
};

