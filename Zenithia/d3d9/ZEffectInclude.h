////////////////////////////////////////////////////////////////
// Name: ZEffectInclude
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZD3D9.h"
#include <string>

//////////////////////////////////////////////////// Definitions

namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @struct ZEffectInclude
	/// @brief Direct3d9 effect system requires this class to be implemented
	///        so #include preprocessor works correctly.
	/// 
	struct ZEffectInclude : public ID3DXInclude
	{
		explicit ZEffectInclude(const std::wstring& shader_file);
		virtual ~ZEffectInclude();

		virtual HRESULT __stdcall Open(D3DXINCLUDE_TYPE IncludeType, LPCSTR pFileName,
			LPCVOID pParentData, LPCVOID* ppData, UINT *pBytes) override;
		virtual HRESULT __stdcall Close(LPCVOID pData) override;

	private:
		const std::wstring _getLocalIncludePath() const;
		std::wstring strShaderFile;
		std::string strIncludedContent;
	};
};
