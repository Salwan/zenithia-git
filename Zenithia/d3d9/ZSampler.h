////////////////////////////////////////////////////////////////
// Name: ZSampler
// Desc: Wrapper around sampler states, static methods.
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes

//////////////////////////////////////////////////// Definitions
namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @enum Sampler filter type
	/// 
	enum ESamplerFilter
	{
		ESF_NONE,
		ESF_POINT,
		ESF_BILINEAR,
		ESF_TRILINEAR,
		ESF_ANISOTROPIC,
	};

	////////////////////////////////////////////////////////////////
	/// @class ZSampler
	/// @brief
	/// 
	class ZSampler 
	{
	public:	
		virtual ~ZSampler();

		// Methods
		static void setSamplerFilter(unsigned int sampler, ESamplerFilter filterType, unsigned int anisotropyLevel = 0);
		static void setTextureWrap(unsigned int sampler);
		static void setTextureBorder(unsigned int sampler, unsigned int borderColor);
		static void setTextureClamp(unsigned int sampler);
		static void setTextureMirror(unsigned int sampler);
		static void setTextureAddressingMode(unsigned int sampler, unsigned int d3dtaddressu, unsigned int d3dtaddressv);

	private:
		ZSampler();
		ZSampler(const ZSampler&){}
		ZSampler& operator=(const ZSampler&){}

	};
};
