////////////////////////////////////////////////////////////////
// Name: ZVertexBuffer
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ZVertexBuffer.h"
#include "ZVertexDeclaration.h"
#include "../utils/ZAssert.h"
#include "../exceptions/ZException.h"
#include "../utils/ZUtils.h"

using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZVertexBuffer::ZVertexBuffer() : pVB(NULL), bLocked(false), pData(NULL)
{
	
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZVertexBuffer::~ZVertexBuffer()
{
	SAFE_RELEASE(pVB);
}

////////////////////////////////////////////////////////////////
/// Creates the vertex buffer using the provided information.
/// If you call create more than once, it will assert. If assert
/// is disabled the vertex buffer will just be recreated using
/// the provided information.
/// @param vertex_declaration The vertex element declaration
/// @param vertex_count the total number of vertices in the buffer
///
void ZVertexBuffer::create(ZVertexDeclaration& vertexDeclaration, 
	unsigned int vertexCount)
{
	ZASSERT(vertexCount > 0, L"ZVertexBuffer::create() vertex count is 0? why would you want to create a vertex buffer with 0 vertices");
	ZASSERT(vertexDeclaration.isComplete(), L"ZVertexBuffer::create() given vertex declaration is incomplete, you should add an end element");
	ZASSERT(!pVB, L"ZVertexBuffer::create() called on an already created vertex buffer");
	if(pVB)
	{
		SAFE_RELEASE(pVB);
	}
	uVertexCount = vertexCount;
	uStride = static_cast<unsigned int>(vertexDeclaration.getVertexSize());
	uElementCount = vertexDeclaration.getElementCount();
	mElementOffset.clear();
	D3DVERTEXELEMENT9* pe = vertexDeclaration.getElementsDeclaration();
	for(unsigned int i = 0; i < uElementCount; ++i)
	{
		mElementOffset.push_back(pe[i].Offset);
	}
	construct();
}

void ZVertexBuffer::construct()
{
	HRESULT hr;
	hr = ZD3D9::device->CreateVertexBuffer(uStride * uVertexCount, 
		D3DUSAGE_WRITEONLY, 0, D3DPOOL_MANAGED, &pVB, NULL);
	if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
}

void ZVertexBuffer::set()
{
	if(!pVB) throw ZEXCEPTION_MESSAGE(L"ZVertexBuffer::set() attempting to set vertex buffer before creation");
	ZD3D9::device->SetStreamSource(0, pVB, 0, uStride);
}

////////////////////////////////////////////////////////////////
/// Locks the entire vertex buffer.
/// Use writeElements or writeVertices instead of you want to 
/// write to a specific segment.
///
void ZVertexBuffer::lock()
{
	if(!pVB) throw ZEXCEPTION_MESSAGE(L"ZVertexBuffer::lock() attempting to modify a vertex buffer that hasn't been created yet");
	ZASSERT(!bLocked, L"ZVertexBuffer::lock() attempting to lock an already locked vertex buffer");
	HRESULT hr = pVB->Lock(0, 0, &pData, 0);
	if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
	bLocked = true;
}

void ZVertexBuffer::lock(unsigned int offsetToLock, unsigned int sizeToLock)
{
	if(!pVB) throw ZEXCEPTION_MESSAGE(L"ZVertexBuffer::lock() attempting to modify a vertex buffer that hasn't been created yet");
	ZASSERT(!bLocked, L"ZVertexBuffer::lock() attempting to lock an already locked vertex buffer");
	HRESULT hr = pVB->Lock(offsetToLock, sizeToLock, &pData, 0);
	if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
	bLocked = true;
}

////////////////////////////////////////////////////////////////
/// Unlocks the vertex buffer
/// 
void ZVertexBuffer::unlock()
{
	ZASSERT(pVB, L"ZVertexBuffer::unlock() attempting to unlock a vertex buffer that hasn't been created yet");
	ZASSERT(bLocked, L"ZVertexBuffer::unlock() attempting to unlock an already unlocked vertex buffer");
	pData = NULL;
	if(!pVB) return;
	pVB->Unlock();
	bLocked = false;
}

////////////////////////////////////////////////////////////////
/// Writes whole vertices into the vertex buffer.
/// @param offset_in_bytes where to start writing vertices.
/// @param vertices raw vertex data
/// @param vertex_count how many vertices to write
/// 
void ZVertexBuffer::writeVertices(unsigned int offsetInBytes, void* vertices, 
	unsigned int vertexCount)
{
	if(!pVB) throw ZEXCEPTION_MESSAGE(L"ZVertexBuffer::writeVertices() attempting to modify a vertex buffer that hasn't been created yet");
	bool didlock = false;
	if(!bLocked)
	{
		didlock = true;
		lock(offsetInBytes, vertexCount * uStride);
	}

	memcpy(pData, vertices, uStride * vertexCount);

	if(didlock)
	{
		unlock();
	}
}