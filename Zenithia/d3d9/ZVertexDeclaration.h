////////////////////////////////////////////////////////////////
// Name: ZVertexElement
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <vector>
#include "ZD3D9.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	namespace VertexDeclUsage
	{
		static const unsigned char Position		= ::D3DDECLUSAGE_POSITION;
		static const unsigned char Normal		= ::D3DDECLUSAGE_NORMAL;
		static const unsigned char BlendWeight	= ::D3DDECLUSAGE_BLENDWEIGHT;
		static const unsigned char BlendIndices	= ::D3DDECLUSAGE_BLENDINDICES;
		static const unsigned char PSize			= ::D3DDECLUSAGE_PSIZE;
		static const unsigned char Color			= ::D3DDECLUSAGE_COLOR;
		static const unsigned char TexCoord		= ::D3DDECLUSAGE_TEXCOORD;
		static const unsigned char Tangent		= ::D3DDECLUSAGE_TANGENT;
		static const unsigned char BiNormal		= ::D3DDECLUSAGE_BINORMAL;
		static const unsigned char TessFactor	= ::D3DDECLUSAGE_TESSFACTOR;
		static const unsigned char PositionT		= ::D3DDECLUSAGE_POSITIONT;
		static const unsigned char Fog			= ::D3DDECLUSAGE_FOG;
		static const unsigned char Depth			= ::D3DDECLUSAGE_DEPTH;
		static const unsigned char Sample		= ::D3DDECLUSAGE_SAMPLE;
	};

	namespace VertexDeclType
	{
		static const unsigned char Float1		= ::D3DDECLTYPE_FLOAT1;
		static const unsigned char Float2		= ::D3DDECLTYPE_FLOAT2;
		static const unsigned char Float3		= ::D3DDECLTYPE_FLOAT3;
		static const unsigned char Float4		= ::D3DDECLTYPE_FLOAT4;
		static const unsigned char UByte4		= ::D3DDECLTYPE_UBYTE4;
		static const unsigned char D3DColor		= ::D3DDECLTYPE_D3DCOLOR;
		static const unsigned char Short2		= ::D3DDECLTYPE_SHORT2;
		static const unsigned char Short4		= ::D3DDECLTYPE_SHORT4;
	};

	////////////////////////////////////////////////////////////////
	/// @class ZVertexElement
	/// @brief Wraps and simplifies the process of creating vertex elements declaration.
	/// 
	class ZVertexDeclaration
	{
	public:
		ZVertexDeclaration();
		virtual ~ZVertexDeclaration();

		// Methods
		void set();
		void pushElement(BYTE d3ddecltype, BYTE d3ddeclusage, BYTE usageIndex);
		void pushEndElement();

		// Accessors
		D3DVERTEXELEMENT9* getElementsDeclaration();
		size_t getVertexSize() const;
		bool isComplete() const {return !bOpened;}
		unsigned int getElementCount() const {return uElementCount;}

	private:
		ZVertexDeclaration(const ZVertexDeclaration&){}
		ZVertexDeclaration& operator=(const ZVertexDeclaration&){}

		void construct();

		IDirect3DVertexDeclaration9* pVD;
		std::vector<D3DVERTEXELEMENT9> vElements;
		WORD wOffset;
		unsigned int uElementCount;
		bool bOpened;
	};
};

