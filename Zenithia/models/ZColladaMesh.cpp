////////////////////////////////////////////////////////////////
// Name: ZColladaMesh
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include <iostream>
#include "ZColladaMesh.h"
#include "../utils/ZAssert.h"
#include "../d3d9/ZVertexDeclaration.h"
#include "../d3d9/ZVertexBuffer.h"
#include "../d3d9/ZIndexBuffer.h"
#include "../models/ZMaterial.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;
using namespace pugi;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZColladaMesh::ZColladaMesh(const std::wstring& colladaFile, bool autoCreateMesh)
	: bAutoCreateMesh(autoCreateMesh), bMeshCreated(false)
{
	_loadFromFile(colladaFile);
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZColladaMesh::~ZColladaMesh()
{
	for(auto iter = mMeshSources.begin(); iter != mMeshSources.end(); ++iter) {
		if(iter->second) {
			if(iter->second->pData) {
				zdelete_array(iter->second->pData);
			}
			zdelete(iter->second);
		}
	}
}

////////////////////////////////////////////////////////////////
// Create the actual mesh.
//
void ZColladaMesh::createMesh()
{
	ZASSERT(!bMeshCreated, L"[ZColladaMesh] createMesh() mesh is already created!");
	if(!bMeshCreated) {
		_createMesh();
	}
}

////////////////////////////////////////////////////////////////
/// Parses a collada file and arranges data.
///
void ZColladaMesh::_loadFromFile(const std::wstring& colladaFile)
{
	_parseColladaMesh(colladaFile);
}

void ZColladaMesh::_parseColladaMesh(const std::wstring& colladaFile)
{
	// COLLADA

	// (1)
	// - asset							// - Description of scene
	// (5)
	// - library_images					// - Materials
	// - library_effects
	// - library_materials
	// (4)
	// - library geometries				// - Geometry library
	// (3)
	// - library_visual_scenes			// - Scene nodes
	// (2)
	// - scene							// - Scenes

	xml_document collada_doc;
	xml_parse_result parse_result = collada_doc.load_file(colladaFile.c_str());
	if(parse_result.status != pugi::status_ok) {
		std::wstringstream ss;
		ss << "ZColladaMesh: Error parsing collada model file \"" << colladaFile << "\"" << std::endl;
		ss << parse_result.description() << std::endl;
		throw ZEXCEPTION_MESSAGE(ss.str());
	} else {
		std::wclog << "[ZColladaMesh] Parsed XML file \"" << colladaFile << "\"" << std::endl;
	}
	xml_node collada = collada_doc.child(L"COLLADA");
	_parseAsset(collada.child(L"asset"));
	_parseScene(collada.child(L"scene"));
	_parseLibraryVisualScenes(collada.child(L"library_visual_scenes"), strInstanceVisualSceneURL);
	_parseLibraryGeometries(collada.child(L"library_geometries"));
	_parseLibraryMaterials(collada);

	if(bAutoCreateMesh) {
		_createMesh();
	}
}

// (1) Description of scene
// <asset>							
//     <contributor> (*)
//	       <author>
//         <authoring_tool>
//         <comments>
//     <created>
//     <modified>
//     <revision>
//     <title>
//     <subject>
//     <keywords>
//     <unit>
//     <up_axis>
void ZColladaMesh::_parseAsset(const xml_node& node)
{
	ZASSERT(wcscmp(node.name(), L"asset") == 0, L"[ZColladaMesh] ::parseAsset() node given is not an asset xml node.");
	strTitle = node.child(L"title").child_value();
	std::wstring up_axis = node.child(L"up_axis").child_value(); // Y_UP by default
	if(up_axis == L"X_UP") {
		eUpAxis = UpAxis_X;
	} else if(up_axis == L"Y_UP") {
		eUpAxis = UpAxis_Y;
	} else if(up_axis == L"Z_UP") {
		eUpAxis = UpAxis_Z;
	}
}

// (2) Scenes
// <scene> (1)
//     <instance_visual_scene> (1)		// -- Root node of scene
void ZColladaMesh::_parseScene(const xml_node& node)
{
	ZASSERT(wcscmp(node.name(), L"scene") == 0, L"[ZColladaMesh] ::parseScene() node given is not  <asset> node.");
	strInstanceVisualSceneURL = _hashlessID(node.child(L"instance_visual_scene").attribute(L"url").as_string());
	ZASSERT(!strInstanceVisualSceneURL.empty(), L"[ZColladaMesh] ::parseScene() no instance visual scene is defined, there must be at most.");
}

// (3) Scene nodes
// <library_visual_scenes>
//     <visual_scene>					// -- Root node
//         <node>							// --- Node name
//             <rotate>						// ---- Node rotation
//             <instance_geometry>			// ---- geometric element name
void ZColladaMesh::_parseLibraryVisualScenes(const xml_node& node, const std::wstring& visualSceneId)
{
	ZASSERT(wcscmp(node.name(), L"library_visual_scenes") == 0, L"[ZColladaMesh] ::parseLibraryVisualScenes() node given is not <library_visual_scenes> node.");
	xml_node visual_scene = node.find_child_by_attribute(L"id", visualSceneId.c_str());
	ZASSERT(visual_scene, L"[ZColladaMesh] ::parseLibraryVisualScenes() no visual scene found with the given id.");
	// TODO each node should feed directly into the Zenithia scene nodes
	// TODO parse transformation data
	// Only first node is read for now
	for(xml_node scene_node = visual_scene.child(L"node"); scene_node; scene_node = scene_node.next_sibling(L"node")) {
		xml_node instance_geometry = scene_node.child(L"instance_geometry");
		if(instance_geometry) {
			strGeometryURLs.push_back(_hashlessID(instance_geometry.attribute(L"url").as_string()));
			break;
		}
	}
	ZASSERT(!strGeometryURLs.empty(), L"[ZColladaMesh] ::parseLibraryVisualScenes() no geometry found in visual scene.");
}

// (4) Geometry library
// <library_geometries>				// - Geometry library
//     <geometry>						// -- geometric element
//         <mesh>
//             <source>						// ---- data source
//                 <float_array>				// ----- data
//                 <technique_common>			// ----- description (count, stride, data type)
//                     <accessor>
//                         <param>
//             <vertices>					// ---- Semantic and source
//             <polygons>|<polylist>		// ---- Polygons
//                 <input>						// ----- Semantic/Vertex structure
//                 <p>							// ----- indices
void ZColladaMesh::_parseLibraryGeometries(const xml_node& node)
{
	ZASSERT(wcscmp(node.name(), L"library_geometries") == 0, L"[ZColladaMesh] ::parseLibraryGeometries() node given is not an <library_geometries> xml node.");
	// TODO: should use strGeometryURLs to parse all geometries, for now only first one is parsed.
	xml_node geometry = node.child(L"geometry");
	ZASSERT(geometry, L"[ZColladaMesh] ::parseLibraryGeometries() error.");
	xml_node mesh = geometry.child(L"mesh");
	ZASSERT(mesh, L"[ZColladaMesh] ::parseLibraryGeometries() error.");

	// Parse Data Sources
	for(xml_node source = mesh.child(L"source"); source; source = source.next_sibling(L"source")) {
		SMeshSource* mesh_source = znew(SMeshSource, ());

		xml_node data_array = source.child(L"float_array");
		if(data_array) {
			mesh_source->eDataType = DataType_Float;
			mesh_source->uDataTypeSize = sizeof(float);
			mesh_source->uCount = data_array.attribute(L"count").as_uint();
			mesh_source->pData = static_cast<void*>(_parseFloatArray(data_array.child_value(), mesh_source->uCount));
		} else {
			throw ZEXCEPTION_MESSAGE(L"ZColladaMesh::parseLibraryGeometries() Unsupported data array found in <source>.");
		}

		xml_node technique_common = source.child(L"technique_common");
		ZASSERT(technique_common, L"[ZColladaMesh] ::parseLibraryGeometries() error.");

		xml_node technique_accessor = technique_common.child(L"accessor");
		ZASSERT(technique_accessor, L"[ZColladaMesh] ::parseLibraryGeometries() error.");

		mesh_source->uElementCount = technique_accessor.attribute(L"count").as_uint();
		mesh_source->uElementStride = technique_accessor.attribute(L"stride").as_uint();
		mesh_source->uElementSize = mesh_source->uDataTypeSize * mesh_source->uElementStride;
		ZASSERT(mesh_source->uElementCount > 0 && mesh_source->uElementStride > 0, L"[ZColladaMesh] ::parseLibraryGeometries() error.");

		for(xml_node accessor_param = technique_accessor.child(L"param"); accessor_param; accessor_param = accessor_param.next_sibling(L"param")) {
			mesh_source->vParamNames.push_back(accessor_param.attribute(L"name").as_string());
		}

		mMeshSources[source.attribute(L"id").as_string()] = mesh_source;
	}

	// Parse Polygons and create actual structures
	xml_node polygons;
	polygons = mesh.child(L"polylist");
	if(!polygons) {
		polygons = mesh.child(L"polygons");
	}
	size_t polygons_count = polygons.attribute(L"count").as_uint();
	ZASSERT(polygons, L"[ZColladaMesh] ::parseLibraryGeometries() error.");

	// Parse <vertices>, overrides input semantic "VERTEX" and provides the actual source of data (why?! who knows)
	xml_node vertices_node = mesh.child(L"vertices");

	// Parse semantics into temporary vertex declaration buffer
	unsigned int vertex_elements_count = 0;
	std::vector<std::wstring> semantics_sources;
	// TODO: There is only one vertex declaration buffer for the first geometry, this should be dynamic
	vVertexDeclarations.clear();
	for(xml_node semantic = polygons.child(L"input"); semantic; semantic = semantic.next_sibling(L"input")) {
		unsigned int semantic_offset = semantic.attribute(L"offset").as_uint();
		// Find out how many elements there are, COLLADA supports defining two different semantics with the same offset.. this is why only the max offset is taken
		if(semantic_offset + 1 > vertex_elements_count) {
			vertex_elements_count = semantic_offset + 1;
		}
		unsigned char semantic_set = static_cast<unsigned char>(semantic.attribute(L"set").as_int(0));
		std::wstring semantic_name = semantic.attribute(L"semantic").as_string();
		std::wstring semantic_source = _hashlessID(semantic.attribute(L"source").as_string());
		// Process vertices special override tag "VERTEX" to "POSITION"
		if(vertices_node && semantic_source == vertices_node.attribute(L"id").as_string()) {
			xml_node position_semantic = vertices_node.child(L"input");
			semantic_name = position_semantic.attribute(L"semantic").as_string();
			semantic_source = _hashlessID(std::wstring(position_semantic.attribute(L"source").as_string()));
		}
		// Parse semantics
		SMeshSource* ms = mMeshSources[semantic_source];
		SVertexDeclaration vd;
		vd.ucDeclarationType = _getDeclType(ms->eDataType, ms->uElementStride);
		vd.ucSet = semantic_set;
		if(semantic_name == L"VERTEX" || semantic_name == L"POSITION") {
			vd.ucDeclarationUsage = VertexDeclUsage::Position;
			semantics_sources.push_back(semantic_source);
			_updatePositionDataForUpAxis(ms->pData, ms->uCount, ms->uElementStride);
		} else if(semantic_name == L"NORMAL") {
			vd.ucDeclarationUsage = VertexDeclUsage::Normal;
			semantics_sources.push_back(semantic_source);
			_updatePositionDataForUpAxis(ms->pData, ms->uCount, ms->uElementStride);
		} else if(semantic_name == L"TEXCOORD") {
			vd.ucDeclarationUsage = VertexDeclUsage::TexCoord;
			semantics_sources.push_back(semantic_source);
		} else {
			std::wostringstream ss;
			ss << "ZColladaMesh::parseLibraryGeometries() found an unsupported semantic: ";
			ss << "\"" << semantic_source << "\"";
			throw ZEXCEPTION_MESSAGE(ss.str());
		}
		vVertexDeclarations.push_back(vd);
	}

	// Sanity check
	ZASSERT(vertex_elements_count <= semantics_sources.size(), L"[ZColladaMesh] ::parseLibraryGeometries() error.");

	// Calculate Vertex size
	size_t vertex_size = 0;
	for(unsigned int s = 0; s < semantics_sources.size(); ++s) {
		SMeshSource* ms = mMeshSources[semantics_sources[s]];
		vertex_size += ms->uElementSize;
	}

	// Parse polygons indices
	//std::vector<unsigned int> vcount;
	//for(xml_node vcount_node = polygons.child(L"vcount"); vcount_node; vcount_node = vcount_node.next_sibling(L"vcount")) {
	//	std::wistringstream iss(vcount_node.child_value());
	//	while(!iss.eof()) {
	//		unsigned int vcount_item;
	//		iss >> vcount_item;
	//		vcount.push_back(vcount_item);
	//	}
	//}
	//unsigned int vcount_index = 0;

	unsigned int polygon_vertex_count = 3;
	unsigned int writing_index = 0;
	char* temp_data = static_cast<char*>(malloc(polygons_count * polygon_vertex_count * vertex_size));
	memset(temp_data, 0, polygons_count * 3 * vertex_size);
	for(xml_node p = polygons.child(L"p"); p; p = p.next_sibling(L"p")) {
		std::wistringstream iss(p.child_value());
		while(!iss.eof()) {
			polygon_vertex_count = 3;

			//if(!vcount.empty()) {
			//	ZASSERT(vcount_index < vcount.size(), L"[ZColladaMesh] parseLibraryGeometries() vcount index is out of range.");
			//	polygon_vertex_count = vcount[vcount_index];
			//	vcount_index++;
			//}

			for(unsigned int ti = 0; ti < polygon_vertex_count; ++ti) {
				for(unsigned int pi = 0; pi < vertex_elements_count; ++pi) {
					unsigned int element_index;
					iss >> element_index;
					SMeshSource* ms = mMeshSources[semantics_sources[pi]];
					char* element_data = static_cast<char*>(ms->pData) + (element_index * ms->uElementSize);
					memcpy(static_cast<void*>(temp_data + writing_index), element_data, ms->uElementSize);
					writing_index += ms->uElementSize;
				}
			}
		}
	}
	mVertexData.pVertexData = temp_data;
	mVertexData.uVertexCount = polygons_count * 3;
}

// (5) Materials library
// Geometry Material Specifier:
// library_visual_scenes->visual_scene->node->instance_geometry->
// bind_material->technique_common->instance_material(symbol/target)
//
// Material specifier:
// library_materials(id/name)->instance_effect(url)
//
// Effect specifier:
// library_effects->effect(id)->profile_COMMON->..
// - Surface: newparam(sid)->surface(sid=file_id-surface->init_from(file_id)
// - Sampler: newparam(sid)->sampler2D(sid=file_id-sampler)->source(file_id-surface)
// - Material: technique(sid)->phong->...
//			- emission: color
//			- ambient: color
//			- diffuse: texture(file_id-sampler)/color
//			- specular: color
//			- shininess: float
//			- transparency: float
//			- index_of_refraction: float
//
// Images specifier:
// library_images->image(id=file_id, name)->init_from(filename)
void ZColladaMesh::_parseLibraryMaterials(const xml_node& top_node)
{
	xml_node library_materials = top_node.child(L"library_materials");
	if(!library_materials) return;
	std::unordered_map<std::wstring, xml_node> materials;
	for(xml_node m = library_materials.child(L"material"); m; m = m.next_sibling(L"material")) {
		materials.insert(std::pair<std::wstring, xml_node>(
			_hashlessID(m.attribute(L"id").as_string()), m));
	}
	if(materials.size() == 0) return;
	xml_node library_effects = top_node.child(L"library_effects");
	std::unordered_map<std::wstring, xml_node> effects;
	for(xml_node e = library_effects.child(L"effect"); e; e = e.next_sibling(L"effect")) {
		effects.insert(std::pair<std::wstring, xml_node>(
			_hashlessID(e.attribute(L"id").as_string()), e));
	}
	xml_node library_images = top_node.child(L"library_images");
	std::unordered_map<std::wstring, std::wstring> textures;
	for(xml_node i = library_images.child(L"image"); i; i = i.next_sibling(L"image")) {
		xml_node texture_file = i.child(L"init_from");
		if(texture_file) {
			textures.insert(std::pair<std::wstring, std::wstring> (
				_hashlessID(i.attribute(L"id").as_string()), 
				texture_file.child_value()));
		}
	}

	for(auto& m : materials) {
		std::wstring effect_url = _hashlessID(m.second.child(L"instance_effect").attribute(L"url").as_string());
		ZASSERT(effects.find(effect_url) != effects.end(), L"[ZColladaMesh] ::_parseLibraryMaterials() sanity check failed.");
		xml_node effect = effects[effect_url];
		xml_node effect_details = effect.child(L"profile_COMMON");
		if(!effect_details) continue;
		std::unordered_map<std::wstring, xml_node> newparams;
		for(xml_node np = effect_details.child(L"newparam"); np; np = np.next_sibling()) {
			newparams.insert(std::pair<std::wstring, xml_node>(
				np.attribute(L"sid").as_string(),
				np));
		}
		xml_node technique = effect_details.child(L"technique");
		if(!technique) continue;
		SMaterialDesc* mtrl = znew(SMaterialDesc, ());
		xml_node ambient_node = technique.child(L"phong").child(L"ambient");
		xml_node diffuse_node = technique.child(L"phong").child(L"diffuse");
		xml_node specular_node = technique.child(L"phong").child(L"specular");
		xml_node shininess_node = technique.child(L"phong").child(L"shininess");
		xml_node transparency_node = technique.child(L"phong").child(L"transparency");
		xml_node index_of_refraction_node = technique.child(L"phong").child(L"index_of_refraction");
		if(ambient_node) {
			if(ambient_node.child(L"color")) {
				mtrl->colorAmbient = _parseColor(ambient_node.child(L"color").child_value());
			}
		}
		if(diffuse_node) {
			if(diffuse_node.child(L"color")) {
				mtrl->colorDiffuse = _parseColor(diffuse_node.child(L"color").child_value());
			}
			if(diffuse_node.child(L"texture")) {
				// TODO assuming now that there is only one set of textures always for set 0
				// Otherwise you need to get it via texcoord property which points to:
				// library_visual_scenes->visual_scene->node->instance_geometry>bind_material
				// ->technique_common->instance_material->bind_vertex_input
				// Attributes: semantic=texcoord, input_semantic="TEXCOORD", and input_set="0"
				xml_node sampler_node = newparams[
					diffuse_node.child(L"texture").attribute(L"texture").as_string()];
				xml_node surface_node = newparams[
					sampler_node.child(L"sampler2D").child(L"source").child_value()];
				// Assumed to always have 2D type textures
				std::wstring texture_id = 
					surface_node.child(L"surface").child(L"init_from").child_value();
				mtrl->mapDiffuseTextures.insert(
					std::pair<std::wstring, std::wstring>(texture_id, textures[texture_id]));
			}
		}
		if(specular_node) {
			if(specular_node.child(L"color")) {
				mtrl->colorSpecular = _parseColor(specular_node.child(L"color").child_value());
			}
		}
		if(shininess_node) {
			std::wistringstream ss(shininess_node.child(L"float").child_value());
			ss >> mtrl->fShininess;
		}
		if(transparency_node) {
			std::wistringstream ss(transparency_node.child(L"float").child_value());
			ss >> mtrl->fTransparency;
		}
		if(index_of_refraction_node) {
			std::wistringstream ss(index_of_refraction_node.child(L"float").child_value());
			ss >> mtrl->fIndexOfRefraction;
		}
		mMaterials.insert(std::pair<std::wstring, SMaterialDesc*>(m.first, mtrl));
	}
}


////////////////////////////////////////////////////////////////////
/// Creates actual device primitive objects and materials for rendering
///
void ZColladaMesh::_createMesh()
{
	ZASSERT(!vVertexDeclarations.empty() && mVertexData.pVertexData && 
		mVertexData.uVertexCount > 0 && !bMeshCreated, 
		L"[ZColladaMesh] ::_createMesh() sanity check failed.");
	unsigned int subset = _createSubset();
	ZVertexDeclaration& vd = _getVD(subset);
	ZVertexBuffer& vb = _getVB(subset);
	//ZIndexBuffer& ib = _getIB(subset);
	ZMaterial& mtrl = _getMaterial(subset);

	// Create vertex declaration
	for(auto& d : vVertexDeclarations) {
		vd.pushElement(d.ucDeclarationType, d.ucDeclarationUsage, d.ucSet);
	}
	vd.pushEndElement();

	// Create vertex buffer
	vb.create(vd, mVertexData.uVertexCount);
	vb.writeVertices(0, mVertexData.pVertexData, mVertexData.uVertexCount);
	free(mVertexData.pVertexData);
	mVertexData.pVertexData = nullptr;
	mVertexData.uVertexCount = 0;

	// Create materials
	// TODO It should create material for this subset only distinguished by geometry material id
	if(mMaterials.size() > 0) {		
		mtrl.create(*(mMaterials.begin()->second));
	}

	bMeshCreated = true;
}

std::wstring ZColladaMesh::_hashlessID(const std::wstring& hashedId)
{
	if(hashedId[0] == L'#') {
		std::wstring out_str = hashedId;
		out_str.erase(0, 1);
		return out_str;
	} else {
		return hashedId;
	}
}

float* ZColladaMesh::_parseFloatArray(const std::wstring& textData, unsigned int count)
{
	ZASSERT(!textData.empty() && count > 0, L"[ZColladaMesh] ::parseFloatArray() invalid data given.");
	float* data = znew_array(float, count);
	std::wistringstream is(textData);
	for(unsigned int i = 0; i < count; ++i) {
		is >> data[i];
	}
	return data;
}

XMFLOAT4 ZColladaMesh::_parseColor(const std::wstring& textData)
{
	ZASSERT(!textData.empty(), L"[ZColladaMesh] ::parseColor() invalid data given.");
	float data [4] = {0.0f, 0.0f, 0.0f, 1.0f};
	std::wistringstream is(textData);
	for(unsigned int i = 0; i < 4; ++i) {
		is >> data[i];
	}
	return XMFLOAT4(data[0], data[1], data[2], data[3]);
}

unsigned char ZColladaMesh::_getDeclType(ESourceDataType dataType, unsigned int elementStride)
{
	unsigned char decl_type = 0;
	switch(dataType) {
		case DataType_Float:
			switch(elementStride) {
				case 1:
					decl_type = VertexDeclType::Float1;
					break;
			
				case 2:
					decl_type = VertexDeclType::Float2;
					break;

				case 3:
					decl_type = VertexDeclType::Float3;
					break;

				case 4:
					decl_type = VertexDeclType::Float4;
					break;
			}
			break;

		default:
			std::wostringstream ss;
			ss << L"ZColladaMesh::getDeclType() unsupported inputs." << std::endl;
			ss << L"Data Type: \"" << dataType << std::endl;
			ss << L"Element Stride: \"" << elementStride;
			throw ZEXCEPTION_MESSAGE(ss.str());
	}
	return decl_type;
}

void ZColladaMesh::_updatePositionDataForUpAxis(void* positionData, unsigned int elementCount, unsigned int elementStride)
{
	// TODO: check conversion function when nodes and transformation are functioning (currently model is displayed in model space)
	if(eUpAxis == UpAxis_Y) return;
	// get rid of annoying warnings
	positionData; 
	elementCount;
	elementStride;
	// Assuming position data to always be full float (4 bytes)
	/*float* data = static_cast<float*>(positionData);
	float* element_data;
	float temp;
	for(unsigned int i = 0; i < elementCount; i+=elementStride) {
		element_data = &data[i];
		if(eUpAxis == UpAxis_Z) {
			temp = element_data[1];
			element_data[1] = -element_data[2];
			element_data[2] = temp;
		} else if(eUpAxis == UpAxis_X) {
		} else {
			throw new ZEXCEPTION_MESSAGE(L"ZColladaMesh::_updatePositionDataForUpAxis() error occured.");
		}
	}*/
}