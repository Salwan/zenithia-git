////////////////////////////////////////////////////////////////
// Name: ZManualMesh
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZMesh.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @class ZManualMesh
	/// @brief
	/// 
	class ZManualMesh : public ZMesh
	{
	public:
		ZManualMesh();
		virtual ~ZManualMesh();

		unsigned int createSubset() {return _createSubset();}
		ZVertexDeclaration& getVertexDeclaration(unsigned int subset) {return _getVD(subset);}
		ZVertexBuffer& getVertexBuffer(unsigned int subset) {return _getVB(subset);}
		ZIndexBuffer& getIndexBuffer(unsigned int subset) {return _getIB(subset);}
		ZMaterial& getMaterial(unsigned int subset) {return _getMaterial(subset);}

	private:
		ZManualMesh(const ZManualMesh&){}
		ZManualMesh& operator=(const ZManualMesh&){}

	};
};

