////////////////////////////////////////////////////////////////
// Name: ZModel
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ZModel.h"
#include "../utils/ZAssert.h"
#include "../resources/ZResource.h"
#include "ZMesh.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default constructor
///
ZModel::ZModel(ZResource* modelResource)
	: mModel(modelResource)
{
	ZASSERT(mModel != nullptr, L"[ZModel] constructor passed null model resource.");
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZModel::~ZModel()
{
}

void ZModel::drawSubset(IDirect3DDevice9& device, unsigned int subset) 
{
	const_cast<ZMesh*>(mModel->getModel())->drawSubset(device, subset);
}

void ZModel::drawAll(IDirect3DDevice9& device)
{
	const_cast<ZMesh*>(mModel->getModel())->drawAll(device);
}

unsigned int ZModel::getSubsetCount() const
{
	return mModel->getModel()->getSubsetCount();
}
