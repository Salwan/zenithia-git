////////////////////////////////////////////////////////////////
// Name: ZEmptyMesh
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <vector>
#include "ZMesh.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @class ZEmptyMesh
	/// @brief
	/// 
	class ZEmptyMesh : public ZMesh
	{
	public:
		ZEmptyMesh() {}
		virtual ~ZEmptyMesh() {}

		// Methods
		virtual void drawSubset(IDirect3DDevice9&, unsigned int) override {};
		virtual void drawAll(IDirect3DDevice9&) override {};
		virtual unsigned int getSubsetCount() const override { return 0; };

	private:
		ZEmptyMesh(const ZEmptyMesh&){}
		ZEmptyMesh& operator=(const ZEmptyMesh&){}

	};
};

