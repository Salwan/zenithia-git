////////////////////////////////////////////////////////////////
// Name: ZMaterial
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "stdafx.h"
#include "ZMaterial.h"
#include "../resources/ZResource.h"
#include "../resources/ZResourceSystem.h"
#include "../d3d9/ZTexture.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZMaterial::ZMaterial()
{

}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZMaterial::~ZMaterial()
{
	
}

void ZMaterial::create(const SMaterialDesc& fromMaterial)
{
	mMaterialDesc = fromMaterial;
	for(auto& dt : mMaterialDesc.mapDiffuseTextures) {
		ZResource* resource = ZResourceSystem::instance()->loadTexture(dt.first, dt.second);
		vDiffuseTextures.push_back(resource);
	}
}

void ZMaterial::set()
{
	if(vDiffuseTextures.size() > 0) {
		ZTexture::setTexture(0, vDiffuseTextures[0]);
	}
}
