////////////////////////////////////////////////////////////////
// Name: ZModel
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "../d3d9/ZD3D9.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZResource;

	////////////////////////////////////////////////////////////////
	/// @class ZEmptyMesh
	/// @brief
	/// 
	class ZModel
	{
	public:
		ZModel(ZResource* resource);
		virtual ~ZModel();

		// Methods
		void drawSubset(IDirect3DDevice9&, unsigned int);
		void drawAll(IDirect3DDevice9&);
		unsigned int getSubsetCount() const;

	private:
		ZModel(const ZModel&){}
		ZModel& operator=(const ZModel&){}

		ZResource* mModel;
	};
};

