////////////////////////////////////////////////////////////////
// Name: ZMaterial
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <vector>
#include <unordered_map>
#include "../d3d9/ZD3D9.h"

namespace zen
{
	class ZResource;
	// Geometry Material Specifier:
	// library_visual_scenes->visual_scene->node->instance_geometry->
	// bind_material->technique_common->instance_material(symbol/target)
	//
	// Material specifier:
	// library_materials(id/name)->instance_effect(url)
	//
	// Effect specifier:
	// library_effects->effect(id)->profile_COMMON->..
	// - Surface: newparam(sid)->surface(sid=file_id-surface->init_from(file_id)
	// - Sampler: newparam(sid)->sampler2D(sid=file_id-sampler)->source(file_id-surface)
	// - Material: technique(sid)->phong->...
	//			- emission: color
	//			- ambient: color
	//			- diffuse: texture(file_id-sampler)/color
	//			- specular: color
	//			- shininess: float
	//			- transparency: float
	//			- index_of_refraction: float
	//
	// Images specifier:
	// library_images->image(id=file_id, name)->init_from(filename)

	////////////////////////////////////////////////////////////////
	/// @struct SMaterialDesc
	/// @brief
	/// 
	struct SMaterialDesc
	{
		SMaterialDesc() : fShininess(50.0f), fTransparency(1.0f), fIndexOfRefraction(1.0f) 
		{
			colorAmbient = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
			colorDiffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
			colorEmission = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
			colorSpecular = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
			fShininess = 50.0f;
			fTransparency = 1.0f;
			fIndexOfRefraction = 1.0f;
		}
		// Alias  ->  Texture file name
		std::unordered_map<std::wstring, std::wstring> mapDiffuseTextures;
		XMFLOAT4 colorEmission;
		XMFLOAT4 colorAmbient;
		XMFLOAT4 colorDiffuse;
		XMFLOAT4 colorSpecular;
		float fShininess;
		float fTransparency;
		float fIndexOfRefraction;
	};

	////////////////////////////////////////////////////////////////
	/// @class ZMaterial
	/// @brief
	/// 
	class ZMaterial 
	{
	public:
		ZMaterial();
		virtual ~ZMaterial();
		
		void create(const SMaterialDesc& fromMaterial);
		void set();
		SMaterialDesc& getDesc() {return mMaterialDesc;}

	private:
		ZMaterial(const ZMaterial&){}
		ZMaterial& operator=(const ZMaterial&){}		

		SMaterialDesc mMaterialDesc;
		std::vector<ZResource*> vDiffuseTextures;
	};
};
