////////////////////////////////////////////////////////////////
// Name: ZMesh
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include <iostream>
#include "ZMesh.h"
#include "../d3d9/ZVertexBuffer.h"
#include "../d3d9/ZVertexDeclaration.h"
#include "../d3d9/ZIndexBuffer.h"
#include "ZMaterial.h"
#include "../utils/ZAssert.h"
#include "../exceptions/ZException.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZMesh::ZMesh()
{

}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZMesh::~ZMesh()
{
	for(unsigned int i = 0; i < mVBufs.size(); ++i)
	{
		zdelete(mVDecls[i]);
		zdelete(mVBufs[i]);
		zdelete(mIBufs[i]);
		if(mMaterials.size() > 0) {
			zdelete(mMaterials[i]);
		}
	}
}

////////////////////////////////////////////////////////////////
/// Draws an indexed mesh subset.
/// 
void ZMesh::drawSubset(IDirect3DDevice9& device, unsigned int subset)
{
	ZASSERT(!mVBufs.empty(), L"ZMesh::drawSubset() there are no defined subsets!");
	ZASSERT(subset < mVBufs.size(), L"ZMesh::drawSubset() given subset is invalid");
	
	_getVD(subset).set();
	ZVertexBuffer& vb = _getVB(subset);
	ZIndexBuffer& ib = _getIB(subset);
	if(mMaterials.size() > 0) {
		ZMaterial& mtrl = _getMaterial(subset);
		mtrl.set();
	}
	vb.set();
	if(!ib.empty()) {
		ib.set();
		device.DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, vb.getVertexCount(),
			0, ib.getFaceCount());
	} else {
		unsigned int triangles_count = vb.getVertexCount() / 3;
		device.DrawPrimitive(D3DPT_TRIANGLELIST, 0, triangles_count);
	}
}

////////////////////////////////////////////////////////////////
/// Draws all subsets within this mesh.
/// 
void ZMesh::drawAll(IDirect3DDevice9& device)
{
	ZASSERT(!mVBufs.empty(), L"ZMesh::drawAll() there are no defined subsets!");
	for(unsigned int i = 0; i < mVBufs.size(); ++i)
	{
		drawSubset(device, i);
	}
}

////////////////////////////////////////////////////////////////
/// @return the number of defined subsets
/// 
unsigned int ZMesh::getSubsetCount() const
{
	return mVBufs.size();
}

unsigned int ZMesh::_createSubset()
{
	mVDecls.push_back(znew(ZVertexDeclaration, ()));
	mVBufs.push_back(znew(ZVertexBuffer, ()));
	mIBufs.push_back(znew(ZIndexBuffer, ()));
	mMaterials.push_back(znew(ZMaterial, ()));
	return mVBufs.size() - 1;
}

ZVertexDeclaration& ZMesh::_getVD(unsigned int subset)
{
	if(mVDecls.empty())
	{
		throw ZEXCEPTION_MESSAGE(L"ZMesh::getVertexDeclaration() there are no defined subsets!");
	}
	if(subset >= mVDecls.size())
	{
		throw ZEXCEPTION_MESSAGE(L"ZMesh::getVertexDeclaration() given subset is invalid");
	}
	return *(mVDecls[subset]);
}

ZVertexBuffer& ZMesh::_getVB(unsigned int subset)
{
	if(mVBufs.empty())
	{
		throw ZEXCEPTION_MESSAGE(L"ZMesh::getVertexBuffer() there are no defined subsets!");
	}
	if(subset >= mVBufs.size())
	{
		throw ZEXCEPTION_MESSAGE(L"ZMesh::getVertexBuffer() given subset is invalid");
	}
	return *(mVBufs[subset]);
}

ZIndexBuffer& ZMesh::_getIB(unsigned int subset)
{
	if(mIBufs.empty())
	{
		throw ZEXCEPTION_MESSAGE(L"ZMesh::getIndexBuffer() there are no defined subsets!");
	}
	if(subset >= mIBufs.size())
	{
		throw ZEXCEPTION_MESSAGE(L"ZMesh::getIndexBuffer() given subset is invalid");
	}
	return *(mIBufs[subset]);
}

ZMaterial& ZMesh::_getMaterial(unsigned int subset)
{
	if(mMaterials.empty())
	{
		throw ZEXCEPTION_MESSAGE(L"ZMesh::getMaterial() there are no defined subsets!");
	}
	if(subset >= mMaterials.size())
	{
		throw ZEXCEPTION_MESSAGE(L"ZMesh::getMaterial() given subset is invalid");
	}
	return *(mMaterials[subset]);
}