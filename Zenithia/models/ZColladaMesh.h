////////////////////////////////////////////////////////////////
// Name: ZColladaMesh
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <string>
#include <unordered_map>
#include "../pugixml/pugixml.hpp"
#include "ZMesh.h"

//////////////////////////////////////////////////// Definitions

namespace zen
{
	struct SMaterialDesc;

	////////////////////////////////////////////////////////////////
	/// @class ZColladaMesh
	/// @brief Collada parser represented as a ZMesh.
	/// 
	class ZColladaMesh : public ZMesh
	{
	public:
		explicit ZColladaMesh(const std::wstring& colladaFile, bool autoCreateMesh = true);
		virtual ~ZColladaMesh();

		void createMesh();

	private:
		ZColladaMesh(const ZColladaMesh&){}
		ZColladaMesh& operator=(const ZColladaMesh&){}
		
		// Collada data type
		enum ESourceDataType
		{
			DataType_Float,
			DataType_Int,
		};
		
		void _loadFromFile(const std::wstring& colladaFile);
		void _parseColladaMesh(const std::wstring& colladaFile);

		void _parseAsset(const pugi::xml_node& node);
		void _parseScene(const pugi::xml_node& node);
		void _parseLibraryVisualScenes(const pugi::xml_node& node, const std::wstring& visualSceneId);
		void _parseLibraryGeometries(const pugi::xml_node& node);
		void _parseLibraryMaterials(const pugi::xml_node& top_node);
		std::wstring _hashlessID(const std::wstring& hashedId);
		float* _parseFloatArray(const std::wstring& textData, unsigned int count);
		XMFLOAT4 _parseColor(const std::wstring& textData);
		unsigned char _getDeclType(ESourceDataType dataType, unsigned int elementStride);
		void _updatePositionDataForUpAxis(void* positionData, unsigned int elementCount, unsigned int elementStride);
		void _createMesh();

		// Asset
		std::wstring strTitle;		
		enum EColladaUpAxis
		{
			UpAxis_X,
			UpAxis_Y,
			UpAxis_Z,
		};
		EColladaUpAxis eUpAxis;

		// Scene
		std::wstring strInstanceVisualSceneURL;

		// Library Visual Scenes
		std::vector<std::wstring> strGeometryURLs;

		// Map/Structure to hold all necessary information about each mesh source
		struct SMeshSource
		{
			void* pData;
			unsigned int uCount;
			unsigned int uElementCount;
			unsigned int uElementStride;
			unsigned int uElementSize;
			unsigned int uDataTypeSize;
			ESourceDataType eDataType;
			std::vector<std::wstring> vParamNames;
		};
		std::unordered_map<std::wstring, SMeshSource*> mMeshSources;

		// Material/Effect information
		std::unordered_map<std::wstring, SMaterialDesc*> mMaterials;

		// Temporary mesh data for defferred creation
		bool bAutoCreateMesh;
		bool bMeshCreated;
		struct SVertexDeclaration
		{
			unsigned char ucDeclarationType;
			unsigned char ucDeclarationUsage;
			unsigned char ucSet;
		};
		std::vector<SVertexDeclaration> vVertexDeclarations;
		struct SMeshData
		{
			SMeshData() : pVertexData(nullptr), uVertexCount(0) {}
			char* pVertexData;
			unsigned int uVertexCount;
		};
		SMeshData mVertexData;
	};
};

