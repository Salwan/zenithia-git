////////////////////////////////////////////////////////////////
// Name: ZMesh
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <vector>
#include "../d3d9/ZD3D9.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZVertexDeclaration;
	class ZVertexBuffer;
	class ZIndexBuffer;
	class ZMaterial;

	////////////////////////////////////////////////////////////////
	/// @class ZMesh
	/// @brief
	/// 
	class ZMesh 
	{
	public:
		ZMesh();
		virtual ~ZMesh();

		// Methods
		virtual void drawSubset(IDirect3DDevice9& device, unsigned int subset);
		virtual void drawAll(IDirect3DDevice9& device);
		virtual unsigned int getSubsetCount() const;

	protected:
		// Methods
		unsigned int _createSubset();
		ZVertexDeclaration& _getVD(unsigned int subset);
		ZVertexBuffer& _getVB(unsigned int subset);
		ZIndexBuffer& _getIB(unsigned int subset);
		ZMaterial& _getMaterial(unsigned int subset);

	private:
		ZMesh(const ZMesh&){}
		ZMesh& operator=(const ZMesh&){}

		std::vector<ZVertexDeclaration*> mVDecls;
		std::vector<ZVertexBuffer*> mVBufs;
		std::vector<ZIndexBuffer*> mIBufs;
		std::vector<ZMaterial*> mMaterials;
	};
};

