////////////////////////////////////////////////////////////////
// Name: ZError
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <string>

//////////////////////////////////////////////////// Definitions
namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @class ZError
	/// @brief Provides error utilities relevant to ZGUI responsibilities.
	/// 
	class ZError 
	{
	public:
		virtual ~ZError(){}

		static void getWindowsErrorMessage(unsigned long windowsErrorMessage, 
			std::wstring& stringOut);
	
		static void getLastWindowsErrorMessage(std::wstring& stringOut);

		static void showError(const std::wstring& errorMessage);

	private:
		ZError(){}
		ZError(const ZError&){}
		ZError& operator=(const ZError&){}

	};
};
