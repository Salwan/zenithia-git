////////////////////////////////////////////////////////////////
// Name: ZError
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ZError.h"
#include <sstream>
#include <Windows.h>

using namespace zen;

////////////////////////////////////////////////////////////////
/// Retrieves and formats a windows error message code (DWORD)
/// @param [in] windows_error_message windows error message (DWORD)
/// @param [out] string_out the output string that will receive the message
///
void ZError::getWindowsErrorMessage(unsigned long windowsErrorMessage, 
	std::wstring& stringOut)
{
	std::wstringstream ss;
	wchar_t* error_buffer = znew_array(wchar_t, 129);
	if(!FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM, NULL, windowsErrorMessage, 
		0, (LPWSTR)error_buffer, 128, NULL))
	{
		ss<<L"Couldn't retrieve error message: "<<windowsErrorMessage;
		stringOut = ss.str();
		OutputDebugStringW(stringOut.c_str());
	}
	else
	{
		ss<<error_buffer;
		stringOut = ss.str();
	}
	zdelete_array(error_buffer);
}

////////////////////////////////////////////////////////////////
/// Retrieves and formats the last windows error via GetLast Error.
/// @param [out] string_out the output string that will receive the message
///
void ZError::getLastWindowsErrorMessage(std::wstring& stringOut)
{
	getWindowsErrorMessage(GetLastError(), stringOut);
}

////////////////////////////////////////////////////////////////
/// Shows a simple message box with the error message.
/// @param error_message the error message to show.
///
void ZError::showError(const std::wstring& errorMessage)
{
	MessageBoxW(NULL, errorMessage.c_str(), L"Error", MB_OK|MB_ICONERROR);
}
