////////////////////////////////////////////////////////////////
// Name: ZException
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ZException.h"
#include <Windows.h>
#include <DxErr.h>

using namespace zen;

////////////////////////////////////////////////////////////////
/// Retrieves and formats an HRESULT DirectX error message.
///
ZException DXException(long hresult, const wchar_t* file, int line)
{
	std::wstringstream ss;
	ss << L"DirectX Error" << std::endl;
	ss << L"File: " << file << std::endl;
	ss << L"Line: " << line << std::endl;
	ss << L"Error: " << ::DXGetErrorStringW(hresult) << std::endl;
	ss << L"Details: " << ::DXGetErrorDescriptionW(hresult) << std::endl;
	return ZException(ss.str().c_str());
}