////////////////////////////////////////////////////////////////
// Name: ZException
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <string>
#include <sstream>
#include "../ZDefs.h"
#include "ZError.h"

//////////////////////////////////////////////////// Definitions
#define ZEXCEPTION_MESSAGE(message) new zen::ZException(message, __WFILE__, __LINE__)
#define ZEXCEPTION_ERROR(error) new zen::ZException(static_cast<unsigned long>(error), __WFILE__, __LINE__);
#define ZEXCEPTION_DXERROR(hresult) DXException(hresult, __WFILE__, __LINE__)

namespace zen
{

	////////////////////////////////////////////////////////////////
	/// @class ZException
	/// @brief a useful exception class with formatting capabilities.
	/// Use macros: 
	/// - ZEXCEPTION_MESSAGE to pass a string message
	/// - ZEXCEPTION_ERROR to pass a windows error
	///
	class ZException
	{
	public:
		ZException(const std::wstring& message)
		{
			strMessage = message;
		}

		ZException(const std::wstring& message, const wchar_t* file, int line)
		{
			std::wstringstream ss;
			ss<<L"File: "<<file<<std::endl;
			ss<<L"Line: "<<line<<std::endl;
			ss<<L"Message: "<<message<<std::endl;
			strMessage = ss.str();
			strMessage = message;
		}

		ZException(unsigned long windowsErrorCode, const wchar_t* file, int line)
		{
			std::wstringstream ss;
			ss<<L"File: "<<file<<std::endl;
			ss<<L"Line: "<<line<<std::endl;
			ZError::getWindowsErrorMessage(windowsErrorCode, strMessage);
			ss<<L"Message: "<<strMessage;
			strMessage = ss.str();
		}

		virtual ~ZException(){}

		const std::wstring& getMessage() const {return strMessage;}

	private:
		ZException(){}
		// default public copy constructor and assignment constructor are ok

		std::wstring strMessage;
	};
};

/////////////////////////////////////////////////////////////////
/// Create DirectX Exception via hresult, use macro ZEXCEPTION_DXERROR
///
zen::ZException DXException(long hresult, const wchar_t* file, int line);

