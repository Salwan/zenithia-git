////////////////////////////////////////////////////////////////
// Name: ZLightNode
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "../ZNode.h"
#include "../../d3d9/ZLight.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @class ZLightNode
	/// @brief
	/// 
	class ZLightNode : public ZNode
	{
		friend class ZNode;
		friend class ZSceneGraph;

	public:
		virtual ~ZLightNode();

		ZLight& getLight() {return mLight;}

	private:
		ZLightNode(ZSceneGraph* scene_graph, const ZLight& light);
		ZLightNode& operator=(const ZLightNode&){}

		ZLight mLight;
	};
};
