////////////////////////////////////////////////////////////////
// Name: ZCameraNode
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "stdafx.h"
#include "ZCameraNode.h"
#include "../../d3d9/ZD3D9.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZCameraNode::ZCameraNode(ZSceneGraph* scene_graph) : ZNode(scene_graph)
{
	XMFLOAT4 default_right(1.0f, 0.0f, 0.0f, 0.0f);
	XMFLOAT4 default_up(0.0f, 1.0f, 0.0f, 0.0f);
	XMFLOAT4 default_look(0.0f, 0.0f, 1.0f, 0.0f);
	XMFLOAT4 default_pos(0.0f, 0.0f, -10.0f, 0.0f);
	vRight = XMLoadFloat4(&default_right);
	vUp = XMLoadFloat4(&default_up);
	vLook = XMLoadFloat4(&default_look);
	vPosition = XMLoadFloat4(&default_pos);	
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZCameraNode::~ZCameraNode()
{
}

void ZCameraNode::strafeHorizontal(float units)
{
	vPosition += vRight * units;
}

void ZCameraNode::strafeVertical(float units)
{
	vPosition += vUp * units;
}

void ZCameraNode::walk(float units)
{
	vPosition += vLook * units;
}

void ZCameraNode::pitch(float angle)
{
	XMMATRIX t = XMMatrixRotationAxis(vRight, angle);
	vUp = XMVector3TransformCoord(vUp, t);
	vLook = XMVector3TransformCoord(vLook, t);
}

void ZCameraNode::yaw(float angle)
{
	XMMATRIX t;
	t = XMMatrixRotationY(angle);
	vRight = XMVector3TransformCoord(vRight, t);
	vLook = XMVector3TransformCoord(vLook, t);
}

XMMATRIX ZCameraNode::getViewMatrix()
{
	// Keep axes orthognal to each other
	vLook = XMVector3Normalize(vLook);
	vUp = XMVector3Cross(vLook, vRight);
	vUp = XMVector3Normalize(vUp);
	vRight = XMVector3Cross(vUp, vLook);
	vRight = XMVector3Normalize(vRight);

	return XMMatrixLookToLH(vPosition, vLook, vUp);
}

XMVECTOR ZCameraNode::getPosition() const
{
	return vPosition;
}

void ZCameraNode::setPosition(const XMVECTOR& position)
{
	vPosition = position;
}

XMVECTOR ZCameraNode::getRightVector() const
{
	return vRight;
}

XMVECTOR ZCameraNode::getUpVector() const
{
	return vUp;
}

XMVECTOR ZCameraNode::getLookVector() const
{
	return vLook;
}

