////////////////////////////////////////////////////////////////
// Name: ZCameraNode
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <Windows.h>
#include <xnamath.h>
#include "../ZNode.h"

#if defined(_MSC_VER)
#pragma warning(push) // Save warning settings.
#pragma warning(disable : 4324) // "structure was padded due to __declspec(align())"
#endif 

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZSceneGraph;
	class ZNode;

	////////////////////////////////////////////////////////////////
	/// @class ZCameraNode
	/// @brief
	/// 
	class ZCameraNode : public ZNode
	{
		friend class ZSceneGraph;
		friend class ZNode;

	public:
		virtual ~ZCameraNode();

		void strafeHorizontal(float units);
		void strafeVertical(float units);
		void walk(float units);
		void pitch(float angle);
		void yaw(float angle);

		XMMATRIX getViewMatrix();
		XMVECTOR getPosition() const;
		void setPosition(const XMVECTOR& position);
		XMVECTOR getRightVector() const;
		XMVECTOR getUpVector() const;
		XMVECTOR getLookVector() const;

	private:
		ZCameraNode(ZSceneGraph* scene_graph);
		//ZCameraNode(const ZCameraNode&){}
		ZCameraNode& operator=(const ZCameraNode&){}
				
		__declspec(align(16)) XMVECTOR vRight;
		__declspec(align(16)) XMVECTOR vUp;
		__declspec(align(16)) XMVECTOR vLook;
		__declspec(align(16)) XMVECTOR vPosition;
	};
};

#if defined(_MSC_VER)
#pragma warning(pop) // Restore warnings to previous state.
#endif 