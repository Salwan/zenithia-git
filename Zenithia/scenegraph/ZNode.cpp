////////////////////////////////////////////////////////////////
// Name: ZNode
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include "ZNode.h"
#include "nodes/ZCameraNode.h"
#include "nodes/ZLightNode.h"
#include "components/ZCTransform.h"
#include "components/ZCModel.h"
#include "components/ZCMesh.h"
#include "components/ZCLogic.h"
#include "../resources/ZResource.h"
#include "../effects/ZEffect.h"
#include "../d3d9/ZLight.h"
#include "../resources/ZRModel.h"
#include "../resources/ZRMesh.h"
#include "../utils/ZMemoryDebug.h"
#include "../utils/ZAssert.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZNode::ZNode(ZSceneGraph* scene_graph)
	: pSceneGraph(scene_graph), 
	cTransform(znew(ZCTransform, (this))), 
	cMesh(nullptr), cModel(nullptr), cLogic(nullptr)
{
	ZASSERT(pSceneGraph != nullptr, L"Given scene graph is null!");
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZNode::~ZNode()
{
	DELETE_OBJECT(cTransform);
	DELETE_OBJECT(cMesh);
	DELETE_OBJECT(cModel);
	DELETE_OBJECT(cLogic);
	for(auto c = lChildren.begin(); c != lChildren.end(); ++c) {
		zdelete(*c);
	}
}

////////////////////////////////////////////////////////////////
/// Scene Graph
///
unsigned int ZNode::numChildren() const
{
	return lChildren.size();
}

ZNode* ZNode::addChild()
{
	ZNode* child = znew(ZNode, (pSceneGraph));
	lChildren.push_back(child);
	return child;
}

ZCameraNode* ZNode::addChildCamera()
{
	ZCameraNode* child_camera = znew(ZCameraNode, (pSceneGraph));
	lChildren.push_back(child_camera);
	return child_camera;
}

ZLightNode* ZNode::addChildLight(const ZLight& light)
{
	ZLightNode* child_light = znew(ZLightNode, (pSceneGraph, light));
	lChildren.push_back(child_light);
	return child_light;
}

void ZNode::removeChild(ZNode* node)
{
	lChildren.remove(node);
}

/////////////////////////////////////////////////////////////////
/// Components
///
void ZNode::createMeshComponent(ZRMesh* mesh, ZEffect* effect)
{
	ZASSERT(mesh != nullptr && effect != nullptr, L"Sanity check failed.");
	if(cMesh) 
	{
		DELETE_OBJECT(cMesh);
	}
	cMesh = znew(ZCMesh, (this));
	cMesh->init(mesh, effect);
}

void ZNode::createModelComponent(ZResource* model_resource, ZResource* model_effect)
{
	if(model_resource && model_resource->getType() != ZResource::EResourceType::Resource_ColladaModel) {
		std::wostringstream ss;
		ss << L"ZNode::createModelComponent() given model resource is not a model!" <<std::endl;
		ss << L"Resource type: " << static_cast<unsigned int>(model_resource->getType());
		throw ZEXCEPTION_MESSAGE(ss.str());
	}
	if(cModel) {
		DELETE_OBJECT(cModel);
	}
	if(model_effect && model_effect->getType() != ZResource::EResourceType::Resource_Effect) {
		std::wostringstream ss;
		ss << L"ZNode::createModelComponent() given model effect is not an effect!" <<std::endl;
		ss << L"Resource type: " << static_cast<unsigned int>(model_effect->getType());
		throw ZEXCEPTION_MESSAGE(ss.str());
	}
	cModel = znew(ZCModel, (this));
	cModel->init(dynamic_cast<ZRModel*>(model_resource), model_effect);
}