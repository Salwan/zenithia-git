////////////////////////////////////////////////////////////////
// Name: ZSceneGraph
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include "ZSceneGraph.h"
#include "ZNode.h"
#include "../utils/ZMemoryDebug.h"
#include "../d3d9/ZLight.h"
#include "nodes/ZLightNode.h"
#include "../utils/ZAssert.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZSceneGraph::ZSceneGraph() : pActiveCamera(nullptr)
{
	pRootNode = znew(ZNode, (this));
	pActiveCamera = pRootNode->addChildCamera();
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZSceneGraph::~ZSceneGraph()
{
	zdelete(pRootNode);
}

////////////////////////////////////////////////////////////////
/// Processing function, called per frame, updates and renders
///
void ZSceneGraph::process()
{
	_updateParamsGeneral();
	_recurseProcess(pRootNode);
}

void ZSceneGraph::setProjection(const XMMATRIX& projection)
{
	mSceneParams.matProjection = projection;
}

void ZSceneGraph::_updateParamsGeneral()
{
	mSceneParams.matView = pActiveCamera->getViewMatrix();
	// Projection is set by the application for now
	// TODO: should be set by Zenithia
	XMVECTOR dummy_vector;
	mSceneParams.matViewInverse = XMMatrixInverse(&dummy_vector, mSceneParams.matView);
	mSceneParams.matViewProjection = XMMatrixMultiply(mSceneParams.matView, mSceneParams.matProjection);
	mSceneParams.matWorldViewProjection = mSceneParams.matViewProjection;
	// Lights
	// TODO: currently only one directional light
	if(mDirectionalLights.size() > 0) {
		ZLightNode* node = *mDirectionalLights.begin();
		const XMFLOAT4 direction = node->getLight().getDirection();
		const XMFLOAT4 diffuse = node->getLight().getDiffuse();
		mSceneParams.vecDirectionalLightDirection = XMLoadFloat4(&direction);
		mSceneParams.vecDirectionalLightDiffuse = XMLoadFloat4(&diffuse);
		mSceneParams.fDirectionalLightActive = 1.0f;
	} else {
		mSceneParams.fDirectionalLightActive = 0.0f;
	}
	// TODO: currently only one point light
	if(mPointLights.size() > 0) {
		ZLightNode* node = *mPointLights.begin();
		const XMFLOAT4 position = node->getLight().getPosition();
		const XMFLOAT4 diffuse = node->getLight().getDiffuse();
		const XMFLOAT3 attenuation = node->getLight().getAttenuation();
		mSceneParams.vecPointLightPosition = XMLoadFloat4(&position);
		mSceneParams.vecPointLightDiffuse = XMLoadFloat4(&diffuse);
		mSceneParams.vecPointLightAttenuation = XMLoadFloat3(&attenuation);
		mSceneParams.fPointLightActive = 1.0f;
	} else {
		mSceneParams.fPointLightActive = 0.0f;
	}
}

void ZSceneGraph::_updateParamsNode()
{
	XMVECTOR dummy_vector;
	mSceneParams.matWorld = matStack.top();
	mSceneParams.matWorldInverseTranspose = ::XMMatrixTranspose(XMMatrixInverse(&dummy_vector, mSceneParams.matWorld));
	mSceneParams.matWorldViewProjection = XMMatrixMultiply(matStack.top(), mSceneParams.matViewProjection);
}

void ZSceneGraph::_recurseProcess(ZNode* node)
{
	IDirect3DDevice9& device = *(ZD3D9::device);
	if(node->transform()) {
		matStack.push(node->transform()->getMatrix());
	}
	_updateParamsNode();
	if(node->logic()) {
		node->logic()->onUpdate();
		node->logic()->onPreRender(mSceneParams);
	}
	if(node->mesh()) {
		// Render
		node->mesh()->render(device);
	}
	if(node->logic()) {
		node->logic()->onPostRender();
	}
	for(auto iter = node->begin(); iter != node->end(); ++iter) {
		_recurseProcess(*iter);
	}
	if(node->transform()) {
		matStack.pop();
	}
}

void ZSceneGraph::_addLightNode(ZLightNode* light_node)
{
	switch(light_node->mLight.getType()) {
		case ZLight::ELightType::LightType_Directional:
			mDirectionalLights.push_back(light_node);
			break;

		case ZLight::ELightType::LightType_Point:
			mPointLights.push_back(light_node);
			break;

		case ZLight::ELightType::LightType_Spot:
			// TODO
			ZASSERT(false, L"Not supported yet.");
			break;
	}
}

void ZSceneGraph::_removeLightNode(ZLightNode* light_node)
{
	switch(light_node->mLight.getType()) {
		case ZLight::ELightType::LightType_Directional:
			mDirectionalLights.remove(light_node);
			break;

		case ZLight::ELightType::LightType_Point:
			mPointLights.remove(light_node);
			break;

		case ZLight::ELightType::LightType_Spot:
			// TODO
			ZASSERT(false, L"Not supported yet.");
			break;
	}
}