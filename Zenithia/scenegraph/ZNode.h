////////////////////////////////////////////////////////////////
// Name: ZNode
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <memory>
#include <list>

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZCTransform;
	class ZSceneGraph;
	class ZComponent;
	class ZCModel;
	class ZCMesh;
	class ZMesh;
	class ZRModel;
	class ZResource;
	class ZRMesh;
	class ZCameraNode;
	class ZCLogic;
	class ZEffect;
	class ZLightNode;
	class ZLight;

	////////////////////////////////////////////////////////////////
	/// @class ZNode
	/// @brief Zenithia's basic scene node
	/// 
	class ZNode
	{
		friend class ZSceneGraph;
		friend class ZCModel;
		friend class ZCMesh;
	public:
		virtual ~ZNode();

		// Components accessors
		ZCTransform* transform() const {return cTransform;}
		ZCMesh* mesh() const {return cMesh; }
		ZCModel* model() const {return cModel;}
		ZCLogic* logic() const {return cLogic; }

		// Hierarchy		
		unsigned int numChildren() const;
		ZNode* addChild();
		ZCameraNode* addChildCamera();
		ZLightNode* addChildLight(const ZLight& light);
		void removeChild(ZNode* node);

		// Iteration	
		typedef std::list<ZNode*>::iterator iterator;
		typedef std::list<ZNode*>::const_iterator const_iterator;
		iterator begin() {return lChildren.begin();}
		const_iterator begin() const {return lChildren.begin();}
		iterator end() {return lChildren.end();}
		const_iterator end() const {return lChildren.end();}

		// Component creation
		void createMeshComponent(ZRMesh* mesh, ZEffect* effect);
		void createModelComponent(ZResource* model_resource, ZResource* model_effect);
		template <class LogicClass> void createLogicComponent();

	protected:
		ZNode(ZSceneGraph* scene_graph);

		// Internal accessors
		ZSceneGraph* sceneGraph() const {return pSceneGraph;}

	private:
		ZNode(const ZNode&){}
		ZNode& operator=(const ZNode&){}

		ZSceneGraph* pSceneGraph;
		ZCTransform* cTransform;
		ZCMesh* cMesh;
		ZCModel* cModel;
		ZCLogic* cLogic;

		std::list<ZNode*> lChildren;
		std::list<ZComponent*> lComponents;
	};
	#include "ZNode.inl"
};
