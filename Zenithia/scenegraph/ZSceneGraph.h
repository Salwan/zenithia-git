////////////////////////////////////////////////////////////////
// Name: ZSceneGraph
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "../utils/ZMatrixStack.h"
#include "ZSceneParams.h"
#include "ZNode.h"
#include "nodes/ZCameraNode.h"
// Components
#include "components/ZCTransform.h"
#include "components/ZCMesh.h"
#include "components/ZCModel.h"
#include "components/ZCLogic.h"


//////////////////////////////////////////////////// Definitions
namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @class ZSceneGraph
	/// @brief Zenithia's scene graph
	/// 
	class ZSceneGraph
	{
		friend class ZLightNode;

	public:
		ZSceneGraph();
		virtual ~ZSceneGraph();

		void process();

		// Accessors
		ZNode* rootNode() const {return pRootNode;}
		const ZSceneParams& sceneParams() const {return mSceneParams;}
		ZCameraNode& activeCamera() const {return *pActiveCamera;}

		void setProjection(const XMMATRIX& projection);

	private:
		ZSceneGraph(const ZSceneGraph&){}
		ZSceneGraph& operator=(const ZSceneGraph&){}

		void _updateParamsGeneral();
		void _updateParamsNode();
		void _recurseProcess(ZNode* node);

		void _addLightNode(ZLightNode* light_node);
		void _removeLightNode(ZLightNode* light_node);

		ZMatrixStack matStack;
		ZNode* pRootNode;
		ZCameraNode* pActiveCamera;
		ZSceneParams mSceneParams;

		std::list<ZLightNode*> mDirectionalLights;
		std::list<ZLightNode*> mPointLights;
	};
};
