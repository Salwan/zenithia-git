////////////////////////////////////////////////////////////////
// Name: ZSceneParams
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include <Windows.h>
#include <xnamath.h>

//////////////////////////////////////////////////// Definitions
namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @struct ZSceneParams
	/// @brief Zenithia's scene parameters
	/// 
	__declspec(align(16)) struct ZSceneParams
	{
		ZSceneParams()
		{
			matProjection = XMMatrixIdentity();
			matView = XMMatrixIdentity();
			matViewInverse = XMMatrixIdentity();
			matViewProjection = XMMatrixIdentity();
			matWorldViewProjection = XMMatrixIdentity();
		}

		// Transformation
		XMMATRIX matProjection;
		XMMATRIX matView;
		XMMATRIX matViewInverse;
		XMMATRIX matViewProjection;
		XMMATRIX matWorld;
		XMMATRIX matWorldInverseTranspose;
		XMMATRIX matWorldViewProjection;

		// Lights
		XMVECTOR vecDirectionalLightDirection;
		XMVECTOR vecDirectionalLightDiffuse;
		XMVECTOR vecPointLightPosition;
		XMVECTOR vecPointLightDiffuse;
		XMVECTOR vecPointLightAttenuation;
		float fDirectionalLightActive;
		float fPointLightActive;
	};
};
