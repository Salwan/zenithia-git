////////////////////////////////////////////////////////////////
// Name: ZCLogic
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
// Notes:
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include "ZCLogic.h"
#include "../ZSceneParams.h"
#include "../ZNode.h"
#include "ZCModel.h"
#include "ZCTransform.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZCLogic::ZCLogic() : ZComponent(nullptr), transform(nullptr)
{

}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZCLogic::~ZCLogic()
{
	onDestroy();
}

////////////////////////////////////////////////////////////////
/// Internal initialization
///
void ZCLogic::init(ZNode* owner)
{
	pOwner = owner;
	transform = pOwner->transform();
	onInit();
}

////////////////////////////////////////////////////////////////
/// Component initialization, called when the component is created.
///
void ZCLogic::onInit()
{
}

////////////////////////////////////////////////////////////////
/// Component update, called every frame before rendering
///
void ZCLogic::onUpdate()
{

}

void ZCLogic::onPreRender(const ZSceneParams&)
{

}

void ZCLogic::onPostRender()
{

}

////////////////////////////////////////////////////////////////
/// Component destruction, called when component is destroyed
///
void ZCLogic::onDestroy()
{

}

ZEffect& ZCLogic::effect() const
{
	return *(pOwner->model()->effect());
}