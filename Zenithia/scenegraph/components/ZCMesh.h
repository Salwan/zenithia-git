////////////////////////////////////////////////////////////////
// Name: ZCMesh
// Desc: Component that represents a raw mesh.
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZComponent.h"
#include "../../events/ZEventListener.h"
#include "../../d3d9/ZD3D9.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZRMesh;
	class ZEffect;

	////////////////////////////////////////////////////////////////
	/// @class ZCMesh
	/// @brief 3D model component
	/// 
	class ZCMesh : public ZComponent
	{
		friend class ZSceneGraph;

	public:
		explicit ZCMesh(ZNode* owner);
		virtual ~ZCMesh();

		void init(ZRMesh* mesh, ZEffect* effect);

	private:
		ZCMesh(const ZCMesh&){}
		ZCMesh& operator=(const ZCMesh&){}

		void render(IDirect3DDevice9& device);

		ZRMesh* mMesh;
		ZEffect* pEffect;
	};
};
