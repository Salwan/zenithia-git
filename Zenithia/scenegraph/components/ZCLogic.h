////////////////////////////////////////////////////////////////
// Name: ZCLogic
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZComponent.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	struct ZSceneParams;
	class ZEffect;
	class ZCTransform;

	////////////////////////////////////////////////////////////////
	/// @class ZCLogic
	/// @brief Logic component
	/// 
	class ZCLogic : public ZComponent
	{
		friend class ZSceneGraph;
		friend class ZNode;
		
	public:
		explicit ZCLogic();
		virtual ~ZCLogic();

	protected:
		virtual void onInit();
		virtual void onUpdate();
		virtual void onPreRender(const ZSceneParams& sceneParams);
		virtual void onPostRender();
		virtual void onDestroy();

		ZEffect& effect() const;
		
		ZCTransform* transform;

	private:
		ZCLogic(const ZCLogic&){}
		ZCLogic& operator=(const ZCLogic&){}
		
		void init(ZNode* owner);		
	};
};
