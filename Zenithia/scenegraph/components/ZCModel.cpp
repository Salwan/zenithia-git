////////////////////////////////////////////////////////////////
// Name: ZCModel
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include "ZCModel.h"
#include "../../utils/ZAssert.h"
#include "../../exceptions/ZException.h"
#include "../../resources/ZRModel.h"
#include "../ZNode.h"
#include "../../effects/ZEffect.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZCModel::ZCModel(ZNode* owner)
	: ZComponent(owner), pModelResource(nullptr)
{
	
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZCModel::~ZCModel()
{
	DELETE_OBJECT(pModelEffect);
}

void ZCModel::onResourceLoaded(const ZEvent&)
{
	initModel();
}

void ZCModel::init(ZRModel* model_resource, ZResource* model_effect)
{
	// Effect
	ZASSERT(model_effect != nullptr, L"Given model effect resource is null!");
	ZASSERT(model_effect->getType() == ZResource::EResourceType::Resource_Effect, L"Given model effect resource is not an effect!");
	pModelEffect = znew(ZEffect, ());
	pModelEffect->setEffect(model_effect);
	
	// Model
	ZASSERT(model_resource != nullptr, L"Given model resource is null");
	if(pModelResource != nullptr) {
		std::wostringstream ss;
		ss << L"ZCModel::init() multiple init calls detected, model is already initialized." <<std::endl;
		throw ZEXCEPTION_MESSAGE(ss.str());
	}
	pModelResource = model_resource;
	if(pModelResource->isLoadingComplete()) {
		initModel();
	} else {
		pModelResource->addEventListener(ZResource::EVENT_LOADING_COMPLETE, this, 
			EVENT_HANDLER_PTR(&ZCModel::onResourceLoaded)); 
	}
}

void ZCModel::initModel()
{
	for(auto m : pModelResource->getMeshResources()) {
		ZNode* mesh = pOwner->addChild();
		mesh->createMeshComponent(m, pModelEffect);
	}
}


