////////////////////////////////////////////////////////////////
// Name: ZCMesh
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include "ZCMesh.h"
#include "../ZSceneGraph.h"
#include "../ZNode.h"
#include "../../utils/ZAssert.h"
#include "../../exceptions/ZException.h"
#include "../../models/ZMesh.h"
#include "../../models/ZManualMesh.h"
#include "../../resources/ZRMesh.h"
#include "../../effects/ZEffect.h"
#include "../../models/ZMaterial.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZCMesh::ZCMesh(ZNode* owner)
	: ZComponent(owner), mMesh(nullptr), pEffect(nullptr)
{

}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZCMesh::~ZCMesh()
{
	
}

void ZCMesh::init(ZRMesh* mesh, ZEffect* effect)
{
	ZASSERT(mesh != nullptr, L"Given mesh is null");
	if(mMesh != nullptr) {
		std::wostringstream ss;
		ss << L"ZCMesh::init() multiple init calls detected, mesh is already initialized." <<std::endl;
		throw ZEXCEPTION_MESSAGE(ss.str());
	}
	mMesh = mesh;
	ZASSERT(effect != nullptr, L"Given effect is null");
	pEffect = effect;
	pOwner->transform()->setMatrix(mMesh->matTransform);
}

void ZCMesh::render(IDirect3DDevice9& device)
{
	ZASSERT(mMesh != nullptr && pEffect != nullptr, L"Sanity check failed.");
	
	/// Zenithia common shader library
	const ZSceneParams& scene_params = pOwner->sceneGraph()->sceneParams();
	/// - Matrices
	(*pEffect)("matWorldViewProjection") = scene_params.matWorldViewProjection;
	(*pEffect)("matViewInverse") = scene_params.matViewInverse;
	(*pEffect)("matWorld") = scene_params.matWorld;
	(*pEffect)("matWorldInverseTranspose") = scene_params.matWorldInverseTranspose;
	/// - Light
	/// -- Directional
	(*pEffect)("Light_Directional_Enabled") = scene_params.fDirectionalLightActive;
	if(scene_params.fDirectionalLightActive > 0.0f) {
		(*pEffect)("Light_Directional_Direction") = scene_params.vecDirectionalLightDirection;
		(*pEffect)("Light_Directional_Diffuse") = scene_params.vecDirectionalLightDiffuse;
	}
	/// -- Point
	(*pEffect)("Light_Point_Enabled") = scene_params.fPointLightActive;
	if(scene_params.fPointLightActive > 0.0f) {
		(*pEffect)("Light_Point_Position") = scene_params.vecPointLightPosition;
		(*pEffect)("Light_Point_Diffuse") = scene_params.vecPointLightDiffuse;
		(*pEffect)("Light_Point_Attenuation0") = scene_params.vecPointLightAttenuation.m128_f32[0];
		(*pEffect)("Light_Point_Attenuation1") = scene_params.vecPointLightAttenuation.m128_f32[1];
		(*pEffect)("Light_Point_Attenuation2") = scene_params.vecPointLightAttenuation.m128_f32[2];
	}

	pEffect->beginTechnique();
	while(pEffect->nextPass()) {
		for(unsigned int i = 0; i < mMesh->mMesh->getSubsetCount(); ++i) {
			/// - Material
			SMaterialDesc& mtrl = mMesh->mMesh->getMaterial(i).getDesc();
			(*pEffect)("Material_Ambient") = mtrl.colorAmbient;
			(*pEffect)("Material_Diffuse") = mtrl.colorDiffuse;
			(*pEffect)("Material_Specular") = mtrl.colorSpecular;
			(*pEffect)("Material_Emission") = mtrl.colorEmission;
			(*pEffect)("Material_Power") = mtrl.fShininess;
			mMesh->getMesh()->drawAll(device);
		}
	}
	pEffect->endTechnique();
}



