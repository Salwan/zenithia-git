////////////////////////////////////////////////////////////////
// Name: ZCTransform
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZComponent.h"
#include "../../d3d9/ZD3D9.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	////////////////////////////////////////////////////////////////
	/// @class ZCTransform
	/// @brief Transform component
	/// 
	__declspec(align(16)) class ZCTransform : public ZComponent
	{
	public:
		explicit ZCTransform(ZNode* owner);
		virtual ~ZCTransform();

		XMMATRIX getMatrix() const;
		XMFLOAT4X4 getFloat4x4() const;
		void setMatrix(XMMATRIX& matrix);
		void setMatrix(XMFLOAT4X4& matrix);

	private:
		ZCTransform(const ZCTransform&){}
		ZCTransform& operator=(const ZCTransform&){}

		XMMATRIX mTransform;
	};
};
