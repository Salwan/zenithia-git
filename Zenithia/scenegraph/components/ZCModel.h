////////////////////////////////////////////////////////////////
// Name: ZCModel
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "ZComponent.h"
#include "../../events/ZEventListener.h"
#include "../../d3d9/ZD3D9.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZResource;
	class ZRModel;
	class ZEffect;

	////////////////////////////////////////////////////////////////
	/// @class ZCModel
	/// @brief 3D model component
	/// 
	class ZCModel : public ZComponent

	{
	public:
		explicit ZCModel(ZNode* owner);
		virtual ~ZCModel();

		void init(ZRModel* model_resource, ZResource* model_effect);

		// Accessors
		ZEffect* effect() const {return pModelEffect;}

	private:
		ZCModel(const ZCModel&){}
		ZCModel& operator=(const ZCModel&){}

		void onResourceLoaded(const ZEvent&);
		void initModel();

		ZRModel* pModelResource;
		ZEffect* pModelEffect;
	};
};
