////////////////////////////////////////////////////////////////
// Name: ZCTransform
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
// Notes:
// XMMATRIX must be aligned to 16byte boundary due to use of SIMD, 
// regs/instrs ZCTransform is declared with that in mind.
// Align declarations to 16 byte boundary:
// - MSVC: __declspec(align(16)) declarator;
// - GCC: __attribute__((aligned (16)))
// malloc and free don't like alignments, use new/delete or safer
// aligned memory allocators:
// - MSVC: _aligned_malloc
// - GCC: posix_memalign
/////////////////////////////////////////////////////// Includes
#include "StdAfx.h"
#include "ZCTransform.h"

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZCTransform::ZCTransform(ZNode* owner)
	: ZComponent(owner)
{
	mTransform = XMMatrixIdentity();
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZCTransform::~ZCTransform()
{
	
}

////////////////////////////////////////////////////////////////
/// Returns XMMATRIX
///
XMMATRIX ZCTransform::getMatrix() const
{
	return mTransform;
}

////////////////////////////////////////////////////////////////
/// Returns XMFLOAT4X4
///
XMFLOAT4X4 ZCTransform::getFloat4x4() const 
{
	XMFLOAT4X4 transform;
	XMStoreFloat4x4(&transform, mTransform);
	return transform;
}

////////////////////////////////////////////////////////////////
/// Sets matrix from an XMMATRIX
///
void ZCTransform::setMatrix(XMMATRIX& matrix)
{
	mTransform = matrix;
}

////////////////////////////////////////////////////////////////
/// Sets matrix from an XMFLOAT4X4
///
void ZCTransform::setMatrix(XMFLOAT4X4& matrix)
{
	mTransform = XMLoadFloat4x4(&matrix);
}

