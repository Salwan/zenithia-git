////////////////////////////////////////////////////////////////
// Name: ZComponent
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#pragma once

/////////////////////////////////////////////////////// Includes
#include "../../events/ZEventListener.h"

//////////////////////////////////////////////////// Definitions
namespace zen
{
	class ZNode;

	////////////////////////////////////////////////////////////////
	/// @class ZCTransform
	/// @brief Transform component
	/// 
	class ZComponent : public ZEventListener
	{
	public:
		explicit ZComponent(ZNode* owner = nullptr);
		virtual ~ZComponent();

	protected:
		ZNode* pOwner;

	private:
		ZComponent(const ZComponent&){}
		ZComponent& operator=(const ZComponent&){}

	};
};
