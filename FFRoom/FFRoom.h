////////////////////////////////////////////////////////////////
// Name: FFRoom
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _TESTAPPLICATION_HEADER_
#define _TESTAPPLICATION_HEADER_

/////////////////////////////////////////////////////// Includes
#include "Z/Zenithia.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// @class FFRoom
/// @brief
/// 
class FFRoom : public ZApplication
{
public:
	explicit FFRoom();
	virtual ~FFRoom();

	virtual void init();
	virtual void start();
	virtual void update();
	virtual void render();
	virtual void end();
	virtual void onDeviceReset();
	virtual void onWindowResize(const ZEvent& e);

protected:
	// Methods
	void setupD3D();
	void setMatrices();
	void updateDebug();
	void createRenderTarget();

private:
	FFRoom(const FFRoom&){}
	FFRoom& operator=(const FFRoom&){}

	ZD3D9* mD3D9;

	ZManualMesh mMesh;
	ZManualMesh mHUDRectMesh;
	ZManualMesh mDebugDepth;
	ZTexture* mTexFloor;
	ZTexture* mTexWall;
	ZTexture* mTexCeiling;
	ZTexture* mTexFlare;
	ZLight SceneLight;

	ID3DXMesh* pTestMesh;
	ZTexture* mTexTest;
	ZMaterial MtrlTest;
	ZLight TestLight;

	ZMaterial MtrlFloor;
	ZMaterial MtrlWall;
	ZMaterial MtrlCeiling;

	D3DXMATRIX matWorld;
	D3DXMATRIX matProj;
	D3DXMATRIX matView;
	D3DXMATRIX matWorldHUD;
	D3DXMATRIX matProjHUD;
	D3DXMATRIX matViewHUD;

	IDirect3DTexture9* pDepthStencilTexture;
	IDirect3DTexture9* pColorTexture;
	ZTexture* mDTexture;
	ZTexture* mCTexture;
};

#endif
