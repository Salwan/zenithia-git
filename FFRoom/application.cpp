#include "FFRoom.h"

INT WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, INT)
{
	try
	{
		ZLog::logToFile();
		ZWindow window(L"FFRoom by ZenithSal", 1024, 640);
		FFRoom application;
		application.run(&window);
	}
	catch(ZException& e)
	{
		ZError::showError(e.getMessage());
	}
	return 0;
}