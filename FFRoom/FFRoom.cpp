////////////////////////////////////////////////////////////////
// Name: FFRoom
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "FFRoom.h"
#include <string>
#include <sstream>
#include <iostream>

//////////////////////////////////////////////////////// Globals
IDirect3DSurface9* DeviceFrameBuffer;
IDirect3DSurface9* DeviceDepthBuffer;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
FFRoom::FFRoom() : mTexFloor(NULL), mTexWall(NULL), mTexCeiling(NULL),
				   mTexFlare(NULL), pTestMesh(NULL), mTexTest(NULL),
				   mDTexture(NULL), mCTexture(NULL), 
				   pDepthStencilTexture(NULL), pColorTexture(NULL)
{
	
}

////////////////////////////////////////////////////////////////
/// Destructor
///
FFRoom::~FFRoom()
{
	DELETE_OBJECT(mTexFloor);
	DELETE_OBJECT(mTexWall);
	DELETE_OBJECT(mTexCeiling);
	DELETE_OBJECT(mTexFlare);
	DELETE_OBJECT(mTexTest);
	DELETE_OBJECT(mDTexture);
	DELETE_OBJECT(mCTexture);
}

void FFRoom::init()
{
	mD3D9 = new ZD3D9(*pWindow);
}

void FFRoom::start()
{
	setupD3D();
	createRenderTarget();

	::D3DXCreateBox(&mD3D9->getDevice(), 3.0f, 3.0f, 3.0f, &pTestMesh, NULL);
	mTexTest = new ZTexture(L"Media/cyberpunk.jpg");
	MtrlTest.setDiffuse(ZColor(1.0f, 0.2f, 0.2f));

	D3DXVECTOR3 mv [] = {
		D3DXVECTOR3(50.0f, 0.0f, 100.0f),
		D3DXVECTOR3(70.0f, 0.0f, 100.0f),
		D3DXVECTOR3(70.0f, 0.0f, 0.0f),
		D3DXVECTOR3(50.0f, 0.0f, 0.0f),
	};
	D3DXVECTOR3 mn [] = {
		D3DXVECTOR3(0.0f, 1.0f, 0.0f),
		D3DXVECTOR3(0.0f, 1.0f, 0.0f),
		D3DXVECTOR3(0.0f, 1.0f, 0.0f),
		D3DXVECTOR3(0.0f, 1.0f, 0.0f),
	};
	D3DXVECTOR2 mt [] = {
		D3DXVECTOR2(0.0f, 0.0f),
		D3DXVECTOR2(2.0f, 0.0f),
		D3DXVECTOR2(2.0f, 10.0f),
		D3DXVECTOR2(0.0f, 10.0f),
	};
	short mi [] = {
		0, 1, 3,
		3, 1, 2,
	};
	mMesh.createSubset();
	ZVertexDeclaration& vd = mMesh.getVertexDeclaration(0);
	ZVertexBuffer& vb = mMesh.getVertexBuffer(0);
	ZIndexBuffer& ib = mMesh.getIndexBuffer(0);
	vd.pushElement(::D3DDECLTYPE_FLOAT3, ::D3DDECLUSAGE_POSITION, 0);
	vd.pushElement(::D3DDECLTYPE_FLOAT3, ::D3DDECLUSAGE_NORMAL, 0);
	vd.pushElement(::D3DDECLTYPE_FLOAT2, ::D3DDECLUSAGE_TEXCOORD, 0);
	vd.pushEndElement();
	vb.create(vd, 4);
	vb.writeElements<D3DXVECTOR3>(0, mv, 4);
	vb.writeElements<D3DXVECTOR3>(1, mn, 4);
	vb.writeElements<D3DXVECTOR2>(2, mt, 4);
	ib.create(6);
	ib.writeIndices(0, mi, 6);

	// Walls
	D3DXVECTOR3 wv [] = {
		D3DXVECTOR3(50.0f, 0.0f, 0.0f),				// left
		D3DXVECTOR3(50.0f, 0.0f, 100.0f),
		D3DXVECTOR3(50.0f, 15.0f, 100.0f),
		D3DXVECTOR3(50.0f, 15.0f, 0.0f),

		D3DXVECTOR3(70.0f, 0.0f, 0.0f),		// 4	// right
		D3DXVECTOR3(70.0f, 0.0f, 100.0f),	// 5
		D3DXVECTOR3(70.0f, 15.0f, 100.0f),	// 6
		D3DXVECTOR3(70.0f, 15.0f, 0.0f),	// 7

		D3DXVECTOR3(70.0f, 0.0f, 0.0f),		// 8	// back
		D3DXVECTOR3(50.0f, 0.0f, 0.0f),		// 9
		D3DXVECTOR3(50.0f, 15.0f, 0.0f),	// 10
		D3DXVECTOR3(70.0f, 15.0f, 0.0f),	// 11

		D3DXVECTOR3(70.0f, 0.0f, 100.0f),	// 12	// front
		D3DXVECTOR3(50.0f, 0.0f, 100.0f),	// 13
		D3DXVECTOR3(50.0f, 15.0f, 100.0f),	// 14
		D3DXVECTOR3(70.0f, 15.0f, 100.0f),	// 15
	};
	D3DXVECTOR3 wn [] = {
		D3DXVECTOR3(1.0f, 0.0f, 0.0f),
		D3DXVECTOR3(1.0f, 0.0f, 0.0f),
		D3DXVECTOR3(1.0f, 0.0f, 0.0f),
		D3DXVECTOR3(1.0f, 0.0f, 0.0f),

		D3DXVECTOR3(-1.0f, 0.0f, 0.0f),
		D3DXVECTOR3(-1.0f, 0.0f, 0.0f),
		D3DXVECTOR3(-1.0f, 0.0f, 0.0f),
		D3DXVECTOR3(-1.0f, 0.0f, 0.0f),

		D3DXVECTOR3(0.0f, 0.0f, 1.0f),
		D3DXVECTOR3(0.0f, 0.0f, 1.0f),
		D3DXVECTOR3(0.0f, 0.0f, 1.0f),
		D3DXVECTOR3(0.0f, 0.0f, 1.0f),

		D3DXVECTOR3(0.0f, 0.0f, -1.0f),
		D3DXVECTOR3(0.0f, 0.0f, -1.0f),
		D3DXVECTOR3(0.0f, 0.0f, -1.0f),
		D3DXVECTOR3(0.0f, 0.0f, -1.0f),
	};
	D3DXVECTOR2 wt [] = {
		D3DXVECTOR2(10.0, 1.0f),
		D3DXVECTOR2(0.0, 1.0f),
		D3DXVECTOR2(0.0f, 0.0f),
		D3DXVECTOR2(10.0f, 0.0f),

		D3DXVECTOR2(1.0f, 1.0f),
		D3DXVECTOR2(0.0f, 1.0f),
		D3DXVECTOR2(0.0f, 0.0f),
		D3DXVECTOR2(1.0f, 0.0f),
	};
	short wi [] = {
		0, 3, 1,	// left
		1, 3, 2,
		4, 5, 7,	// right
		5, 6, 7, 
		8, 9, 10,	// back
		10, 11, 8,
		12, 13,	14, // front
		14, 15, 12, 
	};
	mMesh.createSubset();
	ZVertexBuffer& vb2 = mMesh.getVertexBuffer(1);
	ZIndexBuffer& ib2 = mMesh.getIndexBuffer(1);
	ZVertexDeclaration& vd2 = mMesh.getVertexDeclaration(1);
	vd2.pushElement(::D3DDECLTYPE_FLOAT3, ::D3DDECLUSAGE_POSITION, 0);
	vd2.pushElement(::D3DDECLTYPE_FLOAT3, ::D3DDECLUSAGE_NORMAL, 0);
	vd2.pushElement(::D3DDECLTYPE_FLOAT2, ::D3DDECLUSAGE_TEXCOORD, 0);
	vd2.pushEndElement();
	vb2.create(vd2, 16);
	vb2.writeElements<D3DXVECTOR3>(0, wv, 16);
	vb2.writeElements<D3DXVECTOR3>(1, wn, 16);
	vb2.writeElements<D3DXVECTOR2>(2, wt, 4);
	vb2.writeElements<D3DXVECTOR2>(2, wt, 4, 4);
	vb2.writeElements<D3DXVECTOR2>(2, &wt[4], 4, 8);
	vb2.writeElements<D3DXVECTOR2>(2, &wt[4], 4, 12);
	ib2.create(24);
	ib2.writeIndices(0, wi, 24);

	D3DXVECTOR3 cv [] = {
		D3DXVECTOR3(50.0f, 15.0f, 0.0f),
		D3DXVECTOR3(50.0f, 15.0f, 100.0f),
		D3DXVECTOR3(70.0f, 15.0f, 100.0f),
		D3DXVECTOR3(70.0f, 15.0f, 0.0f),
	};
	D3DXVECTOR3 cn [] = {
		D3DXVECTOR3(0.0f, -1.0f, 0.0f),
		D3DXVECTOR3(0.0f, -1.0f, 0.0f),
		D3DXVECTOR3(0.0f, -1.0f, 0.0f),
		D3DXVECTOR3(0.0f, -1.0f, 0.0f),
	};
	D3DXVECTOR2 ct [] = {
		D3DXVECTOR2(10.0, 1.0f),
		D3DXVECTOR2(0.0, 1.0f),
		D3DXVECTOR2(0.0f, 0.0f),
		D3DXVECTOR2(10.0f, 0.0f),

		D3DXVECTOR2(10.0, 1.0f),
		D3DXVECTOR2(0.0, 1.0f),
		D3DXVECTOR2(0.0f, 0.0f),
		D3DXVECTOR2(10.0f, 0.0f),
	};
	short ci [] = {
		0, 3, 1,
		1, 3, 2,
	};
	mMesh.createSubset();
	ZVertexBuffer& vb3 = mMesh.getVertexBuffer(2);
	ZIndexBuffer& ib3 = mMesh.getIndexBuffer(2);
	ZVertexDeclaration& vd3 = mMesh.getVertexDeclaration(2);
	vd3.pushElement(::D3DDECLTYPE_FLOAT3, ::D3DDECLUSAGE_POSITION, 0);
	vd3.pushElement(::D3DDECLTYPE_FLOAT3, ::D3DDECLUSAGE_NORMAL, 0);
	vd3.pushElement(::D3DDECLTYPE_FLOAT2, ::D3DDECLUSAGE_TEXCOORD, 0);
	vd3.pushElement(::D3DDECLTYPE_FLOAT2, ::D3DDECLUSAGE_TEXCOORD, 1);
	vd3.pushEndElement();
	vb3.create(vd3, 4);
	vb3.writeElements<D3DXVECTOR3>(0, cv, 4);
	vb3.writeElements<D3DXVECTOR3>(1, cn, 4);
	vb3.writeElements<D3DXVECTOR2>(2, ct, 4);
	vb3.writeElements<D3DXVECTOR2>(3, &ct[4], 4);
	ib3.create(6);
	ib3.writeIndices(0, ci, 6);

	// Textures
	mTexFloor = new ZTexture(L"Media/Belmonte.jpg");
	mTexWall = new ZTexture(L"Media/wall.jpg");
	mTexCeiling = new ZTexture(L"Media/ceiling.jpg");
	mTexFlare = new ZTexture(L"Media/flare.jpg");

	mMesh.getMaterial(0).addTexture(0, mTexFloor);
	mMesh.getMaterial(1).addTexture(0, mTexWall);
	mMesh.getMaterial(2).addTexture(0, mTexCeiling);
	mMesh.getMaterial(2).addTexture(1, mTexFlare);
	mMesh.getMaterial(2).setTextureStageBlend(1, ::ZTEXTURESTAGEBLEND_ADD);

	// Test input
	ZInput::instance()->mapKeyToAction(133, DIK_A, ZINPUT_RELEASED);

	// HUD Rect
	mHUDRectMesh.createSubset();
	ZVertexBuffer& vbhud = mHUDRectMesh.getVertexBuffer(0);
	ZIndexBuffer& ibhud = mHUDRectMesh.getIndexBuffer(0);
	ZVertexDeclaration& vdhud = mHUDRectMesh.getVertexDeclaration(0);
	D3DXVECTOR4 hud_vertices [] = {
		D3DXVECTOR4(50.0f, 50.0f, 0.0f, 1.0f),
		D3DXVECTOR4(550.0f, 50.0f, 0.0f, 1.0f),
		D3DXVECTOR4(550.0f, 400.0f, 0.0f, 1.0f),
		D3DXVECTOR4(50.0f, 400.0f, 0.0f, 1.0f),
		D3DXVECTOR4(550.0f, 200.0f, 0.0f, 1.0f),
		D3DXVECTOR4(1000.0f, 200.0f, 0.0f, 1.0f),
		D3DXVECTOR4(1000.0f, 650.0f, 0.0f, 1.0f),
		D3DXVECTOR4(400.0f, 650.0f, 0.0f, 1.0f),
	};
	D3DXVECTOR4 hud_colors [] = {
		D3DXVECTOR4(1.0f, 0.0f, 0.0f, 0.0f),
		D3DXVECTOR4(0.0f, 1.0f, 0.0f, 0.0f),
		D3DXVECTOR4(1.0f, 1.0f, 0.0f, 0.0f),
		D3DXVECTOR4(0.0f, 0.0f, 1.0f, 0.0f),
	};
	short hud_indices [] = {
		0, 1, 3,
		1, 2, 3,
		4, 5, 7,
		5, 6, 7,
	};
	// - Vertex Declaration
	vdhud.pushElement(::D3DDECLTYPE_FLOAT4, ::D3DDECLUSAGE_POSITION, 0);
	vdhud.pushElement(::D3DDECLTYPE_FLOAT4, ::D3DDECLUSAGE_COLOR, 0);
	vdhud.pushEndElement();
	// - Vertex Buffer
	vbhud.create(vdhud, 8);
	vbhud.writeElements<D3DXVECTOR4>(0, hud_vertices, 8);
	vbhud.writeElements<D3DXVECTOR4>(1, hud_colors, 4);
	vbhud.writeElements<D3DXVECTOR4>(1, hud_colors, 4, 4);
	// - Index Buffer
	ibhud.create(12);
	ibhud.writeIndices(0, hud_indices, 12);
	
	// Debug Depth
	const float width = static_cast<float>(pWindow->getWidth());
	const float height = static_cast<float>(pWindow->getHeight());
	const float dwi = 300.0f;
	const float dhi = 168.0f;
	float dleft = pWindow->getWidth() - dwi;
	float dtop = pWindow->getHeight() - dhi;
	float dright = static_cast<float>(pWindow->getWidth());
	float dbottom = static_cast<float>(pWindow->getHeight());
	D3DXVECTOR4 depth_vertices [] = {
		D3DXVECTOR4(dleft, dtop, 0.0f, 1.0f),
		D3DXVECTOR4(dright, dtop, 0.0f, 1.0f),
		D3DXVECTOR4(dright, dbottom, 0.0f, 1.0f),
		D3DXVECTOR4(dleft, dbottom, 0.0f, 1.0f),
	};
	D3DXVECTOR4 color_vertices [] = {
		D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f),
		D3DXVECTOR4(width, 0.0f, 0.0f, 1.0f),
		D3DXVECTOR4(width, height, 0.0f, 1.0f),
		D3DXVECTOR4(0.0f, height, 0.0f, 1.0f),
	};
	D3DXVECTOR2 depth_coords [] = {
		D3DXVECTOR2(0.0f, 0.0f),
		D3DXVECTOR2(1.0f, 0.0f),
		D3DXVECTOR2(1.0f, 1.0f),
		D3DXVECTOR2(0.0f, 1.0f),
	};
	short depth_indices [] = {
		0, 1, 3,
		1, 2, 3,
	};
	// Color Subset
	mDebugDepth.createSubset();
	ZVertexBuffer& vbdepth = mDebugDepth.getVertexBuffer(0);
	ZIndexBuffer& ibdepth = mDebugDepth.getIndexBuffer(0);
	ZVertexDeclaration& vddepth = mDebugDepth.getVertexDeclaration(0);
	// - Vertex Declaration
	vddepth.pushElement(D3DDECLTYPE_FLOAT4, D3DDECLUSAGE_POSITION, 0);
	vddepth.pushElement(D3DDECLTYPE_FLOAT2, D3DDECLUSAGE_TEXCOORD, 0);
	vddepth.pushEndElement();
	// - Vertex buffer
	vbdepth.create(vddepth, 4);
	vbdepth.writeElements<D3DXVECTOR4>(0, color_vertices, 4);
	vbdepth.writeElements<D3DXVECTOR2>(1, depth_coords, 4);
	// - Index buffer
	ibdepth.create(6);
	ibdepth.writeIndices(0, depth_indices, 6);

	// Color Subset
	mDebugDepth.createSubset();
	ZVertexBuffer& vbdepth2 = mDebugDepth.getVertexBuffer(1);
	ZIndexBuffer& ibdepth2 = mDebugDepth.getIndexBuffer(1);
	ZVertexDeclaration& vddepth2 = mDebugDepth.getVertexDeclaration(1);
	// - Vertex Declaration
	vddepth2.pushElement(D3DDECLTYPE_FLOAT4, D3DDECLUSAGE_POSITION, 0);
	vddepth2.pushElement(D3DDECLTYPE_FLOAT2, D3DDECLUSAGE_TEXCOORD, 0);
	vddepth2.pushEndElement();
	// - Vertex buffer
	vbdepth2.create(vddepth, 4);
	vbdepth2.writeElements<D3DXVECTOR4>(0, depth_vertices, 4);
	vbdepth2.writeElements<D3DXVECTOR2>(1, depth_coords, 4);
	// - Index buffer
	ibdepth2.create(6);
	ibdepth2.writeIndices(0, depth_indices, 6);

	mDebugDepth.getMaterial(0).addTexture(0, mCTexture);
	mDebugDepth.getMaterial(1).addTexture(0, mDTexture);
}

void FFRoom::setupD3D()
{
	ZD3D9::device->SetRenderState(::D3DRS_LIGHTING, true);
	setMatrices();
	ZSampler::setSamplerFilter(0, ESF_BILINEAR);
	ZSampler::setTextureMirror(0);

	SceneLight.setPointLight(ZColor(1.0f, 1.0f, 1.0f), D3DXVECTOR3(60.0f, 7.5f, 80.0f), 100.0f);
	SceneLight.setAttenuation(0.0f, 0.0125f, 0.0f);
	SceneLight.set(0);
	ZLight::Enable(0);

	TestLight.setPointLight(ZColor(1.0f, 1.0f, 1.0f), D3DXVECTOR3(60.0f, 5.0f, 20.0f), 50.0f);
	TestLight.setAttenuation(0.0f, 0.018f, 0.0f);
	TestLight.set(1);
	ZLight::Enable(1);

}



void FFRoom::createRenderTarget()
{
	DELETE_OBJECT(mCTexture);
	DELETE_OBJECT(mDTexture);
	HRESULT hr;
	hr = D3DXCreateTexture(&mD3D9->getDevice(), pWindow->getWidth(), pWindow->getHeight(), 1,
		D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &pColorTexture);
	if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
	mCTexture = new ZTexture(pColorTexture);
	hr = D3DXCreateTexture(&mD3D9->getDevice(), pWindow->getWidth(), pWindow->getHeight(), 1,
		D3DUSAGE_DEPTHSTENCIL, D3DFMT_D24S8, D3DPOOL_DEFAULT, &pDepthStencilTexture);
	if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
	mDTexture = new ZTexture(pDepthStencilTexture);
	
	ZD3D9::device->GetRenderTarget(0, &DeviceFrameBuffer);
	ZD3D9::device->GetDepthStencilSurface(&DeviceDepthBuffer);
}

void FFRoom::update()
{
	if(GetAsyncKeyState(0x46) & 0x8000 && !isFullscreen())
	{
		goFullscreen(*mD3D9);
	}
	if(GetAsyncKeyState(0x57) & 0x8000 && isFullscreen())
	{
		goWindowed(*mD3D9);
	}

	static double accum = 0.0;
	accum += ZClock::dTimeDelta;
	if(accum >= 1.0)
	{
		accum -= 1.0;
		std::ostringstream ss;
		ss<<"FPS: "<<ZClock::uFPS<<std::endl;
		OutputDebugStringA(ss.str().c_str());
	}

	// Test Input
	ZInput::instance()->process();
	if(ZInput::instance()->getActionState(133))
	{
		OutputDebugStringA("I GOT BALLZ OF STEEEEEL!\n");
	}
}

void FFRoom::render()
{
	IDirect3DDevice9& device = mD3D9->getDevice();

	IDirect3DSurface9* rt_color;
	IDirect3DSurface9* rt_depth;
	pColorTexture->GetSurfaceLevel(0, &rt_color);
	pDepthStencilTexture->GetSurfaceLevel(0, &rt_depth);

	device.SetRenderTarget(0, rt_color);
	device.SetDepthStencilSurface(rt_depth);

	mD3D9->clear(0xff444444);
	device.BeginScene();

	// Magic for HUD/Stencil
		ZStencil::begin();
	ZStencil::setTest(ESC_ALWAYS);
	ZStencil::setRefMask(0x1);
	ZStencil::setOperation(ESO_REPLACE);
		ZLight::DisableLighting();
	device.SetRenderState(D3DRS_ZWRITEENABLE, false);
		ZAlphaBlending::begin();
	ZAlphaBlending::setSourceBlend(D3DBLEND_ZERO);
	ZAlphaBlending::setDestinationBlend(D3DBLEND_ONE);
	device.SetTransform(D3DTS_WORLD, &matWorldHUD);
	device.SetTransform(D3DTS_PROJECTION, &matProjHUD);
	device.SetTransform(D3DTS_VIEW, &matViewHUD);
	mHUDRectMesh.drawAll(device);
		ZAlphaBlending::end();
	device.SetRenderState(D3DRS_ZWRITEENABLE, true);
	// Magic for scene
		ZLight::EnableLighting();
	ZLight::Disable(1);
	device.SetTransform(D3DTS_WORLD, &matWorld);
	device.SetTransform(D3DTS_PROJECTION, &matProj);
	device.SetTransform(D3DTS_VIEW, &matView);
	// Stencil masking
	ZStencil::setOperation();
	ZStencil::setTest(ESC_EQUAL);
	ZStencil::setRefMask(0x1);
	MtrlFloor.set();
	mMesh.drawSubset(device, 0);
	MtrlWall.set();
	mMesh.drawSubset(device, 1);
	MtrlCeiling.set();
	mMesh.drawSubset(device, 2);
		ZStencil::end();

	// Magic for blending
	ZAlphaBlending::takeAlphaFromDiffuse();
		ZAlphaBlending::begin();
	ZAlphaBlending::setSourceBlend(::D3DBLEND_SRCALPHA);
	ZAlphaBlending::setDestinationBlend(::D3DBLEND_INVSRCALPHA);
	ZLight::Enable(1);
	MtrlTest.setDiffuse(ZColor(1.0f, 0.0f, 0.0f, 0.75f));
	static float test_rotation = 0.0f;
	test_rotation += ZClock::fTimeDelta;
	D3DXMATRIX tm, rm, sm;
	::D3DXMatrixTranslation(&tm, 60.0f, 7.5f, 40.0f);
	::D3DXMatrixRotationY(&rm, test_rotation);
	::D3DXMatrixScaling(&sm, 4.0f, 4.0f, 4.0f);
	tm = sm * rm * tm;
	device.SetTransform(D3DTS_WORLD, &tm);
	MtrlTest.set();
	pTestMesh->DrawSubset(0);
		ZAlphaBlending::end();

	device.EndScene();

	device.SetRenderTarget(0, DeviceFrameBuffer);
	device.SetDepthStencilSurface(DeviceDepthBuffer);
	device.BeginScene();
	ZLight::DisableLighting();
	// Depth/Stencil
	device.SetRenderState(D3DRS_ZWRITEENABLE, false);
	device.SetTransform(D3DTS_WORLD, &matWorldHUD);
	device.SetTransform(D3DTS_PROJECTION, &matProjHUD);
	device.SetTransform(D3DTS_VIEW, &matViewHUD);
	D3DXMATRIX dm;
	D3DXMatrixIdentity(&dm);
	//mDebugDepth.drawAll(device);
	mDebugDepth.drawSubset(*mD3D9->device, 0);

	device.EndScene();

	// Do the swap chain magic
	mD3D9->present();

	device.SetRenderTarget(0, rt_color);
	device.SetDepthStencilSurface(rt_depth);
}

void FFRoom::end()
{
}

void FFRoom::onDeviceReset()
{
	setupD3D();
	createRenderTarget();
}

void FFRoom::onWindowResize(const ZEvent& e)
{
	const ZEvent_WindowResize& ewr = static_cast<const ZEvent_WindowResize&>(e);
	char buffer [255];
	sprintf_s<255>(buffer, "Window Resize Event to %dx%d\n", ewr.iWidth, ewr.iHeight);
	OutputDebugStringA(buffer);
	DELETE_OBJECT(mCTexture);
	DELETE_OBJECT(mDTexture);
	resizeD3D(*mD3D9);
}

void FFRoom::setMatrices()
{
	// Perspective
	D3DXMatrixPerspectiveFovLH(&matProj, 3.1415f / 4.0f, 
		pWindow->getWidth() / static_cast<float>(pWindow->getHeight()), 
		1.0f, 1000.0f);
	D3DXVECTOR3 eye(60.0f, 10.0f, 10.0f), 
				at(60.0f, 10.0f, 20.0f), 
				up(0.0f, 1.0f, 0.0f);
	D3DXMatrixLookAtLH(&matView, &eye, &at, &up);
	D3DXMatrixIdentity(&matWorld);

	// Ortho for HUD
	D3DXMatrixOrthoOffCenterLH(&matProjHUD, 0.0f, static_cast<float>(pWindow->getWidth()),
		0.0f, static_cast<float>(pWindow->getHeight()), 0.0f, 1.0f);
	D3DXMatrixIdentity(&matViewHUD);
	// Flip vertically and shift by window height so Ortho starts at top left instead of bottom left
	D3DXMatrixIdentity(&matWorldHUD);
	D3DXMatrixScaling(&matWorldHUD, 1.0f, -1.0f, 1.0f);
	D3DXMATRIX w;
	D3DXMatrixTranslation(&w, 0.0f, static_cast<float>(pWindow->getHeight()), 0.0f);
	matWorldHUD = matWorldHUD * w;
}