////////////////////////////////////////////////////////////////
// Name: ZTexture
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "ZTexture.h"
#include "ZException.h"
#include "ZAssert.h"
#include "ZUtils.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZTexture::ZTexture(const wchar_t* filename)
{
	ZASSERT(filename != NULL, "ZTexture::ZTexture() given NULL file name");
	IDirect3DTexture9* texture;
	HRESULT hr = D3DXCreateTextureFromFile(ZD3D9::device, filename, &texture);
	if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
	mTexture = texture;
}

////////////////////////////////////////////////////////////////
///
ZTexture::ZTexture(const IDirect3DTexture9* texture)
{
	ZASSERT(texture != NULL, "ZTexture::ZTexture() given NULL texture");
	mTexture = const_cast<IDirect3DTexture9*>(texture);
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZTexture::~ZTexture()
{
	SAFE_RELEASE(mTexture);
}

////////////////////////////////////////////////////////////////
/// Set the texture to the given stage
/// 
void ZTexture::set(unsigned int stage)
{
	ZASSERT(mTexture != NULL, "ZTexture::set() attempting to set NULL texture!");
	ZD3D9::device->SetTexture(stage, mTexture);
}
