////////////////////////////////////////////////////////////////
// Name: ZUtils
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "ZUtils.h"
#include <Windows.h>
#include <DxErr.h>

using namespace zen;

////////////////////////////////////////////////////////////////
/// Retrieves and formats an HRESULT DirectX error message.
///
ZException DXException(long hresult, const char* file, int line)
{
	std::ostringstream ss;
	ss<<"DirectX Error"<<std::endl;
	ss<<"File: "<<file<<std::endl;
	ss<<"Line: "<<line<<std::endl;
	ss<<"Error: "<<::DXGetErrorStringA(hresult)<<std::endl;
	ss<<"Details: "<<::DXGetErrorDescriptionA(hresult)<<std::endl;
	return ZException(ss.str().c_str());
}
