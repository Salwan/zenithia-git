////////////////////////////////////////////////////////////////
// Name: ZAlphaBlending
// Desc: Alpha blending rendering
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZALPHABLENDING_HEADER_
#define _ZALPHABLENDING_HEADER_

/////////////////////////////////////////////////////// Includes
#include "ZD3D9.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// @class ZAlphaBlending
/// @brief
/// 
class ZAlphaBlending 
{
public:
	virtual ~ZAlphaBlending();

	static void begin();
	static void end();
	static void takeAlphaFromTexture(unsigned int stage = 0);
	static void takeAlphaFromDiffuse(unsigned int stage = 0);
	static void setSourceBlend(unsigned int src_factor = D3DBLEND_ONE);
	static void setDestinationBlend(unsigned int dest_factor = D3DBLEND_ZERO);

private:
	ZAlphaBlending();
	ZAlphaBlending(const ZAlphaBlending&){}
	ZAlphaBlending& operator=(const ZAlphaBlending&){}

};

#endif
