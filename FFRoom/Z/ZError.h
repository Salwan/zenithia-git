////////////////////////////////////////////////////////////////
// Name: ZError
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZERROR_HEADER_
#define _ZERROR_HEADER_

/////////////////////////////////////////////////////// Includes
#include <string>

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// @class ZError
/// @brief Provides error utilities relevant to ZGUI responsibilities.
/// 
class ZError 
{
public:
	virtual ~ZError(){}

	static void getWindowsErrorMessage(unsigned long windows_error_message, 
		std::string& string_out);
	
	static void getLastWindowsErrorMessage(std::string& string_out);

	static void showError(const std::string& error_message);

private:
	explicit ZError(){}
	ZError(const ZError&){}
	ZError& operator=(const ZError&){}

};

#endif
