////////////////////////////////////////////////////////////////
// Name: ZMesh
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZMESH_HEADER_
#define _ZMESH_HEADER_

/////////////////////////////////////////////////////// Includes
#include <vector>
#include "ZD3D9.h"

//////////////////////////////////////////////////// Definitions
class ZVertexDeclaration;
class ZVertexBuffer;
class ZIndexBuffer;
class ZMaterial;

////////////////////////////////////////////////////////////////
/// @class ZMesh
/// @brief
/// 
class ZMesh 
{
public:
	explicit ZMesh();
	virtual ~ZMesh();

	// Methods
	void drawSubset(IDirect3DDevice9& device, unsigned int subset);
	void drawAll(IDirect3DDevice9& device);
	unsigned int getSubsetCount() const;

protected:
	// Methods
	unsigned int _createSubset();
	ZVertexDeclaration& _getVD(unsigned int subset);
	ZVertexBuffer& _getVB(unsigned int subset);
	ZIndexBuffer& _getIB(unsigned int subset);
	ZMaterial& _getMaterial(unsigned int subset);

private:
	ZMesh(const ZMesh&){}
	ZMesh& operator=(const ZMesh&){}

	std::vector<ZVertexDeclaration*> mVDecls;
	std::vector<ZVertexBuffer*> mVBufs;
	std::vector<ZIndexBuffer*> mIBufs;
	std::vector<ZMaterial*> mMaterials;
};

#endif
