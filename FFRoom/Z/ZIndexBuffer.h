////////////////////////////////////////////////////////////////
// Name: ZIndexBuffer
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZINDEXBUFFER_HEADER_
#define _ZINDEXBUFFER_HEADER_

/////////////////////////////////////////////////////// Includes
#include "ZD3D9.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// @class ZIndexBuffer
/// @brief
/// 
class ZIndexBuffer 
{
public:
	explicit ZIndexBuffer();
	virtual ~ZIndexBuffer();

	// Methods
	void create(unsigned int index_count, D3DFORMAT indices_format = D3DFMT_INDEX16);
	void set();
	void lock();
	void unlock();
	void writeIndices(unsigned int offset_in_indices, void* indices, unsigned int index_count);

	// Accessors
	unsigned int getFaceCount() const {return uIndexCount / 3;}

private:
	ZIndexBuffer(const ZIndexBuffer&){}
	ZIndexBuffer& operator=(const ZIndexBuffer&){}

	void lock(unsigned int offset_to_lock, unsigned int size_to_lock);
	void construct();

	IDirect3DIndexBuffer9* pIB;
	D3DFORMAT mFormat;
	unsigned int uIndexCount;
	unsigned int uIndexStride;
	void* pData;
	bool bLocked;
};

#endif
