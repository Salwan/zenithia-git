////////////////////////////////////////////////////////////////
// Name: ZMaterial
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZMATERIAL_HEADER_
#define _ZMATERIAL_HEADER_

/////////////////////////////////////////////////////// Includes
#include <vector>
#include "ZD3D9.h"

//////////////////////////////////////////////////// Definitions
class ZColor;
class ZTexture;
enum ZTextureStageBlend;

const unsigned int MaxTextureCount = 2;

////////////////////////////////////////////////////////////////
/// @enum ZTextureStageBlend
/// @brief high level texture stage blending
enum ZTextureStageBlend
{
	ZTEXTURESTAGEBLEND_NONE	= 0,
	ZTEXTURESTAGEBLEND_ADD,
	ZTEXTURESTAGEBLEND_MULTIPLY,
};

////////////////////////////////////////////////////////////////
/// @class ZMaterial
/// @brief
/// 
class ZMaterial 
{
public:
	explicit ZMaterial();
	virtual ~ZMaterial();

	void set();
	void addTexture(unsigned int stage, ZTexture* texture);
	void setTextureStageBlend(unsigned int stage, ZTextureStageBlend blend_mode = ZTEXTURESTAGEBLEND_NONE);
	void clearTextures();

	void setAmbient(const ZColor& ambient);
	void setDiffuse(const ZColor& diffuse);
	void setSpecular(const ZColor& specular);
	void setEmissive(const ZColor& emissive);
	void setPower(float power);

private:
	ZMaterial(const ZMaterial&){}
	ZMaterial& operator=(const ZMaterial&){}

	D3DMATERIAL9 mMaterial;
	std::vector<ZTexture*> mTextures;
	std::vector<ZTextureStageBlend> mTexturesStageBlend;
};

#endif
