////////////////////////////////////////////////////////////////
// Name: ZEventListener
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#ifndef _ZEVENTLISTENER_HEADER_
#define _ZEVENTLISTENER_HEADER_

/////////////////////////////////////////////////////// Includes

//////////////////////////////////////////////////// Definitions
class ZEvent;

////////////////////////////////////////////////////////////////
/// @class ZEventListener
/// @brief
/// 
class ZEventListener 
{
public:
	explicit ZEventListener();
	virtual ~ZEventListener();

private:
	ZEventListener(const ZEventListener&){}
	ZEventListener& operator=(const ZEventListener&){}

};
typedef void(ZEventListener::*ZEventHandlerPtr)(const ZEvent&);
#define EVENT_HANDLER_PTR(ptr) static_cast<ZEventHandlerPtr>(ptr)

#endif
