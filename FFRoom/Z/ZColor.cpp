////////////////////////////////////////////////////////////////
// Name: ZColor
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "ZColor.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZColor::ZColor() : r(1.0f), g(1.0f), b(1.0f), a(1.0f)
{
	
}

////////////////////////////////////////////////////////////////
/// Direct Constructor
/// 
ZColor::ZColor(float _r, float _g, float _b, float _a) : r(_r), g(_g), b(_b), a(_a)
{

}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZColor::~ZColor()
{
	
}

////////////////////////////////////////////////////////////////
/// Copy constructor
/// 
ZColor::ZColor(const ZColor& color)
{
	r = color.r;
	g = color.g;
	b = color.b;
	a = color.a;
}

////////////////////////////////////////////////////////////////
/// Assignment operator
/// 
ZColor& ZColor::operator=(const ZColor& color)
{
	r = color.r;
	g = color.g;
	b = color.b;
	a = color.a;
	return *this;
}

ZColor::operator D3DCOLORVALUE() const
{
	return toD3DColorValue();
}

ZColor::operator D3DXVECTOR4() const
{
	return toD3DXVector4();
}

ZColor::operator int() const
{
	return toInt();
}

ZColor::operator unsigned int() const
{
	return toUInt();
}

////////////////////////////////////////////////////////////////
/// Convert to D3DCOLORVALUE
/// 
D3DCOLORVALUE ZColor::toD3DColorValue() const
{
	D3DCOLORVALUE c;
	c.r = r;
	c.g = g;
	c.b = b;
	c.a = a;
	return c;
}

////////////////////////////////////////////////////////////////
/// Convert to D3DXVECTOR4
/// 
D3DXVECTOR4 ZColor::toD3DXVector4() const
{
	return D3DXVECTOR4(r, g, b, a);
}

////////////////////////////////////////////////////////////////
/// Convert to int
/// 
int ZColor::toInt() const
{
	return int(
		(unsigned int(unsigned char(b * 255.0f))) + 
		(unsigned int(unsigned char(g * 255.0f)) << 8U) + 
		(unsigned int(unsigned char(r * 255.0f)) << 16U) + 
		(unsigned int(unsigned char(a * 255.0f)) << 24U));
}

////////////////////////////////////////////////////////////////
/// Convert to unsigned int
/// 
unsigned int ZColor::toUInt() const
{
	return unsigned int(
		(unsigned int(unsigned char(b * 255.0f))) + 
		(unsigned int(unsigned char(g * 255.0f)) << 8U) + 
		(unsigned int(unsigned char(r * 255.0f)) << 16U) + 
		(unsigned int(unsigned char(a * 255.0f)) << 24U));
}

////////////////////////////////////////////////////////////////
/// Convert from D3DCOLORVALUE
/// 
void ZColor::fromD3DColorValue(const D3DCOLORVALUE& color)
{
	r = color.r;
	g = color.g;
	b = color.b;
	a = color.a;
}

////////////////////////////////////////////////////////////////
/// Convert from D3DXVECTOR4
/// 
void ZColor::fromD3DXVector4(const D3DXVECTOR4& color)
{
	r = color.x;
	g = color.y;
	b = color.z;
	a = color.w;
}

////////////////////////////////////////////////////////////////
/// Convert from int
/// 
void ZColor::fromInt(int color)
{
	a = GET_ALPHA(color) / 255.0f;
	r = GET_RED(color) / 255.0f;
	g = GET_GREEN(color) / 255.0f;
	b = GET_BLUE(color) / 255.0f;
}

////////////////////////////////////////////////////////////////
/// Convert from unsigned int
/// 
void ZColor::fromUInt(unsigned int color)
{
	a = GET_ALPHA(color) / 255.0f;
	r = GET_RED(color) / 255.0f;
	g = GET_GREEN(color) / 255.0f;
	b = GET_BLUE(color) / 255.0f;
}

////////////////////////////////////////////////////////////////
/// D3DCOLORVALUE constructor
/// 
D3DCOLORVALUE ZColor::createD3DColorValue(float r, float g, float b, float a)
{
	D3DCOLORVALUE c;
	c.r = r;
	c.g = g;
	c.b = b;
	c.a = a;
	return c;
}

