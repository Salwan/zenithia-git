////////////////////////////////////////////////////////////////
// Name: ZVertexElement
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZVERTEXDECLARATION_HEADER_
#define _ZVERTEXDECLARATION_HEADER_

/////////////////////////////////////////////////////// Includes
#include <vector>
#include "ZD3D9.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// @class ZVertexElement
/// @brief Wraps and simplifies the process of creating vertex elements declaration.
/// 
class ZVertexDeclaration
{
public:
	explicit ZVertexDeclaration();
	virtual ~ZVertexDeclaration();

	// Methods
	void set();
	void pushElement(BYTE d3ddecltype, BYTE d3ddeclusage, BYTE usage_index);
	void pushEndElement();

	// Accessors
	D3DVERTEXELEMENT9* getElementsDeclaration();
	size_t getVertexSize() const;
	bool isComplete() const {return !bOpened;}
	unsigned int getElementCount() const {return uElementCount;}

private:
	ZVertexDeclaration(const ZVertexDeclaration&){}
	ZVertexDeclaration& operator=(const ZVertexDeclaration&){}

	void construct();

	IDirect3DVertexDeclaration9* pVD;
	std::vector<D3DVERTEXELEMENT9> vElements;
	WORD wOffset;
	unsigned int uElementCount;
	bool bOpened;
};

#endif
