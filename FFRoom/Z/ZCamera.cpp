////////////////////////////////////////////////////////////////
// Name: ZCamera
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "ZCamera.h"
#include "ZInput.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZCamera::ZCamera()
{

}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZCamera::~ZCamera()
{
	
}

void ZCamera::strafeHorizontal(float units)
{
	vPosition += vRight * units;
}

void ZCamera::strafeVertical(float units)
{
	vPosition += vUp * units;
}

void ZCamera::walk(float units)
{
	vPosition += vLook * units;
}

void ZCamera::pitch(float angle)
{
	D3DXMATRIX t;
	D3DXMatrixRotationAxis(&t, &vRight, angle);
	D3DXVec3TransformCoord(&vUp, &vUp, &t);
	D3DXVec3TransformCoord(&vLook, &vLook, &t);
}

void ZCamera::yaw(float angle)
{
	D3DXMATRIX t;
	D3DXMatrixRotationY(&t, angle);
	D3DXVec3TransformCoord(&vRight, &vRight, &t);
	D3DXVec3TransformCoord(&vLook, &vLook, &t);
}

void ZCamera::getViewMatrix(D3DXMATRIX& view_out)
{
	// Keep axes orthognal to each other
	D3DXVec3Normalize(&vLook, &vLook);
	D3DXVec3Cross(&vUp, &vLook, &vRight);
	D3DXVec3Normalize(&vUp, &vUp);
	D3DXVec3Cross(&vRight, &vUp, &vLook);
	D3DXVec3Normalize(&vRight, &vRight);

	// Build the view
	float x = -D3DXVec3Dot(&vRight, &vPosition);
	float y = -D3DXVec3Dot(&vUp, &vPosition);
	float z = -D3DXVec3Dot(&vLook, &vPosition);

	view_out(0, 0) = vRight.x;
	view_out(0, 1) = vUp.x;
	view_out(0, 2) = vLook.x;
	view_out(0, 3) = 0.0f;

	view_out(1, 0) = vRight.y;
	view_out(1, 1) = vUp.y;
	view_out(1, 2) = vLook.y;
	view_out(1, 3) = 0.0f;

	view_out(2, 0) = vRight.z;
	view_out(2, 1) = vUp.z;
	view_out(2, 2) = vLook.z;
	view_out(2, 3) = 0.0f;

	view_out(3, 0) = x;
	view_out(3, 1) = y;
	view_out(3, 2) = z;
	view_out(3, 3) = 1.0f;
}

void ZCamera::getPosition(D3DXVECTOR3& position_out) const
{
	position_out = vPosition;
}

void ZCamera::setPosition(const D3DXVECTOR3& position)
{
	vPosition = position;
}

void ZCamera::getRightVector(D3DXVECTOR3& right) const
{
	right = vRight;
}

void ZCamera::getUpVector(D3DXVECTOR3& up) const
{
	up = vUp;
}

void ZCamera::getLookVector(D3DXVECTOR3& look) const
{
	look = vLook;
}

