////////////////////////////////////////////////////////////////
// Name: ZEventDispatcher
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "ZEventDispatcher.h"
#include <map>
#include <list>
#include <vector>
#include "ZAssert.h"

//////////////////////////////////////////////////// Definitions
struct ZEventRecord
{
	ZEventRecord() : listener(nullptr), handler(nullptr) {}
	ZEventRecord(ZEventListener* _listener, ZEventHandlerPtr _handler, ZHeapPtr* _heapPtr = nullptr) 
		: listener(_listener), handler(_handler), pHeap(_heapPtr) {}
	ZEventListener* listener;
	ZEventHandlerPtr handler;
	ZHeapPtr* pHeap;
};
typedef std::list<ZEventRecord> EventList;
typedef std::list<ZEventRecord>::iterator EventListIter;
typedef std::map<unsigned int, EventList> EventMap;
typedef std::map<unsigned int, EventList>::iterator EventMapIter;
struct ZEventDispatcher::PIMPL
{
	EventMap events;
};

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZEventDispatcher::ZEventDispatcher()
{
	pimpl = new PIMPL;
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZEventDispatcher::~ZEventDispatcher()
{
	delete pimpl;
}

////////////////////////////////////////////////////////////////
/// Registers a new event listener directly, object will be
/// responsible for removing the event when its destructed, 
/// otherwise the world is gonna end.
///
void ZEventDispatcher::addEventListener(unsigned int _type, ZEventListener* _listener, ZEventHandlerPtr _handler)
{
	ZASSERT(_listener && _handler, "ZEventDispatcher::addEventListener() given null event listener and/or event handler.");
	EventList& elist = pimpl->events[_type];
	elist.push_back(ZEventRecord(_listener, _handler));
}

////////////////////////////////////////////////////////////////
/// Registers a new event listener.
///
void ZEventDispatcher::addEventListener(unsigned int _type, ZHeapPtr* _listener, 
	ZEventHandlerPtr _handler)
{
	ZASSERT(_listener && _handler, "ZEventDispatcher::addEventListener() given null heap pointer and/or event handler.");
	EventList& elist = pimpl->events[_type];
	elist.push_back(ZEventRecord(_listener->getPtr<ZEventListener>(), _handler, _listener));
}

////////////////////////////////////////////////////////////////
/// Removes an event listener stored directly
///
void ZEventDispatcher::removeEventListener(unsigned int _type, ZEventListener* _listener, 
	ZEventHandlerPtr _handler)
{
	ZASSERT(_listener && _handler, "ZEventDispatcher::removeEventListener() given null event listener and/or event handler.");
	EventList& elist = pimpl->events[_type];
	if(elist.size() > 0)
	{
		for(EventListIter iter = elist.begin(); iter != elist.end(); ++iter)
		{
			if(!iter->pHeap && iter->listener == _listener && iter->handler == _handler)
			{
				elist.erase(iter);
				break;
			}
		}
	}
}

////////////////////////////////////////////////////////////////
/// Removes an event listener stored as heap pointer
///
void ZEventDispatcher::removeEventListener(unsigned int _type, ZHeapPtr* _listener,
	ZEventHandlerPtr _handler)
{
	ZASSERT(_listener && _handler, "ZEventDispatcher::removeEventListener() given null heap pointer and/or event handler.");
	EventList& elist = pimpl->events[_type];
	if(elist.size() > 0)
	{
		for(EventListIter iter = elist.begin(); iter != elist.end(); ++iter)
		{
			if(iter->pHeap && iter->pHeap == _listener && iter->handler == _handler)
			{
				elist.erase(iter);
				break;
			}
		}
	}
}

////////////////////////////////////////////////////////////////
/// Dispatches an event
///
void ZEventDispatcher::dispatchEvent(const ZEvent& e)
{
	std::vector<EventListIter> to_delete;
	EventList& elist = pimpl->events[e.getType()];
	if(elist.size() > 0)
	{
		for(EventListIter iter = elist.begin(); iter != elist.end(); ++iter)
		{
			if(iter->pHeap)
			{
				// stored using heap pointer
				if(iter->pHeap->isValid())
				{
					(iter->listener->*(iter->handler))(e);
				}
				else
				{
					to_delete.push_back(iter);
				}
			}
			else
			{
				// stored directly
				(iter->listener->*(iter->handler))(e);
			}
		}
		if(to_delete.size() > 0)
		{
			for(unsigned int i = 0; i < to_delete.size(); ++i)
			{
				elist.erase(to_delete[i]);
			}
		}
	}
}
