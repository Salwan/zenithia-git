////////////////////////////////////////////////////////////////
// Name: ZEvent
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZEVENT_HEADER_
#define _ZEVENT_HEADER_

/////////////////////////////////////////////////////// Includes

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// @class ZEvent
/// @brief
/// 
class ZEvent 
{
public:
	explicit ZEvent(unsigned int _type = 0) : uType(_type) {}
	virtual ~ZEvent() {}

	// Accessors
	unsigned int getType() const {return uType;} 

protected:
	unsigned int uType;

private:
	ZEvent(const ZEvent&){}
	ZEvent& operator=(const ZEvent&){}

};

#endif
