////////////////////////////////////////////////////////////////
// Name: ZColor
// Desc: Various color operations and utilities
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZCOLOR_HEADER_
#define _ZCOLOR_HEADER_

/////////////////////////////////////////////////////// Includes
#include "ZD3D9.h"

//////////////////////////////////////////////////// Definitions
#define GET_ALPHA(color) ((color & 0xff000000) >> 24U)
#define GET_RED(color) ((color & 0x00ff0000) >> 16U)
#define GET_GREEN(color) ((color & 0x0000ff00) >> 8U)
#define GET_BLUE(color) (color & 0x000000ff)

////////////////////////////////////////////////////////////////
/// @class ZColor
/// @brief
/// 
class ZColor 
{
public:
	// Constructors
	explicit ZColor();
	ZColor(float _r, float _g, float _b, float _a = 1.0f);
	virtual ~ZColor();
	// Copy and Assignment
	ZColor(const ZColor& color);
	ZColor& operator=(const ZColor& color);
	// Casting
	operator D3DCOLORVALUE() const;
	operator D3DXVECTOR4() const;
	operator int() const;
	operator unsigned int() const;

	D3DCOLORVALUE toD3DColorValue() const;
	D3DXVECTOR4 toD3DXVector4() const;
	int toInt() const;
	unsigned int toUInt() const;

	void fromD3DColorValue(const D3DCOLORVALUE& color);
	void fromD3DXVector4(const D3DXVECTOR4& color);
	void fromInt(int color);
	void fromUInt(unsigned int color);
	
	float r;
	float g;
	float b;
	float a;

private:

public:
	static D3DCOLORVALUE createD3DColorValue(float r, float g, float b, float a = 1.0f);
};

#endif
