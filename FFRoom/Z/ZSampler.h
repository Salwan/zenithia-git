////////////////////////////////////////////////////////////////
// Name: ZSampler
// Desc: Wrapper around sampler states, static methods.
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZSAMPLER_HEADER_
#define _ZSAMPLER_HEADER_

/////////////////////////////////////////////////////// Includes

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// @enum Sampler filter type
/// 
enum ESamplerFilter
{
	ESF_NONE,
	ESF_POINT,
	ESF_BILINEAR,
	ESF_TRILINEAR,
	ESF_ANISOTROPIC,
};

////////////////////////////////////////////////////////////////
/// @class ZSampler
/// @brief
/// 
class ZSampler 
{
public:	
	virtual ~ZSampler();

	// Methods
	static void setSamplerFilter(unsigned int sampler, ESamplerFilter filter_type, unsigned int anisotropy_level = 0);
	static void setTextureWrap(unsigned int sampler);
	static void setTextureBorder(unsigned int sampler, unsigned int border_color);
	static void setTextureClamp(unsigned int sampler);
	static void setTextureMirror(unsigned int sampler);
	static void setTextureAddressingMode(unsigned int sampler, unsigned int d3dtaddressu, unsigned int d3dtaddressv);

private:
	explicit ZSampler();
	ZSampler(const ZSampler&){}
	ZSampler& operator=(const ZSampler&){}

};

#endif
