////////////////////////////////////////////////////////////////
// Name: ZUtils
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZUTILS_HEADER_
#define _ZUTILS_HEADER_

//////////////////////////////////////////////////// Includes
#include "ZException.h"

//////////////////////////////////////////////////// Definitions
#define DELETE_OBJECT(obj) if(obj) {delete obj; obj = NULL;}
#define SAFE_RELEASE(object) if(object){object->Release(); object = NULL;}
#define ZEXCEPTION_DXERROR(hresult) DXException(hresult, __FILE__, __LINE__)

//////////////////////////////////////////////////// Functions

// Create DirectX Exception via hresult, use macro ZEXCEPTION_DXERROR
ZException DXException(long hresult, const char* file, int line);

#endif