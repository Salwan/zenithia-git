////////////////////////////////////////////////////////////////
// Name: ZAlphaBlending
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "ZAlphaBlending.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZAlphaBlending::ZAlphaBlending()
{
	
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZAlphaBlending::~ZAlphaBlending()
{
	
}

////////////////////////////////////////////////////////////////
/// Starts alpha blending
/// 
void ZAlphaBlending::begin()
{
	ZD3D9::device->SetRenderState(::D3DRS_ALPHABLENDENABLE, TRUE);
}

void ZAlphaBlending::end()
{
	ZD3D9::device->SetRenderState(::D3DRS_ALPHABLENDENABLE, FALSE);
}

void ZAlphaBlending::takeAlphaFromTexture(unsigned int stage)
{
	ZD3D9::device->SetTextureStageState(stage, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	ZD3D9::device->SetTextureStageState(stage, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
}

void ZAlphaBlending::takeAlphaFromDiffuse(unsigned int stage)
{
	ZD3D9::device->SetTextureStageState(stage, D3DTSS_ALPHAARG1, D3DTA_DIFFUSE);
	ZD3D9::device->SetTextureStageState(stage, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
}

void ZAlphaBlending::setSourceBlend(unsigned int src_factor)
{
	ZD3D9::device->SetRenderState(::D3DRS_SRCBLEND, src_factor);
}

void ZAlphaBlending::setDestinationBlend(unsigned int dest_factor)
{
	ZD3D9::device->SetRenderState(::D3DRS_DESTBLEND, dest_factor);
}

