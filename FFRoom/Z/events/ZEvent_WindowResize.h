////////////////////////////////////////////////////////////////
// Name: ZEvent_WindowResize
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZEVENT_WINDOWRESIZE_HEADER_
#define _ZEVENT_WINDOWRESIZE_HEADER_

/////////////////////////////////////////////////////// Includes
#include "../ZEvent.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// @class ZEvent_WindowResize
/// @brief
/// 
class ZEvent_WindowResize : public ZEvent
{
public:
	static const unsigned int EVENT_WINDOW_RESIZE = 1;

	int iWidth;
	int iHeight;

public:
	explicit ZEvent_WindowResize(int _newWidth, int _newHeight) 
		: ZEvent(EVENT_WINDOW_RESIZE), iWidth(_newWidth), 
		  iHeight(_newHeight) {}
	virtual ~ZEvent_WindowResize() {}

private:
	ZEvent_WindowResize(const ZEvent_WindowResize&){}
	ZEvent_WindowResize& operator=(const ZEvent_WindowResize&){}

};

#endif
