////////////////////////////////////////////////////////////////
// Name: ZWindow
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZWINDOW_HEADER_
#define _ZWINDOW_HEADER_

/////////////////////////////////////////////////////// Includes
#include <string>
#include "ZEventDispatcher.h"
#include "events/ZEvent_WindowResize.h"

//////////////////////////////////////////////////// Definitions
class ZD3D9;

////////////////////////////////////////////////////////////////
/// @class ZWindow
/// @brief The main application window class.
/// 
class ZWindow : public ZEventDispatcher
{
	friend class ZD3D9;
public:
	explicit ZWindow(const wchar_t* window_title = L"ZGUI Window", int width = 800, int height = 500);
	virtual ~ZWindow();

	void start();
	void process();
	void goFullscreen(ZD3D9& zd3d9);
	void goWindowed();
	void doWindowResize();

	// Accessors
	int getWidth() const {return iWidth;}
	int getHeight() const {return iHeight;}
	bool isAlive() const {return bAlive;}

private:
	ZWindow(const ZWindow&){}
	ZWindow& operator=(const ZWindow&){}

	void construct();

	// PIMPL
	struct PIMPL;
	PIMPL* pimpl;

	// Private methods
	void* getWindowHandle() const;

protected:
	int iWidth;
	int iHeight;
	std::wstring strTitle;
	bool bAlive;
	bool bFullscreen;
};

#endif
