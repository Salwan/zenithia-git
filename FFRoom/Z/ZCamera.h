////////////////////////////////////////////////////////////////
// Name: ZCamera
// Desc: A simple fly camera with embedded controls using ZInput.
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZCAMERA_HEADER_
#define _ZCAMERA_HEADER_

/////////////////////////////////////////////////////// Includes
#include "ZD3D9.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// @class ZCamera
/// @brief
/// 
class ZCamera 
{
public:
	explicit ZCamera();
	virtual ~ZCamera();

	void strafeHorizontal(float units);
	void strafeVertical(float units);
	void walk(float units);
	void pitch(float angle);
	void yaw(float angle);

	void getViewMatrix(D3DXMATRIX& view_out);
	void getPosition(D3DXVECTOR3& position_out) const;
	void setPosition(const D3DXVECTOR3& position);
	void getRightVector(D3DXVECTOR3& right) const;
	void getUpVector(D3DXVECTOR3& up) const;
	void getLookVector(D3DXVECTOR3& look) const;

private:
	ZCamera(const ZCamera&){}
	ZCamera& operator=(const ZCamera&){}

	D3DXVECTOR3 vRight;
	D3DXVECTOR3 vUp;
	D3DXVECTOR3 vLook;
	D3DXVECTOR3 vPosition;
};

#endif
