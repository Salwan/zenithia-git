////////////////////////////////////////////////////////////////
// Name: ZStencil
// Notes: (ref & mask) ComparisonOperation (value & mask)
//		  if == true: write pixel to back/depth buffer
//		  else: don't alter the back/depth buffer
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "ZStencil.h"

//////////////////////////////////////////////////// Definitions
bool g_bTwoSidedStencilBegin = false;

////////////////////////////////////////////////////////////////
/// Destructor
///
ZStencil::~ZStencil()
{
	
}

////////////////////////////////////////////////////////////////
/// Starts stencil rendering
///
void ZStencil::begin(bool two_sided_stencil)
{
	ZD3D9::device->SetRenderState(::D3DRS_STENCILENABLE, true);
	if(two_sided_stencil && ZD3D9::supportsTwoSidedStencil())
	{
		g_bTwoSidedStencilBegin = true;
		ZD3D9::device->SetRenderState(::D3DRS_TWOSIDEDSTENCILMODE, true);
	}
}

////////////////////////////////////////////////////////////////
/// Ends stencil rendering
///
void ZStencil::end()
{
	if(g_bTwoSidedStencilBegin)
	{
		g_bTwoSidedStencilBegin = false;
		ZD3D9::device->SetRenderState(::D3DRS_TWOSIDEDSTENCILMODE, false);
	}
	ZD3D9::device->SetRenderState(::D3DRS_STENCILENABLE, false);
}

////////////////////////////////////////////////////////////////
///
void ZStencil::setRefMask(int reference, int mask)
{
	ZD3D9::device->SetRenderState(D3DRS_STENCILREF, reference);
	ZD3D9::device->SetRenderState(D3DRS_STENCILMASK, mask);
}

////////////////////////////////////////////////////////////////
///
void ZStencil::setTest(EStencilComparison stencil_operation)
{
	ZD3D9::device->SetRenderState(D3DRS_STENCILFUNC, stencil_operation);
}

////////////////////////////////////////////////////////////////
///
void ZStencil::setTestCCW(EStencilComparison stencil_operation)
{
	ZD3D9::device->SetRenderState(D3DRS_CCW_STENCILFUNC, stencil_operation);
}

////////////////////////////////////////////////////////////////
///
void ZStencil::setOperation(EStencilOperation pass, 
	EStencilOperation stencil_fail, EStencilOperation z_fail)
{
	ZD3D9::device->SetRenderState(::D3DRS_STENCILPASS, pass);
	ZD3D9::device->SetRenderState(::D3DRS_STENCILFAIL, stencil_fail);
	ZD3D9::device->SetRenderState(::D3DRS_STENCILZFAIL, z_fail);
}

////////////////////////////////////////////////////////////////
///
void ZStencil::setOperationCCW(EStencilOperation pass, 
	EStencilOperation stencil_fail, EStencilOperation z_fail)
{
	ZD3D9::device->SetRenderState(::D3DRS_CCW_STENCILPASS, pass);
	ZD3D9::device->SetRenderState(::D3DRS_CCW_STENCILFAIL, stencil_fail);
	ZD3D9::device->SetRenderState(::D3DRS_CCW_STENCILZFAIL, z_fail);
}

////////////////////////////////////////////////////////////////
///
void ZStencil::setWriteMask(int write_mask)
{
	ZD3D9::device->SetRenderState(::D3DRS_STENCILWRITEMASK, write_mask);
}
