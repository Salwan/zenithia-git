////////////////////////////////////////////////////////////////
// Name: Zenithia
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
/// @file Zenithia.h
/// The main header file for Zenithia framework.
///
#ifndef _ZENITHIA_HEADER_
#define _ZENITHIA_HEADER_

#include "ZDefs.h"
#include "ZUtils.h"

#include "ZLog.h"
#include "ZError.h"
#include "ZException.h"
#include "ZAssert.h"

#include "ZClock.h"
#include "ZApplication.h"
#include "ZWindow.h"

#include "ZD3D9.h"
#include "ZVertexDeclaration.h"
#include "ZVertexBuffer.h"
#include "ZIndexBuffer.h"
#include "ZMesh.h"
#include "ZManualMesh.h"
#include "ZSampler.h"
#include "ZMaterial.h"
#include "ZColor.h"
#include "ZTexture.h"
#include "ZLight.h"
#include "ZAlphaBlending.h"
#include "ZStencil.h"
#include "ZInput.h"

#endif