////////////////////////////////////////////////////////////////
// Name: ZApplication
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "ZApplication.h"
#include "ZWindow.h"
#include "ZAssert.h"
#include "ZClock.h"
#include "ZD3D9.h"
#include "ZInput.h"

//////////////////////////////////////////////////// Definitions
ZApplication* ZApplication::application = NULL;

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZApplication::ZApplication() : bFullscreen(false)
{
	mClock = new ZClock();
	application = this;
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZApplication::~ZApplication()
{
	application = NULL;
	delete mClock;
}

////////////////////////////////////////////////////////////////
/// Runs the application.
///
unsigned int ZApplication::run(ZWindow* window)
{
	ZASSERT(window, "ZApplication::run() given window is NULL!");
	pWindow = window;
	// Subscribe to window events
	pWindow->addEventListener(ZEvent_WindowResize::EVENT_WINDOW_RESIZE, this, EVENT_HANDLER_PTR(&ZApplication::onWindowResize));
	pWindow->start();
	init();
	ZInput::instance()->process();
	start();
	while(pWindow->isAlive())
	{
		pWindow->process();
		mClock->tick();
		ZInput::instance()->process();
		update();
		render();
	}
	end();

	return 0;
}

////////////////////////////////////////////////////////////////
/// Switches the application rendering to fullscreen mode if it's windowed.
/// Window will be resized, and device will be reset.
/// 
void ZApplication::goFullscreen(ZD3D9& zd3d9)
{
	ZASSERT(bFullscreen == false, "ZApplication::goFullscreen() window is already in fullscreen mode");
	if(!bFullscreen)
	{
		pWindow->goFullscreen(zd3d9);
		zd3d9.goFullscreen();
		onDeviceReset();
		bFullscreen = true;
	}
}

////////////////////////////////////////////////////////////////
/// Switches the application rendering to windowed mode if it's fullscreen
/// Window will be resized, and device will be reset.
/// 
void ZApplication::goWindowed(ZD3D9& zd3d9)
{
	ZASSERT(bFullscreen == true, "ZApplication::goWindowed() window is already in windowed mode");
	if(bFullscreen)
	{
		pWindow->goWindowed();
		zd3d9.goWindowed();
		onDeviceReset();
		bFullscreen = false;
	}
}

////////////////////////////////////////////////////////////////
/// Called by window on window resize event
/// 
void ZApplication::onWindowResize(const ZEvent&)
{

}


////////////////////////////////////////////////////////////////
/// Resizes the direct3d device to match the window size if there is a difference.
/// 
void ZApplication::resizeD3D(ZD3D9& zd3d9)
{
	ZASSERT(!bFullscreen, "ZApplication::resizeD3D() window is in fullscreen");
	if(!bFullscreen)
	{
		zd3d9.doResize();
		onDeviceReset();
	}
}
