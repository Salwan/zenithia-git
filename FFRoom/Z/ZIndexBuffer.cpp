////////////////////////////////////////////////////////////////
// Name: ZIndexBuffer
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "ZIndexBuffer.h"
#include "ZAssert.h"
#include "ZException.h"
#include "ZUtils.h"

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZIndexBuffer::ZIndexBuffer() : pIB(NULL), bLocked(false), pData(NULL)
{
	
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZIndexBuffer::~ZIndexBuffer()
{
	SAFE_RELEASE(pIB);
}

void ZIndexBuffer::create(unsigned int index_count, D3DFORMAT indices_format)
{
	ZASSERT(index_count > 0, "ZIndexBuffer::create() index count is 0? :-S");
	ZASSERT(!pIB, "ZIndexBuffer::create() called on an already created index buffer");
	ZASSERT(indices_format == D3DFMT_INDEX16 || indices_format == D3DFMT_INDEX32, "ZIndexBuffer::create() unknwon index format");
	if(pIB)
	{
		SAFE_RELEASE(pIB);
	}
	uIndexCount = index_count;
	mFormat = indices_format;
	if(mFormat == D3DFMT_INDEX16)
	{
		uIndexStride = 2;
	}
	else
	{
		uIndexStride = 4;
	}
	construct();
}

void ZIndexBuffer::construct()
{
	HRESULT hr;
	hr = ZD3D9::device->CreateIndexBuffer(uIndexCount * uIndexStride,
		D3DUSAGE_WRITEONLY, mFormat, D3DPOOL_MANAGED, &pIB, NULL);
	if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
}

void ZIndexBuffer::set()
{
	if(!pIB) throw ZEXCEPTION_MESSAGE("ZIndexBuffer::set() attempting to set an index buffer that hasn't been created yet");
	ZD3D9::device->SetIndices(pIB);
}

void ZIndexBuffer::lock()
{
	if(!pIB) throw ZEXCEPTION_MESSAGE("ZIndexBuffer::lock() attempting to modify an index buffer that hasn't been created yet");
	ZASSERT(!bLocked, "ZIndexBuffer::lock() attempting to lock an index buffer that's already locked");
	HRESULT hr = pIB->Lock(0, 0, &pData, 0);
	if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
	bLocked = true;
}

void ZIndexBuffer::lock(unsigned int offset_to_lock, unsigned int size_to_lock)
{
	if(!pIB) throw ZEXCEPTION_MESSAGE("ZIndexBuffer::lock() attempting to modify an index buffer that hasn't been created yet");
	ZASSERT(!bLocked, "ZIndexBuffer::lock() attempting to lock an index buffer that's already locked");
	HRESULT hr = pIB->Lock(offset_to_lock, size_to_lock, &pData, 0);
	if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
	bLocked = true;
}

void ZIndexBuffer::unlock()
{
	if(!pIB) throw ZEXCEPTION_MESSAGE("ZIndexBuffer::unlock() attempting to modify an index buffer that hasn't been created yet");
	ZASSERT(bLocked, "ZIndexBuffer::unlock() attempting to unlock an index buffer  that's not locked");
	pIB->Unlock();
	bLocked = false;
}

////////////////////////////////////////////////////////////////
/// Write indices to the index buffer.
/// @param offset_in_indices which index to start writing at
/// @param indices index data to write
/// @param index_count number of indices to write
///
void ZIndexBuffer::writeIndices(unsigned int offset_in_indices, void* indices, unsigned int index_count)
{
	if(!pIB) throw ZEXCEPTION_MESSAGE("ZIndexBuffer::writeIndices() attempting to modify an index buffer that hasn't been created yet");
	bool didlock = false;
	if(!bLocked)
	{
		lock(offset_in_indices * uIndexStride, index_count * uIndexStride);
		didlock = true;
	}

	memcpy(pData, indices, index_count * uIndexStride);

	if(didlock)
	{
		unlock();
	}
}