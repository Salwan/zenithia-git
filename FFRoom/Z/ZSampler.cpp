////////////////////////////////////////////////////////////////
// Name: ZSampler
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "ZSampler.h"
#include "ZD3D9.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZSampler::ZSampler()
{
	
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZSampler::~ZSampler()
{
	
}

void ZSampler::setSamplerFilter(unsigned int sampler, ESamplerFilter filter_type, unsigned int anisotropy_level)
{
	unsigned int magf;
	unsigned int minf;
	unsigned int mipf;

	switch(filter_type)
	{
	case ESF_NONE:
		magf = D3DTEXF_NONE;
		minf = D3DTEXF_NONE;
		mipf = D3DTEXF_NONE;
		break;

	case ESF_POINT:
		magf = D3DTEXF_POINT;
		minf = D3DTEXF_POINT;
		mipf = D3DTEXF_POINT;
		break;

	case ESF_BILINEAR:
		magf = D3DTEXF_LINEAR;
		minf = D3DTEXF_LINEAR;
		mipf = D3DTEXF_POINT;
		break;

	case ESF_TRILINEAR:
		magf = D3DTEXF_LINEAR;
		minf = D3DTEXF_LINEAR;
		mipf = D3DTEXF_LINEAR;
		break;

	case ESF_ANISOTROPIC:
		magf = D3DTEXF_ANISOTROPIC;
		minf = D3DTEXF_ANISOTROPIC;
		mipf = D3DTEXF_ANISOTROPIC;
		break;

	default:
		magf = D3DTEXF_POINT;
		minf = D3DTEXF_POINT;
		mipf = D3DTEXF_POINT;
	}
	
	ZD3D9::device->SetSamplerState(sampler, D3DSAMP_MAGFILTER, magf);
	ZD3D9::device->SetSamplerState(sampler, D3DSAMP_MINFILTER, minf);
	ZD3D9::device->SetSamplerState(sampler, D3DSAMP_MIPFILTER, mipf);

	if(filter_type == ESF_ANISOTROPIC)
	{
		anisotropy_level = min(anisotropy_level, ZD3D9::d3d->getCaps().MaxAnisotropy);
		ZD3D9::device->SetSamplerState(sampler, D3DSAMP_MAXANISOTROPY, anisotropy_level);
	}
	else
	{
		ZD3D9::device->SetSamplerState(sampler, D3DSAMP_MAXANISOTROPY, 0);
	}
}

void ZSampler::setTextureWrap(unsigned int sampler)
{
	ZD3D9::device->SetSamplerState(sampler, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	ZD3D9::device->SetSamplerState(sampler, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
}

void ZSampler::setTextureBorder(unsigned int sampler, unsigned int border_color)
{
	ZD3D9::device->SetSamplerState(sampler, D3DSAMP_ADDRESSU, D3DTADDRESS_BORDER);
	ZD3D9::device->SetSamplerState(sampler, D3DSAMP_ADDRESSV, D3DTADDRESS_BORDER);
	ZD3D9::device->SetSamplerState(sampler, D3DSAMP_BORDERCOLOR, border_color);
}

void ZSampler::setTextureClamp(unsigned int sampler)
{
	ZD3D9::device->SetSamplerState(sampler, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
	ZD3D9::device->SetSamplerState(sampler, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
}

void ZSampler::setTextureMirror(unsigned int sampler)
{
	ZD3D9::device->SetSamplerState(sampler, D3DSAMP_ADDRESSU, D3DTADDRESS_MIRROR);
	ZD3D9::device->SetSamplerState(sampler, D3DSAMP_ADDRESSV, D3DTADDRESS_MIRROR);
}

////////////////////////////////////////////////////////////////
/// Manually set the U and V addressing mode to whatever you want.
/// 
void ZSampler::setTextureAddressingMode(unsigned int sampler, unsigned int d3dtaddressu, unsigned int d3dtaddressv)
{
	ZD3D9::device->SetSamplerState(sampler, D3DSAMP_ADDRESSU, d3dtaddressu);
	ZD3D9::device->SetSamplerState(sampler, D3DSAMP_ADDRESSV, d3dtaddressv);
}

