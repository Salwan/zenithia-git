////////////////////////////////////////////////////////////////
// Name: ZDefs
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZDEFS_HEADER_
#define _ZDEFS_HEADER_

/////////////////////////////////////////////////////// Includes

//////////////////////////////////////////////////// Definitions
#define WIN32_LEAN_AND_MEAN

/// If an assert fails, it's logged and a breakpoint is triggered.
/// If this wasn't defined, the assert will be logged but the
/// program is allowed to continue excution anyway.
#define ASSERT_DEBUG

#endif
