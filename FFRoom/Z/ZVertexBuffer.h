////////////////////////////////////////////////////////////////
// Name: ZVertexBuffer
// Desc: Wraps a vertex buffer to simplify the process of 
//       definition and usage. (ZMesh will depend on it)
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZVERTEXBUFFER_HEADER_
#define _ZVERTEXBUFFER_HEADER_

/////////////////////////////////////////////////////// Includes
#include "ZD3D9.h"
#include <vector>

//////////////////////////////////////////////////// Definitions
class ZVertexDeclaration;

////////////////////////////////////////////////////////////////
/// @class ZVertexBuffer
/// @brief Wraps a vertex buffer to simplify it's definition and usage
/// 
class ZVertexBuffer 
{
public:
	explicit ZVertexBuffer();
	virtual ~ZVertexBuffer();

	// Methods
	void create(ZVertexDeclaration& vertex_declaration, unsigned int vertex_count);
	void set();
	void lock();
	void unlock();
	template <typename ElementType>
	void writeElements(unsigned int element_index, const ElementType* element_data, unsigned int element_count, unsigned int element_offset = 0);
	void writeVertices(unsigned int offset_in_bytes, void* vertices, unsigned int vertex_count);

	// Accessors
	unsigned int getVertexCount() const {return uVertexCount;}

private:
	ZVertexBuffer(const ZVertexBuffer&){}
	ZVertexBuffer& operator=(const ZVertexBuffer&){}

	void lock(unsigned int offset_to_lock, unsigned int size_to_lock);
	void construct();

	IDirect3DVertexBuffer9* pVB;
	std::vector<WORD> mElementOffset;
	unsigned int uVertexCount;
	unsigned int uElementCount;
	unsigned int uStride;
	void* pData;
	bool bLocked;
};

#include "ZVertexBuffer.inl"

#endif
