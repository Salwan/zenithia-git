////////////////////////////////////////////////////////////////
// Name: ZVertexBuffer
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "ZVertexBuffer.h"
#include "ZVertexDeclaration.h"
#include "ZAssert.h"
#include "ZException.h"
#include "ZUtils.h"

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZVertexBuffer::ZVertexBuffer() : pVB(NULL), bLocked(false), pData(NULL)
{
	
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZVertexBuffer::~ZVertexBuffer()
{
	SAFE_RELEASE(pVB);
}

////////////////////////////////////////////////////////////////
/// Creates the vertex buffer using the provided information.
/// If you call create more than once, it will assert. If assert
/// is disabled the vertex buffer will just be recreated using
/// the provided information.
/// @param vertex_declaration The vertex element declaration
/// @param vertex_count the total number of vertices in the buffer
///
void ZVertexBuffer::create(ZVertexDeclaration& vertex_declaration, 
	unsigned int vertex_count)
{
	ZASSERT(vertex_count > 0, "ZVertexBuffer::create() vertex count is 0? why would you want to create a vertex buffer with 0 vertices");
	ZASSERT(vertex_declaration.isComplete(), "ZVertexBuffer::create() given vertex declaration is incomplete, you should add an end element");
	ZASSERT(!pVB, "ZVertexBuffer::create() called on an already created vertex buffer");
	if(pVB)
	{
		SAFE_RELEASE(pVB);
	}
	uVertexCount = vertex_count;
	uStride = static_cast<unsigned int>(vertex_declaration.getVertexSize());
	uElementCount = vertex_declaration.getElementCount();
	mElementOffset.clear();
	D3DVERTEXELEMENT9* pe = vertex_declaration.getElementsDeclaration();
	for(unsigned int i = 0; i < uElementCount; ++i)
	{
		mElementOffset.push_back(pe[i].Offset);
	}
	construct();
}

void ZVertexBuffer::construct()
{
	HRESULT hr;
	hr = ZD3D9::device->CreateVertexBuffer(uStride * uVertexCount, 
		D3DUSAGE_WRITEONLY, 0, D3DPOOL_MANAGED, &pVB, NULL);
	if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
}

void ZVertexBuffer::set()
{
	if(!pVB) throw ZEXCEPTION_MESSAGE("ZVertexBuffer::set() attempting to set vertex buffer before creation");
	ZD3D9::device->SetStreamSource(0, pVB, 0, uStride);
}

////////////////////////////////////////////////////////////////
/// Locks the entire vertex buffer.
/// Use writeElements or writeVertices instead of you want to 
/// write to a specific segment.
///
void ZVertexBuffer::lock()
{
	if(!pVB) throw ZEXCEPTION_MESSAGE("ZVertexBuffer::lock() attempting to modify a vertex buffer that hasn't been created yet");
	ZASSERT(!bLocked, "ZVertexBuffer::lock() attempting to lock an already locked vertex buffer");
	HRESULT hr = pVB->Lock(0, 0, &pData, 0);
	if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
	bLocked = true;
}

void ZVertexBuffer::lock(unsigned int offset_to_lock, unsigned int size_to_lock)
{
	if(!pVB) throw ZEXCEPTION_MESSAGE("ZVertexBuffer::lock() attempting to modify a vertex buffer that hasn't been created yet");
	ZASSERT(!bLocked, "ZVertexBuffer::lock() attempting to lock an already locked vertex buffer");
	HRESULT hr = pVB->Lock(offset_to_lock, size_to_lock, &pData, 0);
	if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
	bLocked = true;
}

////////////////////////////////////////////////////////////////
/// Unlocks the vertex buffer
/// 
void ZVertexBuffer::unlock()
{
	ZASSERT(pVB, "ZVertexBuffer::unlock() attempting to unlock a vertex buffer that hasn't been created yet");
	ZASSERT(bLocked, "ZVertexBuffer::unlock() attempting to unlock an already unlocked vertex buffer");
	pData = NULL;
	if(!pVB) return;
	pVB->Unlock();
	bLocked = false;
}

////////////////////////////////////////////////////////////////
/// Writes whole vertices into the vertex buffer.
/// @param offset_in_bytes where to start writing vertices.
/// @param vertices raw vertex data
/// @param vertex_count how many vertices to write
/// 
void ZVertexBuffer::writeVertices(unsigned int offset_in_bytes, void* vertices, 
	unsigned int vertex_count)
{
	if(!pVB) throw ZEXCEPTION_MESSAGE("ZVertexBuffer::writeVertices() attempting to modify a vertex buffer that hasn't been created yet");
	bool didlock = false;
	if(!bLocked)
	{
		didlock = true;
		lock(offset_in_bytes, vertex_count * uStride);
	}

	memcpy(pData, vertices, uStride * vertex_count);

	if(didlock)
	{
		unlock();
	}
}