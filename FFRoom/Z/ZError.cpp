////////////////////////////////////////////////////////////////
// Name: ZError
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "ZError.h"
#include <sstream>
#include <Windows.h>

////////////////////////////////////////////////////////////////
/// Retrieves and formats a windows error message code (DWORD)
/// @param [in] windows_error_message windows error message (DWORD)
/// @param [out] string_out the output string that will receive the message
///
void ZError::getWindowsErrorMessage(unsigned long windows_error_message, 
	std::string& string_out)
{
	std::ostringstream ss;
	LPSTR error_buffer = new char [129];
	if(!FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM, NULL, windows_error_message, 
		0, (LPSTR)error_buffer, 128, NULL))
	{
		ss<<"Couldn't retrieve error message: "<<windows_error_message;
		string_out = ss.str();
		OutputDebugStringA(string_out.c_str());
	}
	else
	{
		ss<<error_buffer;
		string_out = ss.str();
	}
	delete [] error_buffer;
}

////////////////////////////////////////////////////////////////
/// Retrieves and formats the last windows error via GetLast Error.
/// @param [out] string_out the output string that will receive the message
///
void ZError::getLastWindowsErrorMessage(std::string& string_out)
{
	getWindowsErrorMessage(GetLastError(), string_out);
}

////////////////////////////////////////////////////////////////
/// Shows a simple message box with the error message.
/// @param error_message the error message to show.
///
void ZError::showError(const std::string& error_message)
{
	MessageBoxA(NULL, error_message.c_str(), "Error", MB_OK|MB_ICONERROR);
}
