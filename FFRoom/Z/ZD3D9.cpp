////////////////////////////////////////////////////////////////
// Name: ZD3D9
// Notes:
// - Add support for fullscreen mode, use current display mode.
// - I'm already enumerating display modes but not doing anything 
//   with it.
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "DxErr.lib")
#ifndef _DEBUG
#pragma comment(lib, "d3dx9.lib")
#else
#pragma comment(lib, "d3dx9d.lib")
#endif

/////////////////////////////////////////////////////// Includes
#include "ZD3D9.h"
#include <iostream>
#include <string>
#include <sstream>
#include <Windows.h>
#include <DxErr.h>
#include "ZWindow.h"
#include "ZException.h"
#include "ZAssert.h"
#include "ZUtils.h"

//////////////////////////////////////////////////// Definitions
HWND g_hWnd = NULL;
IDirect3DDevice9* ZD3D9::device = NULL;
ZD3D9* ZD3D9::d3d = NULL;
bool g_bCapabilityTwoSidedStencil = false;
int g_iCapabilityMultipleRenderToTexture = 0;
bool g_bCapabilityMRTPostPixelShaderBlending = false;

// Private Implementation
struct ZD3D9::PIMPL
{
	int width;
	int height;
	HWND hWnd;
};

HWND ZD3D9::getWindowHandle()
{
	return g_hWnd;
}

bool ZD3D9::supportsTwoSidedStencil()
{
	return g_bCapabilityTwoSidedStencil;
}

int ZD3D9::numMultipleRenderToTexture()
{
	return g_iCapabilityMultipleRenderToTexture;
}

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZD3D9::ZD3D9(ZWindow& window) : pD3D9(NULL), pDevice(NULL), bFullscreen(false)
{
	d3d = this;
	pimpl = new PIMPL;
	pimpl->width = window.getWidth();
	pimpl->height = window.getHeight();
	HWND* hWnd = static_cast<HWND*>(window.getWindowHandle());
	pimpl->hWnd = *hWnd;
	g_hWnd = *hWnd;

	createD3D9();
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZD3D9::~ZD3D9()
{
	destroyD3D9();
	ZD3D9::device = NULL;
	delete pimpl;
	d3d = NULL;
}

////////////////////////////////////////////////////////////////
/// Clears the backbuffer, depth buffer, and stencil buffer.
///
void ZD3D9::clear(D3DCOLOR fillcolor)
{
	pDevice->Clear(0, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER|D3DCLEAR_STENCIL, fillcolor, 1.0f, 0);
}

////////////////////////////////////////////////////////////////
/// Flips the backbuffer.
/// 
void ZD3D9::present()
{
	HRESULT hr;
	hr = pDevice->Present(0, 0, 0, 0);
	if(FAILED(hr)) ZEXCEPTION_DXERROR(hr);
}

////////////////////////////////////////////////////////////////
/// Initialize Direct3D9
///
void ZD3D9::createD3D9()
{
	HRESULT hr;
	std::clog<<"[ZD3D9] Creating D3D9"<<std::endl;
	pD3D9 = Direct3DCreate9(D3D_SDK_VERSION);
	if(!pD3D9)
	{
		throw ZEXCEPTION_MESSAGE("ZD3D9::createD3D9() couldn't create Direct3D9 object");
	}
	std::clog<<"[ZD3D9] Direct3D9 interface created"<<std::endl;
	// D3DDEVICE_REF: Reference rasterizer
	// D3DDEVICE_HAL: Hardware abstraction layer
	hr = pD3D9->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_REF, &mD3DCaps);
	if(FAILED(hr))
	{
		throw ZEXCEPTION_DXERROR(hr);
	}
	std::clog<<"[ZD3D9] Direct3D9 device caps read"<<std::endl;

	g_bCapabilityTwoSidedStencil = (mD3DCaps.StencilCaps & D3DSTENCILCAPS_TWOSIDED) != 0? true : false;
	g_iCapabilityMultipleRenderToTexture = static_cast<int>(mD3DCaps.NumSimultaneousRTs);
	g_bCapabilityMRTPostPixelShaderBlending = (mD3DCaps.PrimitiveMiscCaps & D3DPMISCCAPS_MRTPOSTPIXELSHADERBLENDING) != 0? true : false;

	// Enumerating display modes
	UINT displayModeCount = pD3D9->GetAdapterModeCount(D3DADAPTER_DEFAULT, D3DFMT_X8R8G8B8);
	std::clog<<"[ZD3D9] 32-bit display modes (XRGB8): "<< displayModeCount <<std::endl;
	D3DDISPLAYMODE dm;
	for(unsigned int i = 0; i < displayModeCount; ++i)
	{
		pD3D9->EnumAdapterModes(D3DADAPTER_DEFAULT, D3DFMT_X8R8G8B8, i, &dm);
		std::clog<<"	"<<i<<": "<<dm.Width<<"x"<<dm.Height<<" @ "<<dm.RefreshRate<<std::endl;
	}

	// Default fullscreen mode is current display mode
	pD3D9->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &mFullscreenMode);

	// Presentation Parameters
	memset(&mPresentParams, 0, sizeof(D3DPRESENT_PARAMETERS));
	mPresentParams.BackBufferWidth = pimpl->width;
	mPresentParams.BackBufferHeight = pimpl->height;
	mPresentParams.BackBufferFormat = D3DFMT_UNKNOWN; // For windowed: D3DFMT_UNKNOWN to use current format
	mPresentParams.BackBufferCount = 1;
	mPresentParams.MultiSampleType = D3DMULTISAMPLE_NONE;
	mPresentParams.MultiSampleQuality = 0;
	mPresentParams.SwapEffect = D3DSWAPEFFECT_DISCARD; // D3DSWAPEFFECT_FLIPEX for Windows 7 is better, investigate
	mPresentParams.hDeviceWindow = pimpl->hWnd;
	mPresentParams.Windowed = true;
	mPresentParams.EnableAutoDepthStencil = true;
	mPresentParams.AutoDepthStencilFormat = D3DFMT_D24S8;
	mPresentParams.Flags = 0;
	mPresentParams.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	mPresentParams.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

	// Behavior Flags:
	// [Vista only] D3DCREATE_DISABLE_PSGP_THREADING: restrict software vertex processing to main thread (if not specified D3D may do computations in worker threads)
	// D3DCREATE_HARDWARE_VERTEXPROCESSING: All hardware vertex processing
	// D3DCREATE_MIXED_VERTEX_PROCESSING: Can optionally use software processing on specific buffers created with software processing flag, otherwise will use hardware and emulate when necessary
	// D3DCREATE_SOFTWARE_VERTEX_PROCESSING: does all vertex processing in software, slowest
	// D3DCREATE_MULTITHREADED: D3D will be thread safe, can degrade performance, this flag must be used when you have one thread for window message and one for d3d calls
	// D3DCREATE_PUREDEVICE: D3D will not do any emulation services, will not save state info so Get* functions are not supported (like GetRenderState), and will not filter redundant state changes, so you have to do that manually
	hr = pD3D9->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, pimpl->hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &mPresentParams, &pDevice);
	if(FAILED(hr))
	{
		std::clog<<"[ZD3D9] WARNING: Couldn't create Direct3D9 hardware vertex processing device"<<std::endl;
		hr = pD3D9->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, pimpl->hWnd, 
			D3DCREATE_MIXED_VERTEXPROCESSING, &mPresentParams, &pDevice);
		if(FAILED(hr))
		{
			hr = pD3D9->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, pimpl->hWnd, 
				D3DCREATE_SOFTWARE_VERTEXPROCESSING, &mPresentParams, &pDevice);
			if(FAILED(hr)) 
			{
				throw ZEXCEPTION_DXERROR(hr);
			}
			std::clog<<"[ZD3D9] Using software vertex processing instead, performance will suffer"<<std::endl;
		}
		else
		{
			std::clog<<"[ZD3D9] Using mixed vertex processing instead, performance may suffer"<<std::endl;
		}
	}
	ZD3D9::device = pDevice;
	std::clog<<"[ZD3D9] Direct3D9 device created"<<std::endl;
}


////////////////////////////////////////////////////////////////
/// Destroys D3D9
///
void ZD3D9::destroyD3D9()
{
	SAFE_RELEASE(pDevice);
	SAFE_RELEASE(pD3D9);
}

void ZD3D9::goFullscreen()
{
	ZASSERT(bFullscreen == false, "ZD3D9::goFullscreen() is already in fullscreen mode");
	if(!bFullscreen)
	{
		RECT wrect;
		GetWindowRect(pimpl->hWnd, &wrect);
		pimpl->width = wrect.right - wrect.left;
		pimpl->height = wrect.bottom - wrect.top;

		D3DPRESENT_PARAMETERS pp;
		memcpy(&pp, &mPresentParams, sizeof(D3DPRESENT_PARAMETERS));
		pp.BackBufferWidth = mFullscreenMode.Width;
		pp.BackBufferHeight = mFullscreenMode.Height;
		pp.BackBufferFormat = mFullscreenMode.Format;
		pp.FullScreen_RefreshRateInHz = mFullscreenMode.RefreshRate;
		pp.Windowed = false;
		HRESULT hr = pDevice->Reset(&pp);
		if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
		bFullscreen = true;
	}
}

void ZD3D9::goWindowed()
{
	ZASSERT(bFullscreen == true, "ZD3D9::goWindowed() is already in windowed mode");
	if(bFullscreen)
	{
		mPresentParams.BackBufferWidth = pimpl->width;
		mPresentParams.BackBufferHeight = pimpl->height;
		HRESULT hr = pDevice->Reset(&mPresentParams);
		if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
		bFullscreen = false;
	}
}

void ZD3D9::doResize()
{
	if(!bFullscreen)
	{
		RECT wr;
		GetWindowRect(pimpl->hWnd, &wr);
		int width = max(wr.right - wr.left, 1);
		int height = max(wr.bottom - wr.top, 1);
		if(width != pimpl->width || 
			height != pimpl->height)
		{
			pimpl->width = width;
			pimpl->height = height;
			mPresentParams.BackBufferWidth = pimpl->width;
			mPresentParams.BackBufferHeight = pimpl->height;
			HRESULT hr = pDevice->Reset(&mPresentParams);
			if(FAILED(hr)) throw ZEXCEPTION_DXERROR(hr);
			OutputDebugStringA("<Device Reset due to resize>\n");
		}
	}
}