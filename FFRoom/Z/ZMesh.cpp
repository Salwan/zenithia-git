////////////////////////////////////////////////////////////////
// Name: ZMesh
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "ZMesh.h"
#include "ZVertexBuffer.h"
#include "ZVertexDeclaration.h"
#include "ZIndexBuffer.h"
#include "ZMaterial.h"
#include "ZAssert.h"
#include "ZException.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZMesh::ZMesh()
{

}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZMesh::~ZMesh()
{
	for(unsigned int i = 0; i < mVBufs.size(); ++i)
	{
		delete mVDecls[i];
		delete mVBufs[i];
		delete mIBufs[i];
		delete mMaterials[i];
	}
}

////////////////////////////////////////////////////////////////
/// Draws an indexed mesh subset.
/// 
void ZMesh::drawSubset(IDirect3DDevice9& device, unsigned int subset)
{
	ZASSERT(!mVBufs.empty(), "ZMesh::drawSubset() there are no defined subsets!");
	ZASSERT(subset < mVBufs.size(), "ZMesh::drawSubset() given subset is invalid");
	
	_getVD(subset).set();
	ZVertexBuffer& vb = _getVB(subset);
	ZIndexBuffer& ib = _getIB(subset);
	ZMaterial& mtrl = _getMaterial(subset);
	vb.set();
	ib.set();
	mtrl.set();
	device.DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, vb.getVertexCount(),
		0, ib.getFaceCount());
}

////////////////////////////////////////////////////////////////
/// Draws all subsets within this mesh.
/// 
void ZMesh::drawAll(IDirect3DDevice9& device)
{
	ZASSERT(!mVBufs.empty(), "ZMesh::drawAll() there are no defined subsets!");
	for(unsigned int i = 0; i < mVBufs.size(); ++i)
	{
		drawSubset(device, i);
	}
}

////////////////////////////////////////////////////////////////
/// @return the number of defined subsets
/// 
unsigned int ZMesh::getSubsetCount() const
{
	return mVBufs.size();
}

unsigned int ZMesh::_createSubset()
{
	mVDecls.push_back(new ZVertexDeclaration());
	mVBufs.push_back(new ZVertexBuffer());
	mIBufs.push_back(new ZIndexBuffer());
	mMaterials.push_back(new ZMaterial());
	return mVBufs.size() - 1;
}

ZVertexDeclaration& ZMesh::_getVD(unsigned int subset)
{
	if(mVDecls.empty())
	{
		throw ZEXCEPTION_MESSAGE("ZMesh::getVertexDeclaration() there are no defined subsets!");
	}
	if(subset >= mVDecls.size())
	{
		throw ZEXCEPTION_MESSAGE("ZMesh::getVertexDeclaration() given subset is invalid");
	}
	return *(mVDecls[subset]);
}

ZVertexBuffer& ZMesh::_getVB(unsigned int subset)
{
	if(mVBufs.empty())
	{
		throw ZEXCEPTION_MESSAGE("ZMesh::getVertexBuffer() there are no defined subsets!");
	}
	if(subset >= mVBufs.size())
	{
		throw ZEXCEPTION_MESSAGE("ZMesh::getVertexBuffer() given subset is invalid");
	}
	return *(mVBufs[subset]);
}

ZIndexBuffer& ZMesh::_getIB(unsigned int subset)
{
	if(mIBufs.empty())
	{
		throw ZEXCEPTION_MESSAGE("ZMesh::getIndexBuffer() there are no defined subsets!");
	}
	if(subset >= mIBufs.size())
	{
		throw ZEXCEPTION_MESSAGE("ZMesh::getIndexBuffer() given subset is invalid");
	}
	return *(mIBufs[subset]);
}

ZMaterial& ZMesh::_getMaterial(unsigned int subset)
{
	if(mMaterials.empty())
	{
		throw ZEXCEPTION_MESSAGE("ZMesh::getMaterial() there are no defined subsets!");
	}
	if(subset >= mMaterials.size())
	{
		throw ZEXCEPTION_MESSAGE("ZMesh::getMaterial() given subset is invalid");
	}
	return *(mMaterials[subset]);
}