////////////////////////////////////////////////////////////////
// Name: ZLight
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "ZLight.h"
#include "ZColor.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZLight::ZLight()
{
	mLight.Type = ::D3DLIGHT_POINT;
	mLight.Ambient = ZColor::createD3DColorValue(0.1f, 0.1f, 0.1f);
	mLight.Diffuse = ZColor::createD3DColorValue(1.0f, 1.0f, 1.0f);
	mLight.Specular = ZColor::createD3DColorValue(1.0f, 1.0f, 1.0f);
	mLight.Attenuation0 = 0.0f;
	mLight.Attenuation1 = 0.125f;
	mLight.Attenuation2 = 0.0f;
	mLight.Direction.x = 0.0f;
	mLight.Direction.y = -1.0f;
	mLight.Direction.z = 0.0f;
	mLight.Position.x = 0.0f;
	mLight.Position.y = 0.0f;
	mLight.Position.z = 0.0f;
	mLight.Range = 100.0f;
	mLight.Falloff = 0.0f;
	mLight.Phi = 0.0f;
	mLight.Theta = 0.0f;
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZLight::~ZLight()
{
	
}

////////////////////////////////////////////////////////////////
/// Sets light
/// 
void ZLight::set(unsigned int index)
{
	ZD3D9::device->SetLight(index, &mLight);
}

////////////////////////////////////////////////////////////////
/// Set this light to be point light, other parameters:
/// - Attenuation0, 1, and 2
/// 
void ZLight::setPointLight(const ZColor& diffuse, const D3DXVECTOR3& position, float range)
{
	mLight.Type = ::D3DLIGHT_POINT;
	mLight.Diffuse = diffuse.toD3DColorValue();
	setPosition(position);
	mLight.Range = range;
}

////////////////////////////////////////////////////////////////
/// Set this light to be directional
/// 
void ZLight::setDirectionalLight(const ZColor& diffuse, const D3DXVECTOR3& direction)
{
	mLight.Type = ::D3DLIGHT_DIRECTIONAL;
	mLight.Diffuse = diffuse.toD3DColorValue();
	setDirection(direction);
}

////////////////////////////////////////////////////////////////
/// Set this light to be spot, other parameters:
/// - Attenuation0, 1, and 2
/// 
void ZLight::setSpotLight(const ZColor& diffuse, const D3DXVECTOR3& position, const D3DXVECTOR3& direction, float range, float theta, float phi, float falloff)
{
	mLight.Type = ::D3DLIGHT_SPOT;
	mLight.Diffuse = diffuse.toD3DColorValue();
	setPosition(position);
	setDirection(direction);
	mLight.Range = range;
	mLight.Theta = theta;
	mLight.Phi = phi;
	mLight.Falloff = falloff;
}

void ZLight::setAttenuation(float attenuation_0, float attenuation_1, float attenuation_2)
{
	mLight.Attenuation0 = attenuation_0;
	mLight.Attenuation1 = attenuation_1;
	mLight.Attenuation2 = attenuation_2;
}

void ZLight::setPosition(const D3DXVECTOR3& position)
{
	mLight.Position.x = position.x;
	mLight.Position.y = position.y;
	mLight.Position.z = position.z;
}

void ZLight::setDirection(const D3DXVECTOR3& direction)
{
	mLight.Direction.x = direction.x;
	mLight.Direction.y = direction.y;
	mLight.Direction.z = direction.z;
}

void ZLight::setDiffuse(const ZColor& diffuse)
{
	mLight.Diffuse = diffuse.toD3DColorValue();
}

void ZLight::setAmbient(const ZColor& ambient)
{
	mLight.Ambient = ambient.toD3DColorValue();
}

////////////////////////// STATIC //////////////////////////////
//==============================================================
////////////////////////////////////////////////////////////////
/// Enables a light in Direct3D
/// 
void ZLight::Enable(unsigned int index)
{
	ZD3D9::device->LightEnable(index, true);
}

////////////////////////////////////////////////////////////////
/// Disabled a light in Direct3D
/// 
void ZLight::Disable(unsigned int index)
{
	ZD3D9::device->LightEnable(index, false);
}

////////////////////////////////////////////////////////////////
/// Figures out whether a light is enabled or disabled
/// Only works with non-pure devices
/// 
bool ZLight::IsEnabled(unsigned int index)
{
	BOOL en;
	ZD3D9::device->GetLightEnable(index, &en);
	return en? true : false;
}

void ZLight::EnableLighting()
{
	ZD3D9::device->SetRenderState(::D3DRS_LIGHTING, TRUE);
}

void ZLight::DisableLighting()
{
	ZD3D9::device->SetRenderState(::D3DRS_LIGHTING, FALSE);
}