////////////////////////////////////////////////////////////////
// Name: ZTexture
// Desc: A Direct3D9 texture
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZTEXTURE_HEADER_
#define _ZTEXTURE_HEADER_

/////////////////////////////////////////////////////// Includes
#include <string>
#include "ZD3D9.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// @class ZTexture
/// @brief A Direct3D9 texture
/// 
class ZTexture 
{
public:
	ZTexture(const wchar_t* filename);
	ZTexture(const IDirect3DTexture9* texture);
	virtual ~ZTexture();

	void set(unsigned int stage);

private:
	ZTexture(const ZTexture&){}
	ZTexture& operator=(const ZTexture&){}

	IDirect3DTexture9* mTexture;

};

#endif
