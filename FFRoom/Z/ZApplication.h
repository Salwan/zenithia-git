////////////////////////////////////////////////////////////////
// Name: ZApplication
// Desc: Represents the actual application running via a ZWindow
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZAPPLICATION_HEADER_
#define _ZAPPLICATION_HEADER_

/////////////////////////////////////////////////////// Includes
#include "ZEventListener.h"

//////////////////////////////////////////////////// Definitions
class ZWindow;
class ZClock;
class ZD3D9;

////////////////////////////////////////////////////////////////
/// @class ZApplication
/// @brief The actual application, override and do your magic.
/// 
class ZApplication : public ZEventListener
{
	friend class ZWindow;
public:
	static ZApplication* application;

public:
	explicit ZApplication();
	virtual ~ZApplication();

	unsigned int run(ZWindow* window);

protected:
	// Methods
	virtual void init()				= 0;
	virtual void start()			= 0;
	virtual void update()			= 0;
	virtual void render()			= 0;
	virtual void end()				= 0;
	virtual void onDeviceReset()	= 0;
	virtual void onWindowResize(const ZEvent& e);

	void goFullscreen(ZD3D9& zd3d9);
	void goWindowed(ZD3D9& zd3d9);
	void resizeD3D(ZD3D9& zd3d9);

	// Members
	ZWindow* pWindow;

	// Accessors
	bool isFullscreen() const {return bFullscreen;}

private:
	ZApplication(const ZApplication&){}
	ZApplication& operator=(const ZApplication&){}

	ZClock* mClock;
	bool bFullscreen;
};

#endif
