////////////////////////////////////////////////////////////////
// Name: ZMaterial
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// Includes
#include "ZMaterial.h"
#include "ZColor.h"
#include "ZException.h"
#include "ZTexture.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// Default Constructor
///
ZMaterial::ZMaterial()
{
	mMaterial.Ambient = ZColor::createD3DColorValue(0.1f, 0.1f, 0.1f);
	mMaterial.Diffuse = ZColor::createD3DColorValue(1.0f, 1.0f, 1.0f);
	mMaterial.Specular = ZColor::createD3DColorValue(1.0f, 1.0f, 1.0f);
	mMaterial.Emissive = ZColor::createD3DColorValue(0.0f, 0.0f, 0.0f);
	mMaterial.Power = 0.5f;

	mTextures.reserve(MaxTextureCount);
	for(unsigned int i = 0; i < MaxTextureCount; ++i)
	{
		mTextures.push_back(NULL);
	}

	mTexturesStageBlend.reserve(MaxTextureCount);
	for(unsigned int i = 0; i < MaxTextureCount; ++i)
	{
		mTexturesStageBlend.push_back(ZTEXTURESTAGEBLEND_NONE);
	}
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZMaterial::~ZMaterial()
{
	
}

void ZMaterial::set()
{
	ZD3D9::device->SetMaterial(&mMaterial);
	for(unsigned int i = 0; i < MaxTextureCount; ++i)
	{
		if(mTextures[i] != NULL)
		{
			mTextures[i]->set(i);
		}
		else
		{
			ZD3D9::device->SetTexture(i, NULL);
		}

		switch(mTexturesStageBlend[i])
		{
		case ZTEXTURESTAGEBLEND_ADD:
			ZD3D9::device->SetTextureStageState(i, D3DTSS_COLORARG1, D3DTA_TEXTURE);
			ZD3D9::device->SetTextureStageState(i, D3DTSS_COLORARG2, D3DTA_CURRENT);
			ZD3D9::device->SetTextureStageState(i, D3DTSS_COLOROP, D3DTOP_ADD);
			break;

		case ZTEXTURESTAGEBLEND_MULTIPLY:
			ZD3D9::device->SetTextureStageState(i, D3DTSS_COLORARG1, D3DTA_TEXTURE);
			ZD3D9::device->SetTextureStageState(i, D3DTSS_COLORARG2, D3DTA_CURRENT);
			ZD3D9::device->SetTextureStageState(i, D3DTSS_COLOROP, D3DTOP_MODULATE);
			break;

		default:
			ZD3D9::device->SetTextureStageState(i, D3DTSS_COLORARG1, D3DTA_TEXTURE);
			ZD3D9::device->SetTextureStageState(i, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
		}
	}
}

void ZMaterial::addTexture(unsigned int stage, ZTexture* texture)
{
	if(stage > MaxTextureCount - 1)
	{
		throw ZEXCEPTION_MESSAGE("ZMaterial::addTexture() attempting to add texture to stage that exceeds the max texture count allowable by ZMaterial");
	}
	mTextures[stage] = texture;
}

void ZMaterial::setTextureStageBlend(unsigned int stage, ZTextureStageBlend blend_mode)
{
	if(stage > MaxTextureCount - 1)
	{
		throw ZEXCEPTION_MESSAGE("ZMaterial::setTextureStageBlend() attempting to add blend mode to stage that exceeds the max texture count allowable by ZMaterial");
	}
	mTexturesStageBlend[stage] = blend_mode;
}

void ZMaterial::setAmbient(const ZColor& ambient)
{
	mMaterial.Ambient = ambient.toD3DColorValue();
}

void ZMaterial::setDiffuse(const ZColor& diffuse)
{
	mMaterial.Diffuse = diffuse.toD3DColorValue();
}

void ZMaterial::setSpecular(const ZColor& specular)
{
	mMaterial.Specular = specular.toD3DColorValue();
}

void ZMaterial::setEmissive(const ZColor& emissive)
{
	mMaterial.Emissive = emissive.toD3DColorValue();
}

void ZMaterial::setPower(float power)
{
	mMaterial.Power = power;
}