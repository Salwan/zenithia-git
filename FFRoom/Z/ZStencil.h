////////////////////////////////////////////////////////////////
// Name: ZStencil
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#ifndef _ZSTENCIL_HEADER_
#define _ZSTENCIL_HEADER_

/////////////////////////////////////////////////////// Includes
#include "ZD3D9.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// @enum Stencil Comparison operation
///
enum EStencilComparison
{
	ESC_NEVER			= D3DCMP_NEVER,
	ESC_LESS			= D3DCMP_LESS,
	ESC_EQUAL			= D3DCMP_EQUAL,
	ESC_LESS_EQUAL		= D3DCMP_LESSEQUAL,
	ESC_GREATER			= D3DCMP_GREATER,
	ESC_NOT_EQUAL		= D3DCMP_NOTEQUAL,
	ESC_GREATER_EQUAL	= D3DCMP_GREATEREQUAL,
	ESC_ALWAYS			= D3DCMP_ALWAYS,
};

////////////////////////////////////////////////////////////////
/// @enum Stencil pass operation
///
enum EStencilOperation
{
	ESO_KEEP				= D3DSTENCILOP_KEEP,
	ESO_ZERO				= D3DSTENCILOP_ZERO,
	ESO_REPLACE				= D3DSTENCILOP_REPLACE,
	ESO_INCREMENT			= D3DSTENCILOP_INCR,
	ESO_INCREMENT_SATURATE	= D3DSTENCILOP_INCRSAT,
	ESO_DECREMENT			= D3DSTENCILOP_DECR,
	ESO_DECREMENT_SATURATE	= D3DSTENCILOP_DECRSAT,
	ESO_INVERT				= D3DSTENCILOP_INVERT,
};

////////////////////////////////////////////////////////////////
/// @class ZStencil
/// @brief
/// 
class ZStencil 
{
public:
	virtual ~ZStencil();

	static void begin(bool two_sided_stencil = false);
	static void end();
	static void setRefMask(int reference = 0, int mask = 0xffffffff);
	static void setTest(EStencilComparison stencil_operation = ESC_NEVER);
	static void setTestCCW(EStencilComparison stencil_operation = ESC_NEVER);
	static void setOperation(EStencilOperation pass = ESO_KEEP, EStencilOperation stencil_fail = ESO_KEEP, EStencilOperation z_fail = ESO_KEEP);
	static void setOperationCCW(EStencilOperation pass = ESO_KEEP, EStencilOperation stencil_fail = ESO_KEEP, EStencilOperation z_fail = ESO_KEEP);
	static void setWriteMask(int write_mask = 0xffffffff);

private:
	ZStencil(){};
	ZStencil(const ZStencil&){}
	ZStencil& operator=(const ZStencil&){}

};

#endif
