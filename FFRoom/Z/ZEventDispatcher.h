////////////////////////////////////////////////////////////////
// Name: ZEventDispatcher
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#ifndef _ZEVENTDISPATCHER_HEADER_
#define _ZEVENTDISPATCHER_HEADER_

/////////////////////////////////////////////////////// Includes
#include "ZEventListener.h"
#include "ZEvent.h"
#include "ZHeapPtr.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// @class ZEventDispatcher
/// @brief
/// 
class ZEventDispatcher 
{
public:
	explicit ZEventDispatcher();
	virtual ~ZEventDispatcher();

	void addEventListener(unsigned int _type, ZEventListener* _listener, ZEventHandlerPtr _handler);
	void addEventListener(unsigned int _type, ZHeapPtr* _listener, ZEventHandlerPtr _handler);
	void removeEventListener(unsigned int _type, ZEventListener* _listener, ZEventHandlerPtr _handler);
	void removeEventListener(unsigned int _type, ZHeapPtr* _listener, ZEventHandlerPtr _handler);
	void dispatchEvent(const ZEvent& e);

protected:
	struct PIMPL;
	PIMPL* pimpl;

private:
	ZEventDispatcher(const ZEventDispatcher&){}
	ZEventDispatcher& operator=(const ZEventDispatcher&){}

};

#endif
