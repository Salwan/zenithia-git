////////////////////////////////////////////////////////////////
// Name: ZWindow
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "ZWindow.h"
#include <Windows.h>
#include "ZException.h"
#include "ZError.h"
#include "ZAssert.h"
#include "ZD3D9.h"
#include "events/ZEvent_WindowResize.h"

//////////////////////////////////////////////////// Definitions
const wchar_t ZWindowClassName [] = L"zgui_window";
static ZWindow* MainWindow = NULL;

////////////////////////////////////////////////////////////////
/// @struct PIMPL
/// Private implementation
///
struct ZWindow::PIMPL
{
	PIMPL() : hInstance(NULL), hWnd(NULL), hMenu(NULL)
	{
		ZeroMemory(&msg, sizeof(MSG));
	}
	HINSTANCE hInstance;
	HWND hWnd;
	MSG msg;
	HMENU hMenu;
	RECT windowRect;
};

////////////////////////////////////////////////////////////////
/// Default window procedure
///
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	static bool prev_size_msg = false;
	RECT rect;
	switch(msg)
	{
	case WM_CLOSE:
		DestroyWindow(hWnd);
		return 0;

	case WM_KEYUP:
		switch(wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hWnd);
			return 0;
		}
		break;

	case WM_SIZING:
		prev_size_msg = true;
		break;

	case WM_EXITSIZEMOVE:
		if(prev_size_msg)
		{
			prev_size_msg = false;
			GetWindowRect(hWnd, &rect);
			MainWindow->doWindowResize();
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

////////////////////////////////////////////////////////////////
/// Default Constructor, 800x500 window
///
ZWindow::ZWindow(const wchar_t* window_title, int width, int height)
	: strTitle(window_title), iWidth(width), iHeight(height),
	bFullscreen(false)
{
	MainWindow = this;
	construct();
}

////////////////////////////////////////////////////////////////
/// Destructor
///
ZWindow::~ZWindow()
{
	if(bFullscreen)
	{
		goWindowed();
	}
	UnregisterClass(ZWindowClassName, pimpl->hInstance);
	delete pimpl;
	MainWindow = NULL;
}

////////////////////////////////////////////////////////////////
/// Initializes the window class, creates the window
///
void ZWindow::construct()
{
	bAlive = false;

	pimpl = new PIMPL;
	pimpl->hInstance = (HINSTANCE)GetModuleHandle(NULL);
	ZeroMemory(&pimpl->msg, sizeof(MSG));

	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW|CS_VREDRAW;
	wc.lpfnWndProc = (WNDPROC)WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = pimpl->hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = ZWindowClassName;
	wc.hIconSm = NULL;

	if(!RegisterClassEx(&wc))
	{
		throw ZEXCEPTION_ERROR(GetLastError());
	}

	pimpl->hWnd = CreateWindowEx(0, ZWindowClassName, strTitle.c_str(), WS_OVERLAPPEDWINDOW, 
		CW_USEDEFAULT, CW_USEDEFAULT, iWidth, iHeight, NULL, NULL, pimpl->hInstance, NULL);
	if(!pimpl->hWnd)
	{
		throw ZEXCEPTION_ERROR(GetLastError());
	}

	BOOL ret = GetWindowRect(pimpl->hWnd, &pimpl->windowRect);
	if(!ret)
	{
		throw ZEXCEPTION_ERROR(GetLastError());
	}
}

////////////////////////////////////////////////////////////////
/// Shows the window and updates it once, forcing it to paint.
///
void ZWindow::start()
{
	ZASSERT(bAlive == false, "ZWindow::start() called but the window is already alive!");
	bAlive = true;
	ShowWindow(pimpl->hWnd, SW_SHOW);
	UpdateWindow(pimpl->hWnd);	
}

////////////////////////////////////////////////////////////////
/// The window message processor.
/// Must be called once every frame to process incoming windows 
/// messages to this window.
///
void ZWindow::process()
{
	if(bAlive)
	{
		if(PeekMessage(&pimpl->msg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&pimpl->msg);
			DispatchMessage(&pimpl->msg);
		}
		if(pimpl->msg.message == WM_QUIT)
		{
			// Window was closed by the user
			bAlive = false;
		}
	}
}

void ZWindow::goFullscreen(ZD3D9& zd3d9)
{
	ZASSERT(bFullscreen == false, "ZWindow::goFullscreen() window is already in fullscreen mode");
	if(!bFullscreen)
	{
		GetWindowRect(pimpl->hWnd, &pimpl->windowRect);
		SetWindowLong(pimpl->hWnd, GWL_STYLE, WS_POPUP|WS_SYSMENU|WS_VISIBLE);
		if(pimpl->hMenu)
		{
			SetMenu(pimpl->hWnd, NULL);
		}
		SetWindowPos(pimpl->hWnd, HWND_NOTOPMOST, 0, 0, 
			zd3d9.getFullscreenMode().Width,
			zd3d9.getFullscreenMode().Height, SWP_SHOWWINDOW);
		bFullscreen = true;
	}
}

void ZWindow::goWindowed()
{
	ZASSERT(bFullscreen == true, "ZWindow::goWindowed() window is already in windowed mode");
	if(bFullscreen)
	{
		SetWindowLong(pimpl->hWnd, GWL_STYLE, WS_OVERLAPPEDWINDOW|WS_VISIBLE);
		if(pimpl->hMenu)
		{
			SetMenu(pimpl->hWnd, pimpl->hMenu);
		}
		SetWindowPos(pimpl->hWnd, HWND_NOTOPMOST, 
			pimpl->windowRect.left,
			pimpl->windowRect.top, 
			pimpl->windowRect.right - pimpl->windowRect.left,
			pimpl->windowRect.bottom - pimpl->windowRect.top, 
			SWP_SHOWWINDOW);
		bFullscreen = false;
	}
}

////////////////////////////////////////////////////////////////
/// Returns a pointer to the window handle (HWND).
/// It's written like this to keep the Windows API stuff away from
/// the header, and it can only be called by friend classes.
///
void* ZWindow::getWindowHandle() const
{
	return &pimpl->hWnd;
}

////////////////////////////////////////////////////////////////
/// Called by the message procedure when the window is resized
/// 
void ZWindow::doWindowResize()
{
	GetWindowRect(pimpl->hWnd, &pimpl->windowRect);
	iWidth = pimpl->windowRect.right - pimpl->windowRect.left;
	iHeight = pimpl->windowRect.bottom - pimpl->windowRect.top;
	ZEvent_WindowResize e(iWidth, iHeight);
	dispatchEvent(e);
}
