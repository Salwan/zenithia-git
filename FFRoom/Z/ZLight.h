////////////////////////////////////////////////////////////////
// Name: ZLight
// Desc: Represents a basic light
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZLIGHT_HEADER_
#define _ZLIGHT_HEADER_

/////////////////////////////////////////////////////// Includes
#include "ZD3D9.h"

//////////////////////////////////////////////////// Definitions
class ZColor;

////////////////////////////////////////////////////////////////
/// @class ZLight
/// @brief
/// 
class ZLight 
{
public:
	explicit ZLight();
	virtual ~ZLight();

	void set(unsigned int index);
	void setPointLight(const ZColor& diffuse, const D3DXVECTOR3& position, float range);
	void setDirectionalLight(const ZColor& diffuse, const D3DXVECTOR3& direction);
	void setSpotLight(const ZColor& diffuse, const D3DXVECTOR3& position, const D3DXVECTOR3& direction, float range, float theta, float phi, float falloff = 1.0f);
	void setAttenuation(float attenuation_0, float attenuation_1, float attenuation_2);
	void setPosition(const D3DXVECTOR3& position);
	void setDirection(const D3DXVECTOR3& direction);
	void setDiffuse(const ZColor& diffuse);
	void setAmbient(const ZColor& ambient);

private:
	ZLight(const ZLight&){}
	ZLight& operator=(const ZLight&){}

	D3DLIGHT9 mLight;

public:
	static void Enable(unsigned int index);
	static void Disable(unsigned int index);
	static bool IsEnabled(unsigned int index);
	static void EnableLighting();
	static void DisableLighting();
};

#endif
