////////////////////////////////////////////////////////////////
// Name: ZHeapPtr
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#ifndef _ZHEAPPTR_HEADER_
#define _ZHEAPPTR_HEADER_

/////////////////////////////////////////////////////// Includes
#include "ZException.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// @class ZHeapPtr
/// @brief A pointer wrapper with ability to delete and retrieve.
///	ZHeapPtr assumes that its the sole owner of the pointer, you
/// should never delete pointer directly rather use ZHeapPtr delete
/// method.
/// If ZHeapPtr is instantiated on the stack, once it goes out of 
/// scope it will automatically delete the pointer it has.
/// If ZHeapPtr is allocated on the heap and not deleted, it will
/// leak and this is part of its purpose, it can be used to detect
/// leaking heap ptrs. And it has a small footprint: 2xsizeof(ptr).
/// Suggesion: heap pointer might be a very good candidate for 
/// custom memory pool allocation, since its small and could 
/// potentially be instantiated many many times per frame.
/// 
class ZHeapPtr 
{
public:
	explicit ZHeapPtr(void* _pointer, bool _isArray = false) : pPointer(_pointer), bIsArray(_isArray) {}
	virtual ~ZHeapPtr()
	{
		deletePtr();
	}

	bool isValid() const 
	{
		if(pPointer) 
			return true;
		else 
			return false;
	}

	template<typename T>
	T* getPtr() const 
	{
		if(isValid())
		{
			return static_cast<T*>(pPointer);
		}
		else
		{
			throw ZEXCEPTION_MESSAGE("ZHeapPtr: invalid pointer retrieval, try to check first using isValid.");
		}
	}

	void deletePtr()
	{
		if(pPointer)
		{
			if(bIsArray) 
				delete [] pPointer;
			else
				delete pPointer;
			pPointer = nullptr;
		}
	}

protected:
	void* pPointer;
	bool bIsArray;

private:
	ZHeapPtr(const ZHeapPtr&){}
	ZHeapPtr& operator=(const ZHeapPtr&){}

};

#endif
