////////////////////////////////////////////////////////////////
// Name: ZAssert
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZASSERT_HEADER_
#define _ZASSERT_HEADER_

/////////////////////////////////////////////////////// Includes
#include "ZDefs.h"

//////////////////////////////////////////////////// Definitions
namespace ZUtils
{
	bool AssertMessage(const char* text, const char* file, int line);
}

#ifdef _DEBUG
#define ZASSERT(expression, text)	\
	if(!(expression) && ZUtils::AssertMessage(text, __FILE__, __LINE__)) __debugbreak();
#else
#define ZASSERT(expression, text)
#endif

#endif
