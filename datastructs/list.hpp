#ifndef _LIST_HPP_
#define _LIST_HPP_

#include <iostream>
#include <assert.h>

template <class Type>
class List
{
private:
	struct Node
	{
		Node() : next(NULL){}
		Type element;
		Node* next;
	};
	Node* mRoot;
	Node* mIterator;

public:
	List() : mRoot(new Node)
	{
		mIterator = mRoot;
	}
	virtual ~List()
	{
		clear();
		delete mRoot;
	}

	void add(const Type& element)
	{
		Node* n = mRoot;
		while(n->next)
		{
			n = n->next;
		}
		n->next = new Node;
		n->next->element = element;
	}

	void remove(const Type& element)
	{
		if(!empty())
		{
			first();
			Type value;
			Node* prevNode = mRoot;
			while(next(value))
			{
				if(element == value)
				{
					prevNode->next = mIterator->next;
					delete mIterator;
					mIterator = mRoot;
					break;
				}
				prevNode = mIterator;
			}
		}
	}

	void clear()
	{
		if(!empty())
		{
			Type value;
			first();
			Node* prevNode = mRoot;
			while(next(value))
			{
				delete prevNode;
				prevNode = mIterator;
			}
			mRoot = new Node;
			mIterator = mRoot;
		}
	}

	void first()
	{
		mIterator = mRoot;
	}

	bool next(Type& element)
	{
		if(mIterator->next)
		{
			mIterator = mIterator->next;
			element = mIterator->element;
			return true;
		}
		else
		{
			return false;
		}
	}

	bool empty() const
	{
		return mRoot->next == NULL? true : false;
	}

	void print(std::ostream& out_stream)
	{
		first();
		Type value;
		while(next(value))
		{
			out_stream<<value<<" ";
		}
		out_stream<<std::endl;
	}

	void insertionSort()
	{
		if(empty()) return;

		Type element;
		Node* prevNode, tempNode;

		first();
		next(element);
		prevNode = mIterator;
		while(next(element))
		{
			if(element < prevNode->element)
			{
				//tempNode = mIterator;
				// problem: how am I supposed to go backwards if it's a singly linked list?? :<
			}
		}
	}
};

#endif