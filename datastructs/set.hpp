#ifndef _SET_HPP_
#define _SET_HPP_

#include "list.hpp"

template <class Type>
class Set : public List<Type>
{
public:
	Set(){}
	virtual ~Set(){}

	void createIntersection(Set<Type>& other_set, Set<Type>& out_set)
	{
		Type value1, value2;
		other_set.first();
		while(other_set.next(value1))
		{
			first();
			while(next(value2))
			{
				if(value1 == value2)
				{
					out_set.add(value1);
					break;
				}
			}
		}
	}

	void createUnion(Set<Type>& other_set, Set<Type>& out_set)
	{
		Type value;
		first();
		while(next(value))
		{
			out_set.add(value);
		}
		other_set.first();
		while(other_set.next(value))
		{
			out_set.add(value);
		}
	}
};

#endif