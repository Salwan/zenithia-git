#ifndef _STACK_HPP_
#define _STACK_HPP_

#include <iostream>

template <class Type, int MaxSize>
class Stack
{
private:
	Type mElements[MaxSize];
	int iPointer;

public:
	Stack() : iPointer(0) {}
	~Stack(){}

	void push(const Type& element)
	{
		iPointer++;
		if(iPointer >= MaxSize) iPointer = 0;
		mElements[iPointer] = element;
	}

	void pop(Type& element)
	{
		int p = iPointer--;
		if(iPointer < 0) iPointer = MaxSize - 1;
		element = mElements[p];
	}

	void print(std::ostream& out_stream)
	{
		for(int i = 0; i < MaxSize; ++i)
		{
			out_stream<<mElements[i]<<" ";
		}
		out_stream<<std::endl;
	}
};

#endif