#include <iostream>
#include "stack.hpp"
#include "list.hpp"
#include "set.hpp"

int main()
{
	std::cout<<"Hello World!"<<std::endl;

	std::cout<<"> Stack"<<std::endl;
	Stack<int, 25> mahstack;
	for(int i = 0; i < 255; ++i)
			mahstack.push(std::rand());
	mahstack.print(std::cout);

	std::cout<<"> List"<<std::endl;
	List<int> mahlist;
	for(int i = 0; i < 25; ++i)
		mahlist.add(std::rand() % 10);
	mahlist.print(std::cout);
	mahlist.remove(1);
	mahlist.print(std::cout);
	std::cout<<std::endl;

	std::cout<<"> Set"<<std::endl;
	Set<int> s1, s2, s3;
	s1.add(1);
	s1.add(2);
	s1.add(5);
	s1.add(10);
	s1.add(12);
	s2.add(7);
	s2.add(9);
	s2.add(12);
	s2.add(5);
	std::cout<<"set 1: ";
	s1.print(std::cout);
	std::cout<<"set 2: ";
	s2.print(std::cout);
	std::cout<<"Intersection..."<<std::endl;
	s1.createIntersection(s2, s3);
	std::cout<<"set 3: ";
	s3.print(std::cout);
	s3.clear();
	std::cout<<"Union..."<<std::endl;
	s1.createUnion(s2, s3);
	std::cout<<"set 3: ";
	s3.print(std::cout);
	std::cout<<std::endl;

	std::cout<<"> Sorting"<<std::endl;
	mahlist.clear();
	for(int i = 0; i < 100; ++i)
		mahlist.add(std::rand());
	mahlist.print(std::cout);
	mahlist.insertionSort();
	std::cout<<"After sorting..."<<std::endl;
	mahlist.print(std::cout);

	std::getchar();
}