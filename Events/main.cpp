#include <iostream>
#include <map>
#include <list>
#include <vector>
#include "heapptr.h"

class Event
{
public:
	static const unsigned int EVENT_NORMAL = 110;
	Event(unsigned int _type = EVENT_NORMAL) : type(_type)
	{
	}
	~Event(){}
	unsigned int getType() const {return type;}

protected:
	unsigned int type;
};

class EventListener
{
public:
	EventListener()
	{
	}
	~EventListener(){}
};
typedef void(EventListener::*EventHandlerPtr)(const Event&);
#define EVENT_HANDLER(ptr) static_cast<EventHandlerPtr>(ptr)

class EventDispatcher
{
protected:
	struct EventRecord
	{
		EventRecord(){}
		EventRecord(HeapPtr* _listener, EventHandlerPtr _handler) 
			: listener(_listener), handler(_handler){}
		HeapPtr* listener;
		EventHandlerPtr handler;
	};
	typedef std::map<unsigned int, std::list<EventRecord>> EventMap;
	typedef std::map<unsigned int, std::list<EventRecord>>::iterator EventMapIter;
	typedef std::list<EventRecord> EventList;
	typedef std::list<EventRecord>::iterator EventListIter;
	EventMap mEvents;

public:
	EventDispatcher()
	{
	}
	~EventDispatcher(){}
	
	void dispatchEvent(const Event& e)
	{
		std::vector<EventListIter> to_delete;
		EventList& elist = mEvents[e.getType()];
		for(EventListIter iter = elist.begin(); iter != elist.end(); ++iter)
		{
			if(iter->listener->isValid())
			{
				(iter->listener->getPtr<EventListener>()->*(iter->handler))(e);
			}
			else
			{
				to_delete.push_back(iter);
			}
		}
		if(to_delete.size() > 0)
		{
			for(unsigned int i = 0; i < to_delete.size(); ++i)
			{
				std::cout<<"[EventDispatcher] Deleted listener because the pointer is invalid."<<std::endl;
				elist.erase(to_delete[i]);
			}
		}
	}

	void addEventListener(unsigned int _type, HeapPtr* _listener, EventHandlerPtr _handler)
	{
		EventList& elist = mEvents[_type];
		elist.push_back(EventRecord(_listener, _handler));
	}	
};

class TestDispatcher : public EventDispatcher
{
public:
	TestDispatcher()
	{
	}
	~TestDispatcher(){}

	void run()
	{
		Event *ev = new Event(12345);
		dispatchEvent(*ev);
		delete ev;
	}
};

class TestListener : public EventListener
{
public:
	TestListener(const char* _message) : message(_message) {}
	~TestListener(){}

	void onSomething(const Event& event)
	{
		std::cout<<message<<std::endl;
	}

protected:
	const char* message;
};

int main()
{
	std::cout<<"Event System"<<std::endl;
	std::cout<<"---------------------------------------------"<<std::endl;

	HeapPtr listener1(new TestListener("Listener 1: onSomething called!"));
	HeapPtr listener2(new TestListener("Listener 2: onSomething called! MAGIC"));
	HeapPtr listener3(new TestListener("Listener 3: onSomething called will die!"));
	HeapPtr dispatcher(new TestDispatcher());
	dispatcher.getPtr<EventDispatcher>()->addEventListener(Event::EVENT_NORMAL, 
		&listener1, EVENT_HANDLER(&TestListener::onSomething));
	dispatcher.getPtr<EventDispatcher>()->addEventListener(Event::EVENT_NORMAL,
		&listener2, EVENT_HANDLER(&TestListener::onSomething));
	dispatcher.getPtr<EventDispatcher>()->addEventListener(Event::EVENT_NORMAL,
		&listener3, EVENT_HANDLER(&TestListener::onSomething));
	listener3.deletePtr();

	Event e;
	dispatcher.getPtr<EventDispatcher>()->dispatchEvent(e);

	std::cin.get();
	return 0;
}