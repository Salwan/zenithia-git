#include <exception>

class HeapPtr
{
public:
	HeapPtr(void* _pointer, bool _isArray = false) : pointer(_pointer), bIsArray(_isArray) {}
	virtual ~HeapPtr()
	{
		deletePtr();
	}

	void deletePtr()
	{
		if(pointer)
		{
			if(bIsArray) delete [] pointer;
			else delete pointer;
			pointer = nullptr;
		}
	}

	bool isValid() const 
	{
		if(pointer) return true;
		else return false;
	}

	template<typename T>
	T* getPtr() const
	{
		if(isValid())
		{
			return static_cast<T*>(pointer);
		}
		else
		{
			throw new std::exception("Invalid heap pointer accessed.");
		}
	}

protected:
	bool bIsArray;
	void* pointer;
};