#define STRICT
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <string>

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

	switch(msg)
	{
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		if(hdc)
		{
			SelectObject(hdc, CreateSolidBrush(RGB(0, 0, 0)));
			SelectObject(hdc, CreatePen(PS_SOLID, 1, 0xffffff));
			SetBkColor(hdc, RGB(0, 0, 0));
			SetTextColor(hdc, RGB(255, 255, 255));
			std::wstring text = L"Welcome to Zenithia!";
			TextOut(hdc, 5, 5, text.c_str(), text.length());
			SelectObject(hdc, CreateSolidBrush(RGB(255, 255, 255)));
			Rectangle(hdc, 10, 100, 50, 140);
			Ellipse(hdc, 10, 150, 60, 180);
			SelectObject(hdc, CreateSolidBrush(RGB(255, 255, 0)));
			RoundRect(hdc, 10, 200, 100, 280, 20, 20);
			SelectObject(hdc, CreatePen(PS_SOLID, 1, 0xff0000)); // PS_DASH etc...
			// CreatePenIndirect(...) uses LOGPEN struct to create HPEN
			// GetObject and GetCurrentObject(hdc, OBJ_PEN) can be used to retrieve
			// must use DeleteObject on created objects when you finish (WM_DESTROY)
			// Arc/Chord/Pie overwrite each other, comment out to see individuals
			SelectObject(hdc, CreateHatchBrush(HS_DIAGCROSS, RGB(100, 55, 0)));
			Arc(hdc, 150, 10, 250, 110, 200, 20, 170, 100);
			Chord(hdc, 150, 10, 250, 110, 200, 20, 170, 100);
			Pie(hdc, 150, 10, 250, 110, 200, 20, 170, 100);
			// CreateBrushIndirect(LOGBRUSH), styles: BS_SOLID/HOLLOW/HATCHED/PATTERN/DIBPATTERNPT
		}
		EndPaint(hWnd, &ps);
		break;

	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;

	case WM_DESTROY:		
		PostQuitMessage(0);
		return 0;

	case WM_KEYUP:
		switch(wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hWnd);
			return 0;
		}
		break;
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}

void ShowLastError(const std::wstring& title)
{
	TCHAR* error_buffer = new TCHAR [129];
	if(!FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(), 0, (LPTSTR)error_buffer, 128, NULL))
	{
		OutputDebugString(L"FormatMessage failed");
	}
	else
	{
		OutputDebugString(L"GetLastError: ");
		OutputDebugString(error_buffer);
		MessageBox(NULL, error_buffer, title.c_str(), MB_OK|MB_ICONERROR);
	}
	delete [] error_buffer;
}

INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW|CS_VREDRAW;
	wc.lpfnWndProc = (WNDPROC)WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = L"zenithia_winhello";
	wc.hIconSm = NULL;

	if(!RegisterClassEx(&wc))
	{
		ShowLastError(L"RegisterClassEx() - Failed");
		return false;
	}

	HWND hWnd = CreateWindowEx(0, L"zenithia_winhello", L"Zenithia WinHello", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 800, 500, NULL, NULL, hInstance, NULL);
	if(!hWnd)
	{
		ShowLastError(L"CreateWindowEx() - Failed");
		return false;
	}

	ShowWindow(hWnd, SW_SHOW);

	// Message loop
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));
	while(msg.message != WM_QUIT)
	{
		if(PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			// do yer thang
		}
	}

	UnregisterClass(L"zenithia_winhello", hInstance);

	return msg.wParam;
}