////////////////////////////////////////////////////////////////
// Name: ZD3D9
// Desc: Represents Direct3D9, does all basic functions needed 
//		 like initializing, loading, updating, resizing, etc...
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZD3D9_HEADER_
#define _ZD3D9_HEADER_

/////////////////////////////////////////////////////// Includes
#include <d3d9.h>

//////////////////////////////////////////////////// Definitions
#define ZEXCEPTION_DXERROR(hresult) ZD3D9::DXException(hresult, __FILE__, __LINE__)
#define SAFE_RELEASE(object) if(object){object->Release(); object = NULL;}
class ZWindow;
class ZException;

////////////////////////////////////////////////////////////////
/// @class ZD3D9
/// @brief
/// 
///-------------------------------------------------------------
class ZD3D9 
{
public:
	explicit ZD3D9(ZWindow& window);
	virtual ~ZD3D9();

	// Create DirectX Exception via hresult, use macro ZEXCEPTION_DXERROR
	static ZException DXException(long hresult, const char* file, int line);

	// Methods
	IDirect3D9& getD3D9() const {return *pD3D9;}
	IDirect3DDevice9& getDevice() const {return *pDevice;}

protected:

private:
	ZD3D9(const ZD3D9&){}
	ZD3D9& operator=(const ZD3D9&){}

	void createD3D9();
	void destroyD3D9();

	// Private Implementation
	struct PIMPL;
	PIMPL* pimpl;

	IDirect3D9* pD3D9;
	IDirect3DDevice9* pDevice;
	D3DCAPS9 mD3DCaps;
};

#endif
