////////////////////////////////////////////////////////////////
// Name: ZException
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZEXCEPTION_HEADER_
#define _ZEXCEPTION_HEADER_

/////////////////////////////////////////////////////// Includes
#include <string>
#include <sstream>
#include "ZError.h"

//////////////////////////////////////////////////// Definitions
#define ZEXCEPTION_MESSAGE(message) ZException(message, __FILE__, __LINE__)
#define ZEXCEPTION_ERROR(error) ZException(static_cast<unsigned long>(error), __FILE__, __LINE__);

////////////////////////////////////////////////////////////////
/// @class ZException
/// @brief a useful exception class with formatting capabilities.
/// Use macros: 
/// - ZEXCEPTION_MESSAGE to pass a string message
/// - ZEXCEPTION_ERROR to pass a windows error
///-------------------------------------------------------------
class ZException
{
public:
	ZException(const std::string& message)
	{
		strMessage = message;
	}

	ZException(const std::string& message, const char* file, int line)
	{
		std::ostringstream ss;
		ss<<"File: "<<file<<std::endl;
		ss<<"Line: "<<line<<std::endl;
		ss<<"Message: "<<message<<std::endl;
		strMessage = ss.str();
		strMessage = message;
	}

	ZException(unsigned long windows_error_code, const char* file, int line)
	{
		std::ostringstream ss;
		ss<<"File: "<<file<<std::endl;
		ss<<"Line: "<<line<<std::endl;
		ZError::getWindowsErrorMessage(windows_error_code, strMessage);
		ss<<"Message: "<<strMessage;
		strMessage = ss.str();
	}

	virtual ~ZException(){}

	const std::string& getMessage() const {return strMessage;}

private:
	explicit ZException(){}
	// default public copy constructor and assignment constructor are ok

	std::string strMessage;
};

#endif
