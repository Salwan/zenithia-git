////////////////////////////////////////////////////////////////
// Name: ZWindow
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZWINDOW_HEADER_
#define _ZWINDOW_HEADER_

/////////////////////////////////////////////////////// Includes

//////////////////////////////////////////////////// Definitions
class ZD3D9;

////////////////////////////////////////////////////////////////
/// @class ZWindow
/// @brief The main application window class.
/// 
///-------------------------------------------------------------
class ZWindow 
{
	friend class ZD3D9;
public:
	explicit ZWindow();
	ZWindow(int width, int height);
	virtual ~ZWindow();

	void start();
	void process();

	// Accessors
	int getWidth() const {return iWidth;}
	int getHeight() const {return iHeight;}
	bool isAlive() const {return bAlive;}

private:
	ZWindow(const ZWindow&){}
	ZWindow& operator=(const ZWindow&){}

	void construct();

	// PIMPL
	struct PIMPL;
	PIMPL* pimpl;

	// Private methods
	void* getWindowHandle() const;

protected:
	int iWidth;
	int iHeight;
	bool bAlive;
};

#endif
