////////////////////////////////////////////////////////////////
// Name: TestApplication
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _TESTAPPLICATION_HEADER_
#define _TESTAPPLICATION_HEADER_

/////////////////////////////////////////////////////// Includes
#include "ZGUI.h"

//////////////////////////////////////////////////// Definitions

////////////////////////////////////////////////////////////////
/// @class TestApplication
/// @brief
/// 
///-------------------------------------------------------------
class TestApplication : public ZApplication
{
public:
	explicit TestApplication();
	virtual ~TestApplication();

	virtual void init();
	virtual void start();
	virtual void update();
	virtual void render();
	virtual void end();

private:
	TestApplication(const TestApplication&){}
	TestApplication& operator=(const TestApplication&){}

	ZD3D9* mD3D9;

};

#endif
