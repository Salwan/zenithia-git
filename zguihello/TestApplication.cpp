////////////////////////////////////////////////////////////////
// Name: TestApplication
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "TestApplication.h"
#include <string>
#include <sstream>
#include "ZClock.h"

////////////////////////////////////////////////////////////////
/// Default Constructor
///-------------------------------------------------------------
TestApplication::TestApplication()
{
	
}

////////////////////////////////////////////////////////////////
/// Destructor
///-------------------------------------------------------------
TestApplication::~TestApplication()
{
	
}

void TestApplication::init()
{
	mD3D9 = new ZD3D9(*pWindow);
}

void TestApplication::start()
{
}

void TestApplication::update()
{
	static double accum = 0.0;
	accum += ZClock::dTimeDelta;
	if(accum >= 1.0)
	{
		accum -= 1.0;
		std::ostringstream ss;
		ss<<"FPS: "<<ZClock::uFPS<<std::endl;
		OutputDebugStringA(ss.str().c_str());
	}
}

void TestApplication::render()
{
	mD3D9->getDevice().Clear(0, 0, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, 0x002f2f00, 1.0f, 0);
	mD3D9->getDevice().Present(0, 0, 0, 0);
}

void TestApplication::end()
{
}
