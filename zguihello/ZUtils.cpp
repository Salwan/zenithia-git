////////////////////////////////////////////////////////////////
// Name: ZUtils
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "ZUtils.h"
#include <Windows.h>
#include <iostream>
#include <sstream>

bool ZUtils::AssertMessage(const char* text, const char* file, int line)
{
	std::ostringstream ss;
	ss<<"ZAssert: "<<text<<std::endl;
	ss<<"File: "<<file<<std::endl;
	ss<<"Line: "<<line<<std::endl;
	OutputDebugStringA(ss.str().c_str());
	std::clog<<ss.str().c_str();

#ifdef ASSERT_DEBUG
	return true;
#else
	return false;
#endif
}