////////////////////////////////////////////////////////////////
// Name: ZApplication
// Desc: Represents the actual application running via a ZWindow
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZAPPLICATION_HEADER_
#define _ZAPPLICATION_HEADER_

/////////////////////////////////////////////////////// Includes

//////////////////////////////////////////////////// Definitions
class ZWindow;
class ZClock;

////////////////////////////////////////////////////////////////
/// @class ZApplication
/// @brief The actual application, override and do your magic.
/// 
///-------------------------------------------------------------
class ZApplication 
{
public:
	explicit ZApplication();
	virtual ~ZApplication();

	unsigned int run(ZWindow* window);

protected:
	// Methods
	virtual void init();
	virtual void start();
	virtual void update();
	virtual void render();
	virtual void end();

	// Members
	ZWindow* pWindow;

private:
	ZApplication(const ZApplication&){}
	ZApplication& operator=(const ZApplication&){}

	ZClock* mClock;

};

#endif
