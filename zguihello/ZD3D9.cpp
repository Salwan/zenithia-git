////////////////////////////////////////////////////////////////
// Name: ZD3D9
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "DxErr.lib")
#ifndef _DEBUG
#pragma comment(lib, "d3dx9.lib")
#else
#pragma comment(lib, "d3dx9d.lib")
#endif

/////////////////////////////////////////////////////// Includes
#include "ZD3D9.h"
#include <iostream>
#include <string>
#include <sstream>
#include <Windows.h>
#include <DxErr.h>
#include "ZWindow.h"
#include "ZException.h"

// Private Implementation
struct ZD3D9::PIMPL
{
	int width;
	int height;
	HWND hWnd;
};

////////////////////////////////////////////////////////////////
/// Default Constructor
///-------------------------------------------------------------
ZD3D9::ZD3D9(ZWindow& window) : pD3D9(NULL), pDevice(NULL)
{
	pimpl = new PIMPL;
	pimpl->width = window.getWidth();
	pimpl->height = window.getHeight();
	HWND* hWnd = static_cast<HWND*>(window.getWindowHandle());
	pimpl->hWnd = *hWnd;

	createD3D9();
}

////////////////////////////////////////////////////////////////
/// Destructor
///-------------------------------------------------------------
ZD3D9::~ZD3D9()
{
	SAFE_RELEASE(pDevice);
	SAFE_RELEASE(pD3D9);
	delete pimpl;
}

////////////////////////////////////////////////////////////////
/// Retrieves and formats an HRESULT DirectX error message.
///-------------------------------------------------------------
ZException ZD3D9::DXException(long hresult, const char* file, int line)
{
	std::ostringstream ss;
	ss<<"DirectX Error"<<std::endl;
	ss<<"File: "<<file<<std::endl;
	ss<<"Line: "<<line<<std::endl;
	ss<<"Error: "<<::DXGetErrorStringA(hresult)<<std::endl;
	ss<<"Details: "<<::DXGetErrorDescriptionA(hresult)<<std::endl;
	return ZException(ss.str().c_str());
}

////////////////////////////////////////////////////////////////
/// Initialize Direct3D9
///-------------------------------------------------------------
void ZD3D9::createD3D9()
{
	HRESULT hr;
	std::clog<<"[ZD3D9] Creating D3D9"<<std::endl;
	pD3D9 = Direct3DCreate9(D3D_SDK_VERSION);
	if(!pD3D9)
	{
		throw ZEXCEPTION_MESSAGE("ZD3D9::createD3D9() couldn't create Direct3D9 object");
	}
	std::clog<<"[ZD3D9] Direct3D9 interface created"<<std::endl;
	// D3DDEVICE_REF: Reference rasterizer
	// D3DDEVICE_HAL: Hardware abstraction layer
	hr = pD3D9->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_REF, &mD3DCaps);
	if(FAILED(hr))
	{
		throw ZEXCEPTION_DXERROR(hr);
	}
	std::clog<<"[ZD3D9] Direct3D9 device caps read"<<std::endl;

	D3DPRESENT_PARAMETERS pp;
	memset(&pp, 0, sizeof(D3DPRESENT_PARAMETERS));
	pp.BackBufferWidth = pimpl->width;
	pp.BackBufferHeight = pimpl->height;
	pp.BackBufferFormat = D3DFMT_A8R8G8B8; // For windowed: D3DFMT_UNKNOWN to use current format
	pp.BackBufferCount = 1;
	pp.MultiSampleType = D3DMULTISAMPLE_NONE;
	pp.MultiSampleQuality = 0;
	pp.SwapEffect = D3DSWAPEFFECT_DISCARD; // D3DSWAPEFFECT_FLIPEX for Windows 7 is better, investigate
	pp.hDeviceWindow = pimpl->hWnd;
	pp.Windowed = true;
	pp.EnableAutoDepthStencil = true;
	pp.AutoDepthStencilFormat = D3DFMT_D24S8;
	pp.Flags = 0;
	pp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	pp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

	// Behavior Flags:
	// [Vista only] D3DCREATE_DISABLE_PSGP_THREADING: restrict software vertex processing to main thread (if not specified D3D may do computations in worker threads)
	// D3DCREATE_HARDWARE_VERTEXPROCESSING: All hardware vertex processing
	// D3DCREATE_MIXED_VERTEX_PROCESSING: Can optionally use software processing on specific buffers created with software processing flag, otherwise will use hardware and emulate when necessary
	// D3DCREATE_SOFTWARE_VERTEX_PROCESSING: does all vertex processing in software, slowest
	// D3DCREATE_MULTITHREADED: D3D will be thread safe, can degrade performance, this flag must be used when you have one thread for window message and one for d3d calls
	// D3DCREATE_PUREDEVICE: D3D will not do any emulation services, will not save state info so Get* functions are not supported (like GetRenderState), and will not filter redundant state changes, so you have to do that manually
	hr = pD3D9->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, pimpl->hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &pp, &pDevice);
	if(FAILED(hr))
	{
		std::clog<<"[ZD3D9] WARNING: Couldn't create Direct3D9 hardware vertex processing device"<<std::endl;
		hr = pD3D9->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, pimpl->hWnd, 
			D3DCREATE_MIXED_VERTEXPROCESSING, &pp, &pDevice);
		if(FAILED(hr))
		{
			hr = pD3D9->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, pimpl->hWnd, 
				D3DCREATE_SOFTWARE_VERTEXPROCESSING, &pp, &pDevice);
			if(FAILED(hr)) 
			{
				throw ZEXCEPTION_DXERROR(hr);
			}
			std::clog<<"[ZD3D9] Using software vertex processing instead, performance will suffer"<<std::endl;
		}
		else
		{
			std::clog<<"[ZD3D9] Using mixed vertex processing instead, performance may suffer"<<std::endl;
		}
	}
	std::clog<<"[ZD3D9] Direct3D9 device created"<<std::endl;
}
