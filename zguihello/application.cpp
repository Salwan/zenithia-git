#include <Windows.h>
#include "ZGUI.h"
#include "TestApplication.h"

INT WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, INT)
{
	try
	{
		ZLog::logToFile();
		ZWindow window(1024, 640);
		TestApplication application;
		application.run(&window);
	}
	catch(ZException& e)
	{
		ZError::showError(e.getMessage());
	}
	return 0;
}