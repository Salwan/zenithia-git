﻿////////////////////////////////////////////////////////////////
// Name: ZLog
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "ZLog.h"
#include <iostream>
#include <fstream>

static std::streambuf* StdBuf = NULL;
static std::ofstream LogFile;

void ZLog::logToFile(const std::string& log_filename)
{
	LogFile.open(log_filename);	
	if(!StdBuf)
	{
		StdBuf = std::clog.rdbuf();
	}
	std::clog.rdbuf(LogFile.rdbuf());
	
	std::clog<<"Zenithia Log"<<std::endl;
	std::clog<<"====================================================="<<std::endl;
}

void ZLog::logToStd()
{
	if(StdBuf)
	{
		std::clog.rdbuf(StdBuf);
	}
	if(LogFile.is_open())
	{
		LogFile.close();
	}
}
