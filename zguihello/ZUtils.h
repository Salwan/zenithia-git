////////////////////////////////////////////////////////////////
// Name: ZUtils
// Desc: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#ifndef _ZUTILS_HEADER_
#define _ZUTILS_HEADER_

/////////////////////////////////////////////////////// Includes
#include "ZDefs.h"

//////////////////////////////////////////////////// Definitions
namespace ZUtils
{
	bool AssertMessage(const char* text, const char* file, int line);
}

#ifdef _DEBUG
#define ZASSERT(expression, text)	\
	if(!(expression) && ZUtils::AssertMessage(text, __FILE__, __LINE__)) _asm int 3;
#else
#define ZASSERT(expression, text)
#endif

#endif
