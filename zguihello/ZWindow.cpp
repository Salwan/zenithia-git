////////////////////////////////////////////////////////////////
// Name: ZWindow
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "ZWindow.h"
#include <Windows.h>
#include "ZException.h"
#include "ZUtils.h"

//////////////////////////////////////////////////////// Globals
const wchar_t ZWindowClassName [] = L"zgui_window";

////////////////////////////////////////////////////////////////
/// @struct PIMPL
/// Private implementation
///-------------------------------------------------------------
struct ZWindow::PIMPL
{
	HINSTANCE hInstance;
	HWND hWnd;
	MSG msg;
};

////////////////////////////////////////////////////////////////
/// Default window procedure
///-------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
	case WM_CLOSE:
		DestroyWindow(hWnd);
		return 0;

	case WM_KEYUP:
		switch(wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hWnd);
			return 0;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

////////////////////////////////////////////////////////////////
/// Default Constructor, 800x500 window
///-------------------------------------------------------------
ZWindow::ZWindow() 
	: iWidth(800), iHeight(500)
{
	construct();
}

////////////////////////////////////////////////////////////////
/// Dimensions constructor
/// @param width window width
/// @param height window height
///-------------------------------------------------------------
ZWindow::ZWindow(int width, int height)
	: iWidth(width), iHeight(height)
{
	construct();
}

////////////////////////////////////////////////////////////////
/// Destructor
///-------------------------------------------------------------
ZWindow::~ZWindow()
{
	UnregisterClass(ZWindowClassName, pimpl->hInstance);
	delete pimpl;
}

////////////////////////////////////////////////////////////////
/// Initializes the window class, creates the window
///-------------------------------------------------------------
void ZWindow::construct()
{
	bAlive = false;

	pimpl = new PIMPL;
	pimpl->hInstance = (HINSTANCE)GetModuleHandle(NULL);
	ZeroMemory(&pimpl->msg, sizeof(MSG));

	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW|CS_VREDRAW;
	wc.lpfnWndProc = (WNDPROC)WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = pimpl->hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = ZWindowClassName;
	wc.hIconSm = NULL;

	if(!RegisterClassEx(&wc))
	{
		throw ZEXCEPTION_ERROR(GetLastError());
	}

	pimpl->hWnd = CreateWindowEx(0, ZWindowClassName, L"ZGUI Hello World", WS_OVERLAPPEDWINDOW, 
		CW_USEDEFAULT, CW_USEDEFAULT, iWidth, iHeight, NULL, NULL, pimpl->hInstance, NULL);
	if(!pimpl->hWnd)
	{
		throw ZEXCEPTION_ERROR(GetLastError());
	}
}

////////////////////////////////////////////////////////////////
/// Shows the window and updates it once, forcing it to paint.
///-------------------------------------------------------------
void ZWindow::start()
{
	ZASSERT(bAlive == false, "ZWindow::start() called but the window is already alive!");
	bAlive = true;
	ShowWindow(pimpl->hWnd, SW_SHOW);
	UpdateWindow(pimpl->hWnd);	
}

////////////////////////////////////////////////////////////////
/// The window message processor.
/// Must be called once every frame to process incoming windows 
/// messages to this window.
///-------------------------------------------------------------
void ZWindow::process()
{
	if(bAlive)
	{
		if(PeekMessage(&pimpl->msg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&pimpl->msg);
			DispatchMessage(&pimpl->msg);
		}
		if(pimpl->msg.message == WM_QUIT)
		{
			// Window was closed by the user
			bAlive = false;
		}
	}
}

////////////////////////////////////////////////////////////////
/// Returns a pointer to the window handle (HWND).
/// It's written like this to keep the Windows API stuff away from
/// the header, and it can only be called by friend classes.
///-------------------------------------------------------------
void* ZWindow::getWindowHandle() const
{
	return &pimpl->hWnd;
}
