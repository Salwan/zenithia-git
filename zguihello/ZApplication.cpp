////////////////////////////////////////////////////////////////
// Name: ZApplication
// Notes: 
//
// Cloud Mill Games (c) 2012
////////////////////////////////////////////////////////////////
#include "ZApplication.h"
#include "ZWindow.h"
#include "ZUtils.h"
#include "ZClock.h"

////////////////////////////////////////////////////////////////
/// Default Constructor
///-------------------------------------------------------------
ZApplication::ZApplication()
{
	mClock = new ZClock();
}

////////////////////////////////////////////////////////////////
/// Destructor
///-------------------------------------------------------------
ZApplication::~ZApplication()
{
	delete mClock;
}

////////////////////////////////////////////////////////////////
/// Runs the application.
///-------------------------------------------------------------
unsigned int ZApplication::run(ZWindow* window)
{
	ZASSERT(window, "ZApplication::run() given window is NULL!");
	pWindow = window;
	pWindow->start();
	init();
	start();
	while(pWindow->isAlive())
	{
		pWindow->process();
		mClock->tick();
		update();
		render();
	}
	end();

	return 0;
}

void ZApplication::init()
{

}

void ZApplication::start()
{

}

void ZApplication::update()
{

}

void ZApplication::render()
{

}

void ZApplication::end()
{

}
