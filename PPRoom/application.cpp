#include "PPRoom.h"
using namespace zen;

INT WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, INT)
{
	try
	{
		ZLog::logToFile();
		ZWindow window(L"PPRoom by ZenithSal", 1280, 800);
		PPRoom application;
		application.run(&window);
	}
	catch(ZException& e)
	{
		ZError::showError(e.getMessage());
	}
	catch(ZException* e)
	{
		ZError::showError(e->getMessage());
	}	
	ZMemoryDebug::logMemoryReport();
	return 0;
}
