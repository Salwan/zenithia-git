////////////////////////////////////////////////////////////////
// Name: PPRoom
// Notes: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#include "PPRoom.h"
#include <string>
#include <sstream>
#include <iostream>

using namespace zen;

//////////////////////////////////////////////////////// Globals

class TestLogic : public ZCLogic
{
public:
	TestLogic() : fRotation(0.0f) {
	}
protected:
	virtual void onInit() {
		mTestEffect.setEffect(ZResourceSystem::instance()->getResource(L"basicfx"));
	}

	virtual void onUpdate() {		
		fRotation += ZClock::fTimeDelta;
		XMMATRIX tm = XMMatrixRotationY(fRotation);
		transform->setMatrix(tm);
	}

	virtual void onPreRender(const ZSceneParams&) {

	}

	virtual void onPostRender() {
	}

	virtual void onDestroy() {
	}

private:
	float fRotation;
	ZEffect mTestEffect;
};

////////////////////////////////////////////////////////////////
/// Default Constructor
///
PPRoom::PPRoom()
	: mD3D9(nullptr)//,
	//resTexture(nullptr), resModel(nullptr), monkeyEffect(nullptr)
{
	
}

////////////////////////////////////////////////////////////////
/// Destructor
///
PPRoom::~PPRoom()
{
}

void PPRoom::init()
{
	mD3D9 = znew(ZD3D9, (*pWindow));
}

void PPRoom::start()
{
	setupD3D();

	// Test input
	ZInput::instance()->mapKeyToAction(133, DIK_A, ZINPUT_RELEASED);

	// Test Resource System
	ZResourceSystem::instance()->setAssetsFolder(L"assets/");
	ZResourcePack test_resource_pack(L"test");
	test_resource_pack.addModel(L"tcube", L"tcube.dae");
	test_resource_pack.addModel(L"cubesphere", L"tcubesphere.dae");
	test_resource_pack.addModel(L"direction_light", L"Nodes/directional_light.dae");
	test_resource_pack.addEffect(L"basicfx", L"PPRoom/basic_per_pixel.fx");
	test_resource_pack.addEffect(L"wireframefx", L"PPRoom/wireframe.fx");
	ZResourceSystem::instance()->loadResourcePack(test_resource_pack);
	resEffect = ZResourceSystem::instance()->getResource(L"basicfx");
	ZResource* resWireframe = ZResourceSystem::instance()->getResource(L"wireframefx");

	ZNode* cubesphere_node = mSceneGraph.rootNode()->addChild();
	ZResource* cubesphere_res = ZResourceSystem::instance()->getResource(L"cubesphere");
	cubesphere_node->createModelComponent(cubesphere_res, resEffect);
	cubesphere_node->createLogicComponent<TestLogic>();

	ZNode* tcube_node = mSceneGraph.rootNode()->addChild();
	ZResource* tcube_res = ZResourceSystem::instance()->getResource(L"tcube");
	tcube_node->createModelComponent(tcube_res, resEffect);
	XMMATRIX tcube_tm = XMMatrixTranslation(3.5f, 0.0f, 0.0f);
	tcube_node->transform()->setMatrix(tcube_tm);

	ZNode* dirlight_node = mSceneGraph.rootNode()->addChild();
	ZResource* dirlight_res = ZResourceSystem::instance()->getResource(L"direction_light");
	dirlight_node->createModelComponent(dirlight_res, resWireframe);
	XMMATRIX dirlight_tm = XMMatrixTranslation(-3.5f, 0.0f, 0.0f);
	dirlight_node->transform()->setMatrix(dirlight_tm);

	ZLight dirlight;
	dirlight.setDirectionalLight(ZColor(1, 1, 1, 1), XMFLOAT3(0.5f, -0.5f, 1.0f));
	mSceneGraph.rootNode()->addChildLight(dirlight);

	ZLight pointlight;
	pointlight.setPointLight(ZColor(0.5, 0.5, 0.5, 1), XMFLOAT3(2.0f, 0.0f, 0.0f), 10.0f);
	mSceneGraph.rootNode()->addChildLight(pointlight);
}

void PPRoom::end()
{
	zdelete(mD3D9);
}

void PPRoom::setupD3D()
{
	ZLight::DisableLighting();
	initMatrices();
	ZSampler::setSamplerFilter(0, ESF_TRILINEAR);
	ZSampler::setTextureMirror(0);
}

void PPRoom::update()
{
	if(GetAsyncKeyState(0x46) & 0x8000 && !isFullscreen())
	{
		goFullscreen(*mD3D9);
	}
	if(GetAsyncKeyState(0x57) & 0x8000 && isFullscreen())
	{
		goWindowed(*mD3D9);
	}

	static double accum = 0.0;
	accum += ZClock::dTimeDelta;
	if(accum >= 1.0)
	{
		accum -= 1.0;
		std::ostringstream ss;
		ss<<"FPS: "<<ZClock::uFPS<<std::endl;
		OutputDebugStringA(ss.str().c_str());
	}

	// Test Input
	if(ZInput::instance()->getActionState(133))
	{
		OutputDebugStringW(L"IT WORKS!\n");
	}
}

void PPRoom::render()
{
	IDirect3DDevice9& device = mD3D9->getDevice();

	mD3D9->clear(0xff444444);

	device.BeginScene();
	
	mSceneGraph.process();

	device.EndScene();

	// Do the swap chain magic
	mD3D9->present();
}

void PPRoom::onDeviceReset()
{
	setupD3D();
}

void PPRoom::onWindowResize(const ZEvent& e)
{
	const ZEvent_WindowResize& ewr = static_cast<const ZEvent_WindowResize&>(e);
	wchar_t buffer [255];
	swprintf_s<255>(buffer, L"Window Resize Event to %dx%d\n", ewr.iWidth, ewr.iHeight);
	OutputDebugStringW(buffer);
	resizeD3D(*mD3D9);
}

void PPRoom::initMatrices()
{
	// Perspective
	matProj = XMMatrixPerspectiveFovLH(3.1415f / 4.0f, 
		pWindow->getWidth() / static_cast<float>(pWindow->getHeight()), 
		1.0f, 1000.0f);
	mSceneGraph.setProjection(matProj);

	XMFLOAT4 eye(0.0f, 20.0f, -50.0f, 0.0f), 
				at(0.0f, 0.0f, 0.0f, 0.0f), 
				up(0.0f, 1.0f, 0.0f, 0.0f);
	XMVECTOR veye = XMLoadFloat4(&eye);
	XMVECTOR vat = XMLoadFloat4(&at);
	XMVECTOR vup = XMLoadFloat4(&up);
	matView = XMMatrixLookAtLH(veye, vat, vup);
	matWorld = XMMatrixIdentity();

	// Ortho for HUD
	matProjHUD = XMMatrixOrthographicOffCenterLH(0.0f, static_cast<float>(pWindow->getWidth()), 
		0.0f, static_cast<float>(pWindow->getHeight()), 0.0f, 1.0f);
	matViewHUD = XMMatrixIdentity();
	// Flip vertically and shift by window height so Ortho starts at top left instead of bottom left
	matWorldHUD = XMMatrixIdentity();
	matWorldHUD = XMMatrixScaling(1.0f, -1.0f, 1.0f);
	XMMATRIX w = XMMatrixTranslation(0.0f, static_cast<float>(pWindow->getHeight()), 0.0f);
	matWorldHUD = matWorldHUD * w;
}
