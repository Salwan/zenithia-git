////////////////////////////////////////////////////////////////
// Name: PPRoom
// Desc: 
//
// Cloud Mill Games (c) 2013
////////////////////////////////////////////////////////////////
#ifndef _TESTAPPLICATION_HEADER_
#define _TESTAPPLICATION_HEADER_

/////////////////////////////////////////////////////// Includes
#include <Zenithia.h>
#pragma comment(lib, "Zenithia.lib")
#include <memory>

//////////////////////////////////////////////////// Definitions
using namespace zen;

////////////////////////////////////////////////////////////////
/// @class PPRoom
/// @brief
/// 
class PPRoom : public ZApplication
{
public:
	explicit PPRoom();
	virtual ~PPRoom();

	virtual void init();
	virtual void start();
	virtual void update();
	virtual void render();
	virtual void end();
	virtual void onDeviceReset();
	virtual void onWindowResize(const ZEvent& e);

protected:
	// Methods
	void setupD3D();
	void initMatrices();

private:
	PPRoom(const PPRoom&){}
	PPRoom& operator=(const PPRoom&){}

	ZD3D9* mD3D9;
	
	XMMATRIX matWorld;
	XMMATRIX matProj;
	XMMATRIX matView;
	XMMATRIX matWorldHUD;
	XMMATRIX matProjHUD;
	XMMATRIX matViewHUD;

	ZResource* resTexture;
	//ZResource* resModel;
	ZResource* resEffect;
	std::shared_ptr<ZModel> monkeyModel;
	ZEffect basicEffect;
	ZEffect wireframeEffect;

	ZSceneGraph mSceneGraph;
};

#endif
