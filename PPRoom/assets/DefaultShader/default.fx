float4x4 matViewProj : WorldViewProjection;

float4 mainVS(float3 pos : POSITION) : POSITION
{
	return mul(float4(pos.xyz, 1.0), matViewProj);
}

float4 mainPS() : COLOR 
{
	return float4(1.0, 1.0, 1.0, 1.0);
}

technique technique0 
{
	pass p0 
	{
		VertexShader = compile vs_2_0 mainVS();
		PixelShader = compile ps_2_0 mainPS();
	}
}
