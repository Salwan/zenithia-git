/*

Basic shader
Per-Pixel lighting for 1 directional light, 1 point lights.

*/

#include "zenithia.fx"

////////////////////////////////////////////////
struct VS_INPUT
{
	float4 pos : POSITION;
	float4 normal : NORMAL;
	float2 texcoord : TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 pos : POSITION;
	float4 objectPos : TEXCOORD0;
	float4 normal : TEXCOORD1;
	float2 texcoord : TEXCOORD2;
};

VS_OUTPUT mainVS(VS_INPUT Input)
{
	VS_OUTPUT Output;
	Output.pos = mul(Input.pos, matWorldViewProjection);
	Output.objectPos = Input.pos;
	Output.normal = Input.normal;
	Output.texcoord = Input.texcoord;
	return Output;
}

float4 mainPS(VS_OUTPUT Input) : COLOR 
{
	return tex2D(Texture_Diffuse_Sampler, Input.texcoord) *
		calculateBasicLighting(Input.objectPos, Input.normal);
}

technique technique0 
{
	pass p0 
	{
		VertexShader = compile vs_3_0 mainVS();
		PixelShader = compile ps_3_0 mainPS();
	}
}
