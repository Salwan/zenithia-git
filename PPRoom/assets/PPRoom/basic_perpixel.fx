/*

Basic shader
Per-pixel lighting for 1 directional light, 1 point light

*/

#include "zenithia.fx"

////////////////////////////////////////////////
struct VS_INPUT
{
	float4 pos : POSITION;
	float4 normal : NORMAL;
	float2 texcoord : TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 pos : POSITION;
	float4 color : COLOR0;
	float2 texcoord : TEXCOORD0;
};

VS_OUTPUT mainVS(VS_INPUT Input)
{
	VS_OUTPUT Output;
	Output.pos = mul(float4(Input.pos.xyz, 1.0), matWorldViewProjection);
	Output.color = calculateBasicLighting(Input.pos, Input.normal);
	Output.texcoord = Input.texcoord;
	return Output;
}

float4 mainPS(VS_OUTPUT Input) : COLOR 
{
	return tex2D(Texture_Diffuse_Sampler, Input.texcoord) * Input.color;
}

technique technique0 
{
	pass p0 
	{
		VertexShader = compile vs_3_0 mainVS();
		PixelShader = compile ps_3_0 mainPS();
	}
}
