/*

% Description of my shader.
% Second line of description for my shader.

keywords: material classic

date: YYMMDD

*/

//float4x4 WorldViewProj : WorldViewProjection;
#include "zenithia.fx"

float4 mainVS(float4 pos : POSITION) : POSITION
{
	return mul(pos, matWorldViewProjection);
}

float4 mainPS() : COLOR 
{
	return float4(1.0, 1.0, 0.0, 1.0);
}

technique technique0 
{
	pass p0 
	{
		CullMode = None;
		FillMode = Wireframe;
		VertexShader = compile vs_3_0 mainVS();
		PixelShader = compile ps_3_0 mainPS();
	}
}
