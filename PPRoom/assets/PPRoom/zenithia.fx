/*

Zenithia
A common library for shaders.

*/

////////////////////////////////////////////////
// Matrices
float4x4 matWorldViewProjection : WorldViewProjection;
float4x4 matViewInverse : ViewInverse;
float4x4 matWorld : World;
float4x4 matWorldInverseTranspose : WorldInverseTranspose;

////////////////////////////////////////////////
// Materials
float4 Material_Ambient : Ambient;
float4 Material_Diffuse : Diffuse;
float4 Material_Specular : Specular;
float4 Material_Emission : Emission;
float Material_Power : Power;

////////////////////////////////////////////////
// Textures
texture Texture_Diffuse : DiffuseMap;
sampler Texture_Diffuse_Sampler = sampler_state {
	Texture = Texture_Diffuse;
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
};

////////////////////////////////////////////////
// Lights
// Directional Light
float4 Light_Directional_Diffuse : Diffuse <string Object = "DirectionalLight0";>;
float4 Light_Directional_Direction : Direction <string Object = "DirectionalLight0";>;
// Point Light
float4 Light_Point_Diffuse : Diffuse <string Object = "PointLight0";>;
float4 Light_Point_Position : Position <string Object = "PointLight0";>;
float Light_Point_Attenuation0 : ConstantAttenuation <string Object = "PointLight0";>;
float Light_Point_Attenuation1 : LinearAttenuation <string Object = "PointLight0";>;
float Light_Point_Attenuation2 : QuadraticAttenuation <string Object = "PointLight0";>;
// Light switches
float Light_Directional_Enabled;
float Light_Point_Enabled;

///////////////////////////////////////////////
// Lighting functions
float4 calculateBasicLighting(float4 vertex_position, float4 vertex_normal)
{
	// Calculating vectors
	vertex_position = mul(vertex_position, matWorld);
	float4 vertex_normal_object = normalize(mul(vertex_normal, matWorldInverseTranspose));
	float4 eye_position = matViewInverse[3];
	float4 vertex_to_light_dir = Light_Directional_Enabled > 0.0f? 
		-normalize(Light_Directional_Direction) : float4(0.0f, 0.0f, 0.0f, 0.0f);
	float4 vertex_to_light_point = Light_Point_Enabled > 0.0f? 
		normalize(Light_Point_Position - vertex_position) : float4(0.0f, 0.0f, 0.0f, 0.0f);
	float distance_point = distance(vertex_position, Light_Point_Position);
	
	float4 vertex_to_eye = normalize(eye_position - vertex_position);
	float4 light_color_dir = (Light_Directional_Diffuse * Light_Directional_Enabled);
	float4 light_color_point = (Light_Point_Diffuse * Light_Point_Enabled);
	float NdotL_dir = dot(vertex_normal_object, vertex_to_light_dir);
	float NdotL_point = dot(vertex_normal_object, vertex_to_light_point);
	float NdotH = dot(vertex_normal, vertex_to_eye);
	float attenuation_point = 1 / (
		(Light_Point_Attenuation0) + 
		(Light_Point_Attenuation1 * distance_point) +
		(Light_Point_Attenuation2 * pow(distance_point, 2)));
	
	// Calculating lighting
	float4 ambient_term = (Material_Ambient * light_color_dir * Light_Directional_Enabled) + 
		(Material_Ambient * light_color_point * Light_Point_Enabled);
	float4 diffuse_term = 
		(Material_Diffuse * light_color_dir * max(NdotL_dir, 0.0f) * Light_Directional_Enabled) + 
		(Material_Diffuse * light_color_point * max(NdotL_point, 0.0f) * attenuation_point 
		* Light_Point_Enabled);
	float4 specular_factor = Material_Specular * pow(max(NdotH, 0), Material_Power);
	float4 specular_term = 
		(specular_factor * saturate(NdotL_dir) * Light_Directional_Enabled) + 
		(specular_factor * saturate(NdotL_point) * attenuation_point * Light_Point_Enabled);
	float4 emissive_term = Material_Emission;
	// Return full lit pixel
	float4 lit_color = ambient_term + diffuse_term + specular_term + emissive_term;
	lit_color.w = 1.0f;
	return lit_color;
}
