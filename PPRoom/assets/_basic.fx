/*

Basic shader.

*/

float4x4 matWorldViewProjection : WorldViewProjection;
texture texDiffuse;
sampler texDiffuseSampler = 
sampler_state
{
	Texture = texDiffuse;
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
};

struct VS_INPUT
{
	float3 pos : POSITION;
	float3 normal : NORMAL;
	float2 texcoord : TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 pos : POSITION;
	float2 texcoord : TEXCOORD0;
};

VS_OUTPUT mainVS(VS_INPUT Input)
{
	VS_OUTPUT Output;
	Output.pos = mul(float4(Input.pos.xyz, 1.0), matWorldViewProjection);
	Output.texcoord = Input.texcoord;
	return Output;
}

float4 mainPS(VS_OUTPUT Input) : COLOR 
{
	return tex2D(texDiffuseSampler, Input.texcoord);
}

technique technique0 
{
	pass p0 
	{
		VertexShader = compile vs_3_0 mainVS();
		PixelShader = compile ps_3_0 mainPS();
	}
}
