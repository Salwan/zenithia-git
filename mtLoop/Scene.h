#pragma once

#include "precomp.h"
#include "Entity.h"

class Game;

class Scene
{
public:
	friend class Game;

public:
	static unsigned int getID() 
	{
		return uCurrentID++;
	}

public:
	explicit Scene() : bMultithreaded(false)
	{
	}

	virtual ~Scene()
	{
	}

	void init();

private:
	Scene(const Scene& scene){}
	Scene& operator=(const Scene& scene){}

	void prepare(bool run_multithreaded = false);
	void update();
	void render();
	void finish();

	void doUpdate();
	void doRender();

private:
	typedef std::map<unsigned int, Entity> TEntityMap;
	typedef std::map<unsigned int, Entity>::iterator TEntityMapIter;
	TEntityMap mEntities;
	TEntityMap mEntitiesBuffer;
	bool bMultithreaded;
	boost::thread mUpdateThread;
	boost::thread mRenderThread;

	static unsigned int uCurrentID;
};
