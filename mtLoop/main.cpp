#include "precomp.h"
#include "Game.h"

int main()
{
	char buf[128];
	int loops;
	bool multithreaded;

	std::cout<<"Hello :D"<<std::endl;
	std::cout<<"Would you like to multithread (Y/N)? ";
	std::cin>>buf;
	multithreaded = false;
	if(buf[0] == '1' || buf[0] == 'Y' || buf[0] == 'y')
	{
		multithreaded = true;
	}
	std::cout<<"How many loops do you want to run? ";
	std::cin>>loops;
	if(loops < 1) loops = 1;

	std::cout<<"running experimental threading game.."<<std::endl;

	Game game(multithreaded);
	game.start();
	game.run(loops);
	
	std::cout<<"END";
	std::cin.ignore();
	std::getchar();
}
