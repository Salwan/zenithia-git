#pragma once

#include "precomp.h"

class Scene;

class Game
{
public:
	explicit Game();
	Game(bool multithreaded);
	virtual ~Game(){}

	void start();

	void run(int loops);

	void end()
	{
	}

private:
	Game(const Game& game){}
	Game& operator=(const Game& game){}

	bool bMultithreaded;
	boost::shared_ptr<Scene> mScene;
};