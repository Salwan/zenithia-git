#pragma once

class Scene;

class Entity
{
public:
	friend class Scene;

public:
	explicit Entity();

	Entity(const Entity& ent);

	Entity& operator=(const Entity& ent);

	virtual ~Entity()
	{
	}

	int getX() const {return x;}
	int getY() const {return y;}
	unsigned int getID() const {return id;}

	void update();

private:
	int x;
	int y;
	unsigned int id;
};