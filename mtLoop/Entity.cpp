#include "precomp.h"
#include "Entity.h"
#include "Scene.h"

Entity::Entity() : x(0), y(0), id(Scene::getID())
{
}

Entity::Entity(const Entity& ent)
{
	x = ent.x;
	y = ent.y;
	id = Scene::getID();
}

Entity& Entity::operator=(const Entity& ent)
{
	x = ent.x;
	y = ent.y;
	id = Scene::getID();
	return *this;
}

void Entity::update()
{
	x += 1;
	boost::this_thread::sleep(boost::posix_time::milliseconds(10));
}