#include "precomp.h"
#include "Scene.h"

unsigned int Scene::uCurrentID = 0;

void Scene::init()
{
	for(int i = 0; i < 25; ++i)
	{
		Entity e;
		e.x = std::rand() % 100;
		mEntities[e.id] = e;
	}
}

void Scene::prepare(bool run_multithreaded)
{
	bMultithreaded = run_multithreaded;
	if(bMultithreaded)
	{
		// copy entities to render buffer
		mEntitiesBuffer.clear();
		//std::copy(mEntities.begin(), mEntities.end(), std::inserter<TEntityMap>(mEntitiesBuffer, mEntitiesBuffer.begin()));
		for(TEntityMapIter i = mEntities.begin(); i != mEntities.end(); ++i)
		{
			mEntitiesBuffer[i->first] = i->second;
		}
	}
}

void Scene::update()
{
	if(bMultithreaded)
	{
		// launch update thread
		mUpdateThread = boost::thread(boost::bind(&Scene::doUpdate, this));
	}
	else
	{
		doUpdate();
	}
}

void Scene::render()
{
	if(bMultithreaded)
	{
		// launch render thread
		mRenderThread = boost::thread(boost::bind(&Scene::doRender, this));
	}
	else
	{
		doRender();
	}
}

void Scene::finish()
{
	if(bMultithreaded)
	{
		mUpdateThread.join();
		mRenderThread.join();
	}
	else
	{
		return;
	}
}

void Scene::doUpdate()
{
	std::cout<<"<update>"<<std::endl;
	for(TEntityMapIter i = mEntities.begin();
		i != mEntities.end();
		++i)
	{
		i->second.update();
	}
	std::cout<<"</update>"<<std::endl;
}

void Scene::doRender()
{
	TEntityMap& entities = mEntities;
	if(bMultithreaded)
	{
		entities = mEntitiesBuffer;
	}
	std::cout<<"<render>"<<std::endl;
	for(TEntityMapIter i = entities.begin();
		i != entities.end();
		++i)
	{
		Entity& ent = i->second;
		std::cout<<"[Entity "<<ent.getID()<<"] "<<ent.getX()<<std::endl;
		std::cout.flush();
	}
	std::cout<<"</render>"<<std::endl;
}