#include "precomp.h"
#include "Game.h"
#include "Scene.h"

Game::Game() : bMultithreaded(false)
{
}

Game::Game(bool multithreaded) : bMultithreaded(multithreaded)
{
}

void Game::start()
{
	mScene.reset(new Scene());
	mScene->init();
}

void Game::run(int loops)
{
	// Main loop
	while(loops-- > 0)
	{
		boost::timer::auto_cpu_timer t;
		std::cout<<"<FRAME "<<loops<<">"<<std::endl;
		mScene->prepare(bMultithreaded);
		mScene->update();
		mScene->render();
		mScene->finish();
		std::cout<<"</FRAME>"<<std::endl;
	}
}