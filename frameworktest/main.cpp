#include <iostream>

template <typename T>
class Constant 
{
public:
	explicit Constant(T _value) : value(_value) {}
	virtual ~Constant() {}

	operator T() const {
		return value;
	}

	Constant& operator=(T _value) {
		value = _value;
		return *this;
	}

private:
	T value;
};

int main()
{
	std::cout<<"Hello World!"<<std::endl;
	Constant<int> c1(5);
	Constant<float> c2(0.54345f);
	Constant<double> c3(0.35235235235);
	std::cout<<"Value1: "<<c1<<std::endl;
	std::cout<<"Value2: "<<c2<<std::endl;
	std::cout<<"Value3: "<<c3<<std::endl;
	c1 = 3;
	c2 = 0.34f;
	c3 = 0.22f;
	std::cout<<"Value1: "<<c1<<std::endl;
	std::cout<<"Value2: "<<c2<<std::endl;
	std::cout<<"Value3: "<<c3<<std::endl;
	std::getchar();
}