#include <iostream>
#include <Python.h>

int main()
{
	std::cout<<"Welcome to Embedded Python Test"<<std::endl;
	std::cout<<"==============================================="<<std::endl;

	Py_SetProgramName("WhatProgram");
	Py_Initialize();

	PyRun_SimpleString(	"from time import time, ctime\n"
						"print 'Today is', ctime(time())\n");
	Py_Finalize();
	std::getchar();
}