﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Extensibility;
using EnvDTE;
using EnvDTE80;
using System.Diagnostics;

namespace SourceCreator
{
	/// <summary>frmClassProperties</summary>
	public partial class frmClassProperties : Form
	{
		private DTE2 appObject;

		/// <summary>frmClassProperties</summary>
		public frmClassProperties(DTE2 application_object)
		{
			InitializeComponent();
			appObject = application_object;

			// Add all projects to combo box
			Projects projects = appObject.Solution.Projects;
			if (projects.Count > 0)
			{
				foreach (Project p in projects)
				{
					cbProjects.Items.Add(p.Name);
				}

				// Always select first project found
				cbProjects.SelectedItem = cbProjects.Items[0];
			}
			else
			{
				MessageBox.Show("No Projects found!\nThis Add-in requires at least one open project to operate.");
				Close();
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void btnFinish_Click(object sender, EventArgs e)
		{
			if (tbClassName.Text == "")
			{
				MessageBox.Show("You must specify at least a class name!");
				return;
			}
			tbClassName.Text.Trim();
			autoFillFileNames();
			Projects projects = appObject.Solution.Projects;
			string selected_project_name = (string)cbProjects.SelectedItem;
			Debug.WriteLine("Selected project name: " + selected_project_name);
			Project selected_project = null;
			foreach (Project p in projects)
			{
				if (p.Name == selected_project_name)
				{
					selected_project = p;
					break;
				}
			}
			if (selected_project != null)
			{
				Debug.WriteLine("Source will be created into: " + selected_project.Name);
				createSourceFiles(selected_project);
				Close();                
			}
			else
			{
				MessageBox.Show("You must specify which Project this file should be added to.");
			}
		}

		private void createSourceFiles(Project project)
		{
			Debug.WriteLine("Project File Name: " + project.FileName);
			Debug.WriteLine("Project Full Name: " + project.FullName);
			Debug.WriteLine("Project items count: " + project.ProjectItems.Count);

			createVirtualFolder(project, "Header Files");
			createVirtualFolder(project, "Source Files");

			ProjectItems header_items = project.ProjectItems.Item("Header Files").ProjectItems;
			ProjectItems source_items = project.ProjectItems.Item("Source Files").ProjectItems;

			string header_file_name = tbHFile.Text;
			string source_file_name = tbCppFile.Text;

			string header_path = createNewFile(project, "Header Files", header_file_name);
			string source_path = createNewFile(project, "Source Files", source_file_name);

			if (header_path != "")
			{
				createHeaderFile(project, header_path);
			}

			if (source_path != "")
			{
				createSourceFile(project, source_path);
			}
		}

		private void createVirtualFolder(Project project, string name)
		{
			if (project.ProjectItems.Item(name) == null)
			{
				Debug.WriteLine("Creating virtual folder \"" + name + "\"...");
				project.ProjectItems.AddFolder(name, Constants.vsProjectItemKindVirtualFolder);
			}
		}

		private string createNewFile(Project project, string folder_name, string name)
		{
			ProjectItems p_items = project.ProjectItems.Item(folder_name).ProjectItems;

			// File exists?
			if (p_items.Item(name) != null)
			{
				Debug.WriteLine("File \"" + name + "\" already exists in the project.");
				return "";
			}
			else
			{
				string project_path = getProjectPath(project);
				string file_path = project_path + name;

				// Create empty file
				Debug.WriteLine("Creating file \"" + name + "\"...");
				StreamWriter sw = File.CreateText(file_path);
				sw.Close();
				return file_path;
			}
		}

		private string getProjectPath(Project project)
		{
			string fullname = project.FullName;
			int index = fullname.LastIndexOf('\\');
			if (index == -1)
			{
				throw new Exception("While trying to retrieve the project path, something wrong happened.");
			}
			return fullname.Substring(0, index + 1);
		}

		private void createHeaderFile(Project project, string header_path)
		{
			Debug.WriteLine("Writing header file...");

			string content = "";
			string class_name = tbClassName.Text;
			string base_name = tbBaseClass.Text;
			string def_name = class_name.ToUpper();
			string project_path = getProjectPath(project);

			// Does project have: hfile.template file?
			if (project.ProjectItems.Item("hfile.template") != null)
			{
                string pathi = project.ProjectItems.Item("hfile.template").FileNames[0];
				Debug.WriteLine("Header file template has been found!");
				//StreamReader sr = File.OpenText(project_path + "hfile.template");
                StreamReader sr = File.OpenText(pathi);
				content = sr.ReadToEnd();
				sr.Close();
				applyTemplate(ref content);
			}
			else // No? I thought so... ok I'll just write a simple class definition
			{
				Debug.WriteLine("No header file template, writing simple class definition...");
				string base_text = base_name != "" ? String.Format(": {0}\n", base_name) : "\n";
				content = String.Format(
					"#pragma once\n" +
					"\n" +
					"class {0}{1}" +
					"{{\n" +
					"public:\n" +
					"   {0}();\n" +
					"   virtual ~{0}();\n" +
					"\n" +
					"private:\n" +
					"\n" +
					"}}\n", class_name, base_text);
			}

			StreamWriter sw = File.CreateText(header_path);
			sw.Write(content);
			sw.Close();
			// Add to project
			ProjectItem item = project.ProjectItems.Item("Header Files").ProjectItems.AddFromFile(tbHFile.Text);
			item.Open(Constants.vsViewKindCode).Activate();
			Debug.WriteLine("Done.");
		}

		private void createSourceFile(Project project, string source_path)
		{
			Debug.WriteLine("Writing source file...");

			string content = "";
			string class_name = tbClassName.Text;
			string base_name = tbBaseClass.Text;
			string header_name = tbHFile.Text;
			string project_path = getProjectPath(project);

			// Does project have: cppfile.template file?
			if (project.ProjectItems.Item("cppfile.template") != null)
			{
                string pathi = project.ProjectItems.Item("cppfile.template").FileNames[0];
				Debug.WriteLine("Source file template has been found!");
				//StreamReader sr = File.OpenText(project_path + "cppfile.template");
                StreamReader sr = File.OpenText(pathi);
				content = sr.ReadToEnd();
				sr.Close();
				applyTemplate(ref content);
			}
			else // No? I thought so... ok I'll just write a simple class implementation
			{
				Debug.WriteLine("No source file template, writing simple class implementation...");
				content = String.Format(
					"#include \"{1}\"\n\n" +
					"{0}::{0}()\n" +
					"{{\n" +
					"}}\n\n" +
					"{0}::~{0}()\n" +
					"{{\n" +
					"}}\n", class_name, header_name);
			}

			StreamWriter sw = File.CreateText(source_path);
			sw.Write(content);
			sw.Close();
			// Add to project
			ProjectItem item = project.ProjectItems.Item("Source Files").ProjectItems.AddFromFile(tbCppFile.Text);
			item.Open(Constants.vsViewKindCode).Activate();
			Debug.WriteLine("Done.");
		}

		private void applyTemplate(ref string content)
		{
			Debug.WriteLine("Applying template...");

			string class_name = tbClassName.Text;
			string base_name = tbBaseClass.Text.Trim();
            string base_inherit = "";
            if (base_name.Length > 0)
            {
                base_inherit = ": public " + tbBaseClass.Text.Trim();
            }
			string def_name = class_name.Trim().ToUpper();
			string header_name = tbHFile.Text.Trim();

			// Class Name
			changeTagIntoValue(ref content, "ClassName", class_name);
			// Base Name
			changeTagIntoValue(ref content, "BaseName", base_name);
            // Base Inheritance
            changeTagIntoValue(ref content, "BaseInheritance", base_inherit);
			// Def Name
			changeTagIntoValue(ref content, "ClassDefName", def_name);
			// Header Name
			changeTagIntoValue(ref content, "ClassHeader", header_name);
		}

		private void changeTagIntoValue(ref string content, string tag, string value)
		{
			content = content.Replace("{" + tag + "}", value);
		}

		private void eventFormShown(object sender, EventArgs e)
		{
			// Set focus to first text box
			tbClassName.Focus();
		}

		private void eventClassNameLeave(object sender, EventArgs e)
		{
			autoFillFileNames();
		}

		private void autoFillFileNames()
		{
			if (tbClassName.Text != "")
			{
				if (tbHFile.Text == "")
				{
					tbHFile.Text = tbClassName.Text + ".h";
				}
				if (tbCppFile.Text == "")
				{
					tbCppFile.Text = tbClassName.Text + ".cpp";
				}
				if (!tbHFile.Text.EndsWith(".h"))
				{
					tbHFile.Text += ".h";
				}
				if (!tbCppFile.Text.EndsWith(".cpp"))
				{
					tbCppFile.Text += ".cpp";
				}
			}
		}
	}
}
